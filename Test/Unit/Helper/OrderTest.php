<?php

namespace Trilix\CrefoPay\Test\Unit\Helper;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager as ObjectManagerHelper;
use Magento\Sales\Api\Data\OrderInterface;
use Trilix\CrefoPay\Helper\Order as OrderHelper;
use Magento\Sales\Model\OrderRepository;
use Magento\Sales\Api\Data\OrderSearchResultInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Sales\Model\Order;

class OrderTest extends \PHPUnit\Framework\TestCase
{
    public function testGetOrderByIncrementId()
    {
        $searchResultMock = $this->createMock(OrderSearchResultInterface::class);

        $searchResultMock
            ->expects($this->any())
            ->method('getItems')
            ->willReturn(
                [
                    (new ObjectManagerHelper($this))->getObject(Order::class),
                    (new ObjectManagerHelper($this))->getObject(Order::class),
                ]
            );

        $orderRepositoryMock = $this->createMock(OrderRepository::class);

        $orderRepositoryMock
            ->expects($this->any())
            ->method('getList')
            ->willReturn($searchResultMock);

        $searchCriteriaBuilderMock = $this->createMock(SearchCriteriaBuilder::class);

        $searchCriteriaBuilderMock
            ->expects($this->any())
            ->method('addFilter')
            ->willReturnSelf();

        $searchCriteriaBuilderMock
            ->expects($this->any())
            ->method('create')
            ->willReturn($this->createMock(SearchCriteriaInterface::class));

        /** @var OrderHelper $uut */
        $uut = (new ObjectManagerHelper($this))->getObject(
            OrderHelper::class,
            ['orderRepository' => $orderRepositoryMock, 'searchCriteriaBuilder' => $searchCriteriaBuilderMock]
        );

        $order = $uut->getOrderByIncrementId('1');
        self::assertInstanceOf(OrderInterface::class, $order);
    }
}
