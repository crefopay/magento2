<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Test\Unit\Model\UI\CreditCard3D;

use PHPUnit\Framework\MockObject\MockObject;
use Trilix\CrefoPay\Model\Ui\CreditCard3D\ConfigProvider;
use Trilix\CrefoPay\Gateway\Config\Config;
use Trilix\CrefoPay\Model\PaymentMethodsCodesMap;

class ConfigProviderTest extends \PHPUnit\Framework\TestCase
{
    public function testGetConfig()
    {
        /** @var Config|MockObject $config */
        $config = $this->getMockBuilder(Config::class)
            ->disableOriginalConstructor()
            ->getMock();

        $configProvider = new ConfigProvider($config);

        $config->expects($this->once())
            ->method('isActive')
            ->willReturn(true);
        $config->expects($this->once())
            ->method('getPublicToken')
            ->willReturn('publicToken');
        $config->expects($this->once())
            ->method('getSecureFieldsUrl')
            ->willReturn('secureFieldsUrl');
        $config->expects(self::once())
            ->method('getSmartSignupUrl')
            ->willReturn('smartSignupUrl');
        $config->expects($this->once())
            ->method('getLogoUrl')
            ->with(ConfigProvider::CODE)
            ->willReturn('logoUrl');

        $actual = $configProvider->getConfig();
        $expected = [
            'payment' => [
                ConfigProvider::CODE => [
                    'isActive'           => true,
                    'publicToken'        => 'publicToken',
                    'secureFieldsUrl'    => 'secureFieldsUrl',
                    'crefoPayMethodCode' => PaymentMethodsCodesMap::getCrefoPayLibraryCode(ConfigProvider::CODE),
                    'ccVaultCode'        => ConfigProvider::CC_VAULT_CODE,
                    'logo'               => 'logoUrl',
                    'smartSignupUrl'     => 'smartSignupUrl'
                ]
            ]
        ];

        $this->assertEquals($expected, $actual);
    }
}
