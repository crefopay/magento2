<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Test\Unit\Model\UI\Bill;

use PHPUnit\Framework\MockObject\MockObject;
use Trilix\CrefoPay\Model\CrefoPayDob;
use Trilix\CrefoPay\Model\Ui\Bill\ConfigProvider;
use Trilix\CrefoPay\Gateway\Config\Config;
use Trilix\CrefoPay\Model\PaymentMethodsCodesMap;

class ConfigProviderTest extends \PHPUnit\Framework\TestCase
{
    public function testGetConfig()
    {
        /** @var Config|MockObject $config */
        $config = $this->getMockBuilder(Config::class)
            ->disableOriginalConstructor()
            ->getMock();

        $crefoPayDob = $this->createMock(CrefoPayDob::class);

        $configProvider = new ConfigProvider($config, $crefoPayDob);

        $config->expects(self::once())
            ->method('isActive')
            ->willReturn(true);
        $config->expects(self::once())
            ->method('getPublicToken')
            ->willReturn('publicToken');
        $config->expects(self::once())
            ->method('getSecureFieldsUrl')
            ->willReturn('secureFieldsUrl');
        $config->expects(self::once())
            ->method('getSmartSignupUrl')
            ->willReturn('smartSignupUrl');
        $config->expects(self::once())
            ->method('getLogoUrl')
            ->with(ConfigProvider::CODE)
            ->willReturn('logoUrl');
        $crefoPayDob->expects(self::once())
            ->method('getDob')
            ->willReturn('11.11.2011');
        $crefoPayDob->expects(self::once())
            ->method('getDateFormatForJs')
            ->willReturn('dd.mm.yy');
        $config->expects(self::once())
            ->method('isB2BEnabled')
            ->willReturn(true);

        $actual = $configProvider->getConfig();
        $expected = [
            'payment' => [
                ConfigProvider::CODE => [
                    'isActive'           => true,
                    'publicToken'        => 'publicToken',
                    'secureFieldsUrl'    => 'secureFieldsUrl',
                    'crefoPayMethodCode' => PaymentMethodsCodesMap::getCrefoPayLibraryCode(ConfigProvider::CODE),
                    'logo'               => 'logoUrl',
                    'smartSignupUrl'     => 'smartSignupUrl',
                    'dob'                => '11.11.2011',
                    'dateFormat'         => 'dd.mm.yy',
                    'isB2BEnabled'       => true
                ]
            ]
        ];

        self::assertEquals($expected, $actual);
    }
}
