<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Test\Unit\Model\UI\CreditCard;

use PHPUnit\Framework\MockObject\MockObject;
use Magento\Vault\Model\Ui\TokenUiComponentInterfaceFactory;
use Magento\Framework\UrlInterface;
use Magento\Vault\Api\Data\PaymentTokenInterface;
use Magento\Vault\Model\Ui\TokenUiComponentInterface;
use Trilix\CrefoPay\Model\Ui\CreditCard\TokenUiComponentProvider;

class TokenUiComponentProviderTest extends \PHPUnit\Framework\TestCase
{
    public function testGetComponentForToken()
    {
        /** @var TokenUiComponentInterfaceFactory|MockObject $tokenUiComponentFactoryMock */
        $tokenUiComponentFactoryMock = $this->getMockBuilder(TokenUiComponentInterfaceFactory::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var UrlInterface|MockObject $urlInterfaceMock */
        $urlInterfaceMock = $this->getMockBuilder(UrlInterface::class)
            ->getMock();

        /** @var PaymentTokenInterface|MockObject $tokenMock */
        $tokenMock = $this->createMock(PaymentTokenInterface::class);

        /** @var TokenUiComponentInterface|MockObject $tokenUiComponentMock */
        $tokenUiComponentMock = $this->getMockBuilder(TokenUiComponentInterface::class)
            ->getMock();

        $tokenUiComponentProvider = new TokenUiComponentProvider(
            $tokenUiComponentFactoryMock,
            $urlInterfaceMock,
            \Trilix\CrefoPay\Model\Ui\CreditCard3D\ConfigProvider::CODE,
            'vault_code'
        );

        $tokenMock->expects($this->once())
            ->method('getTokenDetails')
            ->willReturn('{"details":"token_details"}');

        $tokenUiComponentFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($tokenUiComponentMock);

        $tokenMock->expects($this->once())
            ->method('getGatewayToken')
            ->willReturn('{"gatewayToken":"gateway_token"}');

        $tokenMock->expects($this->once())
            ->method('getPublicHash')
            ->willReturn('hash');

        $tokenUiComponentProvider->getComponentForToken($tokenMock);
    }
}
