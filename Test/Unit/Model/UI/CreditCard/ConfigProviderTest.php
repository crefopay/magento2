<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Test\Unit\Model\UI\CreditCard;

use PHPUnit\Framework\MockObject\MockObject;
use Trilix\CrefoPay\Model\Ui\CreditCard\ConfigProvider;
use Trilix\CrefoPay\Gateway\Config\Config;
use Trilix\CrefoPay\Model\PaymentMethodsCodesMap;

class ConfigProviderTest extends \PHPUnit\Framework\TestCase
{
    public function testGetConfig()
    {
        /** @var Config|MockObject $config */
        $config = $this->getMockBuilder(Config::class)
            ->disableOriginalConstructor()
            ->getMock();

        $configProvider = new ConfigProvider($config);

        $config->expects($this->once())
            ->method('isActive')
            ->willReturn(true);
        $config->expects($this->once())
            ->method('getPublicToken')
            ->willReturn('publicToken');
        $config->expects($this->once())
            ->method('getLogoUrl')
            ->with(ConfigProvider::CODE)
            ->willReturn('logoUrl');

        $actual = $configProvider->getConfig();
        $expected = [
            'payment' => [
                ConfigProvider::CODE => [
                    'isActive'           => true,
                    'publicToken'        => 'publicToken',
                    'crefoPayMethodCode' => PaymentMethodsCodesMap::getCrefoPayLibraryCode(ConfigProvider::CODE),
                    'ccVaultCode'        => ConfigProvider::CC_VAULT_CODE,
                    'logo'               => 'logoUrl'
                ]
            ]
        ];

        $this->assertEquals($expected, $actual);
    }
}
