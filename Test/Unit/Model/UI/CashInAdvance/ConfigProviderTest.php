<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Test\Unit\Model\UI\CashInAdvance;

use PHPUnit\Framework\MockObject\MockObject;
use Trilix\CrefoPay\Model\Ui\CashInAdvance\ConfigProvider;
use Trilix\CrefoPay\Gateway\Config\Config;
use Trilix\CrefoPay\Model\PaymentMethodsCodesMap;

class ConfigProviderTest extends \PHPUnit\Framework\TestCase
{
    public function testGetConfig()
    {
        /** @var Config|MockObject $config */
        $config = $this->getMockBuilder(Config::class)
            ->disableOriginalConstructor()
            ->getMock();

        $configProvider = new ConfigProvider($config);

        $config->expects(self::once())
            ->method('isActive')
            ->willReturn(true);
        $config->expects(self::once())
            ->method('getPublicToken')
            ->willReturn('publicToken');
        $config->expects(self::once())
            ->method('getSecureFieldsUrl')
            ->willReturn('secureFieldsUrl');
        $config->expects(self::once())
            ->method('getSmartSignupUrl')
            ->willReturn('smartSignupUrl');
        $config->expects(self::once())
            ->method('getLogoUrl')
            ->with(ConfigProvider::CODE)
            ->willReturn('logoUrl');

        $actual = $configProvider->getConfig();
        $expected = [
            'payment' => [
                ConfigProvider::CODE => [
                    'isActive'           => true,
                    'publicToken'        => 'publicToken',
                    'secureFieldsUrl'    => 'secureFieldsUrl',
                    'crefoPayMethodCode' => PaymentMethodsCodesMap::getCrefoPayLibraryCode(ConfigProvider::CODE),
                    'logo'               => 'logoUrl',
                    'smartSignupUrl'     => 'smartSignupUrl'
                ]
            ]
        ];

        self::assertEquals($expected, $actual);
    }
}
