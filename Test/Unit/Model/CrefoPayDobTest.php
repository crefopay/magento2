<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Test\Unit\Model;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Magento\Customer\Model\Session;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Block\Widget\Dob;
use Magento\Customer\Api\Data\CustomerInterface;
use Trilix\CrefoPay\Model\CrefoPayDob;

class CrefoPayDobTest extends TestCase
{
    /** @var CrefoPayDob */
    private $crefoPayDob;

    /** @var CustomerRepositoryInterface|MockObject */
    private $customerRepository;

    /** @var Session|MockObject */
    private $customerSession;

    /** @var Dob|MockObject */
    private $dob;

    public function setUp(): void
    {
        $this->customerRepository = $this->createMock(CustomerRepositoryInterface::class);
        $this->customerSession = $this->createMock(Session::class);
        $this->dob = $this->createMock(Dob::class);

        $this->crefoPayDob = new CrefoPayDob(
            $this->customerRepository,
            $this->customerSession,
            $this->dob
        );
    }

    public function testGetDobIfCustomerIsRegistered()
    {
        $customer = $this->createMock(CustomerInterface::class);

        $this->customerSession->expects(self::once())
            ->method('getCustomerId')
            ->willReturn(1);

        $this->customerRepository->expects(self::once())
            ->method('getById')
            ->with(1)
            ->willReturn($customer);

        $customer->expects(self::once())
            ->method('getDob')
            ->willReturn('11.11.2011');

        $actualResult = $this->crefoPayDob->getDob();
        self::assertEquals('11.11.2011', $actualResult);
    }

    public function testGetDobIfCustomerIsGuest()
    {
        $this->customerSession->expects(self::once())
            ->method('getCustomerId')
            ->willReturn(null);

        $this->customerRepository->expects(self::never())
            ->method('getById');

        $actualResult = $this->crefoPayDob->getDob();
        self::assertEquals(null, $actualResult);
    }

    public function testGetDateFormatForJs()
    {
        $this->dob->expects(self::once())
            ->method('getDateFormat')
            ->willReturn('dd.mm.yy');

        $actualResult = $this->crefoPayDob->getDateFormatForJs();
        self::assertEquals('dd.mm.yy', $actualResult);
    }
}
