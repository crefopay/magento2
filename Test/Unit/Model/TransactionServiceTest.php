<?php

namespace Trilix\CrefoPay\Test\Unit\Model;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager as ObjectManagerHelper;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Payment;
use Trilix\CrefoPay\Model\TransactionService;
use Upg\Library\Request as UpgRequest;
use Upg\Library\Response\SuccessResponse;

class TransactionServiceTest extends \PHPUnit\Framework\TestCase
{
    public function testAddTransactionReserve()
    {
        $paymentMock = $this->getPaymentMock(true);

        $paymentMock
            ->expects($this->once())
            ->method('setIsTransactionClosed')
            ->with(false);

        $paymentMock
            ->expects($this->once())
            ->method('setTransactionId')
            ->with($this->callback(function ($hash) {
                return strlen($hash) === 40; // sha1
            }));

        $paymentMock
            ->expects($this->once())
            ->method('setIsTransactionPending')
            ->with(true);

        $responseMock = $this->getResponseMock($paymentMock);
        $paymentDOMock = $this->getPaymentDOMock($paymentMock);

        /** @var TransactionService $uut */
        $uut = (new ObjectManagerHelper($this))->getObject(TransactionService::class);
        $uut->addTransaction($paymentDOMock, $this->createMock(UpgRequest\Reserve::class), $responseMock);
    }

    public function testAddTransactionCapture()
    {
        $captureId = 'C4P7UR31D';
        $paymentMock = $this->getPaymentMock();

        $requestMock = $this->createConfiguredMock(UpgRequest\Capture::class, ['getCaptureId' => $captureId]);

        $paymentMock
            ->expects($this->once())
            ->method('setTransactionAdditionalInfo')
            ->with('capture_id', $captureId);

        /** @var TransactionService $uut */
        $uut = (new ObjectManagerHelper($this))->getObject(TransactionService::class);
        $uut->addTransaction($this->getPaymentDOMock($paymentMock), $requestMock, $this->getResponseMock());
    }

    private function getResponseMock($paymentMock = null)
    {
        $transactionAdditionalInfo = $paymentMock ? ['a' => 'b', 'c' => 'd'] : [];

        if ($paymentMock) {
            $paymentMock
                ->expects($this->exactly(2))
                ->method('setTransactionAdditionalInfo')
                ->withConsecutive(['a', 'b'], ['c', 'd']);
        }

        return $responseMock = $this->createConfiguredMock(SuccessResponse::class, ['getAllData' => $transactionAdditionalInfo]);
    }

    private function getPaymentDOMock($paymentMock)
    {
        $paymentDOMock = $this->createPartialMock(PaymentDataObjectInterface::class, ['getPayment', 'getOrder']);

        $paymentDOMock
            ->expects($this->any())
            ->method('getPayment')
            ->willReturn($paymentMock);

        return $paymentDOMock;
    }

    private function getPaymentMock($isGateway = false)
    {
        $paymentMethodInstanceMock = $this->createMock(\Magento\Payment\Model\MethodInterface::class);

        $paymentMethodInstanceMock
            ->expects($this->any())
            ->method('isGateway')
            ->willReturn($isGateway);

        $paymentMock = $this->createPartialMock(
            Payment::class,
            [
                'getOrder',
                'getMethodInstance',
                'setTransactionAdditionalInfo',
                'setIsTransactionClosed',
                'setTransactionId',
                'setIsTransactionPending',
            ]
        );

        $paymentMock
            ->expects($this->any())
            ->method('getOrder')
            ->willReturn((new ObjectManagerHelper($this))->getObject(Order::class));

        $paymentMock
            ->expects($this->any())
            ->method('getMethodInstance')
            ->willReturn($paymentMethodInstanceMock);

        return $paymentMock;
    }
}
