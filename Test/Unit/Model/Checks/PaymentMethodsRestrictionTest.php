<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Test\Unit\Model\Checks;

use Magento\Payment\Model\MethodInterface;
use Magento\Quote\Model\Quote;
use PHPUnit\Framework\MockObject\MockObject;
use Trilix\CrefoPay\Model\Checks\PaymentMethodsRestriction;
use Trilix\CrefoPay\Model\CrefoPayTransactionRepository;
use Trilix\CrefoPay\Api\Data\CrefoPayTransactionInterface;
use Trilix\CrefoPay\Gateway\Config\Config;

class PaymentMethodsRestrictionTest extends \PHPUnit\Framework\TestCase
{
    /** @var CrefoPayTransactionRepository|MockObject $crefoPayTransactionRepositoryMock */
    protected $crefoPayTransactionRepositoryMock;

    /** @var Config|MockObject $configMock */
    protected $configMock;

    /** @var Quote|MockObject $quoteMock */
    protected $quoteMock;

    /** @var MethodInterface|MockObject $paymentMethodMock */
    protected $paymentMethodMock;

    /** @var CrefoPayTransactionInterface|MockObject */
    protected $crefoPayTransactionMock;

    /** @var PaymentMethodsRestriction|MockObject */
    protected $model;

    public function setUp(): void
    {
        $this->crefoPayTransactionRepositoryMock = $this
            ->createMock(CrefoPayTransactionRepository::class);

        $this->configMock = $this->createMock(Config::class);
        $this->quoteMock = $this->createMock(Quote::class);
        $this->paymentMethodMock = $this->createMock(MethodInterface::class);
        $this->crefoPayTransactionMock = $this->createMock(CrefoPayTransactionInterface::class);

        $this->model = new PaymentMethodsRestriction($this->crefoPayTransactionRepositoryMock, $this->configMock);
    }

    public function testIsApplicableWhenMethodInNotInCrefoPayGroup()
    {
        $this->paymentMethodMock->expects($this->once())
            ->method('getCode')
            ->willReturn('not_crefopay_payment_method');

        $this->configMock->expects($this->never())
            ->method('isActive');

        $actual = $this->model->isApplicable($this->paymentMethodMock, $this->quoteMock);

        self::assertEquals(true, $actual);
    }

    public function testIsApplicableWhenModuleIsDisabled()
    {
        $this->paymentMethodMock->expects($this->once())
            ->method('getCode')
            ->willReturn(\Trilix\CrefoPay\Model\Ui\Bill\ConfigProvider::CODE);

        $this->configMock->expects($this->once())
            ->method('isActive')
            ->willReturn(false);

        $this->crefoPayTransactionRepositoryMock->expects($this->never())
            ->method('getByQuoteId');

        $actual = $this->model->isApplicable($this->paymentMethodMock, $this->quoteMock);

        self::assertEquals(false, $actual);
    }

    /**
     * @param $paymentMethodCode
     * @param $quotePaymentMethodsJson
     * @param $isApplicable
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @dataProvider isApplicableDataProvider
     */
    public function testIsApplicable($paymentMethodCode, $quotePaymentMethodsJson, $isApplicable)
    {
        $this->paymentMethodMock->expects($this->exactly(2))
            ->method('getCode')
            ->willReturn($paymentMethodCode);

        $this->configMock->expects($this->once())
            ->method('isActive')
            ->willReturn(true);

        $this->crefoPayTransactionRepositoryMock->expects($this->once())
            ->method('getByQuoteId')
            ->willReturn($this->crefoPayTransactionMock);

        $this->crefoPayTransactionMock->expects($this->once())
            ->method('getPaymentMethods')
            ->willReturn($quotePaymentMethodsJson);

        $actual = $this->model->isApplicable($this->paymentMethodMock, $this->quoteMock);

        self::assertEquals($isApplicable, $actual);
    }

    public function isApplicableDataProvider()
    {
        $quotePaymentMethods1 = [
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_PREPAID,
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_SU,
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_ZINIA_BNPL,
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_ZINIA_INSTALLMENT,
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_IDEAL,
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_GOOGLE_PAY
        ];

        $quotePaymentMethods2 = [
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_PREPAID,
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_SU,
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_BILL,
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_ZINIA_BNPL,
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_ZINIA_INSTALLMENT,
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_IDEAL,
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_GOOGLE_PAY
        ];

        return [
            [\Trilix\CrefoPay\Model\Ui\Bill\ConfigProvider::CODE, json_encode($quotePaymentMethods1), false],
            [\Trilix\CrefoPay\Model\Ui\Bill\ConfigProvider::CODE, json_encode($quotePaymentMethods2), true],
        ];
    }
}
