<?php

namespace Trilix\CrefoPay\Test\Unit\Model\Mns;

use Trilix\CrefoPay\Client\Constants;
use Trilix\CrefoPay\Model\Mns\MnsConsumerInterface;
use Trilix\CrefoPay\Model\Mns\MnsConsumerPool;
use Trilix\CrefoPay\Model\Mns\MnsEvent;

class MnsConsumerPoolTest extends \Trilix\CrefoPay\Test\Unit\Framework\TestCase
{
    /**
     * @param MnsEvent $mnsEvent
     * @param MnsConsumerInterface[] $consumers
     * @param MnsConsumerInterface[] $applicableConsumers
     * @dataProvider dataProvider
     */
    public function testGetConsumersByEvent($mnsEvent, $consumers, $applicableConsumers)
    {
        $uut = new MnsConsumerPool($consumers);
        $returnedConsumers = $uut->getConsumersByEvent($mnsEvent);

        $this->assertEquals(count($returnedConsumers), count($applicableConsumers));

        for ($i = 0; $i < count($returnedConsumers); $i++) {
            $this->assertEquals($applicableConsumers[$i]->getTransactionStatusName(), $returnedConsumers[$i]->getTransactionStatusName());
            $this->assertEquals($applicableConsumers[$i]->getCaptureStatusName(), $returnedConsumers[$i]->getCaptureStatusName());
        }
    }

    public function dataProvider()
    {
        $wet = [];

        $dry = [
            [[[Constants::TX_ACK, '', true], [Constants::TX_MERCHANT, '', false]], [Constants::TX_ACK, 'def']],
            [[[Constants::TX_ACK, Constants::O_INDUNNING, false], ['', Constants::O_INDUNNING, true]], ['', Constants::O_INDUNNING]],
            [[[Constants::TX_ACK, Constants::O_INDUNNING, false], [Constants::TX_MERCHANT, Constants::O_FAILED, false]], [Constants::TX_ACK, Constants::O_FAILED]],
            [[['', '', true]], [Constants::TX_ACK, Constants::O_FAILED]],
        ];

        foreach ($dry as $dryDataSet) {
            $wet[] = $this->generateDataSet($dryDataSet[0], $dryDataSet[1]);
        }

        return $wet;
    }

    private function generateDataSet(array $consumersData, array $eventData)
    {
        /** @var MnsEvent $mnsEvent */
        $mnsEvent = $this->getObject(MnsEvent::class);
        $mnsEvent->setTransactionStatus($eventData[0]);
        $mnsEvent->setCaptureStatus($eventData[1]);
        $mockedConsumers = [];
        $applicableConsumers = [];

        foreach ($consumersData as $consumerData) {
            $consumerMock = $this->getMockBuilder(MnsConsumerInterface::class)->getMock();

            $consumerMock->method('getTransactionStatusName')->willReturn($consumerData[0]);
            $consumerMock->method('getCaptureStatusName')->willReturn($consumerData[1]);

            $mockedConsumers[] = $consumerMock;

            if ($consumerData[2]) {
                $applicableConsumers[] = $consumerMock;
            }
        }

        return [$mnsEvent, $mockedConsumers, $applicableConsumers];
    }
}
