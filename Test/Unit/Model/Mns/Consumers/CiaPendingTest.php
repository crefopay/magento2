<?php

namespace Trilix\CrefoPay\Test\Unit\Model\Mns\Consumers;

use Magento\Sales\Api\Data\OrderPaymentInterface;
use Magento\Sales\Model\Order;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager as ObjectManagerHelper;
use Trilix\CrefoPay\Model\Mns\Consumers\CiaPending;
use Trilix\CrefoPay\Helper\Order as OrderHelper;
use Trilix\CrefoPay\Model\Mns\MnsEvent;
use Trilix\CrefoPay\Model\Ui\PayPal\ConfigProvider as PayPal;

class CiaPendingTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @param $paymentMethod
     * @param $expectedStatus
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @dataProvider processDataProvider
     */
    public function testProcess($paymentMethod, $expectedStatus)
    {
        $orderIncrementId = '1';

        $paymentMock = $this->createMock(OrderPaymentInterface::class);

        $paymentMock
            ->expects($this->any())
            ->method('getMethod')
            ->willReturn($paymentMethod);

        $orderMock = $this->createMock(Order::class);

        $orderMock
            ->expects($this->once())
            ->method('setStatus')
            ->with($expectedStatus);

        $orderMock
            ->expects($this->once())
            ->method('setState')
            ->with(Order::STATE_PENDING_PAYMENT);

        $orderMock
            ->expects($this->any())
            ->method('getPayment')
            ->willReturn($paymentMock);

        $orderHelperMock = $this->createMock(OrderHelper::class);

        $orderHelperMock
            ->expects($this->any())
            ->method('getOrderByIncrementId')
            ->with($orderIncrementId)
            ->willReturn($orderMock);

        $orderHelperMock
            ->expects($this->once())
            ->method('hasCrefoPayment')
            ->willReturn(true);

        /** @var CiaPending $uut */
        $uut = (new ObjectManagerHelper($this))->getObject(CiaPending::class, ['orderHelper' => $orderHelperMock]);
        /** @var MnsEvent $mnsEvent */
        $mnsEvent = (new ObjectManagerHelper($this))->getObject(MnsEvent::class);
        $mnsEvent->setIncrementOrderId($orderIncrementId);
        $uut->process($mnsEvent);
    }

    public function processDataProvider()
    {
        return [
            [PayPal::CODE, 'pending_paypal'],
            ['other_method', Order::STATE_PENDING_PAYMENT]
        ];
    }
}
