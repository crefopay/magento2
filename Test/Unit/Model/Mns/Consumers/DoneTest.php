<?php

namespace Trilix\CrefoPay\Test\Unit\Model\Mns\Consumers
{
    use Magento\Framework\Pricing\PriceCurrencyInterface;
    use Magento\Sales\Model\Order;
    use Magento\Framework\TestFramework\Unit\Helper\ObjectManager as ObjectManagerHelper;
    use Trilix\CrefoPay\Helper\Order as OrderHelper;
    use Trilix\CrefoPay\Model\Mns\Consumers\Done;
    use Trilix\CrefoPay\Model\Mns\MnsEvent;

    class DoneTest extends \PHPUnit\Framework\TestCase
    {
        /**
         * @param $amount
         * @param $isCommentExpected
         *
         * @dataProvider processDataProvider
         */
        public function testProcess($amount, $isCommentExpected)
        {
            $orderIncrementId = '1';

            $orderMock = $this->createMock(Order::class);

            $orderMock
                ->expects($this->once())
                ->method('setState')
                ->with(Order::STATE_COMPLETE);

            if (method_exists(Order::class, 'addCommentToStatusHistory')) {
                $orderMock
                    ->expects($isCommentExpected ? $this->once() : $this->never())
                    ->method('addCommentToStatusHistory');
            } else {
                $orderMock
                    ->expects($isCommentExpected ? $this->once() : $this->never())
                    ->method('addStatusHistoryComment');
            }

            $orderMock
                ->method('getConfig')
                ->willReturn($this->createMock(\Magento\Sales\Model\Order\Config::class));

            $orderHelperMock = $this->createMock(OrderHelper::class);

            $orderHelperMock
                ->expects($this->any())
                ->method('getOrderByIncrementId')
                ->with($orderIncrementId)
                ->willReturn($orderMock);

            $orderHelperMock
                ->expects($this->once())
                ->method('hasCrefoPayment')
                ->willReturn(true);

            $priceCurrencyMock = $this->createMock(PriceCurrencyInterface::class);

            $priceCurrencyMock
                ->expects($this->any())
                ->method('format')
                ->willReturn('');

            /** @var Done $uut */
            $uut = (new ObjectManagerHelper($this))->getObject(
                Done::class,
                ['orderHelper' => $orderHelperMock, 'priceCurrency' => $priceCurrencyMock]
            );

            /** @var MnsEvent $mnsEvent */
            $mnsEvent = (new ObjectManagerHelper($this))->getObject(MnsEvent::class);
            $mnsEvent->setIncrementOrderId($orderIncrementId);
            $mnsEvent->setAmount($amount);
            $uut->process($mnsEvent);
        }

        public function processDataProvider()
        {
            return [
                ['100', false],
                ['0', true]
            ];
        }
    }
}

namespace {
    if (!function_exists('__')) {
        require_once dirname(dirname(dirname(dirname(dirname(dirname(dirname(dirname(dirname(__DIR__))))))))) . '/vendor/magento/magento2-base/app/functions.php';
    }
}
