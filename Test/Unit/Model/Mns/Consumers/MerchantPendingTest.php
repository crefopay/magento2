<?php

namespace Trilix\CrefoPay\Test\Unit\Model\Mns\Consumers
{
    use Magento\Framework\Pricing\PriceCurrencyInterface;
    use Magento\Sales\Model\Order;
    use Trilix\CrefoPay\Model\Mns\Consumers\MerchantPending;
    use Trilix\CrefoPay\Model\Mns\MnsEvent;
    use Trilix\CrefoPay\Model\Ui\CashInAdvance\ConfigProvider as CIA;
    use Trilix\CrefoPay\Test\Unit\Framework\TestCase\Config;

    class MerchantPendingTest extends \Trilix\CrefoPay\Test\Unit\Framework\TestCase
    {
        public function testProcess()
        {
            $orderIncrementId = '1';

            $this->configure(
                [
                    Config::PAYMENT_METHOD => 'some_method',
                    Config::ORDER_INCREMENT_ID => $orderIncrementId,
                ]
            );

            $this->configureOrderMock();

            /** @var MerchantPending $uut */
            $uut = $this->getObject(MerchantPending::class, ['orderHelper' => $this->getMockContainer()->getOrderHelper()]);

            /** @var MnsEvent $mnsEvent */
            $mnsEvent = $this->getObject(MnsEvent::class);
            $mnsEvent->setIncrementOrderId($orderIncrementId);

            $uut->process($mnsEvent);
        }

        public function testProcessForCIA()
        {
            $orderIncrementId = '1';

            $this->configure(
                [
                    Config::PAYMENT_METHOD => CIA::CODE,
                    Config::ORDER_INCREMENT_ID => $orderIncrementId,
                    Config::ORDER_COMMENT => 'Incoming payment: 12.34 EUR',
                ]
            );

            $this->configureOrderMock();

            $priceCurrencyMock = $this->mock(PriceCurrencyInterface::class);
            $this->expects($priceCurrencyMock, 'format', '12.34 EUR');

            /** @var MerchantPending $uut */
            $uut = $this->getObject(
                MerchantPending::class,
                ['orderHelper' => $this->getMockContainer()->getOrderHelper(), 'priceCurrency' => $priceCurrencyMock]
            );

            /** @var MnsEvent $mnsEvent */
            $mnsEvent = $this->getObject(MnsEvent::class);

            $mnsEvent->setIncrementOrderId($orderIncrementId);
            $mnsEvent->setAmount('1234');
            $mnsEvent->setCurrency('EUR');

            $uut->process($mnsEvent);
        }

        private function configureOrderMock()
        {
            $this->expects($this->getMockContainer()->getOrder(), 'setState', Order::STATE_PROCESSING);
            $this->expects($this->getMockContainer()->getOrderRepository(), 'save');
        }
    }
}

namespace {
    if (!function_exists('__')) {
        require_once dirname(dirname(dirname(dirname(dirname(dirname(dirname(dirname(dirname(__DIR__))))))))) . '/vendor/magento/magento2-base/app/functions.php';
    }
}
