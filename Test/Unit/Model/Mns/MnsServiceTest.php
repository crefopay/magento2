<?php


declare(strict_types=1);

namespace Trilix\CrefoPay\Test\Unit\Model\Mns;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaInterface;
use Upg\Library\Callback\MacCalculator;
use Upg\Library\Callback\Exception\MacValidation;
use Trilix\CrefoPay\Api\Data\CrefoPayMnsInterface;
use Trilix\CrefoPay\Api\Data\CrefoPayMnsSearchResultInterface;
use Trilix\CrefoPay\Client\Constants;
use Trilix\CrefoPay\Model\Mns\MnsEvent;
use Trilix\CrefoPay\Client\UpgFactory;
use Trilix\CrefoPay\Logger\MnsLogger;
use Trilix\CrefoPay\Model\Mns\MnsConsumerPool;
use Trilix\CrefoPay\Model\Mns\MnsEventFactory;
use Trilix\CrefoPay\Model\Mns\MnsRepository;
use Trilix\CrefoPay\Model\Mns\MnsService;
use Trilix\CrefoPay\Model\Mns\ProcessSupervisor\SupervisorInterface;

class MnsServiceTest extends TestCase
{
    /** @var MnsService */
    private $mnsService;

    /** @var UpgFactory|MockObject */
    private $upgFactoryMock;

    /** @var LoggerInterface|MockObject */
    private $loggerMock;

    /** @var MnsLogger|MockObject */
    private $mnsLoggerMock;

    /** @var MnsConsumerPool|MockObject */
    private $mnsConsumerPoolMock;

    /** @var MnsEventFactory|MockObject */
    private $mnsEventFactoryMock;

    /** @var MnsRepository|MockObject */
    private $mnsRepositoryMock;

    /** @var SearchCriteriaBuilder|MockObject */
    private $searchCriteriaBuilderMock;

    /** @var SupervisorInterface|MockObject */
    private $supervisorMock;

    /** @var MacCalculator|MockObject */
    private $macCalculatorMock;

    /** @var MnsEvent|MockObject */
    private $mnsEventMock;

    /** @var SearchCriteriaInterface|MockObject */
    private $searchCriteriaMock;

    /** @var CrefoPayMnsSearchResultInterface|MockObject */
    private $crefoPayMnsSearchResultMock;

    public function setUp(): void
    {
        $this->upgFactoryMock = $this->createMock(UpgFactory::class);
        $this->loggerMock = $this->createMock(LoggerInterface::class);
        $this->mnsLoggerMock = $this->createMock(MnsLogger::class);
        $this->mnsConsumerPoolMock = $this->createMock(MnsConsumerPool::class);
        $this->mnsEventFactoryMock = $this->createMock(MnsEventFactory::class);
        $this->mnsRepositoryMock = $this->createMock(MnsRepository::class);
        $this->searchCriteriaBuilderMock = $this->createMock(SearchCriteriaBuilder::class);
        $this->supervisorMock = $this->createMock(SupervisorInterface::class);
        $this->macCalculatorMock = $this->createMock(MacCalculator::class);

        $this->mnsEventMock = $this->createMock(MnsEvent::class);
        $this->mnsEventMock->expects(self::any())
            ->method('getIncrementOrderId')
            ->willReturn(10);
        $this->mnsEventMock->expects(self::any())
            ->method('getStoreId')
            ->willReturn(2);

        $this->searchCriteriaMock = $this->createMock(SearchCriteriaInterface::class);
        $this->crefoPayMnsSearchResultMock = $this->createMock(CrefoPayMnsSearchResultInterface::class);

        $this->mnsService = new MnsService(
            $this->upgFactoryMock,
            $this->loggerMock,
            $this->mnsLoggerMock,
            $this->mnsConsumerPoolMock,
            $this->mnsEventFactoryMock,
            $this->mnsRepositoryMock,
            $this->searchCriteriaBuilderMock,
            $this->supervisorMock
        );
    }

    public function testAcknowledge()
    {
        $this->upgFactoryMock->expects(self::once())
            ->method('createCallbackMacCalculator')
            ->willReturn($this->macCalculatorMock);

        $this->macCalculatorMock->expects(self::once())
            ->method('validateResponse')
            ->willReturn(true);

        $this->mnsEventFactoryMock->expects(self::once())
            ->method('createFromPost')
            ->with([])
            ->willReturn($this->mnsEventMock);

        $this->mnsEventMock->expects(self::exactly(2))
            ->method('getTransactionStatus')
            ->willReturn(Constants::TX_ACK);

        $this->searchCriteriaBuilderMock->expects(self::at(0))
            ->method('addFilter')
            ->with(CrefoPayMnsInterface::ORDER_INCREMENT_ID, 10)
            ->willReturn($this->searchCriteriaBuilderMock);

        $this->searchCriteriaBuilderMock->expects(self::at(1))
            ->method('addFilter')
            ->with(CrefoPayMnsInterface::STORE_ID, 2)
            ->willReturn($this->searchCriteriaBuilderMock);

        $this->searchCriteriaBuilderMock->expects(self::at(2))
            ->method('addFilter')
            ->with(CrefoPayMnsInterface::TRANSACTION_STATUS, Constants::TX_ACK)
            ->willReturn($this->searchCriteriaBuilderMock);

        $this->searchCriteriaBuilderMock->expects(self::once())
            ->method('create')
            ->willReturn($this->searchCriteriaMock);

        $this->mnsRepositoryMock->expects(self::once())
            ->method('getList')
            ->with($this->searchCriteriaMock)
            ->willReturn($this->crefoPayMnsSearchResultMock);

        $this->crefoPayMnsSearchResultMock->expects(self::once())
            ->method('getTotalCount')
            ->willReturn(null);

        $this->mnsRepositoryMock->expects(self::once())
            ->method('save');

        $this->mnsService->acknowledge([]);
    }

    public function testAcknowledgeWhenNotificationAlreadyExists()
    {
        $this->upgFactoryMock->expects(self::once())
            ->method('createCallbackMacCalculator')
            ->willReturn($this->macCalculatorMock);

        $this->macCalculatorMock->expects(self::once())
            ->method('validateResponse')
            ->willReturn(true);

        $this->mnsEventFactoryMock->expects(self::once())
            ->method('createFromPost')
            ->with([])
            ->willReturn($this->mnsEventMock);

        $this->mnsEventMock->expects(self::exactly(2))
            ->method('getTransactionStatus')
            ->willReturn(Constants::TX_ACK);

        $this->searchCriteriaBuilderMock->expects(self::at(0))
            ->method('addFilter')
            ->with(CrefoPayMnsInterface::ORDER_INCREMENT_ID, 10)
            ->willReturn($this->searchCriteriaBuilderMock);

        $this->searchCriteriaBuilderMock->expects(self::at(1))
            ->method('addFilter')
            ->with(CrefoPayMnsInterface::STORE_ID, 2)
            ->willReturn($this->searchCriteriaBuilderMock);

        $this->searchCriteriaBuilderMock->expects(self::at(2))
            ->method('addFilter')
            ->with(CrefoPayMnsInterface::TRANSACTION_STATUS, Constants::TX_ACK)
            ->willReturn($this->searchCriteriaBuilderMock);

        $this->searchCriteriaBuilderMock->expects(self::once())
            ->method('create')
            ->willReturn($this->searchCriteriaMock);

        $this->mnsRepositoryMock->expects(self::once())
            ->method('getList')
            ->with($this->searchCriteriaMock)
            ->willReturn($this->crefoPayMnsSearchResultMock);

        $this->crefoPayMnsSearchResultMock->expects(self::once())
            ->method('getTotalCount')
            ->willReturn(2);

        $this->mnsRepositoryMock->expects(self::never())
            ->method('save');

        $this->mnsService->acknowledge([]);
    }

    public function testAcknowledgeWhenNotificationHasNewStatus()
    {
        $this->upgFactoryMock->expects(self::once())
            ->method('createCallbackMacCalculator')
            ->willReturn($this->macCalculatorMock);

        $this->macCalculatorMock->expects(self::once())
            ->method('validateResponse')
            ->willReturn(true);

        $this->mnsEventFactoryMock->expects(self::once())
            ->method('createFromPost')
            ->with([])
            ->willReturn($this->mnsEventMock);

        $this->mnsEventMock->expects(self::exactly(1))
            ->method('getTransactionStatus')
            ->willReturn(Constants::TX_NEW);

        $this->searchCriteriaBuilderMock->expects(self::never())
            ->method('addFilter');

        $this->mnsRepositoryMock->expects(self::once())
            ->method('save');

        $this->mnsService->acknowledge([]);
    }

    public function testAcknowledgeMacValidationException()
    {
        $this->upgFactoryMock->expects(self::once())
            ->method('createCallbackMacCalculator')
            ->willReturn($this->macCalculatorMock);

        $this->macCalculatorMock->expects(self::once())
            ->method('validateResponse')
            ->willThrowException(new MacValidation('', '', []));

        $this->mnsLoggerMock->expects(self::once())
            ->method('error')
            ->with('MAC validation failed for CrefoPay MNS event: ');

        $this->mnsService->acknowledge([]);
    }

    public function testAcknowledgeException()
    {
        $this->upgFactoryMock->expects(self::once())
            ->method('createCallbackMacCalculator')
            ->willReturn($this->macCalculatorMock);

        $this->macCalculatorMock->expects(self::once())
            ->method('validateResponse')
            ->willReturn(true);

        $this->mnsEventFactoryMock->expects(self::once())
            ->method('createFromPost')
            ->with([])
            ->willThrowException(new \Exception('error'));

        $this->loggerMock->expects(self::once())
            ->method('error')
            ->with('Error during acknowledging MNS event: error');

        $this->mnsLoggerMock->expects(self::once())
            ->method('error')
            ->with('error');

        $this->mnsService->acknowledge([]);
    }

    public function testProcess()
    {
        $this->searchCriteriaBuilderMock->expects(self::once())
            ->method('addFilter')
            ->with(CrefoPayMnsInterface::MNS_STATUS, CrefoPayMnsInterface::STATUS_ACK)
            ->willReturn($this->searchCriteriaBuilderMock);

        $this->searchCriteriaBuilderMock->expects(self::once())
            ->method('setPageSize')
            ->with(100)
            ->willReturn($this->searchCriteriaBuilderMock);

        $this->searchCriteriaBuilderMock->expects(self::once())
            ->method('setCurrentPage')
            ->with(1)
            ->willReturn($this->searchCriteriaBuilderMock);

        $this->searchCriteriaBuilderMock->expects(self::once())
            ->method('create')
            ->willReturn($this->searchCriteriaMock);

        $this->mnsRepositoryMock->expects(self::once())
            ->method('getList')
            ->with($this->searchCriteriaMock)
            ->willReturn($this->crefoPayMnsSearchResultMock);

        $this->crefoPayMnsSearchResultMock->expects(self::exactly(2))
            ->method('getTotalCount')
            ->willReturn(3);

        $this->supervisorMock->expects(self::once())
            ->method('setTotalCount')
            ->with(3);

        $this->crefoPayMnsSearchResultMock->expects(self::once())
            ->method('getItems')
            ->willReturn($this->getMnsEvents());

        $this->searchCriteriaMock->expects(self::once())
            ->method('getCurrentPage');

        $this->searchCriteriaMock->expects(self::once())
            ->method('setCurrentPage')
            ->willReturn($this->searchCriteriaMock);

        $this->searchCriteriaMock->expects(self::once())
            ->method('getPageSize')
            ->willReturn(100);

        $this->mnsRepositoryMock->expects(self::exactly(52))
            ->method('save');

        $this->supervisorMock->expects(self::exactly(52))
            ->method('ok');

        $this->mnsService->process($this->supervisorMock);
    }

    public function getMnsEvents(): array
    {
        $events = [];
        for ($i = 0; $i <= 51; $i++) {
            $events[] = $this->createMock(MnsEvent::class);
        }

        return $events;
    }
}
