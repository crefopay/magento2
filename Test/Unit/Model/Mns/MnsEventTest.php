<?php

namespace Trilix\CrefoPay\Test\Unit\Model\Mns;

use Trilix\CrefoPay\Api\Data\CrefoPayMnsInterface;
use Trilix\CrefoPay\Model\Mns\MnsEvent;

class MnsEventTest extends \Trilix\CrefoPay\Test\Unit\Framework\TestCase
{
    public function testGettersSetters()
    {
        $uut = $this->getUut();

        $uut->setMerchantId('1');
        $uut->setStoreId('2');
        $uut->setIncrementOrderId('3');
        $uut->setCaptureId('4');
        $uut->setMerchantReference('5');
        $uut->setPaymentReference('6');
        $uut->setUserId('7');
        $uut->setTransactionStatus('8');
        $uut->setCaptureStatus('9');
        $uut->setAmount('10');
        $uut->setCurrency('11');
        $uut->setCreatedAt('12');
        $uut->setProcessedAt('13');
        $uut->setErrorDetails('14');
        $uut->setMnsStatus(CrefoPayMnsInterface::STATUS_ACK);
        $uut->setNumberOfAttemptsToProcessNotification(1);


        $this->assertEquals($uut->getMerchantId(), '1');
        $this->assertEquals($uut->getStoreId(), '2');
        $this->assertEquals($uut->getIncrementOrderId(), '3');
        $this->assertEquals($uut->getCaptureId(), '4');
        $this->assertEquals($uut->getMerchantReference(), '5');
        $this->assertEquals($uut->getPaymentReference(), '6');
        $this->assertEquals($uut->getUserId(), '7');
        $this->assertEquals($uut->getTransactionStatus(), '8');
        $this->assertEquals($uut->getCaptureStatus(), '9');
        $this->assertEquals($uut->getAmount(), '10');
        $this->assertEquals($uut->getCurrency(), '11');
        $this->assertEquals($uut->getCreatedAt(), '12');
        $this->assertEquals($uut->getProcessedAt(), '13');
        $this->assertEquals($uut->getErrorDetails(), '14');
        $this->assertEquals($uut->getMnsStatus(), CrefoPayMnsInterface::STATUS_ACK);
        $this->assertEquals($uut->getNumberOfAttemptsToProcessNotification(), 1);
    }

    public function testSetWrongMnsStatus()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->getUut()->setMnsStatus('no_such_status');
    }

    /**
     * @return MnsEvent
     */
    private function getUut()
    {
        return $this->getObject(MnsEvent::class);
    }
}
