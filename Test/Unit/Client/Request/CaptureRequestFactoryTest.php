<?php

namespace Trilix\CrefoPay\Test\Unit\Client\Request;

use Trilix\CrefoPay\Client\Request\CaptureRequestFactory;
use Trilix\CrefoPay\Helper\Order as OrderHelper;
use Magento\Sales\Model\Order as SalesOrder;
use Magento\Sales\Model\ResourceModel\Order\Invoice\Collection as InvoiceCollection;

class CaptureRequestFactoryTest extends AbstractRequestFactoryTest
{
    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     * @dataProvider createDataProvider
     */
    public function testCreate($orderId, $invoiceCnt, $expectedCaptureId)
    {
        $invoiceCollectionMock = $this->createMock(InvoiceCollection::class);

        $invoiceCollectionMock
            ->expects($this->any())
            ->method('count')
            ->willReturn($invoiceCnt);

        $orderMock = $this->createMock(SalesOrder::class);

        $orderMock
            ->expects($this->any())
            ->method('getInvoiceCollection')
            ->willReturn($invoiceCollectionMock);

        $orderHelperMock = $this->createMock(OrderHelper::class);

        $orderHelperMock
            ->expects($this->any())
            ->method('getOrderByIncrementId')
            ->with($orderId)
            ->willReturn($orderMock);

        /** @var CaptureRequestFactory $uut */
        $uut = $this->getUut(CaptureRequestFactory::class, ['orderHelper' => $orderHelperMock]);
        $captureRequest = $uut->create($orderId, 0);

        $this->assertEquals($expectedCaptureId, $captureRequest->getCaptureID());
    }

    public function createDataProvider()
    {
        return [
            ['12345', 0, '12345:1'],
            ['123', 2, '123:3'],
        ];
    }
}
