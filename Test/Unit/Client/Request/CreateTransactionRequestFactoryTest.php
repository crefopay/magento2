<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Test\Unit\Client\Request;

use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager as ObjectManagerHelper;
use Magento\Framework\Locale\ResolverInterface as Locale;
use Upg\Library\Request\Objects\Company;
use Upg\Library\User\Type as UserType;
use Trilix\CrefoPay\Gateway\Request\AddressBuilder;
use Trilix\CrefoPay\Gateway\Request\AmountBuilder;
use Trilix\CrefoPay\Gateway\Request\PersonBuilder;
use Trilix\CrefoPay\Gateway\Request\User\CrefoPayUserFactory;
use Trilix\CrefoPay\Gateway\Request\User\CrefoPayUser;
use Trilix\CrefoPay\Gateway\Request\CompanyBuilder;
use Trilix\CrefoPay\Client\Request\CreateTransactionRequestFactory;

class CreateTransactionRequestFactoryTest extends AbstractRequestFactoryTest
{
    /**
     * @param string $companyAddress
     * @param $userType
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Upg\Library\Serializer\Exception\VisitorCouldNotBeFound
     * @dataProvider createDataProvider
     */
    public function testCreate($companyAddress, $userType, $storeLocale, $expectedCrefoPayLocale)
    {
        $billingAddressMock = $this->getAddressMock();

        $billingAddressMock
            ->expects($this->any())
            ->method('getCompany')
            ->willReturn($companyAddress);

        $shippingAddressMock = $this->getAddressMock();

        $shippingAddressMock
            ->expects($this->once())
            ->method('getEmail')
            ->willReturn('');

        $quoteMock = $this->createMock(Quote::class);

        $quoteMock
            ->expects($this->once())
            ->method('getBillingAddress')
            ->willReturn($billingAddressMock);

        $quoteMock
            ->expects($this->once())
            ->method('getShippingAddress')
            ->willReturn($shippingAddressMock);

        $quoteMock
            ->expects($this->once())
            ->method('isVirtual')
            ->willReturn(false);

        $quoteRepositoryMock = $this->createMock(CartRepositoryInterface::class);

        $quoteRepositoryMock
            ->expects($this->once())
            ->method('getActive')
            ->willReturn($quoteMock);

        $crefoPayUserMock = $this->createMock(CrefoPayUser::class);

        $crefoPayUserMock
            ->expects($this->any())
            ->method('getType')
            ->willReturn($userType);

        $crefoPayUserFactoryMock = $this->createMock(CrefoPayUserFactory::class);

        $crefoPayUserFactoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn($crefoPayUserMock);

        $companyBuilderMock = $this->createMock(CompanyBuilder::class);

        $companyBuilderMock
            ->expects($companyAddress && $userType === UserType::USER_TYPE_BUSINESS ? $this->once() : $this->never())
            ->method('build')
            ->willReturn(new Company());

        $localeMock = $this->createMock(Locale::class);

        $localeMock
            ->expects($this->once())
            ->method('getLocale')
            ->willReturn($storeLocale);

        /** @var CreateTransactionRequestFactory $uut */
        $uut = $this->getUut(
            CreateTransactionRequestFactory::class,
            [
                'locale'              => $localeMock,
                'quoteRepository'     => $quoteRepositoryMock,
                'crefoPayUserFactory' => $crefoPayUserFactoryMock,
                'companyBuilder'      => $companyBuilderMock,
                'amountBuilder'       => (new ObjectManagerHelper($this))->getObject(AmountBuilder::class),
                'personBuilder'       => (new ObjectManagerHelper($this))->getObject(PersonBuilder::class),
                'addressBuilder'      => (new ObjectManagerHelper($this))->getObject(AddressBuilder::class),
            ]
        );

        $createTransactionRequest = $uut->createForFrontedFlow(1);

        if ($companyAddress) {
            $this->assertInstanceOf(Company::class, $createTransactionRequest->getCompanyData());
        } else {
            $this->assertNull($createTransactionRequest->getCompanyData());
        }

        $this->assertEquals($expectedCrefoPayLocale, $createTransactionRequest->getLocale());
    }

    public function createDataProvider()
    {
        return [
            ['', UserType::USER_TYPE_PRIVATE, 'uk_UA', 'EN'],
            ['company address', UserType::USER_TYPE_BUSINESS, 'nl_BE', 'NL'],
        ];
    }

    private function getAddressMock()
    {
        $addressMock = $this->createMock(Address::class);

        $addressMock
            ->expects($this->once())
            ->method('getStreet')
            ->willReturn([]);

        return $addressMock;
    }
}
