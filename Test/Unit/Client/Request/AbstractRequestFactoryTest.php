<?php

namespace Trilix\CrefoPay\Test\Unit\Client\Request;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager as ObjectManagerHelper;
use Trilix\CrefoPay\Client\ConfigFactory;
use Upg\Library\Config as CrefoPayConfig;

abstract class AbstractRequestFactoryTest extends \PHPUnit\Framework\TestCase
{
    protected function getConfigFactoryMock()
    {
        $configMock = $this->createMock(CrefoPayConfig::class);
        $configFactoryMock = $this->createMock(ConfigFactory::class);

        $configFactoryMock
            ->expects($this->any())
            ->method('create')
            ->willReturn($configMock);

        return $configFactoryMock;
    }

    protected function getUut($className, array $diParams = [])
    {
        $diParams['configFactory'] = $this->getConfigFactoryMock();

        return (new ObjectManagerHelper($this))->getObject($className, $diParams);
    }
}
