<?php

namespace Trilix\CrefoPay\Test\Unit\Client\Request;

use Magento\Payment\Gateway\Data\OrderAdapterInterface;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Trilix\CrefoPay\Client\Request\ReserveRequestFactory;
use Magento\Payment\Model\InfoInterface as PaymentInfoInterface;

class ReserveRequestFactoryTest extends AbstractRequestFactoryTest
{
    /**
     * @param string                     $paymentMethod
     * @param PaymentDataObjectInterface $paymentDO
     * @param bool                       $isPaymentInstrumentIdExpected
     * @param bool                       $isAdditionalInfoExpected
     *
     * @throws \Upg\Library\Serializer\Exception\VisitorCouldNotBeFound
     * @dataProvider createDataProvider
     */
    public function testCreate(string $paymentMethod, PaymentDataObjectInterface $paymentDO, bool $isPaymentInstrumentIdExpected, bool $isAdditionalInfoExpected)
    {
        $amountBuilderMock = $this->createMock(\Trilix\CrefoPay\Gateway\Request\AmountBuilder::class);

        $amountBuilderMock
            ->expects($this->once())
            ->method('buildFromOrder')
            ->willReturn(new \Upg\Library\Request\Objects\Amount());

        /** @var ReserveRequestFactory $uut */
        $uut = $this->getUut(ReserveRequestFactory::class, ['amountBuilder' => $amountBuilderMock]);
        $reserveRequest = $uut->create($paymentMethod, $paymentDO);

        if ($isPaymentInstrumentIdExpected) {
            $this->assertNotNull($reserveRequest->getPaymentInstrumentID());
        } else {
            $this->assertNull($reserveRequest->getPaymentInstrumentID());
        }

        if ($isAdditionalInfoExpected) {
            $additionalInfo = new \Trilix\CrefoPay\Client\Request\Structure\AdditionalInfo([
                \Trilix\CrefoPay\Client\Request\Structure\AdditionalInfo::DATE_OF_BIRTH => $this->getDateOfBirth()
            ]);

            $this->assertTrue((string)$additionalInfo === $reserveRequest->getAdditionalInformation());
        }
    }

    public function createDataProvider()
    {
        return [
            ['', $this->createDO(false, true), false, true],
            ['', $this->createDO(true, false), true, false],
        ];
    }

    private function createDO(bool $isPaymentInstrumentIdExpected, bool $isAdditionalInfoExpected)
    {
        $paymentDOMock = $this->createMock(PaymentDataObjectInterface::class);
        $orderMock = $this->createMock(OrderAdapterInterface::class);

        $paymentDOMock
            ->expects($this->any())
            ->method('getOrder')
            ->willReturn($orderMock);

        $additionalInformation = [];

        if ($isPaymentInstrumentIdExpected) {
            $additionalInformation['paymentInstrumentId'] = 'payment instrument id';
        }

        if ($isAdditionalInfoExpected) {
            $additionalInformation[\Trilix\CrefoPay\Client\Request\Structure\AdditionalInfo::DATE_OF_BIRTH] = $this->getDateOfBirth();
            $additionalInformation['something_else'] = 'random value';
        }

        $paymentMock = $this->createMock(PaymentInfoInterface::class);

        $paymentMock
            ->expects($this->once())
            ->method('getAdditionalInformation')
            ->willReturn($additionalInformation);

        $paymentDOMock
            ->expects($this->once())
            ->method('getPayment')
            ->willReturn($paymentMock);

        return $paymentDOMock;
    }

    private function getDateOfBirth()
    {
        return '01-01-1980';
    }
}
