<?php

namespace Trilix\CrefoPay\Test\Unit\Client\Request;

use PHPUnit\Framework\MockObject\MockObject;
use Magento\Customer\Model\Session;
use Trilix\CrefoPay\Gateway\Config\Config;
use Trilix\CrefoPay\Client\Request\UserRiskManager;

class UserRiskManagerTest extends \PHPUnit\Framework\TestCase
{
    /** @var UserRiskManager */
    private $userRiskManager;

    /** @var Session|MockObject */
    private $customerSessionMock;

    /** @var Config|MockObject */
    private $configMock;

    protected function setUp(): void
    {
        $this->customerSessionMock = $this->getMockBuilder(Session::class)
            ->disableOriginalConstructor()
            ->setMethods(['getUserRiskClass'])
            ->getMock();

        $this->configMock = $this->getMockBuilder(Config::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->userRiskManager = new UserRiskManager(
            $this->customerSessionMock,
            $this->configMock
        );
    }

    public function testGetUserRiskClassFromConfig()
    {
        $this->configMock->expects($this->once())
            ->method('getRiskClass')
            ->willReturn(1);

        $this->customerSessionMock->expects($this->once())
            ->method('getUserRiskClass')
            ->willReturn(null);

        $this->assertSame(1, $this->userRiskManager->getUserRiskClass());
    }

    public function testGetUserRiskClassFromSession()
    {
        $this->configMock->expects($this->once())
            ->method('getRiskClass')
            ->willReturn(1);

        $this->customerSessionMock->expects($this->once())
            ->method('getUserRiskClass')
            ->willReturn(['value' => 2]);

        $this->assertSame(2, $this->userRiskManager->getUserRiskClass());
    }
}
