<?php

namespace Trilix\CrefoPay\Test\Unit\Client\Request\Structure;

use Trilix\CrefoPay\Client\Request\Structure\AdditionalInfo;

class AdditionalInfoTest extends \Trilix\CrefoPay\Test\Unit\Framework\TestCase
{
    public function testNewInstance()
    {
        $uut = new AdditionalInfo(['wrong_field' => 'wrong_value', AdditionalInfo::DATE_OF_BIRTH => 'dob']);
        $this->assertEquals('{"dateOfBirth":"dob"}', (string)$uut);
    }

    public function testHasData()
    {
        $uut = new AdditionalInfo([AdditionalInfo::DATE_OF_BIRTH => 'dob']);
        $this->assertTrue($uut->hasData());
        $uut = new AdditionalInfo(['wrong_field' => 'wrong_value']);
        $this->assertFalse($uut->hasData());
    }
}
