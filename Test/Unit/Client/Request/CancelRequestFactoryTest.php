<?php

namespace Trilix\CrefoPay\Test\Unit\Client\Request;

use Trilix\CrefoPay\Client\Request\CancelRequestFactory;

class CancelRequestFactoryTest extends AbstractRequestFactoryTest
{
    public function testCreate()
    {
        $orderId = 123;
        /** @var CancelRequestFactory $uut */
        $uut = $this->getUut(CancelRequestFactory::class);
        $cancelRequest = $uut->create($orderId);

        $this->assertEquals($orderId, $cancelRequest->getOrderID());
    }
}
