<?php

namespace Trilix\CrefoPay\Test\Unit\Framework\TestCase;

use Trilix\CrefoPay\Test\Unit\Framework\Exception;
use Trilix\CrefoPay\Test\Unit\Framework\TestCase;

class MockContainerFactory
{
    /** @var Config */
    private $config;
    /** @var MockContainer */
    private $mockContainer;
    /** @var TestCase */
    private $testCase;

    public function create(Config $config, TestCase $testCase)
    {
        $this->config = $config;
        $this->mockContainer = new MockContainer();
        $this->testCase = $testCase;

        $this->createMocks();
        $this->configureMocks();

        return $this->mockContainer;
    }

    private function createMocks()
    {
        $this->mockContainer->setOrderHelper($this->testCase->mock(\Trilix\CrefoPay\Helper\Order::class));
        $this->mockContainer->setPayment($this->testCase->mock($this->config->getSetting(Config::PAYMENT_CLASS)));
        $this->mockContainer->setOrder($this->testCase->mock(\Magento\Sales\Model\Order::class));
        $this->mockContainer->setPaymentMethod($this->testCase->mock(\Trilix\CrefoPay\Model\Method\Adapter::class));
        $this->mockContainer->setOrderRepository($this->testCase->mock(\Magento\Sales\Model\OrderRepository::class));
        $this->mockContainer->setMnsLogger($this->testCase->mock(\Trilix\CrefoPay\Logger\MnsLogger::class));
        $this->mockContainer->setSearchCriteriaBuilder($this->testCase->mock(\Magento\Framework\Api\SearchCriteriaBuilder::class));
        $this->mockContainer->setSearchCriteria($this->testCase->mock(\Magento\Framework\Api\SearchCriteria::class));
        $this->mockContainer->setMnsRepository($this->testCase->mock(\Trilix\CrefoPay\Model\Mns\MnsRepository::class));
        $this->mockContainer->setMnsSearchResult($this->testCase->mock(\Trilix\CrefoPay\Api\Data\CrefoPayMnsSearchResultInterface::class));
        $this->mockContainer->setMnsConsumerPool($this->testCase->mock(\Trilix\CrefoPay\Model\Mns\MnsConsumerPool::class));
        $this->mockContainer->setMnsSupervisor($this->testCase->mock(\Trilix\CrefoPay\Model\Mns\ProcessSupervisor\SupervisorInterface::class));
        $this->mockContainer->setUpgFactory($this->testCase->mock(\Trilix\CrefoPay\Client\UpgFactory::class));
        $this->mockContainer->setMacCalculator($this->testCase->mock(\Upg\Library\Callback\MacCalculator::class));
    }

    private function configureMocks()
    {
        $this->configureOrderHelper();
        $this->configureOrder();
        $this->configurePayment();
        $this->configureSearchCriteriaBuilder();
        $this->configureMnsRepository();
        $this->configureMnsSearchResult();
        $this->configureMnsConsumerPool();
        $this->configureUpgFactory();
    }

    private function configureOrderHelper()
    {
        $orderIncrementId = $this->config->getSetting(Config::ORDER_INCREMENT_ID);

        $this->testCase->expects(
            $this->mockContainer->getOrderHelper(),
            'hasCrefoPayment',
            $this->config->getSetting(Config::HAS_CREFO_PAYMENT),
            'any'
        );

        if (!is_null($orderIncrementId)) {
            $this->testCase->expectsWith(
                $this->mockContainer->getOrderHelper(),
                'getOrderByIncrementId',
                [$orderIncrementId],
                $this->mockContainer->getOrder(),
                'any'
            );
        }

        $this->testCase->expects(
            $this->mockContainer->getOrderHelper(),
            'getOrderRepository',
            $this->mockContainer->getOrderRepository(),
            'any'
        );
    }

    private function configureOrder()
    {
        $this->testCase->expects($this->mockContainer->getOrder(), 'getPayment', $this->mockContainer->getPayment(), 'any');
        $orderComment = $this->config->getSetting(Config::ORDER_COMMENT);

        if (!is_null($orderComment)) {
            $methodName = method_exists(\Magento\Sales\Model\Order::class, 'addCommentToStatusHistory')
                ? 'addCommentToStatusHistory'
                : 'addStatusHistoryComment';

            if (is_string($orderComment)) {
                $this->testCase->expectsWith($this->mockContainer->getOrder(), $methodName, [$orderComment]);
            } elseif (is_int($orderComment)) {
                $invocationCount = $orderComment;
                $this->testCase->expects($this->mockContainer->getOrder(), $methodName, null, $invocationCount);
            } else {
                throw new Exception(sprintf('Invalid value for setting "%s"', Config::ORDER_COMMENT));
            }
        }

        $this->testCase->expects($this->mockContainer->getOrder(), 'getConfig', $this->testCase->mock(\Magento\Sales\Model\Order\Config::class), 'any');
    }

    private function configurePayment()
    {
        $paymentClass = $this->config->getSetting(Config::PAYMENT_CLASS);

        $this->returnSettingValue($this->mockContainer->getPayment(), 'getMethod', Config::PAYMENT_METHOD);

        if (method_exists($paymentClass, 'getMethodInstance')) {
            $this->testCase->expects($this->mockContainer->getPayment(), 'getMethodInstance', $this->mockContainer->getPaymentMethod(), 'any');
        }
    }

    private function configureSearchCriteriaBuilder()
    {
        $this->mockContainer->getSearchCriteriaBuilder()->method('addFilter')->willReturnSelf();
        $this->mockContainer->getSearchCriteriaBuilder()->method('setPageSize')->willReturnSelf();
        $this->mockContainer->getSearchCriteriaBuilder()->method('setCurrentPage')->willReturnSelf();
        $this->testCase->expects($this->mockContainer->getSearchCriteriaBuilder(), 'create', $this->mockContainer->getSearchCriteria(), 'any');
    }

    private function configureMnsRepository()
    {
        $this->mockContainer->getMnsRepository()->method('getList')->willReturnOnConsecutiveCalls($this->mockContainer->getMnsSearchResult(), null);
    }

    private function configureMnsSearchResult()
    {
        $this->expectArrayOf(Config::MNS_EVENTS, \Trilix\CrefoPay\Model\Mns\MnsEvent::class);
        $mnsEvents = $this->config->getSetting(Config::MNS_EVENTS);
        $this->mockContainer->getMnsSearchResult()->method('getItems')->willReturnOnConsecutiveCalls($mnsEvents, null);
        $this->testCase->expects($this->mockContainer->getMnsSearchResult(), 'getTotalCount', count($mnsEvents), 'any');
    }

    private function configureMnsConsumerPool()
    {
        $this->expectArrayOf(Config::MNS_APPLICABLE_CONSUMERS, \Trilix\CrefoPay\Model\Mns\MnsConsumerInterface::class);
        $consumers = $this->config->getSetting(Config::MNS_APPLICABLE_CONSUMERS);
        $this->mockContainer->getMnsConsumerPool()->method('getConsumersByEvent')->willReturn($consumers);
    }

    private function configureUpgFactory()
    {
        $this->mockContainer->getUpgFactory()->method('createCallbackMacCalculator')->willReturn($this->mockContainer->getMacCalculator());
    }

    private function returnSettingValue($object, $method, $setting)
    {
        if (!$this->config->hasSetting($setting)) {
            return;
        }

        if (!method_exists($object, $method)) {
            throw new Exception(sprintf('Setting "%s" has been explicitly set, but class "%s" does not have method "%s"', $setting, get_class($object), $method));
        }

        $this->testCase->expects($object, $method, $this->config->getSetting($setting), 'any');
    }

    private function expectArrayOf($setting, $instanceOf)
    {
        $objects = $this->config->getSetting($setting);

        if (!is_array($objects)) {
            throw new Exception(sprintf('"%s" setting has to be an array', $setting));
        }

        foreach ($objects as $object) {
            if (!($object instanceof $instanceOf)) {
                throw new Exception(
                    sprintf(
                        "Each object in '%s' setting has to be instance of %s, instance of %s given",
                        $setting,
                        $instanceOf,
                        get_class($object)
                    )
                );
            }
        }
    }
}
