<?php

namespace Trilix\CrefoPay\Test\Unit\Framework\TestCase;

use Trilix\CrefoPay\Test\Unit\Framework\Exception;

class Config
{
    /** @see \Trilix\CrefoPay\Helper\Order::hasCrefoPayment */
    public const HAS_CREFO_PAYMENT = 'orderHelper.hasCrefoPayment'; //
    /** @see \Magento\Sales\Api\Data\OrderPaymentInterface::getMethod */
    public const PAYMENT_METHOD = 'payment.methodCode';
    /** Class name of the Payment object */
    public const PAYMENT_CLASS = 'payment.class';
    /**
     * @see \Magento\Sales\Model\Order::addStatusHistoryComment
     * @see \Magento\Sales\Model\Order::addCommentToStatusHistory
     */
    public const ORDER_COMMENT = 'order.comment';
    /** @see \Trilix\CrefoPay\Helper\Order::getOrderByIncrementId */
    public const ORDER_INCREMENT_ID = 'order.incrementId';
    /** @see \Trilix\CrefoPay\Api\Data\CrefoPayMnsSearchResultInterface::getItems */
    public const MNS_EVENTS = 'mns.events';
    /** @see \Trilix\CrefoPay\Model\Mns\MnsConsumerPool::getConsumersByEvent */
    public const MNS_APPLICABLE_CONSUMERS = 'mns.consumers';

    private $settings = [];
    private $configuredSettings = [];

    public function __construct(array $settings = [])
    {
        $invalidSettings = array_diff(array_keys($settings), self::getAvailableSettings());

        if (!empty($invalidSettings)) {
            throw new Exception('Unknown settings: ', implode(',', $invalidSettings));
        }

        $this->configuredSettings = array_keys($settings);
        $this->settings = array_merge(self::getDefaultSettings(), $settings);
    }

    /**
     * @param string $setting
     *
     * @return mixed
     */
    public function getSetting($setting)
    {
        if (!in_array($setting, self::getAvailableSettings())) {
            throw new Exception('Invalid setting: ' . $setting);
        }

        return $this->settings[$setting];
    }

    /**
     * Whether setting has been explicitly set in constructor
     *
     * @param string $setting
     *
     * @return bool
     */
    public function hasSetting($setting)
    {
        return in_array($setting, $this->configuredSettings);
    }

    /**
     * @return array
     */
    public static function getDefaultSettings()
    {
        return [
            self::HAS_CREFO_PAYMENT => true,
            self::PAYMENT_METHOD    => '',
            self::ORDER_COMMENT => null,
            self::ORDER_INCREMENT_ID => null,
            self::PAYMENT_CLASS => \Magento\Sales\Model\Order\Payment::class,
            self::MNS_EVENTS => [],
            self::MNS_APPLICABLE_CONSUMERS => [],
        ];
    }

    /**
     * @return array
     */
    public static function getAvailableSettings()
    {
        return [
            self::HAS_CREFO_PAYMENT,
            self::PAYMENT_METHOD,
            self::ORDER_COMMENT,
            self::ORDER_INCREMENT_ID,
            self::PAYMENT_CLASS,
            self::MNS_EVENTS,
            self::MNS_APPLICABLE_CONSUMERS,
        ];
    }
}
