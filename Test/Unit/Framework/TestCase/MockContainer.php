<?php

namespace Trilix\CrefoPay\Test\Unit\Framework\TestCase;

use PHPUnit_Framework_MockObject_MockObject as MockObject;

class MockContainer
{
    /** @var \Trilix\CrefoPay\Helper\Order|MockObject */
    private $orderHelper;
    /** @var \Magento\Sales\Model\Order|MockObject */
    private $order;
    /** @var \Magento\Sales\Api\Data\OrderPaymentInterface|MockObject */
    private $payment;
    /** @var \Trilix\CrefoPay\Model\Method\Adapter|MockObject */
    private $paymentMethod;
    /** @var \Magento\Sales\Model\OrderRepository|MockObject */
    private $orderRepository;
    /** @var \Trilix\CrefoPay\Logger\MnsLogger|MockObject */
    private $mnsLogger;
    /** @var \Magento\Framework\Api\SearchCriteriaBuilder|MockObject */
    private $searchCriteriaBuilder;
    /** @var \Magento\Framework\Api\SearchCriteria|MockObject */
    private $searchCriteria;
    /** @var \Trilix\CrefoPay\Model\Mns\MnsRepository|MockObject */
    private $mnsRepository;
    /** @var \Trilix\CrefoPay\Api\Data\CrefoPayMnsSearchResultInterface|MockObject */
    private $mnsSearchResult;
    /** @var \Trilix\CrefoPay\Model\Mns\MnsConsumerPool|MockObject */
    private $mnsConsumerPool;
    /** @var \Trilix\CrefoPay\Model\Mns\ProcessSupervisor\SupervisorInterface|MockObject */
    private $mnsSupervisor;
    /** @var \Trilix\CrefoPay\Client\UpgFactory|MockObject */
    private $upgFactory;
    /** @var \Upg\Library\Callback\MacCalculator|MockObject */
    private $macCalculator;

    /**
     * @return MockObject|\Trilix\CrefoPay\Helper\Order
     */
    public function getOrderHelper()
    {
        return $this->orderHelper;
    }

    /**
     * @param MockObject|\Trilix\CrefoPay\Helper\Order $orderHelper
     */
    public function setOrderHelper($orderHelper)
    {
        $this->orderHelper = $orderHelper;
    }

    /**
     * @return \Magento\Sales\Model\Order|MockObject
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param \Magento\Sales\Model\Order|MockObject $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return \Magento\Sales\Api\Data\OrderPaymentInterface|MockObject
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderPaymentInterface|MockObject $payment
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;
    }

    /**
     * @return MockObject|\Trilix\CrefoPay\Model\Method\Adapter
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @param MockObject|\Trilix\CrefoPay\Model\Method\Adapter $paymentMethod
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return \Magento\Sales\Model\OrderRepository|MockObject
     */
    public function getOrderRepository()
    {
        return $this->orderRepository;
    }

    /**
     * @param \Magento\Sales\Model\OrderRepository|MockObject $orderRepository
     */
    public function setOrderRepository($orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @return \Trilix\CrefoPay\Logger\MnsLogger|MockObject
     */
    public function getMnsLogger()
    {
        return $this->mnsLogger;
    }

    /**
     * @param \Trilix\CrefoPay\Logger\MnsLogger|MockObject $mnsLogger
     */
    public function setMnsLogger($mnsLogger)
    {
        $this->mnsLogger = $mnsLogger;
    }

    /**
     * @return \Magento\Framework\Api\SearchCriteriaBuilder|MockObject
     */
    public function getSearchCriteriaBuilder()
    {
        return $this->searchCriteriaBuilder;
    }

    /**
     * @param \Magento\Framework\Api\SearchCriteriaBuilder|MockObject $searchCriteriaBuilder
     */
    public function setSearchCriteriaBuilder($searchCriteriaBuilder)
    {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @return \Magento\Framework\Api\SearchCriteria|MockObject
     */
    public function getSearchCriteria()
    {
        return $this->searchCriteria;
    }

    /**
     * @param \Magento\Framework\Api\SearchCriteria|MockObject $searchCriteria
     */
    public function setSearchCriteria($searchCriteria)
    {
        $this->searchCriteria = $searchCriteria;
    }

    /**
     * @return MockObject|\Trilix\CrefoPay\Model\Mns\MnsRepository
     */
    public function getMnsRepository()
    {
        return $this->mnsRepository;
    }

    /**
     * @param MockObject|\Trilix\CrefoPay\Model\Mns\MnsRepository $mnsRepository
     */
    public function setMnsRepository($mnsRepository)
    {
        $this->mnsRepository = $mnsRepository;
    }

    /**
     * @return MockObject|\Trilix\CrefoPay\Api\Data\CrefoPayMnsSearchResultInterface
     */
    public function getMnsSearchResult()
    {
        return $this->mnsSearchResult;
    }

    /**
     * @param MockObject|\Trilix\CrefoPay\Api\Data\CrefoPayMnsSearchResultInterface $mnsSearchResult
     */
    public function setMnsSearchResult($mnsSearchResult)
    {
        $this->mnsSearchResult = $mnsSearchResult;
    }

    /**
     * @return MockObject|\Trilix\CrefoPay\Model\Mns\MnsConsumerPool
     */
    public function getMnsConsumerPool()
    {
        return $this->mnsConsumerPool;
    }

    /**
     * @param MockObject|\Trilix\CrefoPay\Model\Mns\MnsConsumerPool $mnsConsumerPool
     */
    public function setMnsConsumerPool($mnsConsumerPool)
    {
        $this->mnsConsumerPool = $mnsConsumerPool;
    }

    /**
     * @return MockObject|\Trilix\CrefoPay\Model\Mns\ProcessSupervisor\SupervisorInterface
     */
    public function getMnsSupervisor()
    {
        return $this->mnsSupervisor;
    }

    /**
     * @param MockObject|\Trilix\CrefoPay\Model\Mns\ProcessSupervisor\SupervisorInterface $mnsSupervisor
     */
    public function setMnsSupervisor($mnsSupervisor)
    {
        $this->mnsSupervisor = $mnsSupervisor;
    }

    /**
     * @return MockObject|\Trilix\CrefoPay\Client\UpgFactory
     */
    public function getUpgFactory()
    {
        return $this->upgFactory;
    }

    /**
     * @param MockObject|\Trilix\CrefoPay\Client\UpgFactory $upgFactory
     */
    public function setUpgFactory($upgFactory)
    {
        $this->upgFactory = $upgFactory;
    }

    /**
     * @return MockObject|\Upg\Library\Callback\MacCalculator
     */
    public function getMacCalculator()
    {
        return $this->macCalculator;
    }

    /**
     * @param MockObject|\Upg\Library\Callback\MacCalculator $macCalculator
     */
    public function setMacCalculator($macCalculator)
    {
        $this->macCalculator = $macCalculator;
    }
}
