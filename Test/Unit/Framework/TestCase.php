<?php

namespace Trilix\CrefoPay\Test\Unit\Framework
{
    class TestCase extends \PHPUnit\Framework\TestCase
    {
        /** @var TestCase\Config */
        private $config;
        /** @var TestCase\MockContainer */
        private $mockContainer;

        protected function configure(array $settings = [])
        {
            $this->config = new TestCase\Config($settings);
            $this->mockContainer = (new TestCase\MockContainerFactory())->create($this->config, $this);
        }

        /**
         * @return TestCase\Config
         */
        public function getConfig()
        {
            return $this->config;
        }

        /**
         * @return TestCase\MockContainer
         */
        public function getMockContainer()
        {
            return $this->mockContainer;
        }

        /**
         * @param string $originalClassName
         *
         * @return \PHPUnit_Framework_MockObject_MockObject
         */
        public function mock($originalClassName)
        {
            return $this->createMock($originalClassName);
        }

        /**
         * @param string $originalClass
         * @param array $methods
         *
         * @return \PHPUnit_Framework_MockObject_MockObject
         */
        public function partialMock($originalClass, array $methods)
        {
            return $this->createPartialMock($originalClass, $methods);
        }

        public function getObject($class, array $diConfig = [])
        {
            return (new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this))->getObject($class, $diConfig);
        }

        public function expects(\PHPUnit_Framework_MockObject_MockObject $object, $method, $willReturn = null, $invocation = 1)
        {
            $object
                ->expects($this->validateInvocation($invocation))
                ->method($method)
                ->willReturn($willReturn);
        }

        public function expectsWith(
            \PHPUnit_Framework_MockObject_MockObject $object,
            $method,
            array $with,
            $willReturn = null,
            $invocation = 'any'
        ) {
            $cfg = $object
                ->expects($this->validateInvocation($invocation))
                ->method($method);

            call_user_func_array([$cfg, 'with'], $with)->willReturn($willReturn);
        }

        /**
         * @param string|int|\PHPUnit_Framework_MockObject_Matcher_Invocation $invocation
         *
         * @return \PHPUnit_Framework_MockObject_Matcher_Invocation
         */
        private function validateInvocation($invocation)
        {
            if ($invocation === 'any') {
                $invocation = $this->any();
            }

            if (is_integer($invocation)) {
                $invocation = new \PHPUnit_Framework_MockObject_Matcher_InvokedCount($invocation);
            }

            if (!($invocation instanceof \PHPUnit_Framework_MockObject_Matcher_Invocation)) {
                throw new Exception('Invalid invocation limiter specification');
            }

            return $invocation;
        }
    }

    class Exception extends \PHPUnit\Framework\Exception
    {
    }
}

namespace {
    if (!function_exists('__')) {
        require_once dirname(dirname(dirname(dirname(dirname(dirname(dirname(__DIR__))))))) . '/vendor/magento/magento2-base/app/functions.php';
    }
}
