<?php

namespace Trilix\CrefoPay\Test\Unit\Gateway\Validator;

use Trilix\CrefoPay\Gateway\Validator\GeneralResponseValidator;
use Magento\Payment\Gateway\Validator\Result;
use Magento\Payment\Gateway\Validator\ResultInterfaceFactory;
use Upg\Library\Response\AbstractResponse;
use Upg\Library\Error\Codes;

class GeneralResponseValidatorTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var GeneralResponseValidator
     */
    protected $responseValidator;

    /**
     * @var Result|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $resultMock;

    /**
     * @var ResultInterfaceFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $resultFactoryMock;

    /**
     * @dataProvider validateResultCodeDataProvider
     */
    public function testValidateWhenNoExpectedErrorsCode($resultCode, $isValid, $errorCodes)
    {
        $this->resultFactoryMock = $this->getMockBuilder(ResultInterfaceFactory::class)
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->resultMock = $this->getMockBuilder(Result::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->responseValidator = new GeneralResponseValidator($this->resultFactoryMock);

        $response = $this->getMockBuilder(AbstractResponse::class)
            ->setMethods(['getData'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $response->expects($this->once())
            ->method('getData')
            ->with('resultCode')
            ->willReturn($resultCode);

        $this->resultFactoryMock->expects($this->once())
            ->method('create')
            ->with(['isValid' => $isValid, 'failsDescription' => [], 'errorCodes' => $errorCodes])
            ->willReturn($this->resultMock);

        $validationSubject = ['response' => $response];

        $this->assertSame($this->resultMock, $this->responseValidator->validate($validationSubject));
    }

    public function validateResultCodeDataProvider()
    {
        return [
            [0, true, [0]],
            [1, true, [1]],
            [Codes::ERROR_PAYMENT_METHOD_REJECTED, false, [Codes::ERROR_PAYMENT_METHOD_REJECTED]],
            [Codes::ERROR_PAYMENT_DECLINED_FRAUD, false, [Codes::ERROR_PAYMENT_DECLINED_FRAUD]],
        ];
    }
}
