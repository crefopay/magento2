<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Test\Unit\Gateway\Config;

use PHPUnit\Framework\MockObject\MockObject;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager as ObjectManagerHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Payment\Gateway\Config\Config as DefaultConfig;
use Magento\Framework\UrlInterface;
use Trilix\CrefoPay\Gateway\Config\Config;
use Trilix\CrefoPay\Model\Adminhtml\Source\Environment;

class ConfigTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var ScopeConfigInterface|MockObject
     */
    protected $scopeConfigMock;

    /**
     * @var WriterInterface|MockObject
     */
    protected $configWriterMock;

    /**
     * @var Config
     */
    protected $model;

    /**
     * @var UrlInterface|MockObject
     */
    protected $urlBuilderMock;

    protected function setUp(): void
    {
        $this->scopeConfigMock = $this->getMockBuilder(ScopeConfigInterface::class)
            ->getMock();

        $this->configWriterMock =  $this->getMockBuilder(WriterInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->urlBuilderMock = $this->getMockBuilder(UrlInterface::class)
            ->getMock();

        /** @var Config $uut */
        $this->model = (new ObjectManagerHelper($this))->getObject(
            Config::class,
            [
                'scopeConfig'  => $this->scopeConfigMock,
                'configWriter' => $this->configWriterMock,
                'urlBuilder'   => $this->urlBuilderMock,
                'methodCode'   => 'method_code',
                'pathPattern'  => DefaultConfig::DEFAULT_PATH_PATTERN
            ]
        );
    }

    /**
     * @dataProvider getSecureFieldsUrlDataProvider
     */
    public function testGetSecureFieldsUrl($environment, $expectedUrl)
    {
        $this->scopeConfigMock->expects($this->once())
            ->method('getValue')
            ->with('payment/method_code/' . Config::KEY_ENVIRONMENT, ScopeInterface::SCOPE_STORE, null)
            ->willReturn($environment);

        $baseUrl = $this->model->getSecureFieldsUrl();

        $this->assertSame($expectedUrl, $baseUrl);
    }

    public function getSecureFieldsUrlDataProvider()
    {
        return [
            [Environment::ENVIRONMENT_PRODUCTION, 'https://api.crefopay.de/secureFields/'],
            [Environment::ENVIRONMENT_SANDBOX, 'https://sandbox.crefopay.de/secureFields/'],
            ['uknown environment', 'https://sandbox.crefopay.de/secureFields/']
        ];
    }

    /**
     * @dataProvider getSmartSignupUrlDataProvider
     */
    public function testGetSmartSignupUrl($environment, $expectedUrl)
    {
        $this->scopeConfigMock->expects($this->once())
            ->method('getValue')
            ->with('payment/method_code/' . Config::KEY_ENVIRONMENT, ScopeInterface::SCOPE_STORE, null)
            ->willReturn($environment);

        $baseUrl = $this->model->getSmartSignupUrl();

        $this->assertSame($expectedUrl, $baseUrl);
    }

    public function getSmartSignupUrlDataProvider()
    {
        return [
            [Environment::ENVIRONMENT_PRODUCTION, 'https://api.crefopay.de/autocomplete/'],
            [Environment::ENVIRONMENT_SANDBOX, 'https://sandbox.crefopay.de/autocomplete/'],
            ['uknown environment', 'https://sandbox.crefopay.de/autocomplete/']
        ];
    }

    /**
     * @dataProvider getBaseUrlDataProvider
     */
    public function testGetBaseUrl($environment, $expectedUrl)
    {
        $this->scopeConfigMock->expects($this->once())
            ->method('getValue')
            ->with('payment/method_code/' . Config::KEY_ENVIRONMENT, ScopeInterface::SCOPE_STORE, null)
            ->willReturn($environment);

        if (!$expectedUrl) {
            $this->expectException('InvalidArgumentException');
        }

        $baseUrl = $this->model->getBaseUrl();

        if ($expectedUrl) {
            $this->assertEquals($expectedUrl, $baseUrl);
        }
    }

    public function getBaseUrlDataProvider()
    {
        return [
            [Environment::ENVIRONMENT_PRODUCTION, 'https://api.crefopay.de/2.0/'],
            [Environment::ENVIRONMENT_SANDBOX, 'https://sandbox.crefopay.de/2.0/'],
            ['uknown environment', null]
        ];
    }

    public function testSetAutoCaptureCategoryIds()
    {
        $this->configWriterMock->expects($this->once())
            ->method('save')
            ->with(
                sprintf(DefaultConfig::DEFAULT_PATH_PATTERN, 'method_code', 'auto_capture_categories'),
                '1,2,3',
                ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
                0
            );

        $this->model->setAutoCaptureCategoryIds([1, 2, 3], ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 0);
    }

    public function testGetAutoCaptureCategoryIdsWhenNoValue()
    {
        $this->scopeConfigMock->expects($this->once())
            ->method('getValue')
            ->with('payment/method_code/auto_capture_categories')
            ->willReturn(null);

        $actualCategoryIds = $this->model->getAutoCaptureCategoryIds();

        $this->assertEquals([], $actualCategoryIds);
    }

    public function testGetAutoCaptureCategoryIds()
    {
        $this->scopeConfigMock->expects($this->once())
            ->method('getValue')
            ->with('payment/method_code/auto_capture_categories')
            ->willReturn('1,2,3');

        $actualCategoryIds = $this->model->getAutoCaptureCategoryIds();

        $this->assertEquals([1 ,2 ,3], $actualCategoryIds);
    }

    public function testGetLogoUrlWhenItExist()
    {
        $this->scopeConfigMock->expects($this->once())
            ->method('getValue')
            ->with('payment/method_code/method_logo')
            ->willReturn('image');

        $this->urlBuilderMock->expects($this->once())
            ->method('getBaseUrl')
            ->with(['_type' => UrlInterface::URL_TYPE_MEDIA])
            ->willReturn('url');

        $actual = $this->model->getLogoUrl('method');

        $this->assertEquals('urlcrefopay/method/logo/image', $actual);
    }

    public function testGetLogoUrlWhenItDoesNotExist()
    {
        $this->scopeConfigMock->expects($this->once())
            ->method('getValue')
            ->with('payment/method_code/method_logo')
            ->willReturn(null);

        $this->urlBuilderMock->expects($this->never())
            ->method('getBaseUrl');

        $actual = $this->model->getLogoUrl('method');

        $this->assertEquals('', $actual);
    }
}
