<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Test\Unit\Gateway\Command;

use Magento\Payment\Gateway\Command\CommandException;
use PHPUnit\Framework\MockObject\MockObject;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Gateway\Validator\ValidatorInterface;
use Magento\Payment\Gateway\Validator\ResultInterface;
use Magento\Payment\Gateway\Response\HandlerInterface;
use Magento\Customer\Model\Session;
use Trilix\CrefoPay\Gateway\Command\Reserve;
use Trilix\CrefoPay\Client\Transport;
use Trilix\CrefoPay\Client\Request\ReserveRequestFactory;
use Trilix\CrefoPay\Model\TransactionService;
use Trilix\CrefoPay\Gateway\SubjectReader;
use Upg\Library\Request\Reserve as ReserveRequest;
use Upg\Library\Response\SuccessResponse;
use Upg\Library\Risk\RiskClass;
use Upg\Library\Api\Exception\ApiError;

class ReserveTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Reserve
     */
    protected $reserveCommand;

    /**
     * @var Session|MockObject
     */
    protected $customerSessionMock;

    /**
     * @var Transport|MockObject
     */
    protected $transportMock;

    /**
     * @var TransactionService|MockObject
     */
    protected $transactionServiceMock;

    /**
     * @var SubjectReader|MockObject
     */
    protected $subjectReaderMock;

    /**
     * @var ReserveRequestFactory|MockObject
     */
    protected $reserveRequestFactoryMock;

    /**
     * @var ReserveRequest|MockObject
     */
    protected $reserveRequestMock;

    /**
     * @var SuccessResponse|MockObject
     */
    protected $successResponseMock;

    /**
     * @var ApiError|MockObject
     */
    protected $apiErrorMock;

    /**
     * @var ValidatorInterface|MockObject
     */
    protected $validatorMock;

    /**
     * @var ResultInterface|MockObject
     */
    protected $validationResultMock;

    /**
     * @var PaymentDataObjectInterface|MockObject
     */
    protected $paymentDataObjectMock;

    /**
     * @var HandlerInterface|MockObject
     */
    protected $handlerMock;

    protected function setUp(): void
    {
        $this->transportMock = $this->getMockBuilder(Transport::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->customerSessionMock = $this->getMockBuilder(Session::class)
            ->disableOriginalConstructor()
            ->setMethods(['setUserRiskClass'])
            ->getMock();

        $this->reserveRequestFactoryMock = $this->getMockBuilder(ReserveRequestFactory::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->transactionServiceMock = $this->getMockBuilder(TransactionService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->subjectReaderMock = $this->getMockBuilder(SubjectReader::class)
            ->getMock();

        $this->reserveRequestMock = $this->getMockBuilder(ReserveRequest::class)
            ->getMock();

        $this->successResponseMock = $this->getMockBuilder(SuccessResponse::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->apiErrorMock = $this->getMockBuilder(ApiError::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->paymentDataObjectMock = $this->getMockBuilder(PaymentDataObjectInterface::class)
            ->getMock();

        $this->validatorMock = $this->getMockBuilder(ValidatorInterface::class)
            ->getMock();

        $this->validationResultMock = $this->getMockBuilder(ResultInterface::class)
            ->setMethods(['isValid', 'getFailsDescription', 'getErrorCodes'])
            ->getMock();

        $this->handlerMock = $this->getMockBuilder(HandlerInterface::class)
            ->getMock();
    }

    public function testExecuteWithoutValidationAndWithoutHandler()
    {
        $paymentMethod = 'PAYPAL';
        $reserveCommand = new Reserve(
            $this->customerSessionMock,
            $this->transportMock,
            $this->reserveRequestFactoryMock,
            $this->transactionServiceMock,
            $this->subjectReaderMock,
            $paymentMethod
        );

        $commandSubject = ['payment' => $this->paymentDataObjectMock];

        $this->subjectReaderMock->expects($this->once())
            ->method('readPayment')
            ->with($commandSubject)
            ->willReturn($this->paymentDataObjectMock);

        $this->reserveRequestFactoryMock->expects($this->once())
            ->method('create')
            ->with($paymentMethod, $this->paymentDataObjectMock)
            ->willReturn($this->reserveRequestMock);

        $this->transportMock->expects($this->once())
            ->method('sendRequest')
            ->with($this->reserveRequestMock)
            ->willReturn($this->successResponseMock);

        $this->transactionServiceMock->expects($this->once())
            ->method('addTransaction')
            ->with($this->paymentDataObjectMock, $this->reserveRequestMock, $this->successResponseMock);

        $this->handlerMock->expects($this->never())
            ->method('handle');

        $reserveCommand->execute($commandSubject);
    }

    public function testExecuteWithApiErrorAndFailedValidation()
    {
        $paymentMethod = 'BILL';
        $reserveCommand = new Reserve(
            $this->customerSessionMock,
            $this->transportMock,
            $this->reserveRequestFactoryMock,
            $this->transactionServiceMock,
            $this->subjectReaderMock,
            $paymentMethod,
            $this->handlerMock,
            $this->validatorMock
        );

        $commandSubject = ['payment' => $this->paymentDataObjectMock];

        $this->subjectReaderMock->expects($this->once())
            ->method('readPayment')
            ->with($commandSubject)
            ->willReturn($this->paymentDataObjectMock);

        $this->reserveRequestFactoryMock->expects($this->once())
            ->method('create')
            ->with($paymentMethod, $this->paymentDataObjectMock)
            ->willReturn($this->reserveRequestMock);

        $this->transportMock->expects($this->once())
            ->method('sendRequest')
            ->with($this->reserveRequestMock)
            ->will($this->throwException($this->apiErrorMock));

        $this->validatorMock->expects($this->once())
            ->method('validate')
            ->with(array_merge($commandSubject, ['response' => $this->apiErrorMock->getParsedResponse()]))
            ->willReturn($this->validationResultMock);

        $this->validationResultMock->expects($this->once())
            ->method('isValid')
            ->willReturn(false);

        $this->customerSessionMock->expects($this->once())
            ->method('setUserRiskClass')
            ->with(['value' => RiskClass::RISK_CLASS_HIGH]);

        $this->transactionServiceMock->expects($this->never())
            ->method('addTransaction');

        $this->expectException(CommandException::class);
        $this->expectExceptionMessage(Reserve::CREFOPAY_RESERVE_ERROR);

        $reserveCommand->execute($commandSubject);
    }

    public function testExecuteWithApiErrorAndSuccessValidation()
    {
        $paymentMethod = 'BILL';
        $reserveCommand = new Reserve(
            $this->customerSessionMock,
            $this->transportMock,
            $this->reserveRequestFactoryMock,
            $this->transactionServiceMock,
            $this->subjectReaderMock,
            $paymentMethod,
            $this->handlerMock,
            $this->validatorMock
        );

        $commandSubject = ['payment' => $this->paymentDataObjectMock];

        $this->subjectReaderMock->expects($this->once())
            ->method('readPayment')
            ->with($commandSubject)
            ->willReturn($this->paymentDataObjectMock);

        $this->reserveRequestFactoryMock->expects($this->once())
            ->method('create')
            ->with($paymentMethod, $this->paymentDataObjectMock)
            ->willReturn($this->reserveRequestMock);

        $this->transportMock->expects($this->once())
            ->method('sendRequest')
            ->with($this->reserveRequestMock)
            ->will($this->throwException($this->apiErrorMock));

        $this->validatorMock->expects($this->once())
            ->method('validate')
            ->with(array_merge($commandSubject, ['response' => $this->apiErrorMock->getParsedResponse()]))
            ->willReturn($this->validationResultMock);

        $this->validationResultMock->expects($this->once())
            ->method('isValid')
            ->willReturn(true);

        $this->customerSessionMock->expects($this->never())
            ->method('setUserRiskClass');

        $this->transactionServiceMock->expects($this->never())
            ->method('addTransaction');

        $this->expectException(CommandException::class);
        $this->expectExceptionMessage('Transaction has been declined. Please try again later.');

        $reserveCommand->execute($commandSubject);
    }

    public function testExecuteWithException()
    {
        $paymentMethod = 'BILL';
        $reserveCommand = new Reserve(
            $this->customerSessionMock,
            $this->transportMock,
            $this->reserveRequestFactoryMock,
            $this->transactionServiceMock,
            $this->subjectReaderMock,
            $paymentMethod,
            $this->handlerMock,
            $this->validatorMock
        );

        $commandSubject = ['payment' => $this->paymentDataObjectMock];

        $this->subjectReaderMock->expects($this->once())
            ->method('readPayment')
            ->with($commandSubject)
            ->willReturn($this->paymentDataObjectMock);

        $this->reserveRequestFactoryMock->expects($this->once())
            ->method('create')
            ->with($paymentMethod, $this->paymentDataObjectMock)
            ->willReturn($this->reserveRequestMock);

        $this->transportMock->expects($this->once())
            ->method('sendRequest')
            ->with($this->reserveRequestMock)
            ->willThrowException(new \Exception());

        $this->validatorMock->expects($this->never())
            ->method('validate');

        $this->transactionServiceMock->expects($this->never())
            ->method('addTransaction');

        $this->expectException(\Exception::class);

        $reserveCommand->execute($commandSubject);
    }

    public function testExecuteWithHandler()
    {
        $paymentMethod = 'BILL';
        $reserveCommand = new Reserve(
            $this->customerSessionMock,
            $this->transportMock,
            $this->reserveRequestFactoryMock,
            $this->transactionServiceMock,
            $this->subjectReaderMock,
            $paymentMethod,
            $this->handlerMock,
            $this->validatorMock
        );

        $commandSubject = ['payment' => $this->paymentDataObjectMock];

        $this->subjectReaderMock->expects($this->once())
            ->method('readPayment')
            ->with($commandSubject)
            ->willReturn($this->paymentDataObjectMock);

        $this->reserveRequestFactoryMock->expects($this->once())
            ->method('create')
            ->with($paymentMethod, $this->paymentDataObjectMock)
            ->willReturn($this->reserveRequestMock);

        $this->transportMock->expects($this->once())
            ->method('sendRequest')
            ->with($this->reserveRequestMock)
            ->willReturn($this->successResponseMock);

        $this->validatorMock->expects($this->never())
            ->method('validate');

        $this->transactionServiceMock->expects($this->once())
            ->method('addTransaction')
            ->with($this->paymentDataObjectMock, $this->reserveRequestMock, $this->successResponseMock);

        $this->handlerMock->expects($this->once())
            ->method('handle')
            ->with($commandSubject, []);

        $reserveCommand->execute($commandSubject);
    }
}
