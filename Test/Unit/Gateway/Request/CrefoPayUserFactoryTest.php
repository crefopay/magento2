<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Test\Unit\Gateway\Request;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\MockObject\MockObject;
use Magento\Quote\Model\Quote\Address;
use Trilix\CrefoPay\Gateway\Request\User\CrefoPayUserFactory;
use Trilix\CrefoPay\Gateway\Config\Config;
use Trilix\CrefoPay\Gateway\Request\User\CrefoPayUser;
use Upg\Library\User\Type as UserType;

class CrefoPayUserFactoryTest extends TestCase
{
    /** @var Config|MockObject */
    protected $config;

    /** @var CrefoPayUserFactory */
    protected $crefoPayUserFactory;

    /**
     * @throws \ReflectionException
     */
    public function setUp(): void
    {
        $this->config = $this->createMock(Config::class);

        $this->crefoPayUserFactory = new CrefoPayUserFactory($this->config);
    }

    /**
     * @throws \ReflectionException
     */
    public function testCreateWhenB2BIsEnabledAndCompanyNotExists()
    {
        $this->config->expects(self::once())
            ->method('isB2BEnabled')
            ->willReturn(true);

        $address = $this->createMock(Address::class);
        $address->expects(self::once())
            ->method('getCompany')
            ->willReturn('');

        $email = 'test@gmail.com';

        $actual = $this->crefoPayUserFactory->create($address, $email);

        $this->assertInstanceOf(CrefoPayUser::class, $actual);
        $this->assertSame(UserType::USER_TYPE_PRIVATE, $actual->getType());
        $this->assertSame(md5($email).'B2C', $actual->getId());
    }

    /**
     * @throws \ReflectionException
     * @dataProvider possibleAutoFilledCompanyValuesDataProvider
     */
    public function testCreateWhenB2BIsEnabledAndCompanyIsFromPossibleAutoFilledValues($companyName)
    {
        $this->config->expects(self::once())
            ->method('isB2BEnabled')
            ->willReturn(true);

        $address = $this->createMock(Address::class);
        $address->expects(self::exactly(2))
            ->method('getCompany')
            ->willReturn($companyName);

        $email = 'test@gmail.com';

        $actual = $this->crefoPayUserFactory->create($address, $email);

        $this->assertInstanceOf(CrefoPayUser::class, $actual);
        $this->assertSame(UserType::USER_TYPE_PRIVATE, $actual->getType());
        $this->assertSame(md5($email).'B2C', $actual->getId());
    }

    /**
     * @throws \ReflectionException
     */
    public function testCreateWhenB2BIsEnabledAndCompanyExists()
    {
        $this->config->expects(self::once())
            ->method('isB2BEnabled')
            ->willReturn(true);

        $address = $this->createMock(Address::class);
        $address->expects(self::exactly(2))
            ->method('getCompany')
            ->willReturn('companyAddress');

        $email = 'test@gmail.com';

        $actual = $this->crefoPayUserFactory->create($address, $email);

        $this->assertInstanceOf(CrefoPayUser::class, $actual);
        $this->assertSame(UserType::USER_TYPE_BUSINESS, $actual->getType());
        $this->assertSame(md5($email).'B2B', $actual->getId());
    }

    /**
     * @throws \ReflectionException
     */
    public function testCreateWhenB2BIsDisabled()
    {
        $this->config->expects(self::once())
            ->method('isB2BEnabled')
            ->willReturn(false);

        $address = $this->createMock(Address::class);
        $address->expects(self::never())
            ->method('getCompany');

        $email = 'test@gmail.com';

        $actual = $this->crefoPayUserFactory->create($address, $email);

        $this->assertInstanceOf(CrefoPayUser::class, $actual);
        $this->assertSame(UserType::USER_TYPE_PRIVATE, $actual->getType());
        $this->assertSame(md5($email).'B2C', $actual->getId());
    }

    /**
     * @return array
     */
    public function possibleAutoFilledCompanyValuesDataProvider(): array
    {
        return [
            ['Mr'],
            ['Mr.'],
            ['Ms'],
            ['Ms.'],
            ['Herr'],
            ['Herr.'],
            ['Frau'],
            ['Frau.']
        ];
    }
}
