<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Test\Unit\Gateway\Request;

use PHPUnit\Framework\MockObject\MockObject;
use Magento\Quote\Model\Quote\Address;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Trilix\CrefoPay\Gateway\Request\Address\DataFormatter;
use Upg\Library\Request\Objects\Person;
use Trilix\CrefoPay\Gateway\Request\PersonBuilder;

class PersonBuilderTest extends \PHPUnit\Framework\TestCase
{
    /** @var CustomerRepositoryInterface|MockObject */
    protected $customerRepositoryMock;

    /** @var Address|MockObject */
    protected $addressMock;

    /** @var CustomerInterface|MockObject */
    protected $customerMock;

    /** @var DataFormatter|MockObject */
    protected $dataFormatterMock;

    /** @var PersonBuilder */
    protected $personBuilder;

    protected function setUp(): void
    {
        $this->customerRepositoryMock = $this->getMockBuilder(CustomerRepositoryInterface::class)
            ->getMock();

        $this->addressMock = $this->getMockBuilder(Address::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->customerMock = $this->getMockBuilder(CustomerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->dataFormatterMock = $this->getMockBuilder(DataFormatter::class)
            ->getMock();

        $this->personBuilder = new PersonBuilder($this->customerRepositoryMock, $this->dataFormatterMock);
    }

    public function testBuildWhenThereIsNoCustomerWithProvidedEmail()
    {
        $email = 'test@mail.com';

        $this->customerRepositoryMock->expects($this->once())
            ->method('get')
            ->with($this->equalTo($email))
            ->willReturn(null);

        $this->addressMock->expects($this->once())
            ->method('getFirstName')
            ->willReturn('firstname');

        $this->addressMock->expects($this->once())
            ->method('getLastName')
            ->willReturn('lastname');

        $this->dataFormatterMock->expects($this->once())
            ->method('formatFaxNumber')
            ->willReturn('123');

        $this->dataFormatterMock->expects($this->once())
            ->method('formatPhoneNumber')
            ->willReturn('123456789');

        $actualObject = $this->personBuilder->build($this->addressMock, $email);
        $this->assertEquals('firstname', $actualObject->getName());
        $this->assertEquals('lastname', $actualObject->getSurname());
        $this->assertNull($actualObject->getSalutation());
        $this->assertEquals('123', $actualObject->getFaxNumber());
        $this->assertEquals('123456789', $actualObject->getPhoneNumber());
        $this->assertNull($actualObject->getDateOfBirth());
    }

    /**
     * @dataProvider salutationDataProvider
     */
    public function testBuild($gender, $expected)
    {
        $email = 'test@mail.com';

        $this->customerRepositoryMock->expects($this->once())
            ->method('get')
            ->with($this->equalTo($email))
            ->willReturn($this->customerMock);

        $this->customerMock->expects($this->once())
            ->method('getGender')
            ->willReturn($gender);

        $this->customerMock->expects($this->atLeastOnce())
            ->method('getDob')
            ->willReturn('11.01.2001');

        $this->addressMock->expects($this->once())
            ->method('getFirstName')
            ->willReturn('firstname');

        $this->addressMock->expects($this->once())
            ->method('getLastName')
            ->willReturn('lastname');

        $this->dataFormatterMock->expects($this->once())
            ->method('formatFaxNumber')
            ->willReturn('123');

        $this->dataFormatterMock->expects($this->once())
            ->method('formatPhoneNumber')
            ->willReturn('123456789');

        $actualObject = $this->personBuilder->build($this->addressMock, $email);

        $this->assertEquals('firstname', $actualObject->getName());
        $this->assertEquals('lastname', $actualObject->getSurname());
        $this->assertEquals($expected, $actualObject->getSalutation());
        $this->assertEquals('123', $actualObject->getFaxNumber());
        $this->assertEquals('123456789', $actualObject->getPhoneNumber());
        $this->assertEquals(new \DateTime('11.01.2001'), $actualObject->getDateOfBirth());
    }

    public function salutationDataProvider()
    {
        return [
            [1,  Person::SALUTATIONMALE],
            [2,  Person::SALUTATIONFEMALE],
            [3,  Person::SALUTATIONVARIOUS],
            ['other',  null],
        ];
    }
}
