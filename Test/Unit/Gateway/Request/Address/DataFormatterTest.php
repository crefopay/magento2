<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Test\Unit\Gateway\Request\Address;

use Magento\Quote\Model\Quote\Address;
use PHPUnit\Framework\MockObject\MockObject;
use Trilix\CrefoPay\Gateway\Request\Address\DataFormatter;

class DataFormatterTest extends \PHPUnit\Framework\TestCase
{
    /** @var Address|MockObject */
    protected $addressMock;

    /** @var DataFormatter */
    protected $dataFormatter;

    protected function setUp(): void
    {
        $this->addressMock = $this->getMockBuilder(Address::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->dataFormatter = new DataFormatter();
    }

    /**
     * @dataProvider dataProvider
     * @param $actualPhoneNumber
     * @param $expectedPhoneNumber
     */
    public function testFormatPhoneNumber($actualPhoneNumber, $expectedPhoneNumber)
    {
        $this->addressMock->expects($this->once())
            ->method('getTelephone')
            ->willReturn($actualPhoneNumber);

        $actualPhoneNumber = $this->dataFormatter->formatPhoneNumber($this->addressMock);

        $this->assertSame($expectedPhoneNumber, $actualPhoneNumber);
    }

    /**
     * @dataProvider dataProvider
     * @param $actualPhoneNumber
     * @param $expectedPhoneNumber
     */
    public function testFormatFaxNumber($actualPhoneNumber, $expectedPhoneNumber)
    {
        $this->addressMock->expects($this->once())
            ->method('getFax')
            ->willReturn($actualPhoneNumber);

        $actualPhoneNumber = $this->dataFormatter->formatFaxNumber($this->addressMock);

        $this->assertSame($expectedPhoneNumber, $actualPhoneNumber);
    }

    public function dataProvider()
    {
        return [
          ['123456789', '0123456789'],
          ['test-123456789-test1', '01234567891'],
          ['0test-123456789-test1', '01234567891'],
        ];
    }
}
