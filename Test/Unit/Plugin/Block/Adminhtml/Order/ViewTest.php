<?php

namespace Trilix\CrefoPay\Test\Unit\Plugin\Block\Adminhtml\Order;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager as ObjectManagerHelper;
use Magento\Sales\Block\Adminhtml\Order\View as ParentView;
use Magento\Sales\Model\Order;
use Trilix\CrefoPay\Plugin\Block\Adminhtml\Order\View;

class ViewTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @param $parentView
     * @dataProvider beforeSetLayoutDataProvider
     */
    public function testBeforeSetLayout($parentView)
    {
        /** @var View $uut */
        $uut = (new ObjectManagerHelper($this))->getObject(View::class);
        $uut->beforeSetLayout($parentView);
    }

    public function beforeSetLayoutDataProvider()
    {
        return [
            [$this->getParentViewMock(Order::STATE_CANCELED, '', false)],
            [$this->getParentViewMock(Order::STATE_CLOSED, '', false)],
            [$this->getParentViewMock(Order::STATE_PROCESSING, 'not_a_crefopay_method', false)],
            [$this->getParentViewMock(Order::STATE_PROCESSING, \Trilix\CrefoPay\Model\Ui\Bill\ConfigProvider::CODE, true)],
        ];
    }

    private function getParentViewMock($orderState, $paymentMethod, $isAddButtonExpected)
    {
        $paymentMock = $this->createMock(\Magento\Sales\Api\Data\OrderPaymentInterface::class);

        $paymentMock
            ->method('getMethod')
            ->willReturn($paymentMethod);

        $orderMock = $this->createMock(Order::class);

        $orderMock
            ->expects($this->any())
            ->method('getState')
            ->willReturn($orderState);

        $orderMock
            ->method('getPayment')
            ->willReturn($paymentMock);

        $parentViewMock = $this->createMock(ParentView::class);

        $parentViewMock
            ->expects($this->any())
            ->method('getOrder')
            ->willReturn($orderMock);

        $parentViewMock
            ->expects($isAddButtonExpected ? $this->once() : $this->never())
            ->method('addButton');

        return $parentViewMock;
    }
}
