<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Test\Unit\Plugin\Quote;

use Magento\Quote\Model\PaymentMethodManagement;
use PHPUnit\Framework\MockObject\MockObject;
use Trilix\CrefoPay\Model\CrefoPayTransaction;
use Upg\Library\Request\CreateTransaction as CreateTransactionRequest;
use Upg\Library\Response\SuccessResponse;
use Trilix\CrefoPay\Client\Request\CreateTransactionRequestFactory;
use Trilix\CrefoPay\Client\Transport;
use Trilix\CrefoPay\Model\CrefoPayTransactionFactory;
use Trilix\CrefoPay\Model\CrefoPayTransactionRepository;
use Trilix\CrefoPay\Plugin\Quote\PaymentMethodManagementPlugin;
use Trilix\CrefoPay\Gateway\Config\Config;

class PaymentMethodManagementPluginTest extends \PHPUnit\Framework\TestCase
{
    /** @var CreateTransactionRequestFactory|MockObject */
    protected $createTransactionRequestFactoryMock;

    /** @var Transport|MockObject */
    protected $transportMock;

    /** @var CrefoPayTransactionFactory|MockObject */
    protected $crefoPayTransactionFactoryMock;

    /** @var CrefoPayTransactionRepository|MockObject */
    protected $crefoPayTransactionRepositoryMock;

    /** @var Config|MockObject */
    protected $configMock;

    /** @var PaymentMethodManagement|MockObject */
    protected $paymentMethodManagementMock;

    /** @var CreateTransactionRequest|MockObject */
    protected $createTransactionRequestMock;

    /** @var SuccessResponse|MockObject */
    protected $successResponseMock;

    /** @var CrefoPayTransaction|MockObject */
    protected $crefoPayTransactionMock;

    /** @var PaymentMethodManagementPlugin */
    protected $model;

    public function setUp(): void
    {
        $this->createTransactionRequestFactoryMock =
            $this->createMock(CreateTransactionRequestFactory::class);

        $this->transportMock = $this->createMock(Transport::class);
        $this->crefoPayTransactionFactoryMock = $this->createMock(CrefoPayTransactionFactory::class);
        $this->crefoPayTransactionRepositoryMock =
            $this->createMock(CrefoPayTransactionRepository::class);
        $this->configMock = $this->createMock(Config::class);

        $this->paymentMethodManagementMock = $this->createMock(PaymentMethodManagement::class);

        $this->createTransactionRequestMock = $this->getMockBuilder(CreateTransactionRequest::class)
            ->getMock();

        $this->successResponseMock = $this->createMock(SuccessResponse::class);

        $this->crefoPayTransactionMock = $this->createMock(CrefoPayTransaction::class);

        $this->model = new PaymentMethodManagementPlugin(
            $this->createTransactionRequestFactoryMock,
            $this->transportMock,
            $this->crefoPayTransactionFactoryMock,
            $this->crefoPayTransactionRepositoryMock,
            $this->configMock
        );
    }

    public function testBeforeGetListWhenModuleIsDisabled()
    {
        $this->configMock->expects($this->once())
            ->method('isActive')
            ->willReturn(false);

        $this->createTransactionRequestFactoryMock->expects($this->never())
            ->method('create');

        $this->model->beforeGetList($this->paymentMethodManagementMock, 5);
    }

    public function testBeforeGetList()
    {
        $cartId = 3;
        $orderId = 5;
        $userId = 10;

        $paymentMethods = [
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_PREPAID,
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_SU,
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_BILL,
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_ZINIA_BNPL,
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_ZINIA_INSTALLMENT,
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_IDEAL,            
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_GOOGLE_PAY,
        ];

        $this->configMock->expects($this->once())
            ->method('isActive')
            ->willReturn(true);

        $this->createTransactionRequestFactoryMock->expects($this->once())
            ->method('create')
            ->with($cartId)
            ->willReturn($this->createTransactionRequestMock);

        $this->createTransactionRequestMock->expects($this->once())
            ->method('getOrderID')
            ->willReturn($orderId);

        $this->createTransactionRequestMock->expects($this->once())
            ->method('getUserId')
            ->willReturn($userId);

        $this->crefoPayTransactionFactoryMock->expects($this->once())
            ->method('create')
            ->with([])
            ->willReturn($this->crefoPayTransactionMock);

        $this->crefoPayTransactionMock->expects($this->once())
            ->method('setQuoteId')
            ->with($cartId);

        $this->crefoPayTransactionMock->expects($this->once())
            ->method('setCrefoPayOrderId')
            ->with($orderId);

        $this->crefoPayTransactionMock->expects($this->once())
            ->method('setCrefoPayUserId')
            ->with($userId);

        $this->transportMock->expects($this->once())
            ->method('sendRequest')
            ->with($this->createTransactionRequestMock)
            ->willReturn($this->successResponseMock);

        $this->successResponseMock->expects($this->once())
            ->method('getData')
            ->with('allowedPaymentMethods')
            ->willReturn($paymentMethods);

        $this->crefoPayTransactionMock->expects($this->once())
            ->method('setPaymentMethods')
            ->with(json_encode($paymentMethods));

        $this->crefoPayTransactionRepositoryMock->expects($this->once())
            ->method('save')
            ->with($this->crefoPayTransactionMock);

        $this->model->beforeGetList($this->paymentMethodManagementMock, $cartId);
    }

    public function testBeforeGetListWithExceptionDuringSendRequest()
    {
        $cartId = 3;
        $orderId = 5;
        $userId = 10;

        $paymentMethods = [
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_PREPAID,
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_SU,
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_BILL,
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_ZINIA_BNPL,
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_ZINIA_INSTALLMENT,
            \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_IDEAL
        ];

        $this->configMock->expects($this->once())
            ->method('isActive')
            ->willReturn(true);

        $this->createTransactionRequestFactoryMock->expects($this->once())
            ->method('create')
            ->with($cartId)
            ->willReturn($this->createTransactionRequestMock);

        $this->createTransactionRequestMock->expects($this->once())
            ->method('getOrderID')
            ->willReturn($orderId);

        $this->createTransactionRequestMock->expects($this->once())
            ->method('getUserId')
            ->willReturn($userId);

        $this->crefoPayTransactionFactoryMock->expects($this->once())
            ->method('create')
            ->with([])
            ->willReturn($this->crefoPayTransactionMock);

        $this->crefoPayTransactionMock->expects($this->once())
            ->method('setQuoteId')
            ->with($cartId);

        $this->crefoPayTransactionMock->expects($this->once())
            ->method('setCrefoPayOrderId')
            ->with($orderId);

        $this->crefoPayTransactionMock->expects($this->once())
            ->method('setCrefoPayUserId')
            ->with($userId);

        $this->transportMock->expects($this->once())
            ->method('sendRequest')
            ->with($this->createTransactionRequestMock)
            ->will($this->throwException(new \Exception()));

        $this->successResponseMock->expects($this->never())
            ->method('getData');

        $this->crefoPayTransactionMock->expects($this->once())
            ->method('setPaymentMethods')
            ->with('[]');

        $this->crefoPayTransactionRepositoryMock->expects($this->once())
            ->method('save')
            ->with($this->crefoPayTransactionMock);

        $this->model->beforeGetList($this->paymentMethodManagementMock, $cartId);
    }
}
