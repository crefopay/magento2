<?php

namespace Trilix\CrefoPay\Test\Unit\Controller\Mns;

use Trilix\CrefoPay\Controller\Mns\Sink;
use Magento\Framework\Controller\ResultFactory;

class SinkTest extends \PHPUnit\Framework\TestCase
{
    public function testExecute()
    {
        /** @var \PHPUnit_Framework_MockObject_MockObject $resultMock */
        $resultMock = $this->createPartialMock(\Magento\Framework\Controller\Result\Raw::class, ['setContents']);
        $resultMock->expects($this->once())->method('setContents')->with('');
        /** @var \PHPUnit_Framework_MockObject_MockObject $resultFactoryMock */
        $resultFactoryMock = $this->createPartialMock(ResultFactory::class, ['create']);
        $resultFactoryMock->expects($this->once())->method('create')
            ->with(ResultFactory::TYPE_RAW)
            ->willReturn($resultMock);
        /** @var \PHPUnit_Framework_MockObject_MockObject $contextMock */
        $contextMock = $this->createPartialMock(\Magento\Framework\App\Action\Context::class, ['getResultFactory']);
        $contextMock->expects($this->once())->method('getResultFactory')->willReturn($resultFactoryMock);
        $mnsServiceMock = $this->createPartialMock(\Trilix\CrefoPay\Model\Mns\MnsService::class, ['acknowledge']);
        $mnsServiceMock->expects($this->once())->method('acknowledge');
        $sink = new Sink($contextMock, $mnsServiceMock);
        $result = $sink->execute();
        $this->assertInstanceOf(\Magento\Framework\Controller\ResultInterface::class, $result);
    }
}
