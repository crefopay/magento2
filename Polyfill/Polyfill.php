<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Polyfill;

class Polyfill
{
    /**
     * @return void
     */
    public static function polyfill()
    {
        if (!interface_exists('Magento\Framework\App\CsrfAwareActionInterface')) {
            /**
             * This interface available since Magento 2.3.0, so we mimic the interface for v2.2, to keep the extension code
             * the same for different Magento versions.
             */
            require_once(__DIR__ . '/InvalidRequestException.php');
            require_once(__DIR__ . '/CsrfAwareActionInterface.php');
        }
    }
}
