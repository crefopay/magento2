<?php

declare(strict_types=1);

/**
 * It is required to implement this interface and implement its methods for an action, which expects a request
 * without formkey. But it is not available before 2.3.0, so we mimic the interface for Magento 2.2.
 */

namespace Magento\Framework\App;

use Magento\Framework\App\Request\InvalidRequestException;

interface CsrfAwareActionInterface extends ActionInterface
{
    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException;
    public function validateForCsrf(RequestInterface $request): ?bool;
}
