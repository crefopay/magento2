<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Ui\Component\CategoryMultiselect;

use Magento\Config\Model\ResourceModel\Config\Data\Collection as ConfigCollection;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Trilix\CrefoPay\Gateway\Config\Config as GatewayConfig;

class AutoCaptureDataProvider extends AbstractDataProvider
{
    /**
     * @var GatewayConfig
     */
    private $gatewayConfig;

    /**
     * AutoCaptureDataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param GatewayConfig $gatewayConfig
     * @param ConfigCollection $configCollection
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        string $name,
        string $primaryFieldName,
        string $requestFieldName,
        GatewayConfig $gatewayConfig,
        ConfigCollection $configCollection,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->gatewayConfig = $gatewayConfig;
        $this->collection = $configCollection;
    }

    public function getData()
    {
        $categoryIds = $this->gatewayConfig->getAutoCaptureCategoryIds();

        return [null => $categoryIds];
    }
}
