<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Plugin\Checkout\Model;

use Trilix\CrefoPay\Gateway\Config\Config;

class LayoutProcessor
{
    /** @var Config */
    private $config;

    /**
     * PaymentMethodManagementPlugin constructor.
     * @param Config $config
     */
    public function __construct(
        Config $config
    ) {
        $this->config = $config;
    }

    /**
     * Checkout LayoutProcessor after process plugin.
     *
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $processor
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(\Magento\Checkout\Block\Checkout\LayoutProcessor $processor, $jsLayout)
    {
        if ($this->config->isActive() && $this->config->isSmartSignupEnabled()) {
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
            ['children']['shippingAddress']['children']['shipping-address-fieldset']
            ['children']['company']['component'] = 'Trilix_CrefoPay/js/view/abstract';

            $paymentMethods = &$jsLayout['components']['checkout']['children']['steps']['children']
            ['billing-step']['children']['payment']['children']['payments-list']['children'];

            $paymentMethods['crefopay_sofort-form']['children']
            ['form-fields']['children']['company']['component'] = 'Trilix_CrefoPay/js/view/abstract';

            $paymentMethods['crefopay_zinia_invoice-form']['children']
            ['form-fields']['children']['company']['component'] = 'Trilix_CrefoPay/js/view/abstract';

            $paymentMethods['crefopay_zinia_ratepay-form']['children']
            ['form-fields']['children']['company']['component'] = 'Trilix_CrefoPay/js/view/abstract';

            $paymentMethods['crefopay_ideal-form']['children']
            ['form-fields']['children']['company']['component'] = 'Trilix_CrefoPay/js/view/abstract';

            $paymentMethods['crefopay_direct_debit-form']['children']
            ['form-fields']['children']['company']['component'] = 'Trilix_CrefoPay/js/view/abstract';

            $paymentMethods['crefopay_credit_card-form']['children']
            ['form-fields']['children']['company']['component'] = 'Trilix_CrefoPay/js/view/abstract';

            $paymentMethods['crefopay_cash_on_delivery-form']['children']
            ['form-fields']['children']['company']['component'] = 'Trilix_CrefoPay/js/view/abstract';

            $paymentMethods['crefopay_credit_card-form']['children']
            ['form-fields']['children']['company']['component'] = 'Trilix_CrefoPay/js/view/abstract';

            $paymentMethods['crefopay_credit_card_tds-form']['children']
            ['form-fields']['children']['company']['component'] = 'Trilix_CrefoPay/js/view/abstract';

            $paymentMethods['crefopay_cash_in_advance-form']['children']
            ['form-fields']['children']['company']['component'] = 'Trilix_CrefoPay/js/view/abstract';

            $paymentMethods['crefopay_bill-form']['children']
            ['form-fields']['children']['company']['component'] = 'Trilix_CrefoPay/js/view/abstract';
        }

        return $jsLayout;
    }
}
