<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Plugin\Payment\Checks;

use Magento\Payment\Model\Checks\SpecificationFactory;

class SpecificationFactoryPlugin
{
    public const RESTRICTED_PAYMENT_METHODS = "restricted_payment_methods";

    /**
     * @param SpecificationFactory $subject
     * @param array $result
     * @return array
     */
    public function beforeCreate(SpecificationFactory $subject, $result)
    {
        if (is_array($result)) {
            $result[] = self::RESTRICTED_PAYMENT_METHODS;
            return [$result];
        }

        return [$result];
    }
}
