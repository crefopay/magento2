<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Plugin\Quote;

use Trilix\CrefoPay\Model\AmazonPay\AmazonPayCustomerSession;
use Trilix\CrefoPay\Client\Request\CreateTransactionRequestFactory;
use Trilix\CrefoPay\Client\Transport;
use Trilix\CrefoPay\Model\CrefoPayTransactionFactory;
use Trilix\CrefoPay\Model\CrefoPayTransactionRepository;
use Trilix\CrefoPay\Gateway\Config\Config;

class PaymentMethodManagementPlugin
{
    /** @var CreateTransactionRequestFactory */
    private $createTransactionRequestFactory;

    /** @var AmazonPayCustomerSession */
    private $amazonPayCustomerSession;

    /** @var Transport  */
    private $transport;

    /** @var CrefoPayTransactionFactory */
    private $crefoPayTransactionFactory;

    /** @var CrefoPayTransactionRepository */
    private $crefoPayTransactionRepository;

    /** @var Config */
    private $config;

    /**
     * PaymentMethodManagementPlugin constructor.
     * @param CreateTransactionRequestFactory $createTransactionRequestFactory
     * @param AmazonPayCustomerSession $amazonPayCustomerSession
     * @param Transport $transport
     * @param CrefoPayTransactionFactory $crefoPayTransactionFactory
     * @param CrefoPayTransactionRepository $crefoPayTransactionRepository
     * @param Config $config
     */
    public function __construct(
        CreateTransactionRequestFactory $createTransactionRequestFactory,
        AmazonPayCustomerSession $amazonPayCustomerSession,
        Transport $transport,
        CrefoPayTransactionFactory $crefoPayTransactionFactory,
        CrefoPayTransactionRepository $crefoPayTransactionRepository,
        Config $config
    ) {
        $this->createTransactionRequestFactory        = $createTransactionRequestFactory;
        $this->amazonPayCustomerSession               = $amazonPayCustomerSession;
        $this->transport                              = $transport;
        $this->crefoPayTransactionFactory             = $crefoPayTransactionFactory;
        $this->crefoPayTransactionRepository          = $crefoPayTransactionRepository;
        $this->config                                 = $config;
    }

    public function beforeGetList(\Magento\Quote\Model\PaymentMethodManagement $subject, int $cartId)
    {
        $amazonPayCheckoutFlow = $this->amazonPayCustomerSession->isAmazonPayCheckoutFlow();

        // check if module is enabled
        if (!$this->config->isActive() || $amazonPayCheckoutFlow) {
            return;
        }
        $createTransactionRequest = $this->createTransactionRequestFactory->createForFrontedFlow($cartId);

        $crefoPayOrderId = $createTransactionRequest->getOrderID();
        $crefoPayUserId  = $createTransactionRequest->getUserId();

        $paymentMethods = $this->crefoPayTransactionFactory->create([]);
        $paymentMethods->setQuoteId($cartId);
        $paymentMethods->setCrefoPayOrderId((string) $crefoPayOrderId);
        $paymentMethods->setCrefoPayUserId((string) $crefoPayUserId);

        try {
            $response = $this->transport->sendRequest($createTransactionRequest);
            $allowedPaymentMethods = json_encode($response->getData('allowedPaymentMethods'));
        } catch (\Exception $e) {
            $allowedPaymentMethods = '[]';
        }

        $paymentMethods->setPaymentMethods($allowedPaymentMethods);
        $this->crefoPayTransactionRepository->save($paymentMethods);
    }
}
