<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Plugin\Block\Adminhtml\Order;

use Magento\Sales\Block\Adminhtml\Order\View as ParentView;
use Magento\Sales\Model\Order;
use Magento\Backend\Model\UrlInterface;
use Trilix\CrefoPay\Model\PaymentMethodsCodesMap;

class View
{
    /**
     * @var UrlInterface
     */
    private $backendUrl;

    /**
     * View constructor.
     *
     * @param UrlInterface $backendUrl
     */
    public function __construct(UrlInterface $backendUrl)
    {
        $this->backendUrl = $backendUrl;
    }

    /**
     * @param ParentView $view
     * @return void
     */
    public function beforeSetLayout(ParentView $view)
    {
        $order = $view->getOrder();

        if (!$this->canAddButton($order)) {
            return;
        }

        $url = $this->backendUrl->getUrl('crefopay/order/settle', ['orderId' => $order->getIncrementId()]);

        $view->addButton(
            'order_settle',
            [
                'label' => 'Settle',
                'class' => '',
                'onclick' => "setLocation('{$url}')"
            ]
        );
    }

    /**
     * @param Order $order
     *
     * @return bool
     */
    private function canAddButton(Order $order): bool
    {
        if (in_array($order->getState(), [Order::STATE_CANCELED, Order::STATE_CLOSED])) {
            return false;
        }

        if (!PaymentMethodsCodesMap::isPaymentMethodInCrefoPayGroup($order->getPayment()->getMethod())) {
            return false;
        }

        return true;
    }
}
