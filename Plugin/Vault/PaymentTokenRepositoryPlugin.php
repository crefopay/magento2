<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Plugin\Vault;

use Magento\Vault\Model\PaymentTokenRepository;
use Magento\Vault\Api\Data\PaymentTokenInterface;
use Magento\Vault\Api\Data\PaymentTokenFactoryInterface;
use Trilix\CrefoPay\Client\Request\DeleteUserPaymentInstrumentRequestFactory;
use Trilix\CrefoPay\Client\Transport;
use Trilix\CrefoPay\Gateway\Response\CrefoPayPaymentTokenFactory;

class PaymentTokenRepositoryPlugin
{
    /** @var DeleteUserPaymentInstrumentRequestFactory */
    private $deleteUserPaymentInstrumentRequestFactory;

    /** @var Transport  */
    private $transport;

    /**
     * PaymentTokenRepositoryPlugin constructor.
     * @param DeleteUserPaymentInstrumentRequestFactory $deleteUserPaymentInstrumentRequestFactory
     * @param Transport $transport
     */
    public function __construct(
        DeleteUserPaymentInstrumentRequestFactory $deleteUserPaymentInstrumentRequestFactory,
        Transport $transport
    ) {
        $this->deleteUserPaymentInstrumentRequestFactory = $deleteUserPaymentInstrumentRequestFactory;
        $this->transport = $transport;
    }

    public function beforeDelete(PaymentTokenRepository $subject, PaymentTokenInterface $paymentToken)
    {
        if (
            $paymentToken->getPaymentMethodCode() === PaymentTokenFactoryInterface::TOKEN_TYPE_CREDIT_CARD ||
            $paymentToken->getPaymentMethodCode() === CrefoPayPaymentTokenFactory::TOKEN_TYPE_DIRECT_DEBIT
        ) {
            $request = $this->deleteUserPaymentInstrumentRequestFactory->create($paymentToken->getGatewayToken());
            $this->transport->sendRequest($request);
        }
    }
}
