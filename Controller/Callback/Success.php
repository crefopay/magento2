<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Controller\Callback;

use Magento\Framework\App\Action\Action;

class Success extends Action
{
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $this->_forward('success', 'onepage', 'checkout');
    }
}
