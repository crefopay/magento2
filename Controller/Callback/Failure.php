<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Controller\Callback;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Sales\Model\OrderRepository;
use Magento\Sales\Model\Order;
use Magento\Framework\View\Result\PageFactory as ResultPageFactory;

class Failure extends Action
{
    /**
     * @var Session
     */
    private $checkoutSession;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var ResultPageFactory
     */
    protected $resultPageFactory;

    /** @var Order */
    private $order;

    /**
     * Failure constructor.
     *
     * @param Context           $context
     * @param Session           $checkoutSession
     * @param OrderRepository   $orderRepository
     * @param ResultPageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        Session $checkoutSession,
        OrderRepository $orderRepository,
        ResultPageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->checkoutSession = $checkoutSession;
        $this->orderRepository = $orderRepository;
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * @return \Magento\Framework\View\Result\Page|\Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        if (!$this->getOrder()->getId()) {
            return $this->resultRedirectFactory->create()->setPath('checkout/cart');
        }

        $this->_cancelOrder();

        return $this->resultPageFactory->create();
    }

    /**
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function _cancelOrder()
    {
        $order = $this->getOrder();
        $order->setState(Order::STATE_CANCELED);
        $order->setStatus($order->getConfig()->getStateDefaultStatus(Order::STATE_CANCELED));
        $this->orderRepository->save($order);
    }

    /**
     * @return Order
     */
    private function getOrder(): Order
    {
        if (!$this->order) {
            $this->order = $this->checkoutSession->getLastRealOrder();
        }

        return $this->order;
    }
}
