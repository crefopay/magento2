<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Controller\Adminhtml\Autocapture;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Trilix\CrefoPay\Gateway\Config\Config;

class Save extends \Magento\Backend\App\Action
{
    /** @var Config */
    private $config;

    /**
     * Save constructor.
     *
     * @param Config  $config
     * @param Context $context
     */
    public function __construct(Config $config, Context $context)
    {
        parent::__construct($context);
        $this->config = $config;
    }

    /**
     * Save auto capture categories.
     *
     * @return \Magento\Framework\Controller\Result\Raw
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Raw $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        $categories = $this->getRequest()->getParam('categories');
        $scope = $this->getRequest()->getParam('scope');

        if (!is_array($categories)) {
            return $result;
        }

        $configScope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT;
        $configScopeId = 0;

        if (is_array($scope) && array_key_exists('website', $scope) && $scope['website']) {
            $configScope = 'websites';
            $configScopeId = $scope['website'];
        }

        $this->config->setAutoCaptureCategoryIds($categories, $configScope, $configScopeId);

        return $result;
    }
}
