<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Controller\Adminhtml\Order;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order;
use Trilix\CrefoPay\Client\Request\FinishRequestFactory;
use Trilix\CrefoPay\Client\Transport;
use Trilix\CrefoPay\Model\AmazonPay\AmazonPayTransactionRepository;
use Trilix\CrefoPay\Model\TransactionService;
use Upg\Library\Api\Exception\AbstractException;

class Settle extends \Magento\Backend\App\Action
{
    /** @var Transport */
    private $transport;

    /** @var FinishRequestFactory */
    private $finishRequestFactory;

    /** @var TransactionService */
    private $transactionService;

    /** @var \Trilix\CrefoPay\Helper\Order */
    private $orderHelper;

    /** @var AmazonPayTransactionRepository */
    private $amazonPayTransactionRepository;

    /**
     * Settle constructor.
     * @param Context $context
     * @param Transport $transport
     * @param FinishRequestFactory $finishRequestFactory
     * @param TransactionService $transactionService
     * @param \Trilix\CrefoPay\Helper\Order $orderHelper
     * @param AmazonPayTransactionRepository $amazonPayTransactionRepository
     */
    public function __construct(
        Context $context,
        Transport $transport,
        FinishRequestFactory $finishRequestFactory,
        TransactionService $transactionService,
        \Trilix\CrefoPay\Helper\Order $orderHelper,
        AmazonPayTransactionRepository $amazonPayTransactionRepository
    ) {
        $this->transport = $transport;
        $this->finishRequestFactory = $finishRequestFactory;
        $this->transactionService = $transactionService;
        $this->orderHelper = $orderHelper;
        parent::__construct($context);

        $this->amazonPayTransactionRepository = $amazonPayTransactionRepository;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     * @throws LocalizedException
     */
    public function execute()
    {
        $orderId = $this->getRequest()->getParam('orderId');
        $order = $this->orderHelper->getOrderByIncrementId($orderId);

        $amazonPayTransaction = $this->amazonPayTransactionRepository->getByOrderId($orderId);
        if ($amazonPayTransaction) {
            $orderId = $amazonPayTransaction->getCrefoPayOrderId();
        }

        $finishRequest = $this->finishRequestFactory->create($orderId);

        try {
            $this->transport->sendRequest($finishRequest);
            $this->updateOrder($order);
            $this->messageManager->addSuccessMessage(__('Order settled'));
        } catch (AbstractException $e) {
            $this->messageManager->addErrorMessage(__('Can not execute the command for this order, CrefoPay says: %1', $e->getMessage()));
        }

        return $this->resultRedirectFactory->create()->setRefererUrl();
    }

    /**
     * @param Order $order
     */
    private function updateOrder(Order $order)
    {
        $order->setStatus(Order::STATE_CLOSED);
        $order->setState(Order::STATE_CLOSED);
        $order->addStatusToHistory($order->getStatus(), 'Order settled with CrefoPay');
        $this->orderHelper->getOrderRepository()->save($order);
    }
}
