<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Controller\AmazonPay;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Trilix\CrefoPay\Model\AmazonPay\AmazonPayCustomerSession;
use Trilix\CrefoPay\Model\AmazonPay\AmazonSessionDataFactory;

class Checkout extends Action
{
    /** @var AmazonPayCustomerSession  */
    private $amazonPayCustomerSession;

    /** @var AmazonSessionDataFactory */
    private $amazonSessionDataFactory;

    public function __construct(
        Context $context,
        AmazonPayCustomerSession $amazonPayCustomerSession,
        AmazonSessionDataFactory $amazonSessionDataFactory
    ) {
        parent::__construct($context);

        $this->amazonPayCustomerSession = $amazonPayCustomerSession;
        $this->amazonSessionDataFactory = $amazonSessionDataFactory;
    }

    public function execute()
    {
        $orderId = $this->getRequest()->getParam('orderID');
        $sessionId = $this->getRequest()->getParam('sessionID');

        $amazonSessionData = $this->amazonSessionDataFactory->create(
            [
                'orderId' => $orderId,
                'sessionId' => $sessionId
            ]
        );

        $this->amazonPayCustomerSession->saveAmazonSessionDataInCustomerSession($amazonSessionData);

        return $this->_redirect('checkout', ['_query' => ['crefoPayAmazonCheckoutSessionId' => $sessionId, 'orderId' => $orderId]]);
    }
}
