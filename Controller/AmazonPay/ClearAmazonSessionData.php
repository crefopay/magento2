<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Controller\AmazonPay;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Trilix\CrefoPay\Model\AmazonPay\AmazonPayCustomerSession;

class ClearAmazonSessionData extends Action
{
    /** @var JsonFactory */
    private $resultJsonFactory;

    /** @var AmazonPayCustomerSession */
    private $amazonPayCustomerSession;

    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        AmazonPayCustomerSession $amazonPayCustomerSession
    ) {
        parent::__construct($context);

        $this->resultJsonFactory = $resultJsonFactory;
        $this->amazonPayCustomerSession = $amazonPayCustomerSession;
    }

    public function execute()
    {
        $this->amazonPayCustomerSession->deleteAmazonSessionDataFromCustomerSession();
        $data = ['message' => 'amazonSessionDataWasRemoved'];

        return $this->resultJsonFactory->create()->setData($data);
    }
}
