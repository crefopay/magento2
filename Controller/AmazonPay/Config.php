<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Controller\AmazonPay;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Locale\ResolverInterface as Locale;
use Trilix\CrefoPay\Gateway\Config\Config as CrefoPayConfig;
use Magento\Framework\App\Action\Action;

class Config extends Action
{
    /** @var JsonFactory */
    private $resultJsonFactory;

    /** @var CrefoPayConfig */
    private $config;

    /** @var Locale */
    private $locale;

    /**
     * Config constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param CrefoPayConfig $config
     * @param Locale $locale
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        CrefoPayConfig $config,
        Locale $locale
    ) {
        parent::__construct($context);

        $this->resultJsonFactory = $resultJsonFactory;
        $this->config            = $config;
        $this->locale            = $locale;
    }

    public function execute()
    {
        $data = [
            'shopPublicKey'          => $this->config->getPublicToken(),
            'url'                    => $this->config->getSecureFieldsUrl(),
            'buttonColor'            => $this->config->getAmazonConfig()->getButtonColor(),
            'checkoutLanguage'       => $this->getCheckoutLanguageCode(),
            'sandbox'                => $this->config->isSandboxMode(),
            'autoCapture'            => $this->config->getAmazonConfig()->isAutoCaptureEnabled(),
            'deliverySpecifications' => $this->config->getAmazonConfig()->getDeliverySpecifications(),
        ];

        return $this->resultJsonFactory->create()->setData($data);
    }

    /**
     * @return string
     */
    private function getCheckoutLanguageCode(): string
    {
        $languageCode = $this->locale->getLocale();

        $supportedLanguages = [
            'en_GB', 'de_DE', 'fr_FR', 'it_IT', 'es_ES'
        ];

        if (!in_array($languageCode, $supportedLanguages)) {
            $languageCode = 'en_GB';
        }

        return $languageCode;
    }
}
