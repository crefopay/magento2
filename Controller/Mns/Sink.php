<?php

declare(strict_types=1);

namespace {
    Trilix\CrefoPay\Polyfill\Polyfill::polyfill();
}

namespace Trilix\CrefoPay\Controller\Mns {
    use Magento\Framework\App\CsrfAwareActionInterface;
    use Magento\Framework\App\Action\Action;
    use Magento\Framework\App\Action\Context;
    use Magento\Framework\App\Request\InvalidRequestException;
    use Magento\Framework\App\RequestInterface;
    use Magento\Framework\Controller\ResultFactory;
    use Trilix\CrefoPay\Model\Mns\MnsService;

    class Sink extends Action implements CsrfAwareActionInterface
    {
        /**
         * @var MnsService
         */
        private $mnsService;

        /**
         * Consume constructor.
         * @param Context $context
         * @param MnsService $mnsService
         */
        public function __construct(Context $context, MnsService $mnsService)
        {
            parent::__construct($context);
            $this->mnsService = $mnsService;
        }

        /**
         * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
         */
        public function execute()
        {
            /** @var \Magento\Framework\Controller\Result\Raw $result */
            $result = $this->resultFactory->create(ResultFactory::TYPE_RAW);
            $result->setContents('');

            $this->mnsService->acknowledge($_POST);

            return $result;
        }

        /**
         * Validation exception is not thrown in this action.
         *
         * @param RequestInterface $request
         *
         * @return InvalidRequestException|null
         */
        public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
        {
            return null;
        }

        /**
         * CrefoPay server sends POST requests without Magento formkey, we allow it here.
         *
         * @param RequestInterface $request
         *
         * @return true
         */
        public function validateForCsrf(RequestInterface $request): ?bool
        {
            return true;
        }
    }
}
