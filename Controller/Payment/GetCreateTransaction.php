<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Controller\Payment;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Trilix\CrefoPay\Model\CrefoPayTransactionRepository;

/**
 * Class GetCreateTransaction
 * @package Trilix\CrefoPay\Controller\Payment
 */
class GetCreateTransaction extends Action
{
    /** @var CrefoPayTransactionRepository */
    private $crefoPayTransactionRepository;

    /** @var JsonFactory */
    private $resultJsonFactory;

    /**
     * GetCreateTransaction constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param CrefoPayTransactionRepository $crefoPayTransactionRepository
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        CrefoPayTransactionRepository $crefoPayTransactionRepository
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->crefoPayTransactionRepository = $crefoPayTransactionRepository;
    }

    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        $quoteId = $this->getRequest()->getParam('quoteId');

        $allowedPaymentMethods = '[]';
        try {
            $crefoPayTransaction = $this->crefoPayTransactionRepository->getByQuoteId($quoteId);
            $crefoPayOrderId = $crefoPayTransaction->getCrefoPayOrderId();
            $allowedPaymentMethods = $crefoPayTransaction->getPaymentMethods();
        } catch (NoSuchEntityException $e) {
            $crefoPayOrderId = null;
        }

        return $resultJson->setData([
            'crefoPayOrderId' => $crefoPayOrderId,
            'areAnyPaymentMethodsAllowed' => ($allowedPaymentMethods !== '[]')
        ]);
    }
}
