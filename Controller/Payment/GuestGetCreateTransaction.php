<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Controller\Payment;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\QuoteIdMaskFactory;
use Magento\Quote\Model\QuoteIdMask;
use Trilix\CrefoPay\Model\CrefoPayTransactionRepository;
use Trilix\CrefoPay\Model\Ui;

/**
 * Class GuestGetCreateTransaction
 * @package Trilix\CrefoPay\Controller\Payment
 */
class GuestGetCreateTransaction extends Action
{
    /** @var CrefoPayTransactionRepository */
    private $crefoPayTransactionRepository;

    /** @var JsonFactory */
    private $resultJsonFactory;

    /** @var QuoteIdMaskFactory */
    private $quoteIdMaskFactory;

    /**
     * GuestGetCreateTransaction constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param CrefoPayTransactionRepository $crefoPayTransactionRepository
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        QuoteIdMaskFactory $quoteIdMaskFactory,
        CrefoPayTransactionRepository $crefoPayTransactionRepository
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->crefoPayTransactionRepository = $crefoPayTransactionRepository;
    }

    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();

        $cartId = $this->getRequest()->getParam('quoteId');

        /** @var $quoteIdMask QuoteIdMask */
        $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
        $quoteId = $quoteIdMask->getQuoteId();

        $allowedPaymentMethods = '[]';
        try {
            $crefoPayTransaction = $this->crefoPayTransactionRepository->getByQuoteId($quoteId);
            $crefoPayOrderId = $crefoPayTransaction->getCrefoPayOrderId();
            $allowedPaymentMethods = $crefoPayTransaction->getPaymentMethods();
        } catch (NoSuchEntityException $e) {
            $crefoPayOrderId = null;
        }

        return $resultJson->setData([
            'crefoPayOrderId' => $crefoPayOrderId,
            'areAnyPaymentMethodsAllowed' => ($allowedPaymentMethods !== '[]')
        ]);
    }
}
