<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Controller\Payment;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Trilix\CrefoPay\Client\Request\CreateTransactionRequestFactory;
use Trilix\CrefoPay\Client\Transport;
use Trilix\CrefoPay\Model\CrefoPayTransactionRepository;

/**
 * Handles the recreation of a CrefoPay transaction for a specified cart ID.
 * This functionality is particularly critical for PayPal as a payment provider,
 * especially in scenarios involving discounts, where the order totals may vary
 * depending on the selected payment method.
 *
 * This controller action is responsible for retrieving the relevant transaction
 * and generating a new transaction request using CrefoPay's API. It returns
 * a JSON response indicating whether the operation was successful.
 */
class RecreateTransaction extends Action
{
    /** @var JsonFactory Factory for creating JSON responses. */
    private $resultJsonFactory;

    /** @var CreateTransactionRequestFactory Factory for creating CrefoPay transaction requests. */
    private $createTransactionRequestFactory;

    /** @var Transport Handles communication with the CrefoPay API. */
    private $transport;

    /** @var CrefoPayTransactionRepository Repository for retrieving CrefoPay transactions. */
    private $crefoPayTransactionRepository;

    /**
     * RecreateTransaction constructor.
     *
     * @param Context $context Context object for the controller.
     * @param JsonFactory $resultJsonFactory Factory for creating JSON result objects.
     * @param CreateTransactionRequestFactory $createTransactionRequestFactory Factory for creating transaction requests.
     * @param Transport $transport Communication layer for interacting with CrefoPay.
     * @param CrefoPayTransactionRepository $crefoPayTransactionRepository Repository for managing CrefoPay transactions.
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        CreateTransactionRequestFactory $createTransactionRequestFactory,
        Transport $transport,
        CrefoPayTransactionRepository $crefoPayTransactionRepository
    ) {
        parent::__construct($context);
        $this->resultJsonFactory                      = $resultJsonFactory;
        $this->createTransactionRequestFactory        = $createTransactionRequestFactory;
        $this->transport                              = $transport;
        $this->crefoPayTransactionRepository = $crefoPayTransactionRepository;
    }

    /**
     * Execute the action to recreate a transaction.
     *
     * Retrieves the cart ID from the request, fetches the related CrefoPay transaction,
     * and attempts to recreate the transaction using CrefoPay's API. Returns a JSON response
     * with the result of the operation.
     *
     * @return \Magento\Framework\Controller\Result\Json JSON response indicating success or failure.
     */
    public function execute()
    {
        $cartId = (int) $this->getRequest()->getParam('cartId');
        $successResult = true;
        $message = 'Recreation of transaction completed successfully.';

        try {
            // Retrieve the CrefoPay transaction associated with the given cart ID.
            $crefoPayTransaction = $this->crefoPayTransactionRepository->getByQuoteId($cartId);

            // Extract the CrefoPay order ID from the retrieved transaction.
            $crefoPayOrderId = $crefoPayTransaction->getCrefoPayOrderId();

            // Create a transaction recreation request using the cart ID and order ID.
            $createTransactionRequest = $this->createTransactionRequestFactory->createRecreateRequest($cartId, $crefoPayOrderId);

            // Send the recreation request via the CrefoPay transport layer.
            $response = $this->transport->sendRequest($createTransactionRequest);
        } catch (NoSuchEntityException $e) {
            // Handle case where no transaction is found for the given cart ID.
            $successResult = false;
            $message = 'No transaction found for the given cart ID.';
        } catch (\Exception $e) {
            // Handle any other exceptions that occur during the recreation process.
            $successResult = false;
            $message = 'An error occurred during recreation of transaction: ' . $e->getMessage();
        }

        /** @var \Magento\Framework\Controller\Result\Json $resultJson JSON result object to return the response. */
        $resultJson = $this->resultJsonFactory->create();

        // Return the result as a JSON response with success status and message.
        return $resultJson->setData([
            'success' => $successResult,
            'message' => $message
        ]);
    }
}
