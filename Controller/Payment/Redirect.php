<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Controller\Payment;

use Magento\Sales\Model\Order;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Action\Context;
use Trilix\CrefoPay\Client\Request\GetTransactionStatusRequestFactory;

class Redirect extends Action
{
    /**
     * @var \Trilix\CrefoPay\Helper\Order
     */
    private $orderHelper;

    /**
     * Redirect constructor.
     *
     * @param Context               $context
     * @param \Trilix\CrefoPay\Helper\Order $orderHelper
     */
    public function __construct(
        Context $context,
        \Trilix\CrefoPay\Helper\Order $orderHelper
    ) {
        parent::__construct($context);
        $this->orderHelper = $orderHelper;
    }

    public function execute()
    {
        try {
            /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultFactory->create($this->resultFactory::TYPE_REDIRECT);
            $incrementOrderId = $this->getRequest()->getParam('oid');
            $order = $this->orderHelper->getOrderByIncrementId($incrementOrderId);
            $redirectUrl = $this->getRedirectUrl($order);

            return $resultRedirect->setUrl($redirectUrl);
        } catch (\Exception $e) {
            /** @var \Magento\Framework\Controller\Result\Forward $result404 */
            $result404 = $this->resultFactory->create($this->resultFactory::TYPE_FORWARD);

            return $result404
                ->setModule('Magento_Cms')
                ->setController('Index')
                ->forward('defaultNoRoute');
        }
    }

    /**
     * @param Order $order
     * @return string
     */
    private function getRedirectUrl(Order $order): string
    {
        /** @var \Magento\Sales\Model\Order\Payment $payment */
        $payment     = $order->getPayment();
        $transaction = $payment->getAuthorizationTransaction();
        $method      = $payment->getMethodInstance();
        $methodCode  = $method->getCode();

        if (!$transaction) {
            throw new \InvalidArgumentException(
                sprintf('Can not find authorization transaction for order ID %s', $order->getIncrementId())
            );
        }

        $redirectUrl = $transaction->getAdditionalInformation('redirectUrl');

        if (empty($redirectUrl)) {
            if ($methodCode == 'crefopay_credit_card_tds') {
                $redirectUrl = $this->getBaseUrl() . 'crefopay/callback/success';
            } else {
                throw new \InvalidArgumentException(sprintf('No redirectUrl for order ID %s', $order->getIncrementId()));
            }
        }

        return $redirectUrl;
    }

    /**
     * getting Base Url without index.php
     *
     * @return string
     */
    private function getBaseUrl(): string
    {
        $objectManager = ObjectManager::getInstance();
        $storeManager  = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');

        return $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_LINK);
    }
}
