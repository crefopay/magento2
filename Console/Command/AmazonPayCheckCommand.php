<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Console\Command;

use Magento\Framework\Module\Manager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AmazonPayCheckCommand extends Command
{
    /** @var Manager */
    private $moduleManager;

    /**
     * AmazonPayCheckCommand constructor.
     * @param Manager $moduleManager
     */
    public function __construct(Manager $moduleManager)
    {
        parent::__construct();
        $this->moduleManager = $moduleManager;
    }

    protected function configure()
    {
        $this
            ->setName('crefopay:amazon-pay:check')
            ->setDescription('Check if CrefoPay AmazonPay method can be used');

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $outputStyle = new SymfonyStyle($input, $output);
        if ($this->moduleManager->isEnabled("Amazon_Payment")) {
            $text = "Currently, the native Magento Amazon Pay module is enabled. " .
                "It is not compatible with CrefoPay Amazon Pay. " .
                "You should use just one of them. If you want to use CrefoPay Amazon Pay method. " .
                "It would help if you disabled native Amazon Pay. " .
                "To do so, run next command: \"bin/magento module:disable Amazon_Payment --clear-static-content\"";
            $outputStyle->error($text);
            return 666;
        }
        return 0;
    }
}
