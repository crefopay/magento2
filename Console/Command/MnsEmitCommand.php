<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Console\Command;

use Magento\Framework\App\State as AppState;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\HTTP\ClientFactory;
use Magento\Framework\HTTP\ClientInterface;
use Magento\Framework\UrlInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Trilix\CrefoPay\Client\ConfigFactory;
use Trilix\CrefoPay\Client\Constants;
use Upg\Library\Callback\MacCalculator;

class MnsEmitCommand extends Command
{
    public const ARG_ORDER_ID = 'orderId';
    public const OPT_TRX_STATUS = 'transaction-status';
    public const OPT_ORDER_STATUS = 'order-status';
    public const OPT_CAPTURE_ID = 'captureId';
    public const OPT_AMOUNT = 'amount';
    public const OPT_CURRENCY = 'currency';
    public const OPT_PAYMENT_REFERENCE = 'payment-ref';
    public const OPT_DEBUG = 'debug';

    /** @var ConfigFactory */
    private $configFactory;

    /** @var ClientFactory */
    private $clientFactory;

    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * @var AppState
     */
    private $appState;

    /**
     * MnsEmitCommand constructor.
     *
     * @param ConfigFactory $configFactory
     * @param ClientFactory $clientFactory
     * @param UrlInterface  $url
     * @param AppState      $appState
     */
    public function __construct(
        ConfigFactory $configFactory,
        ClientFactory $clientFactory,
        UrlInterface $url,
        AppState $appState
    ) {
        $this->configFactory = $configFactory;
        $this->clientFactory = $clientFactory;
        $this->url = $url;
        $this->appState = $appState;

        parent::__construct();
    }

    protected function configure()
    {
        /**
         * All commands are always instantiated, no matter what command has been actually required. Here we
         * use this feature to load the Magento 2.3 interface within Magento 2.2. Otherwise
         * 'setup:di:compile' would throw an exception, as it does not include() class files, but reads them
         * and processes tokens.
         */
        \Trilix\CrefoPay\Polyfill\Polyfill::polyfill();

        $this
            ->setName('crefopay:mns:emit')
            ->setDescription('Emit CrefoPay MNS event')
            ->setDefinition([
                new InputArgument(self::ARG_ORDER_ID, InputArgument::REQUIRED, 'Order ID'),
                new InputOption(self::OPT_TRX_STATUS, '-t', InputOption::VALUE_REQUIRED, 'Transaction status'),
                new InputOption(self::OPT_ORDER_STATUS, '-o', InputOption::VALUE_REQUIRED, 'Order status'),
                new InputOption(self::OPT_CAPTURE_ID, '-c', InputOption::VALUE_REQUIRED, 'Capture ID'),
                new InputOption(self::OPT_AMOUNT, '-a', InputOption::VALUE_REQUIRED, 'Transaction amount (default: 0)'),
                new InputOption(self::OPT_CURRENCY, null, InputOption::VALUE_REQUIRED, 'Currency code (default: "EUR")', 'EUR'),
                new InputOption(self::OPT_PAYMENT_REFERENCE, null, InputOption::VALUE_REQUIRED, 'Payment reference'),
                new InputOption(self::OPT_DEBUG, 'd', InputOption::VALUE_NONE, '[DEV] Add XDEBUG_SESSION_START parameter'),
            ]);

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->validateArguments($input);
        $this->appState->setAreaCode(\Magento\Framework\App\Area::AREA_FRONTEND);

        $request = $this->createRequest($input);
        $client = $this->sendRequest($request, $input);
        $this->printReport($client, $input, $output, $request);
    }

    /**
     * @param InputInterface $input
     */
    private function validateArguments(InputInterface $input)
    {
        $transactionStatus = $input->getOption(self::OPT_TRX_STATUS);
        $orderStatus = $input->getOption(self::OPT_ORDER_STATUS);

        if ($transactionStatus && !Constants::isValidTransactionStatus($transactionStatus)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid transaction status '%s'\nAllowed transaction statuses:\n- %s",
                    $transactionStatus,
                    join("\n- ", Constants::getAllTransactionStatuses())
                )
            );
        }

        if ($orderStatus && !Constants::isValidOrderStatus($orderStatus)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid order status '%s'\nAllowed order statuses:\n- %s",
                    $orderStatus,
                    join("\n- ", Constants::getAllOrderStatuses())
                )
            );
        }
    }

    /**
     * @param InputInterface $input
     * @return array
     * @throws FileSystemException
     */
    private function createRequest(InputInterface $input): array
    {
        $config = $this->configFactory->create();

        $request = [
            'merchantID' => $config->getMerchantID(),
            'storeID' => $config->getStoreID(),
            'orderID' => $input->getArgument(self::ARG_ORDER_ID),
            'captureID' => (string)$input->getOption(self::OPT_CAPTURE_ID),
            'merchantReference' => '',
            'paymentReference' => (string)$input->getOption(self::OPT_PAYMENT_REFERENCE),
            'userID' => '1',
            'amount' => $this->getAmount($input),
            'currency' => (string)$input->getOption(self::OPT_CURRENCY),
            'transactionStatus' => (string)$input->getOption(self::OPT_TRX_STATUS),
            'orderStatus' => (string)$input->getOption(self::OPT_ORDER_STATUS),
            'timestamp' => intval(microtime(true) * 1000),
            'version' => '2.1',
        ];

        $macCalculator = new MacCalculator($config, $request);
        $request['mac'] = $macCalculator->calculateMac();

        return $request;
    }

    /**
     * @param InputInterface $input
     * @return int
     */
    private function getAmount(InputInterface $input): int
    {
        $amount = $input->getOption(self::OPT_AMOUNT);

        if (is_null($amount)) {
            return 0;
        }

        return intval($amount * 100);
    }

    /**
     * @param InputInterface $input
     * @return string
     */
    private function getUrl(InputInterface $input)
    {
        $isDebug = (bool)$input->getOption(self::OPT_DEBUG);
        $url = 'crefopay/mns/sink';

        if ($isDebug) {
            $url .= '?XDEBUG_SESSION_START=1';
        }

        return $this->url->getUrl($url);
    }

    /**
     * @param array $request
     * @param InputInterface $input
     * @return ClientInterface
     */
    private function sendRequest(array $request, InputInterface $input): ClientInterface
    {
        echo "sending request\n";
        $client = $this->clientFactory->create();
        $client->post($this->getUrl($input), $request);

        return $client;
    }

    /**
     * @param ClientInterface $client
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param array $request
     */
    private function printReport(ClientInterface $client, InputInterface $input, OutputInterface $output, array $request)
    {
        $outputStyle = new SymfonyStyle($input, $output);
        $text = sprintf("%d POST %s", $client->getStatus(), $this->getUrl($input));

        if ($client->getStatus() === 200) {
            $outputStyle->success($text);
        } else {
            $outputStyle->error($text);
        }

        $outputStyle->text(http_build_query($request));

        if ($client->getBody()) {
            $outputStyle->error("\n" . $client->getBody());
        }
    }
}
