<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Console\Command;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\State as AppState;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Trilix\CrefoPay\Console\Command\MnsProcessCommand\CliSupervisor;
use Trilix\CrefoPay\Model\Mns\MnsRepository;
use Trilix\CrefoPay\Model\Mns\MnsService;

class MnsProcessCommand extends Command
{
    /** @var MnsRepository */
    private $mnsRepository;

    /** @var SearchCriteriaBuilder */
    private $searchCriteriaBuilder;

    /** @var CliSupervisor */
    private $supervisor;

    /** @var MnsService */
    private $mnsService;

    /** @var AppState */
    private $appState;

    /**
     * MnsProcessCommand constructor.
     *
     * @param MnsRepository $mnsRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param CliSupervisor $supervisor
     * @param MnsService $mnsService
     * @param AppState $appState
     */
    public function __construct(
        MnsRepository $mnsRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        CliSupervisor $supervisor,
        MnsService $mnsService,
        AppState $appState
    ) {
        $this->mnsRepository = $mnsRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->supervisor = $supervisor;
        $this->mnsService = $mnsService;
        $this->appState = $appState;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('crefopay:mns:process')
            ->setDescription('Process new MNS events');

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appState->setAreaCode(\Magento\Framework\App\Area::AREA_FRONTEND);
        $this->supervisor->setOutput($output);
        $this->mnsService->process($this->supervisor);
    }
}
