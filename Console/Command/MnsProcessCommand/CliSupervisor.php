<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Console\Command\MnsProcessCommand;

use Symfony\Component\Console\Output\OutputInterface;
use Trilix\CrefoPay\Model\Mns\MnsEvent;
use Trilix\CrefoPay\Model\Mns\ProcessSupervisor\SupervisorInterface;

class CliSupervisor implements SupervisorInterface
{
    /** @var OutputInterface */
    private $output;

    /**
     * {@inheritdoc}
     */
    public function setTotalCount(int $itemsCount)
    {
        if (!$itemsCount) {
            $this->output->writeln('Nothing to do');
        } else {
            $this->output->writeln(sprintf('%d events to process', $itemsCount));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function ok(MnsEvent $mns)
    {
        $this->output->writeln(sprintf('[OK] Event for order %d', $mns->getIncrementOrderId()));
    }

    /**
     * {@inheritdoc}
     */
    public function fail(MnsEvent $mns)
    {
        $this->output->writeln(
            sprintf('[FAIL] Event for order %d, details: %s', $mns->getIncrementOrderId(), $mns->getErrorDetails())
        );
    }

    public function setOutput(OutputInterface $output)
    {
        $this->output = $output;
    }
}
