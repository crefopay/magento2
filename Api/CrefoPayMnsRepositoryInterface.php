<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Trilix\CrefoPay\Api\Data\CrefoPayMnsInterface;
use Trilix\CrefoPay\Api\Data\CrefoPayMnsSearchResultInterface;

interface CrefoPayMnsRepositoryInterface
{
    /**
     * Retrieve all MNS events for search criteria
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return CrefoPayMnsSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria): CrefoPayMnsSearchResultInterface;

    /**
     * Retrieve specific MNS event
     *
     * @param int $id
     * @return CrefoPayMnsInterface
     * @throws NoSuchEntityException
     */
    public function get($id): CrefoPayMnsInterface;

    /**
     * Create MNS event
     *
     * @param CrefoPayMnsInterface $mnsEvent
     * @return CrefoPayMnsInterface
     * @throws StateException
     */
    public function save(CrefoPayMnsInterface $mnsEvent): CrefoPayMnsInterface;

    /**
     * Delete MNS event
     *
     * @param CrefoPayMnsInterface $mnsEvent
     * @return void
     * @throws StateException
     */
    public function delete(CrefoPayMnsInterface $mnsEvent): void;

    /**
     * Delete MNS event by ID
     *
     * @param int $id
     * @return void
     * @throws NoSuchEntityException
     * @throws StateException
     */
    public function deleteById($id): void;
}
