<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Api;

use Trilix\CrefoPay\Api\Data\CrefoPayTransactionInterface;

interface CrefoPayTransactionRepositoryInterface
{
    /**
     * @param CrefoPayTransactionInterface $crefoPayTransaction
     * @return CrefoPayTransactionInterface
     */
    public function save(CrefoPayTransactionInterface $crefoPayTransaction): CrefoPayTransactionInterface;

    /**
     * @param $quoteId
     * @return CrefoPayTransactionInterface
     */
    public function getByQuoteId($quoteId): CrefoPayTransactionInterface;
}
