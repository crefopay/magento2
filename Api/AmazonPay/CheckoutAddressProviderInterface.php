<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Api\AmazonPay;

interface CheckoutAddressProviderInterface
{
    /**
     * @param string $orderId
     * @return mixed
     */
    public function getShippingAddress(string $orderId): array;

    /**
     * @param string $orderId
     * @return mixed
     */
    public function getBillingAddress(string $orderId): array;
}
