<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Api\AmazonPay;

use Trilix\CrefoPay\Api\Data\AmazonPay\AmazonPayTransactionInterface;

interface AmazonPayTransactionRepositoryInterface
{
    /**
     * @param AmazonPayTransactionInterface $amazonPayTransaction
     * @return AmazonPayTransactionInterface
     */
    public function save(AmazonPayTransactionInterface $amazonPayTransaction): AmazonPayTransactionInterface;

    /**
     * @param string $crefoPayOrderId
     * @return AmazonPayTransactionInterface|null
     */
    public function getByCrefoPayOrderId(string $crefoPayOrderId): ?AmazonPayTransactionInterface;

    /**
     * @param string $orderId
     * @return AmazonPayTransactionInterface|null
     */
    public function getByOrderId(string $orderId): ?AmazonPayTransactionInterface;
}
