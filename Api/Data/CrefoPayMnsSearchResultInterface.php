<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface CrefoPayMnsSearchResultInterface extends SearchResultsInterface
{
    /**
     * @return CrefoPayMnsInterface[]
     */
    public function getItems();

    /**
     * @param CrefoPayMnsInterface[] $items
     *
     * @return void
     */
    public function setItems(array $items);
}
