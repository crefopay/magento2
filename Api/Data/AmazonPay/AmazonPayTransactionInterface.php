<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Api\Data\AmazonPay;

interface AmazonPayTransactionInterface
{
    /**
     * @return string
     */
    public function getOrderId(): string;

    /**
     * @return string
     */
    public function getCrefoPayOrderId(): string;

    /**
     * @param string $orderId
     * @return void
     */
    public function setOrderId(string $orderId): void;

    /**
     * @param string $crefoPayOrderId
     */
    public function setCrefoPayOrderId(string $crefoPayOrderId);
}
