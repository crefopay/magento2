<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Api\Data;

interface CrefoPayTransactionInterface
{
    /**
     * @return int
     */
    public function getQuoteId(): int;

    /**
     * @return string
     */
    public function getCrefoPayOrderId(): string;

    /**
     * @return string
     */
    public function getPaymentMethods(): string;

    /**
     * @return string
     */
    public function getCrefoPayUserId(): string;

    /**
     * @param $quoteId
     * @return void
     */
    public function setQuoteId($quoteId): void;

    /**
     * @param string $paymentMethods
     */
    public function setPaymentMethods(string $paymentMethods);

    /**
     * @param string $crefoPayUserId
     */
    public function setCrefoPayUserId(string $crefoPayUserId);

    /**
     * @param string $crefoPayOrderId
     */
    public function setCrefoPayOrderId(string $crefoPayOrderId);
}
