<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Api\Data;

interface CrefoPayMnsInterface
{
    public const MERCHANT_ID = 'merchant_id';
    public const STORE_ID = 'store_id';
    public const ORDER_INCREMENT_ID = 'order_increment_id';
    public const CAPTURE_ID = 'capture_id';
    public const MERCHANT_REFERENCE = 'merchant_reference';
    public const PAYMENT_REFERENCE = 'payment_reference';
    public const USER_ID = 'user_id';
    public const AMOUNT = 'amount';
    public const CURRENCY = 'currency';
    public const TRANSACTION_STATUS = 'transaction_status';
    public const CAPTURE_STATUS = 'capture_status';
    public const CREATED_AT = 'created_at';
    public const MNS_STATUS = 'mns_status'; // See STATUS_* constants
    public const PROCESSED_AT = 'processed_at';
    public const ERROR_DETAILS = 'error_details'; // If mns_status is 'failed'
    public const NUMBER_OF_ATTEMPTS_TO_PROCESS_NOTIFICATION = 'attempts_to_process_notification';

    /**
     * MNS statuses
     */
    public const STATUS_ACK = 'ack'; // Notification has been received and recorded for future processing
    public const STATUS_CONSUMED = 'consumed'; // Notification has been successfully processed
    public const STATUS_FAILED = 'failed'; // There was an error processing the notification

    /**
     * Set CrefoPay Merchant ID
     *
     * @param int $merchantId
     *
     * @return void
     */
    public function setMerchantId($merchantId);

    /**
     * Get CrefoPay Merchant ID
     *
     * @return int
     */
    public function getMerchantId();

    /**
     * Set CrefoPay store ID
     *
     * @param string $storeId
     *
     * @return void
     */
    public function setStoreId($storeId);

    /**
     * Get CrefoPay store ID
     *
     * @return string
     */
    public function getStoreId();

    /**
     * Set increment order ID
     *
     * @param string $orderId
     *
     * @return void
     */
    public function setIncrementOrderId($orderId);

    /**
     * Get increment order ID
     *
     * @return string
     */
    public function getIncrementOrderId();

    /**
     * Set capture ID
     *
     * @param string $captureId
     *
     * @return void
     */
    public function setCaptureId($captureId);

    /**
     * Get capture ID
     *
     * @return string
     */
    public function getCaptureId();

    /**
     * Set merchant reference
     *
     * @param string $merchantReference
     *
     * @return void
     */
    public function setMerchantReference($merchantReference);

    /**
     * Get merchant reference
     *
     * @return string
     */
    public function getMerchantReference();

    /**
     * Set CrefoPay payment reference
     *
     * @param string $paymentReference
     *
     * @return void
     */
    public function setPaymentReference($paymentReference);

    /**
     * Get CrefoPay payment reference
     *
     * @return string
     */
    public function getPaymentReference();

    /**
     * Set CrefoPay user ID
     *
     * @param string $userId
     *
     * @return void
     */
    public function setUserId($userId);

    /**
     * Get CrefoPay user ID
     *
     * @return string
     */
    public function getUserId();

    /**
     * Set amount (formatted as '0.00')
     *
     * @param string $amount
     *
     * @return void
     */
    public function setAmount($amount);

    /**
     * Get amount (formatted as '0.00')
     *
     * @return string
     */
    public function getAmount();

    /**
     * Set currency code (3-letter ISO 4217 alpha)
     *
     * @param string $currency
     *
     * @return void
     */
    public function setCurrency($currency);

    /**
     * Get currency code (3-letter ISO 4217 alpha)
     *
     * @return string
     */
    public function getCurrency();

    /**
     * Set transaction status
     *
     * @param string $transactionStatus
     *
     * @return void
     */
    public function setTransactionStatus($transactionStatus);

    /**
     * Get transaction status
     *
     * @return string
     */
    public function getTransactionStatus();

    /**
     * Set capture status (orderStatus in CrefoPay terms)
     *
     * @param string $captureStatus
     *
     * @return void
     */
    public function setCaptureStatus($captureStatus);

    /**
     * Get capture status (orderStatus in CrefoPay terms)
     *
     * @return string
     */
    public function getCaptureStatus();

    /**
     * Set created at datetime
     *
     * @param string $createdAt
     *
     * @return void
     */
    public function setCreatedAt($createdAt);

    /**
     * Get created at datetime
     *
     * @return string
     */
    public function getCreatedAt();

    /**
     * Set processing status of the notification record
     *
     * @param string $mnsStatus See Status_* constants
     *
     * @return void
     */
    public function setMnsStatus($mnsStatus);

    /**
     * Get processing status of the notification record
     *
     * @return string See STATUS_* constants
     */
    public function getMnsStatus();

    /**
     * Set date and time of record processing (null - if not processed)
     *
     * @param \DateTime|null $processedAt
     *
     * @return void
     */
    public function setProcessedAt($processedAt);

    /**
     * Get date and time of record processing (null - if not processed)
     *
     * @return string|null
     */
    public function getProcessedAt();

    /**
     * Set error details (if record failed to be processed)
     *
     * @param string|null $errorDetails
     *
     * @return void
     */
    public function setErrorDetails($errorDetails);

    /**
     * Get error details (if record failed to be processed)
     *
     * @return string|null
     */
    public function getErrorDetails();

    /**
     * @return int
     */
    public function getNumberOfAttemptsToProcessNotification(): int;

    /**
     * @param int $numberOfAttempts
     */
    public function setNumberOfAttemptsToProcessNotification(int $numberOfAttempts): void;
}
