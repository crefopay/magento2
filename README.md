# CrefoPay Payment Plugin for Magento 2

## Compability

| Version  | Compatibility | Tested | Supported |
|----------|---------------|--------|-----------|
| 2.4.0    | compatible    | yes    | yes       |
| 2.4.1    | compatible    | yes    | yes       |
| 2.4.2    | compatible    | yes    | yes       |
| 2.4.2-p1 | compatible    | yes    | yes       |
| 2.4.x    | compatible    | no     | yes       |
| 2.4.6-p2 | compatible    | yes    | yes       |

### Supported Browsers

* Internet Explorer 11 or later, Microsoft Edge, latest–1
* Firefox latest, latest–1 (any operating system)
* Chrome latest, latest–1 (any operating system)
* Safari latest, latest–1 (Mac OS only)
* Safari Mobile for iPad 2, iPad Mini, iPad with Retina Display (iOS 12 or later), for desktop storefront
* Safari Mobile for iPhone 6 or later; iOS 12 or later, for mobile storefront
* Chrome for mobile latest–1 (Android 4 or later) for mobile storefront

_Here, latest–1 means one major version earlier than the latest released version._

## Requirements

* [Magento 2 technology stack requirements](https://devdocs.magento.com/guides/v2.3/install-gde/system-requirements.html)
* [Magento 2 TLS requirement](https://devdocs.magento.com/guides/v2.3/install-gde/system-requirements_repo-tls1-2.html)
* Security schemes of CrefoPay API requires [TLS 1.2](https://en.wikipedia.org/wiki/Transport_Layer_Security#TLS_1.2) or higher

## Installation via composer

The easiest way to install the extension is to use Composer
Run the following commands:

```shell
composer require crefopay/magento2:1.6.0
```

After, run next magento commands:

```shell
bin/magento module:enable Trilix_CrefoPay
bin/magento setup:upgrade
rm -rf generated/* var/cache/* var/page_cache/* var/view_preprocessed/*
bin/magento setup:static-content:deploy -f
bin/magento setup:di:compile
bin/magento crefopay:amazon-pay:check
```

## Known Issues

Please refer to the section in the [Changelog](CHANGELOG.md#known-issues)
