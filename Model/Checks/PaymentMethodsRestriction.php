<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Checks;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Payment\Model\Checks\SpecificationInterface;
use Trilix\CrefoPay\Model\AmazonPay\AmazonPayCustomerSession;
use Upg\Library\PaymentMethods\Methods;
use Magento\Payment\Model\MethodInterface;
use Magento\Quote\Model\Quote;
use Trilix\CrefoPay\Model\PaymentMethodsCodesMap;
use Trilix\CrefoPay\Model\CrefoPayTransactionRepository;
use Trilix\CrefoPay\Gateway\Config\Config;

class PaymentMethodsRestriction implements SpecificationInterface
{
    /** @var CrefoPayTransactionRepository */
    private $crefoPayTransactionRepository;

    /** @var Config */
    private $config;

    /** @var AmazonPayCustomerSession */
    private $amazonPayCustomerSession;

    /**
     * PaymentMethodsRestriction constructor.
     * @param CrefoPayTransactionRepository $crefoPayTransactionRepository
     * @param Config $config
     * @param AmazonPayCustomerSession $amazonPayCustomerSession
     */
    public function __construct(
        CrefoPayTransactionRepository $crefoPayTransactionRepository,
        Config $config,
        AmazonPayCustomerSession $amazonPayCustomerSession
    ) {
        $this->crefoPayTransactionRepository = $crefoPayTransactionRepository;
        $this->config                        = $config;
        $this->amazonPayCustomerSession      = $amazonPayCustomerSession;
    }

    /**
     * @param MethodInterface $paymentMethod
     * @param Quote $quote
     * @return bool
     * @throws NoSuchEntityException
     */
    public function isApplicable(MethodInterface $paymentMethod, Quote $quote)
    {
        if (!PaymentMethodsCodesMap::isPaymentMethodInCrefoPayGroup($paymentMethod->getCode())) {
            return true;
        }

        if (!$this->config->isActive()) {
            return false;
        }

        $formBlockType = $paymentMethod->getFormBlockType();
        return $this->isAllowed(PaymentMethodsCodesMap::getCrefoPayLibraryCode($paymentMethod->getCode()), $formBlockType, $quote);
    }

    /**
     * @param string $crefoPayCode
     * @param Quote $quote
     * @return bool
     * @throws NoSuchEntityException
     */
    private function isAllowed(string $crefoPayCode, string $formBlockType, Quote $quote): bool
    {
        // Check if this call is initialized from the admin (backend orders)
        if ($formBlockType == "Trilix\CrefoPay\Block\Adminhtml\Form\Bill" || $formBlockType == "Trilix\CrefoPay\Block\Adminhtml\Form\CashInAdvance") {
            return true;
        }

        if ($this->amazonPayCustomerSession->isAmazonPayCheckoutFlow()) {
            if ($crefoPayCode === Methods::PAYMENT_METHOD_TYPE_AMAZON_PAY) {
                return true;
            } else {
                return false;
            }
        }

        $allowedPaymentMethods = $this->crefoPayTransactionRepository
            ->getByQuoteId($quote->getId())->getPaymentMethods();
        $allowedPaymentMethods = json_decode($allowedPaymentMethods, true);

        return in_array($crefoPayCode, array_values($allowedPaymentMethods));
    }
}
