<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Config\Source\Order\Status;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Sales\Model\Order;

/**
 * Order Statuses source model
 */
class NewStatus implements OptionSourceInterface
{
    /**
     * Orders New Statuses
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => Order::STATE_PROCESSING,
                'label' => 'Processing',
            ],
            [
                'value' => Order::STATE_PENDING_PAYMENT,
                'label' => 'Pending Payment',
            ],
        ];
    }
}
