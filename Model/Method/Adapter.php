<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Method;

class Adapter extends \Magento\Payment\Model\Method\Adapter
{
    /**
     * Whether payment has to be automatically captured upon receiving MERCHANTPENDING MNS. Configured in backend.
     *
     * @return bool
     */
    public function isAutoCaptureEnabled(): bool
    {
        return (bool)$this->getConfigData('auto_capture');
    }
}
