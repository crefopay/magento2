<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Adminhtml\Source;

use Magento\Framework\Option\ArrayInterface;
use Upg\Library\Risk\RiskClass as CrefoPayRiskClass;

class RiskClass implements ArrayInterface
{
    /**
     * Possible risk classes
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => CrefoPayRiskClass::RISK_CLASS_TRUSTED,
                'label' => 'Trusted Risk Class',
            ],
            [
                'value' => CrefoPayRiskClass::RISK_CLASS_DEFAULT,
                'label' => 'Default Risk Class'
            ],
            [
                'value' => CrefoPayRiskClass::RISK_CLASS_HIGH,
                'label' => 'High Risk Class'
            ]
        ];
    }
}
