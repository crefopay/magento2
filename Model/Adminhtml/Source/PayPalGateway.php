<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Adminhtml\Source;

use Magento\Framework\Data\OptionSourceInterface;

class PayPalGateway implements OptionSourceInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [[
            'label' => __('PayPal 2 Gateway'),
            'value' => 'paypal2',
        ], [
            'label' => __('PayPal 1 Gateway'),
            'value' => 'paypal1',
        ]];
    }
}
