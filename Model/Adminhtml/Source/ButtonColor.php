<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Adminhtml\Source;

use Magento\Framework\Data\OptionSourceInterface;

class ButtonColor implements OptionSourceInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [[
            'label' => __('Gold'),
            'value' => 'Gold',
        ], [
            'label' => __('Light Gray'),
            'value' => 'LightGray',
        ], [
            'label' => __('Dark Gray'),
            'value' => 'DarkGray',
        ]];
    }
}
