<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Mns extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('crefopay_mns', 'id');
    }
}
