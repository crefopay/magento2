<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\ResourceModel\Mns;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(\Trilix\CrefoPay\Model\Mns\MnsEvent::class, \Trilix\CrefoPay\Model\ResourceModel\Mns::class);
    }
}
