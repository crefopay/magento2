<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model;

class PaymentMethodsCodesMap
{
    /**
     * @return array
     */
    private static function getCrefoPayCodes(): array
    {
        return [
            \Trilix\CrefoPay\Model\Ui\Bill\ConfigProvider::CODE =>
                \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_BILL,
            \Trilix\CrefoPay\Model\Ui\CashInAdvance\ConfigProvider::CODE =>
                \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_PREPAID,
            \Trilix\CrefoPay\Model\Ui\CashOnDelivery\ConfigProvider::CODE =>
                \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_COD,
            \Trilix\CrefoPay\Model\Ui\CreditCard\ConfigProvider::CODE =>
                \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_CC,
            \Trilix\CrefoPay\Model\Ui\CreditCard3D\ConfigProvider::CODE =>
                \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_CC3D,
            \Trilix\CrefoPay\Model\Ui\DirectDebit\ConfigProvider::CODE =>
                \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_DD,
            \Trilix\CrefoPay\Model\Ui\PayPal\ConfigProvider::CODE =>
                \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_PAYPAL,
            \Trilix\CrefoPay\Model\Ui\Sofort\ConfigProvider::CODE =>
                \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_SU,
            \Trilix\CrefoPay\Model\Ui\ZiniaInvoice\ConfigProvider::CODE =>
                \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_ZINIA_BNPL,
            \Trilix\CrefoPay\Model\Ui\ZiniaRatepay\ConfigProvider::CODE =>
                \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_ZINIA_INSTALLMENT,
            \Trilix\CrefoPay\Model\Ui\Ideal\ConfigProvider::CODE =>
                \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_IDEAL,
            \Trilix\CrefoPay\Model\Ui\AmazonPay\ConfigProvider::CODE =>
                \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_AMAZON_PAY,
            \Trilix\CrefoPay\Model\Ui\GooglePay\ConfigProvider::CODE =>
                \Upg\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_GOOGLE_PAY,                
        ];
    }

    /**
     * @param string $code
     * @return bool
     */
    public static function isPaymentMethodInCrefoPayGroup(string $code): bool
    {
        return array_key_exists($code, self::getCrefoPayCodes());
    }

    /**
     * @param string $code
     * @return string
     */
    public static function getCrefoPayLibraryCode(string $code): string
    {
        return self::getCrefoPayCodes()[$code];
    }
}
