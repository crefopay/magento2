<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model;

use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Payment;
use Magento\Sales\Model\Order\Payment\Transaction\BuilderFactory;
use Psr\Log\LoggerInterface;
use Upg\Library\Request as UpgRequest;
use Upg\Library\Response\SuccessResponse;

class TransactionService
{
    /**
     * Using factory instead of Builder object itself because of a bug:
     * Injected Builder instance is a singleton, so it is safer to call reset() before building. However, that method removes
     * some of the properties by unset(), which later causes warning on build().
     *
     * @var BuilderFactory
     */
    private $builderFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * TransactionService constructor.
     *
     * @param BuilderFactory $builder
     * @param LoggerInterface  $logger
     */
    public function __construct(BuilderFactory $builder, LoggerInterface $logger)
    {
        $this->builderFactory = $builder;
        $this->logger = $logger;
    }

    /**
     * Add transactional info to Payment object. Transaction is saved later along with the Order object.
     *
     * @param PaymentDataObjectInterface $paymentDO
     * @param UpgRequest\AbstractRequest $request
     * @param SuccessResponse            $response
     */
    public function addTransaction(
        PaymentDataObjectInterface $paymentDO,
        UpgRequest\AbstractRequest $request,
        SuccessResponse $response
    ) {
        $payment = $paymentDO->getPayment();

        if ($payment instanceof Payment) {
            $order = $payment->getOrder();

            if ($request instanceof UpgRequest\Reserve) {
                $txId = $this->generateTransactionId($order);
                $payment->setTransactionId($txId);
                $payment->setIsTransactionClosed(false);

                if ($payment->getMethodInstance()->isGateway()) {
                    $payment->setIsTransactionPending(true);
                }
            }

            if ($request instanceof UpgRequest\Capture) {
                // Required later by Refund command (if invoked)
                $payment->setTransactionAdditionalInfo('capture_id', $request->getCaptureID());
            }

            foreach ($response->getAllData() as $k => $v) {
                $payment->setTransactionAdditionalInfo($k, $v);
            }
        } else {
            $this->logger->warning(
                sprintf(
                    'Unsaved transaction: expected instance of %s, got instance of %s',
                    Payment::class,
                    get_class($payment)
                ),
                $request->toArray()
            );
        }
    }

    /**
     * Generate unique transaction ID for Magento. This is not passed to CrefoPay.
     *
     * @param Order $order
     *
     * @return string
     */
    private function generateTransactionId(Order $order): string
    {
        return sha1($order->getIncrementId() . microtime(true));
    }
}
