<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\AmazonPay;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class AmazonPayConfig
{
    /** @var ScopeConfigInterface */
    private $scopeConfig;

    /**
     * CrefoPay Amazon Pay Config constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Is Amazon Pay enabled?
     *
     * @return bool
     */
    public function isEnabled($scope = ScopeInterface::SCOPE_STORE, $scopeCode = null)
    {
        $isAmazonPayModuleEnabled = $this->scopeConfig->isSetFlag(
            'payment/crefopay_amazon_pay/active',
            $scope,
            $scopeCode
        );

        $isAmazonHiddenModeEnabled = $this->scopeConfig->getValue('payment/crefopay_amazon_pay/hidden_button_mode', $scope, $scopeCode);

        return $isAmazonPayModuleEnabled && !$isAmazonHiddenModeEnabled;
    }

    /**
     * @param string $scope
     * @param null $scopeCode
     * @return string
     */
    public function getButtonColor(string $scope = ScopeInterface::SCOPE_STORE, $scopeCode = null): string
    {
        return empty($this->scopeConfig->getValue('payment/crefopay_amazon_pay/button_color', $scope, $scopeCode)) ? '' : $this->scopeConfig->getValue('payment/crefopay_amazon_pay/button_color', $scope, $scopeCode);
    }

    /**
     * @param string $scope
     * @param null $scopeCode
     * @return bool
     */
    public function isAutoCaptureEnabled(string $scope = ScopeInterface::SCOPE_STORE, $scopeCode = null): bool
    {
        return (bool) $this->scopeConfig->getValue('payment/crefopay_amazon_pay/auto_capture', $scope, $scopeCode);
    }

    /**
     * @param string $scope
     * @param null $scopeCode
     * @return bool
     */
    public function isPoBoxesShippingRestricted(string $scope = ScopeInterface::SCOPE_STORE, $scopeCode = null): bool
    {
        return (bool) $this->scopeConfig->getValue('payment/crefopay_amazon_pay/shipping_restrictions_po_boxes', $scope, $scopeCode);
    }

    /**
     * @param string $scope
     * @param null $scopeCode
     * @return bool
     */
    public function isPackstationsShippingRestricted(string $scope = ScopeInterface::SCOPE_STORE, $scopeCode = null): bool
    {
        return (bool) $this->scopeConfig->getValue('payment/crefopay_amazon_pay/shipping_restrictions_packstations', $scope, $scopeCode);
    }

    /**
     * @param string $scope
     * @param null $scopeCode
     * @return false|string
     */
    public function getDeliverySpecifications(string $scope = ScopeInterface::SCOPE_STORE, $scopeCode = null)
    {
        $isOnlySpecificCountriesAllowed = (bool) $this->scopeConfig->getValue('payment/crefopay_amazon_pay/allowspecific', $scope, $scopeCode);
        $allowedCountries = $this->scopeConfig->getValue('payment/crefopay_amazon_pay/specificcountry', $scope, $scopeCode);
        $result = '';
        $restrictions = [];
        $countriesRestrictions = [];
        if ($isOnlySpecificCountriesAllowed) {
            $allowedCountries = explode(",", $allowedCountries);
            foreach ($allowedCountries as $country) {
                $countriesRestrictions[$country] = "{}";
            }
        }

        $shippingRestrictions = [];
        if ($this->isPoBoxesShippingRestricted()) {
            $shippingRestrictions[] = 'RestrictPOBoxes';
        }
        if ($this->isPackstationsShippingRestricted()) {
            $shippingRestrictions[] = 'RestrictPackstations';
        }
        if (!empty($shippingRestrictions)) {
            $restrictions['specialRestrictions'] = $shippingRestrictions;
        }
        if (!empty($countriesRestrictions)) {
            $restrictions['addressRestrictions'] = [
                "type" => "Allowed",
                "restrictions" => $countriesRestrictions
            ];
        }
        if (!empty($shippingRestrictions)) {
            $restrictions['specialRestrictions'] = $shippingRestrictions;
        }

        if (!empty($restrictions)) {
            $result = json_encode($restrictions);
        }

        return $result;
    }
}
