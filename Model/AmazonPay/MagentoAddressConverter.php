<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\AmazonPay;

use Upg\Library\Response\AbstractResponse;

class MagentoAddressConverter
{
    /**
     * @param AbstractResponse $response
     * @return array
     */
    public function convert(AbstractResponse $response): array
    {
        $address = $response->getShippingAddress();
        $streetName = $address->getStreet();
        $no = $address->getNo();

        $name = explode(' ', $address->getName());
        if (count($name) < 2) {
            $name[1] = '';
        }

        return [
            'city'       => $address->getCity(),
            'street'     => [$streetName, $no],
            'postcode'   => $address->getZip(),
            'country_id' => $address->getCountry(),
            'email'      => $response->getAdditionalData()['customerEmail'],
            'company'    => '',
            'firstname'  => $name[0],
            'lastname'   => $name[1],
        ];
    }
}
