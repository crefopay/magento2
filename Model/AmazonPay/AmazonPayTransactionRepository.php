<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\AmazonPay;

use Trilix\CrefoPay\Api\AmazonPay\AmazonPayTransactionRepositoryInterface;
use Trilix\CrefoPay\Api\Data\AmazonPay\AmazonPayTransactionInterface;

class AmazonPayTransactionRepository implements AmazonPayTransactionRepositoryInterface
{
    /** AmazonPayTransactionFactory */
    private $amazonPayTransactionFactory;

    /**
     * AmazonPayTransactionRepository constructor.
     * @param AmazonPayTransactionFactory $amazonPayTransactionFactory
     */
    public function __construct(
        AmazonPayTransactionFactory $amazonPayTransactionFactory
    ) {
        $this->amazonPayTransactionFactory = $amazonPayTransactionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function save(AmazonPayTransactionInterface $amazonPayTransaction): AmazonPayTransactionInterface
    {
        $obj = $this->amazonPayTransactionFactory->create();
        $obj->getResource()->load($obj, $amazonPayTransaction->getOrderId(), 'order_id');
        if ($obj->getId()) {
            $obj->setCrefoPayOrderId($amazonPayTransaction->getCrefoPayOrderId());
            $amazonPayTransaction = $obj;
        }

        $amazonPayTransaction->getResource()->save($amazonPayTransaction);

        return $amazonPayTransaction;
    }

    /**
     * {@inheritdoc}
     */
    public function getByCrefoPayOrderId(string $crefoPayOrderId): ?AmazonPayTransactionInterface
    {
        $obj = $this->amazonPayTransactionFactory->create();
        $obj->getResource()->load($obj, $crefoPayOrderId, 'crefopay_order_id');
        if (!$obj->getId()) {
            return null;
        }
        return $obj;
    }

    /**
     * {@inheritdoc}
     */
    public function getByOrderId(string $orderId): ?AmazonPayTransactionInterface
    {
        $obj = $this->amazonPayTransactionFactory->create();
        $obj->getResource()->load($obj, $orderId, 'order_id');
        if (!$obj->getId()) {
            return null;
        }
        return $obj;
    }
}
