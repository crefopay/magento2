<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\AmazonPay;

use Magento\Customer\Model\Session as CustomerSession;

class AmazonPayCustomerSession
{
    /** @var CustomerSession */
    private $customerSession;

    /** @var AmazonPayConfig */
    private $amazonPayConfig;

    /**
     * AmazonPayCustomerSession constructor.
     * @param CustomerSession $customerSession
     * @param AmazonPayConfig $amazonPayConfig
     */
    public function __construct(
        CustomerSession $customerSession,
        AmazonPayConfig $amazonPayConfig
    ) {
        $this->customerSession = $customerSession;
        $this->amazonPayConfig = $amazonPayConfig;
    }

    /**
     * @param AmazonSessionData $amazonSessionData
     */
    public function saveAmazonSessionDataInCustomerSession(AmazonSessionData $amazonSessionData)
    {
        $this->customerSession->setCrefoPayAmazonData($amazonSessionData);
    }

    /**
     * @return AmazonSessionData
     */
    public function getAmazonSessionDataFromCustomerSession(): ?AmazonSessionData
    {
        $data = null;
        if ($this->customerSession->getCrefoPayAmazonData()) {
            $data = $this->customerSession->getCrefoPayAmazonData();
        }
        return $data;
    }

    public function deleteAmazonSessionDataFromCustomerSession(): void
    {
        $this->customerSession->unsCrefoPayAmazonData();
    }

    /***
     * @return CustomerSession
     */
    public function getCustomerSession(): CustomerSession
    {
        return $this->customerSession;
    }

    /**
     * @return bool
     */
    public function isAmazonPayCheckoutFlow(): bool
    {
        return $this->amazonPayConfig->isEnabled() && $this->getAmazonSessionDataFromCustomerSession();
    }
}
