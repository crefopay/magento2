<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\AmazonPay;

use Trilix\CrefoPay\Api\AmazonPay\CheckoutAddressProviderInterface;
use Trilix\CrefoPay\Client\Request\GetTransactionStatusRequestFactory;
use Trilix\CrefoPay\Client\Transport;

class CheckoutAddressProvider implements CheckoutAddressProviderInterface
{
    /** @var Transport  */
    private $transport;

    /** @var GetTransactionStatusRequestFactory  */
    private $getTransactionStatusRequestFactory;

    /** @var MagentoAddressConverter */
    private $magentoAddressConverter;

    /**
     * CheckoutAddressProvider constructor.
     * @param Transport $transport
     * @param GetTransactionStatusRequestFactory $getTransactionStatusRequestFactory
     * @param MagentoAddressConverter $magentoAddressConverter
     */
    public function __construct(
        Transport $transport,
        GetTransactionStatusRequestFactory $getTransactionStatusRequestFactory,
        MagentoAddressConverter $magentoAddressConverter
    ) {
        $this->transport = $transport;
        $this->getTransactionStatusRequestFactory = $getTransactionStatusRequestFactory;
        $this->magentoAddressConverter = $magentoAddressConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function getBillingAddress(string $orderId): array
    {
        return $this->getAmazonAddress($orderId);
    }

    /**
     * {@inheritdoc}
     */
    public function getShippingAddress(string $orderId): array
    {
        return $this->getAmazonAddress($orderId);
    }

    /**
     * @param string $orderId
     * @return array
     */
    private function getAmazonAddress(string $orderId): array
    {
        $getTransactionStatusRequest = $this->getTransactionStatusRequestFactory->create($orderId);
        $response = $this->transport->sendRequest($getTransactionStatusRequest);

        return [$this->magentoAddressConverter->convert($response)];
    }
}
