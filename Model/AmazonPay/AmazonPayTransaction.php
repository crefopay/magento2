<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\AmazonPay;

use Magento\Framework\Model\AbstractModel;
use Trilix\CrefoPay\Api\Data\AmazonPay\AmazonPayTransactionInterface;
use Trilix\CrefoPay\Model\ResourceModel\AmazonPay\AmazonPayTransaction as AmazonPayTransactionResource;

class AmazonPayTransaction extends AbstractModel implements AmazonPayTransactionInterface
{
    protected function _construct()
    {
        $this->_init(AmazonPayTransactionResource::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getOrderId(): string
    {
        return $this->getData('order_id');
    }

    /**
     * {@inheritdoc}
     */
    public function getCrefoPayOrderId(): string
    {
        return $this->getData('crefopay_order_id');
    }

    /**
     * {@inheritdoc}
     */
    public function setOrderId(string $orderId): void
    {
        $this->setData('order_id', $orderId);
    }

    /**
     * {@inheritdoc}
     */
    public function setCrefoPayOrderId(string $crefoPayOrderId)
    {
        $this->setData('crefopay_order_id', $crefoPayOrderId);
    }
}
