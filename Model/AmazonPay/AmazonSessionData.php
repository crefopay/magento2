<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\AmazonPay;

class AmazonSessionData
{
    /** @var string */
    private $orderId;

    /** @var string */
    private $sessionId;

    /**
     * AmazonSessionData constructor.
     * @param string $orderId
     * @param string $sessionId
     */
    public function __construct(
        string $orderId,
        string $sessionId
    ) {
        $this->orderId = $orderId;
        $this->sessionId = $sessionId;
    }

    /**
     * @return string
     */
    public function getOrderId(): string
    {
        return $this->orderId;
    }

    /**
     * @return string
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }
}
