<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model;

use Magento\Framework\Model\AbstractModel;
use Trilix\CrefoPay\Api\Data\CrefoPayTransactionInterface;

class CrefoPayTransaction extends AbstractModel implements CrefoPayTransactionInterface
{
    protected function _construct()
    {
        $this->_init(ResourceModel\CrefoPayTransaction::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getQuoteId(): int
    {
        return $this->getData('quote_id');
    }

    /**
     * {@inheritdoc}
     */
    public function getCrefoPayOrderId(): string
    {
        return $this->getData('crefopay_order_id');
    }

    /**
     * {@inheritdoc}
     */
    public function getPaymentMethods(): string
    {
        return $this->getData('payment_methods');
    }

    /**
     * {@inheritdoc}
     */
    public function getCrefoPayUserId(): string
    {
        return $this->getData('crefopay_user_id');
    }

    /**
     * {@inheritdoc}
     */
    public function setQuoteId($quoteId): void
    {
        $this->setData('quote_id', $quoteId);
    }

    /**
     * {@inheritdoc}
     */
    public function setPaymentMethods(string $paymentMethods)
    {
        $this->setData('payment_methods', $paymentMethods);
    }

    /**
     * {@inheritdoc}
     */
    public function setCrefoPayUserId(string $crefoPayUserId)
    {
        $this->setData('crefopay_user_id', $crefoPayUserId);
    }

    /**
     * {@inheritdoc}
     */
    public function setCrefoPayOrderId(string $crefoPayOrderId)
    {
        $this->setData('crefopay_order_id', $crefoPayOrderId);
    }
}
