<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Ui\CashInAdvance;

use Magento\Checkout\Model\ConfigProviderInterface;
use Trilix\CrefoPay\Gateway\Config\Config;
use Trilix\CrefoPay\Model\PaymentMethodsCodesMap;

/**
 * Class ConfigProvider
 */
class ConfigProvider implements ConfigProviderInterface
{
    public const CODE = 'crefopay_cash_in_advance';

    /**
     * @var Config
     */
    private $config;

    /**
     * Initialize dependencies.
     *
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig()
    {
        return [
            'payment' => [
                self::CODE => [
                    'isActive'           => $this->config->isActive(),
                    'publicToken'        => $this->config->getPublicToken(),
                    'secureFieldsUrl'    => $this->config->getSecureFieldsUrl(),
                    'secureFieldsJsUrl'  => $this->config->getSecureFieldsJsUrl(),
                    'smartSignupUrl'     => $this->config->getSmartSignupUrl(),
                    'crefoPayMethodCode' => PaymentMethodsCodesMap::getCrefoPayLibraryCode(self::CODE),
                    'logo'               => $this->config->getLogoUrl(self::CODE)
                ]
            ]
        ];
    }
}
