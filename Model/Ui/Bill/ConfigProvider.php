<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Ui\Bill;

use Magento\Checkout\Model\ConfigProviderInterface;
use Trilix\CrefoPay\Gateway\Config\Config;
use Trilix\CrefoPay\Model\CrefoPayDob;
use Trilix\CrefoPay\Model\CrefoPayGender;
use Trilix\CrefoPay\Model\PaymentMethodsCodesMap;

/**
 * Class ConfigProvider
 */
class ConfigProvider implements ConfigProviderInterface
{
    public const CODE = 'crefopay_bill';

    /** @var Config */
    private $config;

    /** @var CrefoPayDob */
    private $crefoPayDob;

    /** @var CrefoPayDob */
    private $crefoPayGender;

    /**
     * @param Config $config
     * @param CrefoPayDob $crefoPayDob
     * @param CrefoPayGender $crefoPayGender
     */
    public function __construct(Config $config, CrefoPayDob $crefoPayDob, CrefoPayGender $crefoPayGender)
    {
        $this->config = $config;
        $this->crefoPayDob = $crefoPayDob;
        $this->crefoPayGender = $crefoPayGender;
    }

    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig()
    {
        return [
            'payment' => [
                self::CODE => [
                    'isActive' => $this->config->isActive(),
                    'publicToken' => $this->config->getPublicToken(),
                    'secureFieldsUrl' => $this->config->getSecureFieldsUrl(),
                    'secureFieldsJsUrl' => $this->config->getSecureFieldsJsUrl(),
                    'smartSignupUrl' => $this->config->getSmartSignupUrl(),
                    'crefoPayMethodCode' => PaymentMethodsCodesMap::getCrefoPayLibraryCode(self::CODE),
                    'logo' => $this->config->getLogoUrl(self::CODE),
                    'dob' => $this->crefoPayDob->getDob(),
                    'gender' => $this->crefoPayGender->getGender(),
                    'dateFormat' => $this->crefoPayDob->getDateFormatForJs(),
                    'isB2BEnabled' => $this->config->isB2BEnabled()
                ]
            ]
        ];
    }
}
