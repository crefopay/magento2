<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Ui\CreditCard;

use Magento\Checkout\Model\ConfigProviderInterface;
use Trilix\CrefoPay\Gateway\Config\Config;
use Trilix\CrefoPay\Model\PaymentMethodsCodesMap;

/**
 * Class ConfigProvider
 */
class ConfigProvider implements ConfigProviderInterface
{
    public const CODE = 'crefopay_credit_card';

    public const CC_VAULT_CODE = 'crefopay_credit_card_vault';

    /**
     * @var Config
     */
    private $config;

    /**
     * Initialize dependencies.
     *
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig(): array
    {
        return [
            'payment' => [
                self::CODE => [
                    'isActive'           => $this->config->isActive(),
                    'publicToken'        => $this->config->getPublicToken(),
                    'crefoPayMethodCode' => PaymentMethodsCodesMap::getCrefoPayLibraryCode(self::CODE),
                    'ccVaultCode'        => self::CC_VAULT_CODE,
                    'logo'               => $this->config->getLogoUrl(self::CODE)
                ]
            ]
        ];
    }
}
