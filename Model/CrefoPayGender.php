<?php

namespace Trilix\CrefoPay\Model;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Upg\Library\Request\Objects\Person;

class CrefoPayGender
{
    /** @var CustomerRepositoryInterface */
    private $customerRepository;

    /** @var Session */
    private $customerSession;

    /**
     * CrefoPayDob constructor.
     * @param CustomerRepositoryInterface $customerRepository
     * @param Session $customerSession
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        Session $customerSession
    ) {
        $this->customerRepository = $customerRepository;
        $this->customerSession = $customerSession;
    }

    /**
     * @return string|null
     *
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getGender(): ?string
    {
        $gender = null;
        $customerId = $this->customerSession->getCustomerId();
        if ($customerId) {
            $customer = $this->customerRepository->getById($customerId);
            $genderId = $customer->getGender();
            switch ((int) $genderId) {
                case 1:
                    $gender = Person::SALUTATIONMALE;
                    break;
                case 2:
                    $gender = Person::SALUTATIONFEMALE;
                    break;
                case 3:
                    $gender = Person::SALUTATIONVARIOUS;
                    break;
            }
        }
        return $gender;
    }
}
