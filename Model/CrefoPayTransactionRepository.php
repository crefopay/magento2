<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model;

use Trilix\CrefoPay\Api\Data\CrefoPayTransactionInterface;
use Trilix\CrefoPay\Api\CrefoPayTransactionRepositoryInterface;

use Magento\Framework\Exception\NoSuchEntityException;

class CrefoPayTransactionRepository implements CrefoPayTransactionRepositoryInterface
{
    /** CrefoPayTransactionFactory */
    private $crefoPayTransactionFactory;

    /**
     * CrefoPayTransactionRepository constructor.
     * @param CrefoPayTransactionFactory $crefoPayTransactionFactory
     */
    public function __construct(
        CrefoPayTransactionFactory $crefoPayTransactionFactory
    ) {
        $this->crefoPayTransactionFactory = $crefoPayTransactionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function save(CrefoPayTransactionInterface $crefoPayTransaction): CrefoPayTransactionInterface
    {
        $obj = $this->crefoPayTransactionFactory->create();
        $obj->getResource()->load($obj, $crefoPayTransaction->getQuoteId(), 'quote_id');
        if ($obj->getId()) {
            $obj->setCrefoPayOrderId($crefoPayTransaction->getCrefoPayOrderId());
            $obj->setPaymentMethods($crefoPayTransaction->getPaymentMethods());
            $obj->setCrefoPayUserId($crefoPayTransaction->getCrefoPayUserId());

            $crefoPayTransaction = $obj;
        }

        $crefoPayTransaction->getResource()->save($crefoPayTransaction);

        return $crefoPayTransaction;
    }

    /**
     * {@inheritdoc}
     */
    public function getByQuoteId($quoteId): CrefoPayTransactionInterface
    {
        $obj = $this->crefoPayTransactionFactory->create();
        $obj->getResource()->load($obj, $quoteId, 'quote_id');
        if (!$obj->getId()) {
            throw new NoSuchEntityException(__('Unable to find with ID "%1"', $quoteId));
        }
        return $obj;
    }
}
