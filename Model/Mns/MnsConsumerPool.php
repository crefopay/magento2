<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Mns;

class MnsConsumerPool
{
    /**
     * @var MnsConsumerInterface[]
     */
    private $consumerCollection;

    public function __construct(array $consumerCollection)
    {
        foreach ($consumerCollection as $consumer) {
            if (!$consumer instanceof MnsConsumerInterface) {
                throw new \InvalidArgumentException(
                    __('MNS consumer should be an instance of \Trilix\CrefoPay\Model\Mns\ConsumerInterface')
                );
            }
        }

        $this->consumerCollection = $consumerCollection;
    }

    /**
     * @param MnsEvent $event
     *
     * @return MnsConsumerInterface[]
     */
    public function getConsumersByEvent(MnsEvent $event): array
    {
        $applicableConsumers = [];

        /** @var MnsConsumerInterface $consumer */
        foreach ($this->consumerCollection as $consumer) {
            if ($consumer->getTransactionStatusName() && $consumer->getTransactionStatusName() !== $event->getTransactionStatus()) {
                continue;
            }

            if ($consumer->getCaptureStatusName() && $consumer->getCaptureStatusName() !== $event->getCaptureStatus()) {
                continue;
            }

            $applicableConsumers[] = $consumer;
        }

        return $applicableConsumers;
    }
}
