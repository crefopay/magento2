<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Mns\ProcessSupervisor;

use Trilix\CrefoPay\Model\Mns\MnsEvent;

class DummySupervisor implements SupervisorInterface
{
    /**
     * {@inheritdoc}
     */
    public function setTotalCount(int $itemsCount)
    {
        // Intentionally left blank
    }

    /**
     * {@inheritdoc}
     */
    public function ok(MnsEvent $mns)
    {
        // Intentionally left blank
    }

    /**
     * {@inheritdoc}
     */
    public function fail(MnsEvent $mns)
    {
        // Intentionally left blank
    }
}
