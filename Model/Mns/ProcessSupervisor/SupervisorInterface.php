<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Mns\ProcessSupervisor;

use Trilix\CrefoPay\Model\Mns\MnsEvent;

interface SupervisorInterface
{
    /**
     * Report number of total found records to process
     *
     * @param int $itemsCount
     *
     * @return void
     */
    public function setTotalCount(int $itemsCount);

    /**
     * Report successfully processed event
     *
     * @param MnsEvent $mns
     *
     * @return void
     */
    public function ok(MnsEvent $mns);

    /**
     * Report failed attempt to process an event
     *
     * @param MnsEvent $mns
     *
     * @return void
     */
    public function fail(MnsEvent $mns);
}
