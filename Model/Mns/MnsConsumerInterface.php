<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Mns;

interface MnsConsumerInterface
{
    /**
     * Get the CrefoPay transaction status name consumer responds to ('' for every)
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/transaction-and-order-status
     *
     * @return string
     */
    public function getTransactionStatusName(): string;

    /**
     * Get the CrefoPay capture status (order status) name consumer responds to ('' for every)
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/transaction-and-order-status
     *
     * @return string
     */
    public function getCaptureStatusName(): string;

    /**
     * Process the MNS event
     *
     * @param MnsEvent $event
     */
    public function process(MnsEvent $event);
}
