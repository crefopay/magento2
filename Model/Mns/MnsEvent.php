<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Mns;

use Magento\Framework\Model\AbstractModel;
use Trilix\CrefoPay\Api\Data\CrefoPayMnsInterface;

class MnsEvent extends AbstractModel implements CrefoPayMnsInterface
{
    protected function _construct()
    {
        $this->_init(\Trilix\CrefoPay\Model\ResourceModel\Mns::class);
    }

    public static function getAvailableMnsStatuses()
    {
        return [
            CrefoPayMnsInterface::STATUS_ACK,
            CrefoPayMnsInterface::STATUS_CONSUMED,
            CrefoPayMnsInterface::STATUS_FAILED,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function setMerchantId($merchantId)
    {
        $this->setData(CrefoPayMnsInterface::MERCHANT_ID, $merchantId);
    }

    /**
     * {@inheritdoc}
     */
    public function getMerchantId()
    {
        return $this->getData(CrefoPayMnsInterface::MERCHANT_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setStoreId($storeId)
    {
        $this->setData(CrefoPayMnsInterface::STORE_ID, $storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function getStoreId()
    {
        return $this->getData(CrefoPayMnsInterface::STORE_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setIncrementOrderId($orderId)
    {
        $this->setData(CrefoPayMnsInterface::ORDER_INCREMENT_ID, $orderId);
    }

    /**
     * {@inheritdoc}
     */
    public function getIncrementOrderId()
    {
        return $this->getData(CrefoPayMnsInterface::ORDER_INCREMENT_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setCaptureId($captureId)
    {
        $this->setData(CrefoPayMnsInterface::CAPTURE_ID, $captureId);
    }

    /**
     * {@inheritdoc}
     */
    public function getCaptureId()
    {
        return $this->getData(CrefoPayMnsInterface::CAPTURE_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setMerchantReference($merchantReference)
    {
        $this->setData(CrefoPayMnsInterface::MERCHANT_REFERENCE, $merchantReference);
    }

    /**
     * {@inheritdoc}
     */
    public function getMerchantReference()
    {
        return $this->getData(CrefoPayMnsInterface::MERCHANT_REFERENCE);
    }

    /**
     * {@inheritdoc}
     */
    public function setPaymentReference($paymentReference)
    {
        $this->setData(CrefoPayMnsInterface::PAYMENT_REFERENCE, $paymentReference);
    }

    /**
     * {@inheritdoc}
     */
    public function getPaymentReference()
    {
        return $this->getData(CrefoPayMnsInterface::PAYMENT_REFERENCE);
    }

    /**
     * {@inheritdoc}
     */
    public function setUserId($userId)
    {
        $this->setData(CrefoPayMnsInterface::USER_ID, $userId);
    }

    /**
     * {@inheritdoc}
     */
    public function getUserId()
    {
        return $this->getData(CrefoPayMnsInterface::USER_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setAmount($amount)
    {
        $this->setData(CrefoPayMnsInterface::AMOUNT, $amount);
    }

    /**
     * {@inheritdoc}
     */
    public function getAmount()
    {
        return $this->getData(CrefoPayMnsInterface::AMOUNT);
    }

    /**
     * {@inheritdoc}
     */
    public function setCurrency($currency)
    {
        $this->setData(CrefoPayMnsInterface::CURRENCY, $currency);
    }

    /**
     * {@inheritdoc}
     */
    public function getCurrency()
    {
        return $this->getData(CrefoPayMnsInterface::CURRENCY);
    }

    /**
     * {@inheritdoc}
     */
    public function setTransactionStatus($transactionStatus)
    {
        $this->setData(CrefoPayMnsInterface::TRANSACTION_STATUS, $transactionStatus);
    }

    /**
     * {@inheritdoc}
     */
    public function getTransactionStatus()
    {
        return $this->getData(CrefoPayMnsInterface::TRANSACTION_STATUS);
    }

    /**
     * {@inheritdoc}
     */
    public function setCaptureStatus($captureStatus)
    {
        $this->setData(CrefoPayMnsInterface::CAPTURE_STATUS, $captureStatus);
    }

    /**
     * {@inheritdoc}
     */
    public function getCaptureStatus()
    {
        return $this->getData(CrefoPayMnsInterface::CAPTURE_STATUS);
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedAt($createdAt)
    {
        $this->setData(CrefoPayMnsInterface::CREATED_AT, $createdAt);
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedAt()
    {
        return $this->getData(CrefoPayMnsInterface::CREATED_AT);
    }

    /**
     * {@inheritdoc}
     */
    public function setMnsStatus($mnsStatus)
    {
        if (!in_array($mnsStatus, self::getAvailableMnsStatuses())) {
            throw new \InvalidArgumentException(sprintf('Invalid status "%s" for MNS notification', $mnsStatus));
        }

        $this->setData(CrefoPayMnsInterface::MNS_STATUS, $mnsStatus);
    }

    /**
     * {@inheritdoc}
     */
    public function getMnsStatus()
    {
        return $this->getData(CrefoPayMnsInterface::MNS_STATUS);
    }

    /**
     * {@inheritdoc}
     */
    public function setProcessedAt($processedAt)
    {
        $this->setData(CrefoPayMnsInterface::PROCESSED_AT, $processedAt);
    }

    /**
     * {@inheritdoc}
     */
    public function getProcessedAt()
    {
        return $this->getData(CrefoPayMnsInterface::PROCESSED_AT);
    }

    /**
     * {@inheritdoc}
     */
    public function setErrorDetails($errorDetails)
    {
        $this->setData(CrefoPayMnsInterface::ERROR_DETAILS, $errorDetails);
    }

    /**
     * {@inheritdoc}
     */
    public function getErrorDetails()
    {
        return $this->getData(CrefoPayMnsInterface::ERROR_DETAILS);
    }

    /**
     * {@inheritdoc}
     */
    public function getNumberOfAttemptsToProcessNotification(): int
    {
        return (int) $this->getData(CrefoPayMnsInterface::NUMBER_OF_ATTEMPTS_TO_PROCESS_NOTIFICATION);
    }

    /**
     * {@inheritdoc}
     */
    public function setNumberOfAttemptsToProcessNotification(int $numberOfAttempts): void
    {
        $this->setData(CrefoPayMnsInterface::NUMBER_OF_ATTEMPTS_TO_PROCESS_NOTIFICATION, $numberOfAttempts);
    }
}
