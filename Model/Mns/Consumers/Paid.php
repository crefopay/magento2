<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Mns\Consumers;

use Trilix\CrefoPay\Client\Constants;
use Trilix\CrefoPay\Model\Mns\MnsConsumerInterface;
use Trilix\CrefoPay\Model\Mns\MnsEvent;
use Trilix\CrefoPay\Helper\Order as OrderHelper;
use Magento\Framework\Pricing\PriceCurrencyInterface;

class Paid extends AbstractConsumer implements MnsConsumerInterface
{
    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * Paid constructor.
     *
     * @param OrderHelper            $orderHelper
     * @param PriceCurrencyInterface $priceCurrency
     */
    public function __construct(OrderHelper $orderHelper, PriceCurrencyInterface $priceCurrency)
    {
        parent::__construct($orderHelper);
        $this->priceCurrency = $priceCurrency;
    }

    /**
     * {@inheritdoc}
     */
    public function getTransactionStatusName(): string
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function getCaptureStatusName(): string
    {
        return Constants::O_PAID;
    }

    /**
     * @param MnsEvent $event
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Trilix\CrefoPay\Mns\Consumers\MnsConsumerException
     */
    public function process(MnsEvent $event)
    {
        parent::process($event);
        $order = $this->getOrder($event);

        $this->addCommentToStatusHistory(
            $order,
            __(
                'Amount of %1 assigned as payment for invoice %2',
                $this->getFormattedAmount($event->getAmount(), $event->getCurrency()),
                $event->getCaptureId()
            )
        );

        $this->orderHelper->getOrderRepository()->save($order);
    }

    private function getFormattedAmount(string $amount, string $currency): string
    {
        return $this->priceCurrency->format(
            floatval($amount),
            false,
            PriceCurrencyInterface::DEFAULT_PRECISION,
            null,
            $currency
        );
    }
}
