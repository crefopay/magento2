<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Mns\Consumers;

use Trilix\CrefoPay\Client\Constants;
use Trilix\CrefoPay\Model\Mns\MnsConsumerInterface;
use Trilix\CrefoPay\Model\Mns\MnsEvent;
use Magento\Sales\Model\Order;
use Trilix\CrefoPay\Model\Ui\PayPal\ConfigProvider as PayPal;

class CiaPending extends AbstractConsumer implements MnsConsumerInterface
{
    /**
     * {@inheritdoc}
     */
    public function getTransactionStatusName(): string
    {
        return Constants::TX_CIA;
    }

    /**
     * {@inheritdoc}
     */
    public function getCaptureStatusName(): string
    {
        return '';
    }

    /**
     * @param MnsEvent $event
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws MnsConsumerException
     */
    public function process(MnsEvent $event)
    {
        parent::process($event);
        $order = $this->getOrder($event);

        $order->setState(Order::STATE_PENDING_PAYMENT);

        if ($order->getPayment()->getMethod() === PayPal::CODE) {
            $order->setStatus('pending_paypal'); // Doesn't have a constant
        } else {
            $order->setStatus(Order::STATE_PENDING_PAYMENT);
        }

        $this->orderHelper->getOrderRepository()->save($order);
    }
}
