<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Mns\Consumers;

use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order;
use Trilix\CrefoPay\Client\Constants;
use Trilix\CrefoPay\Model\Mns\MnsConsumerInterface;
use Trilix\CrefoPay\Model\Mns\MnsEvent;

class Cancelled extends AbstractConsumer implements MnsConsumerInterface
{
    /**
     * {@inheritdoc}
     */
    public function getTransactionStatusName(): string
    {
        return Constants::TX_CANCELLED;
    }

    /**
     * {@inheritdoc}
     */
    public function getCaptureStatusName(): string
    {
        return '';
    }

    /**
     * @param MnsEvent $event
     *
     * @throws LocalizedException
     * @throws MnsConsumerException
     */
    public function process(MnsEvent $event)
    {
        parent::process($event);
        $order = $this->getOrder($event);
        $order->registerCancellation();
        $this->orderHelper->getOrderRepository()->save($order);
    }
}
