<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Mns\Consumers;

use Trilix\CrefoPay\Client\Constants;
use Trilix\CrefoPay\Model\Mns\MnsConsumerInterface;
use Trilix\CrefoPay\Model\Mns\MnsEvent;
use Magento\Sales\Model\Order;

class Done extends AbstractConsumer implements MnsConsumerInterface
{
    /**
     * {@inheritdoc}
     */
    public function getTransactionStatusName(): string
    {
        return Constants::TX_DONE;
    }

    /**
     * {@inheritdoc}
     */
    public function getCaptureStatusName(): string
    {
        return '';
    }

    /**
     * @param MnsEvent $event
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Trilix\CrefoPay\Mns\Consumers\MnsConsumerException
     */
    public function process(MnsEvent $event)
    {
        parent::process($event);
        $order = $this->getOrder($event);

        $order->setState(Order::STATE_COMPLETE);
        $order->setStatus($order->getConfig()->getStateDefaultStatus(Order::STATE_COMPLETE));

        if (!floatval($event->getAmount())) {
            $this->addCommentToStatusHistory($order, __('Open amounts have been marked as finished manually'));
        }

        $this->orderHelper->getOrderRepository()->save($order);
    }
}
