<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Mns\Consumers;

use Trilix\CrefoPay\Client\Constants;
use Trilix\CrefoPay\Model\Mns\MnsConsumerInterface;
use Trilix\CrefoPay\Model\Mns\MnsEvent;
use Magento\Sales\Model\Order;

class FraudCancelled extends AbstractConsumer implements MnsConsumerInterface
{
    /**
     * {@inheritdoc}
     */
    public function getTransactionStatusName(): string
    {
        return Constants::TX_FRAUD_CANCEL;
    }

    /**
     * {@inheritdoc}
     */
    public function getCaptureStatusName(): string
    {
        return '';
    }

    /**
     * @param MnsEvent $event
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws MnsConsumerException
     */
    public function process(MnsEvent $event)
    {
        parent::process($event);
        $order = $this->getOrder($event);
        $order->registerCancellation(__('Manually canceled due to fraud'));
        $this->orderHelper->getOrderRepository()->save($order);
    }
}
