<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Mns\Consumers;

use Trilix\CrefoPay\Helper\Order as OrderHelper;
use Magento\Sales\Model\Order as SalesOrder;
use Trilix\CrefoPay\Model\Mns\MnsEvent;

abstract class AbstractConsumer
{
    /**
     * @var OrderHelper
     */
    protected $orderHelper;

    /**
     * AckPending constructor.
     *
     * @param OrderHelper $orderHelper
     */
    public function __construct(OrderHelper $orderHelper)
    {
        $this->orderHelper = $orderHelper;
    }

    /**
     * @param MnsEvent $event
     *
     * @throws MnsConsumerException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function process(MnsEvent $event)
    {
        if (!$this->orderHelper->hasCrefoPayment($this->getOrder($event))) {
            throw new MnsConsumerException(
                sprintf(
                    'Can not process MNS event for increment order ID %s. The order has not been paid with CrefoPay.',
                    $event->getIncrementOrderId()
                ),
                MnsConsumerException::NOT_CREFOPAY
            );
        }
    }

    /**
     * @param MnsEvent $event
     *
     * @return SalesOrder
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getOrder(MnsEvent $event): SalesOrder
    {
        return $this->orderHelper->getOrderByIncrementId($event->getIncrementOrderId());
    }

    /**
     * @param SalesOrder $order
     * @param string     $comment
     * @param bool       $status
     */
    protected function addCommentToStatusHistory(SalesOrder $order, $comment, $status = false)
    {
        if (method_exists($order, 'addCommentToStatusHistory')) {
            $order->addCommentToStatusHistory($comment, $status); // not available in older versions e.g. 2.2.3
        } else {
            $order->addStatusHistoryComment($comment, $status); // deprecated
        }
    }
}
