<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Mns\Consumers;

use Magento\Framework\Exception\LocalizedException;
use Trilix\CrefoPay\Client\Constants;
use Trilix\CrefoPay\Model\Mns\MnsConsumerInterface;
use Trilix\CrefoPay\Model\Mns\MnsEvent;

class InDunning extends AbstractConsumer implements MnsConsumerInterface
{
    /**
     * {@inheritdoc}
     */
    public function getTransactionStatusName(): string
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function getCaptureStatusName(): string
    {
        return Constants::O_INDUNNING;
    }

    /**
     * @param MnsEvent $event
     *
     * @throws LocalizedException
     * @throws \Trilix\CrefoPay\Mns\Consumers\MnsConsumerException
     */
    public function process(MnsEvent $event)
    {
        parent::process($event);
        $order = $this->getOrder($event);
        $this->addCommentToStatusHistory($order, __('Customer is in dunning process for invoice %1', $event->getCaptureId()));
        $this->orderHelper->getOrderRepository()->save($order);
    }
}
