<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Mns\Consumers;

class MnsConsumerException extends \Exception
{
    public const NOT_CREFOPAY = 1;
}
