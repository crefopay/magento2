<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Mns\Consumers;

use Trilix\CrefoPay\Client\Constants;
use Trilix\CrefoPay\Model\Mns\MnsConsumerInterface;
use Trilix\CrefoPay\Model\Mns\MnsEvent;
use Trilix\CrefoPay\Helper\Order as OrderHelper;
use Trilix\CrefoPay\Model\Ui\CashInAdvance\ConfigProvider as CIA;
use Magento\Sales\Model\Order;
use Magento\Framework\Pricing\PriceCurrencyInterface;

class MerchantPending extends AbstractConsumer implements MnsConsumerInterface
{
    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * MerchantPending constructor.
     *
     * @param OrderHelper            $orderHelper
     * @param PriceCurrencyInterface $priceCurrency
     */
    public function __construct(OrderHelper $orderHelper, PriceCurrencyInterface $priceCurrency)
    {
        parent::__construct($orderHelper);
        $this->priceCurrency = $priceCurrency;
    }

    /**
     * {@inheritdoc}
     */
    public function getTransactionStatusName(): string
    {
        return Constants::TX_MERCHANT;
    }

    /**
     * {@inheritdoc}
     */
    public function getCaptureStatusName(): string
    {
        return '';
    }

    /**
     * @param MnsEvent $event
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Trilix\CrefoPay\Model\Mns\Consumers\MnsConsumerException
     */
    public function process(MnsEvent $event)
    {
        parent::process($event);
        $order = $this->getOrder($event);
        /** @var \Magento\Sales\Model\Order\Payment $payment */
        $payment = $order->getPayment();
        /** @var \Trilix\CrefoPay\Model\Method\Adapter $methodInstance */
        $methodInstance = $payment->getMethodInstance();

        $order->setState(Order::STATE_PROCESSING);
        $order->setStatus($order->getConfig()->getStateDefaultStatus(Order::STATE_PROCESSING));

        $amount = $this->getAmount($order, $event);

        if ($this->orderHelper->isEligibleForAutoCapture($order)) {
            $methodInstance->capture($payment, $amount);
            // Skip Magento fraud check - delegate that to CrefoPay
            $payment->registerCaptureNotification($amount, true);
        } elseif ($payment->getMethod() === CIA::CODE) {
            $this->addCommentToStatusHistory(
                $order,
                __('Incoming payment: %1', $this->getFormattedAmount($amount, $event->getCurrency()))
            );
        }

        $this->orderHelper->getOrderRepository()->save($order);
    }

    private function getFormattedAmount(string $amount, string $currency): string
    {
        return $this->priceCurrency->format(
            floatval($amount),
            false,
            PriceCurrencyInterface::DEFAULT_PRECISION,
            null,
            $currency
        );
    }

    /**
     * Sometimes CrefoPay returns '0.00' as amount up for capture. In this case we have to substitute it with the valid amount
     * from the order.
     *
     * @param Order    $order
     * @param MnsEvent $event
     *
     * @return string
     */
    private function getAmount(Order $order, MnsEvent $event): string
    {
        if (floatval($event->getAmount())) {
            return $event->getAmount();
        }

        return (string)$order->getGrandTotal();
    }
}
