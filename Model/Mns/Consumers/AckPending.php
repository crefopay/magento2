<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Mns\Consumers;

use Magento\Framework\Exception\LocalizedException;
use Trilix\CrefoPay\Client\Constants;
use Trilix\CrefoPay\Client\Transport;
use Trilix\CrefoPay\Helper\Order as OrderHelper;
use Trilix\CrefoPay\Model\Mns\MnsConsumerInterface;
use Trilix\CrefoPay\Model\Mns\MnsEvent;
use Trilix\CrefoPay\Model\AmazonPay\AmazonPayTransactionRepository;
use Trilix\CrefoPay\Client\Request\UpdateTransactionDataRequestFactory;

class AckPending extends AbstractConsumer implements MnsConsumerInterface
{
    /** @var AmazonPayTransactionRepository */
    private $amazonPayTransactionRepository;

    /** @var UpdateTransactionDataRequestFactory */
    private $updateTransactionDataRequestFactory;

    /** @var Transport */
    private $transport;

    /**
     * AckPending constructor.
     *
     * @param OrderHelper $orderHelper
     */
    public function __construct(
        OrderHelper $orderHelper,
        AmazonPayTransactionRepository $amazonPayTransactionRepository,
        UpdateTransactionDataRequestFactory $updateTransactionDataRequestFactory,
        Transport $transport
    ) {
        parent::__construct($orderHelper);
        $this->amazonPayTransactionRepository      = $amazonPayTransactionRepository;
        $this->updateTransactionDataRequestFactory = $updateTransactionDataRequestFactory;
        $this->transport                           = $transport;
    }


    /**
     * {@inheritdoc}
     */
    public function getTransactionStatusName(): string
    {
        return Constants::TX_ACK;
    }

    /**
     * {@inheritdoc}
     */
    public function getCaptureStatusName(): string
    {
        return '';
    }

    /**
     * @param MnsEvent $event
     * @throws LocalizedException
     * @throws MnsConsumerException
     */
    public function process(MnsEvent $event)
    {
        parent::process($event);
        $order = $this->getOrder($event);
        $this->addCommentToStatusHistory($order, __('CrefoPay payment reference: %1', $event->getPaymentReference()));
        $this->orderHelper->getOrderRepository()->save($order);

        $amazonPayTransaction = $this->amazonPayTransactionRepository->getByOrderId($event->getIncrementOrderId());
        if ($amazonPayTransaction) {
            $request = $this->updateTransactionDataRequestFactory->create($amazonPayTransaction);
            $this->transport->sendRequest($request);
        }
    }
}
