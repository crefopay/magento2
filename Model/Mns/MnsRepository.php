<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Mns;

use Exception;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\NoSuchEntityException;
use Trilix\CrefoPay\Api\CrefoPayMnsRepositoryInterface;
use Trilix\CrefoPay\Api\Data\CrefoPayMnsInterface;
use Trilix\CrefoPay\Model\ResourceModel\Mns as MnsResource;
use Trilix\CrefoPay\Model\ResourceModel\Mns\Collection;
use Trilix\CrefoPay\Model\ResourceModel\Mns\CollectionFactory;
use Trilix\CrefoPay\Api\Data\CrefoPayMnsSearchResultInterface;
use Trilix\CrefoPay\Api\Data\CrefoPayMnsSearchResultInterfaceFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;

class MnsRepository implements CrefoPayMnsRepositoryInterface
{
    /**
     * @var MnsEventFactory
     */
    private $mnsEventFactory;

    /**
     * @var CollectionFactory
     */
    private $mnsCollectionFactory;

    /**
     * @var CrefoPayMnsSearchResultInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * @var MnsResource
     */
    private $mnsResource;

    /**
     * MnsRepository constructor.
     *
     * @param MnsEventFactory $mnsEventFactory
     * @param CollectionFactory                          $mnsCollectionFactory
     * @param CrefoPayMnsSearchResultInterfaceFactory    $searchResultFactory
     * @param MnsResource                                $mnsResource
     */
    public function __construct(
        MnsEventFactory $mnsEventFactory,
        CollectionFactory $mnsCollectionFactory,
        CrefoPayMnsSearchResultInterfaceFactory $searchResultFactory,
        MnsResource $mnsResource
    ) {
        $this->mnsEventFactory = $mnsEventFactory;
        $this->mnsCollectionFactory = $mnsCollectionFactory;
        $this->searchResultFactory = $searchResultFactory;
        $this->mnsResource = $mnsResource;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(SearchCriteriaInterface $searchCriteria): CrefoPayMnsSearchResultInterface
    {
        /** @var Collection $collection */
        $collection = $this->mnsCollectionFactory->create();

        $this->addFiltersToCollection($searchCriteria, $collection);
        $this->addSortOrdersToCollection($searchCriteria, $collection);
        $this->addPagingToCollection($searchCriteria, $collection);

        $collection->load();

        return $this->buildSearchResult($searchCriteria, $collection);
    }



    /**
     * {@inheritdoc}
     */
    public function get($id): CrefoPayMnsInterface
    {
        $mnsEvent = $this->mnsEventFactory->create();
        $this->mnsResource->load($mnsEvent, $id);

        if (!$mnsEvent->getId()) {
            throw new NoSuchEntityException(sprintf('Unable to find MNS event with id %d', $id));
        }

        return $mnsEvent;
    }

    /**
     * {@inheritdoc}
     * @throws AlreadyExistsException
     */
    public function save(CrefoPayMnsInterface $mnsEvent): CrefoPayMnsInterface
    {
        $this->mnsResource->save($mnsEvent);

        return $mnsEvent;
    }

    /**
     * {@inheritdoc}
     * @throws Exception
     */
    public function delete(CrefoPayMnsInterface $mnsEvent): void
    {
        $this->mnsResource->delete($mnsEvent);
    }

    /**
     * {@inheritdoc}
     * @throws Exception
     */
    public function deleteById($id): void
    {
        $mnsEvent = $this->get($id);
        $this->mnsResource->delete($mnsEvent);
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param Collection $collection
     */
    private function addFiltersToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection): void
    {
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $fields[] = $filter->getField();
                $conditions[] = [$filter->getConditionType() => $filter->getValue()];
            }
            $collection->addFieldToFilter($fields, $conditions);
        }
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param Collection $collection
     */
    private function addSortOrdersToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection): void
    {
        foreach ((array) $searchCriteria->getSortOrders() as $sortOrder) {
            $direction = $sortOrder->getDirection() === SortOrder::SORT_ASC ? 'asc' : 'desc';
            $collection->addOrder($sortOrder->getField(), $direction);
        }
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param Collection $collection
     */
    private function addPagingToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection): void
    {
        $collection->setPageSize($searchCriteria->getPageSize());
        $collection->setCurPage($searchCriteria->getCurrentPage());
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param Collection $collection
     * @return CrefoPayMnsSearchResultInterface
     */
    private function buildSearchResult(SearchCriteriaInterface $searchCriteria, Collection $collection): CrefoPayMnsSearchResultInterface
    {
        /** @var CrefoPayMnsSearchResultInterface $searchResults */
        $searchResults = $this->searchResultFactory->create();

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }
}
