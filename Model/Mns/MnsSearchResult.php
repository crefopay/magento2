<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Mns;

use Magento\Framework\Api\Search\SearchResult;
use Trilix\CrefoPay\Api\Data\CrefoPayMnsSearchResultInterface;

class MnsSearchResult extends SearchResult implements CrefoPayMnsSearchResultInterface
{
}
