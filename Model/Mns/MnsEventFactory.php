<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model\Mns;

use Magento\Framework\ObjectManagerInterface;
use Trilix\CrefoPay\Api\Data\CrefoPayMnsInterface as Mns;
use Trilix\CrefoPay\Model\AmazonPay\AmazonPayTransactionRepository;

class MnsEventFactory
{
    public const KEY_MAP = [
        'merchantID' => Mns::MERCHANT_ID,
        'storeID' => Mns::STORE_ID,
        'orderID' => Mns::ORDER_INCREMENT_ID,
        'captureID' => Mns::CAPTURE_ID,
        'merchantReference' => Mns::MERCHANT_REFERENCE,
        'paymentReference' => Mns::PAYMENT_REFERENCE,
        'userID' => Mns::USER_ID,
        'transactionStatus' => Mns::TRANSACTION_STATUS,
        'orderStatus' => Mns::CAPTURE_STATUS,
    ];

    /** @var ObjectManagerInterface */
    private $objectManager;

    /** @var AmazonPayTransactionRepository */
    private $amazonPayTransactionRepository;

    /**
     * MnsEventFactory constructor.
     *
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        AmazonPayTransactionRepository $amazonPayTransactionRepository
    ) {
        $this->objectManager                  = $objectManager;
        $this->amazonPayTransactionRepository = $amazonPayTransactionRepository;
    }

    /**
     * @param array $payload MNS notification details coming from CrefoPay (as a POST payload)
     *
     * @return MnsEvent
     */
    public function createFromPost(array $payload): MnsEvent
    {
        $mnsEventData = [];

        foreach ($payload as $key => $value) {
            $mnsEventData[$this->mapKey($key)] = $this->convertValue($key, $value);
        }

        $mnsEvent = $this->create($mnsEventData);

        if (!$mnsEvent->getIncrementOrderId()) {
            throw new \LogicException('orderID is missing');
        }

        return $mnsEvent;
    }

    /**
     * @param array $data
     *
     * @return MnsEvent
     */
    public function create(array $data = []): MnsEvent
    {
        /** @var MnsEvent $mnsEvent */
        $mnsEvent = $this->objectManager->create(MnsEvent::class);

        if (!empty($data)) {
            $mnsEvent->setData($data);
        }

        $mnsEvent->setNumberOfAttemptsToProcessNotification(0);

        return $mnsEvent;
    }

    /**
     * Convert CrefoPay key from payload to Magento DataObject key
     *
     * @param string $key CrefoPay payload key
     *
     * @return string Magento DataObject key
     */
    private function mapKey(string $key): string
    {
        return self::KEY_MAP[$key] ?? $key;
    }

    /**
     * Convert value coming from CrefoPay (if needed)
     *
     * @param string $key
     * @param string $value
     *
     * @return string
     */
    private function convertValue(string $key, string $value): string
    {
        if ($key === Mns::AMOUNT) {
            $value = sprintf('%.2f', intval($value) / 100);
        }
        if ($key === 'orderID') {
            $amazonTransaction = $this->amazonPayTransactionRepository->getByCrefoPayOrderId($value);
            if ($amazonTransaction) {
                $value = $amazonTransaction->getOrderId();
            }
        }

        return $value;
    }
}
