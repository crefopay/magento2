<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Model;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Block\Widget\Dob;
use Magento\Customer\Model\Session;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

class CrefoPayDob
{
    /** @var CustomerRepositoryInterface */
    private $customerRepository;

    /** @var Session */
    private $customerSession;

    /** @var Dob */
    private $dob;

    /**
     * CrefoPayDob constructor.
     * @param CustomerRepositoryInterface $customerRepository
     * @param Session $customerSession
     * @param Dob $dob
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        Session $customerSession,
        Dob $dob
    ) {
        $this->customerRepository = $customerRepository;
        $this->customerSession = $customerSession;
        $this->dob = $dob;
    }

    /**
     * @return string|null
     *
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getDob()
    {
        $dob = null;
        $customerId = $this->customerSession->getCustomerId();
        if ($customerId) {
            $customer = $this->customerRepository->getById($customerId);
            $dob = $customer->getDob();
        }
        return $dob;
    }

    /**
     * @return string
     */
    public function getDateFormatForJs()
    {
        return $this->dob->getDateFormat();
    }
}
