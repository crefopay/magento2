<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Gateway\Request\User;

use Magento\Quote\Model\Quote\Address;
use Upg\Library\User\Type as UserType;
use Trilix\CrefoPay\Gateway\Config\Config;

class CrefoPayUserFactory
{
    public const B2B_USER_TYPE = 'B2B';

    public const B2C_USER_TYPE = 'B2C';

    /** @var Config  */
    private $config;

    /**
     * UserIdBuilder constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @param Address $address
     * @param string $email
     * @return CrefoPayUser
     */
    public function create(Address $address, string $email): CrefoPayUser
    {
        $isB2BEnabled = $this->config->isB2BEnabled();
        $userID = md5($email);

        if ($isB2BEnabled && $address->getCompany() &&
            !in_array($address->getCompany(), $this->getPossibleAutoFilledCompanyValues(), true)) {
            $userID .= self::B2B_USER_TYPE;
            $userType = UserType::USER_TYPE_BUSINESS;
        } else {
            $userID .= self::B2C_USER_TYPE;
            $userType = UserType::USER_TYPE_PRIVATE;
        }

        return new CrefoPayUser($userID, $userType);
    }

    /**
     * @return array
     */
    private function getPossibleAutoFilledCompanyValues(): array
    {
        return [
            'Mr',
            'Mr.',
            'Ms',
            'Ms.',
            'Herr',
            'Herr.',
            'Frau',
            'Frau.',
        ];
    }
}
