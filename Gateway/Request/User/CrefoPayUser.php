<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Gateway\Request\User;

class CrefoPayUser
{
    /** @var string */
    private $id;

    /** @var string */
    private $type;

    /**
     * CrefoPayUser constructor.
     * @param string $id
     * @param string $type
     */
    public function __construct(string $id, string $type)
    {
        $this->id = $id;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}
