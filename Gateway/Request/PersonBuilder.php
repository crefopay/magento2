<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Gateway\Request;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Model\Customer;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\Quote\Address;
use Upg\Library\Request\Objects\Person;
use Trilix\CrefoPay\Gateway\Request\Address\DataFormatter;

class PersonBuilder
{
    /** @var CustomerRepositoryInterface */
    private $customerRepository;

    /** @var DataFormatter */
    private $dataFormatter;

    /**
     * PersonBuilder constructor.
     * @param CustomerRepositoryInterface $customerRepository
     * @param DataFormatter $dataFormatter
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        DataFormatter $dataFormatter
    ) {
        $this->customerRepository = $customerRepository;
        $this->dataFormatter      = $dataFormatter;
    }

    /**
     * @param Address $address
     * @param         $email
     *
     * @return Person
     * @throws LocalizedException
     */
    public function build(Address $address, $email): Person
    {
        $customer = $this->getCustomer($email);

        $person = new Person();
        $person->setSalutation($this->getSalutation($customer));
        $person->setName($address->getFirstname());
        $person->setSurname($address->getLastname());
        $person->setEmail($email);
        $person->setFaxNumber($this->dataFormatter->formatFaxNumber($address));
        $person->setPhoneNumber($this->dataFormatter->formatPhoneNumber($address));
        $this->setDateOfBirth($person, $customer);

        return $person;
    }

    /**
     * @param string $email
     * @return CustomerInterface|null
     * @throws LocalizedException
     */
    private function getCustomer(string $email): ?CustomerInterface
    {
        try {
            return $this->customerRepository->get($email);
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * @param \Magento\Customer\Api\Data\CustomerInterface|Customer|null $customer
     *
     * @return string|null
     */
    private function getSalutation($customer): ?string
    {
        if (!$customer) {
            return null;
        }

        switch ((int)$customer->getGender()) {
            case 1: return Person::SALUTATIONMALE;
            case 2: return Person::SALUTATIONFEMALE;
            case 3: return Person::SALUTATIONVARIOUS;
            default: return null;
        }
    }

    /**
     * @param Person $person
     * @param \Magento\Customer\Api\Data\CustomerInterface|Customer|null $customer
     */
    private function setDateOfBirth(Person $person, $customer)
    {
        if (!$customer || !$customer->getDob()) {
            return;
        }

        $person->setDateOfBirth(new \DateTime($customer->getDob()));
    }
}
