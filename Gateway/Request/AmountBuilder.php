<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Gateway\Request;

use Magento\Quote\Model\Quote;
use Magento\Payment\Gateway\Data\OrderAdapterInterface;
use Upg\Library\Request\Objects\Amount;

class AmountBuilder
{
    /**
     * @param Quote $quote
     * @return Amount
     */
    public function buildFromQuote(Quote $quote): Amount
    {
        $amount = new Amount();
        $amount->setAmount(round(floatval($quote->getGrandTotal()), 2) * 100);

        return $amount;
    }

    /**
     * @param OrderAdapterInterface $order
     * @return Amount
     */
    public function buildFromOrder(OrderAdapterInterface $order): Amount
    {
        $amount = new Amount();
        $amount->setAmount(round(floatval($order->getGrandTotalAmount()), 2) * 100);

        return $amount;
    }
}
