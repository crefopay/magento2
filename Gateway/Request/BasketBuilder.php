<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Gateway\Request;

use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Item as QuoteItem;
use Magento\SalesRule\Api\RuleRepositoryInterface;
use Magento\SalesRule\Model\Coupon;
use Upg\Library\Request\Objects\BasketItem;
use Upg\Library\Request\Objects\Amount;
use Upg\Library\Request\CreateTransaction as CreateTransactionRequest;

class BasketBuilder
{
    /** @var Coupon  */
    private $couponModel;

    /** @var RuleRepositoryInterface */
    private $ruleRepository;

    /**
     * @param Coupon $couponModel
     * @param RuleRepositoryInterface $ruleRepository
     */
    public function __construct(
        Coupon $couponModel,
        RuleRepositoryInterface $ruleRepository
    ) {
        $this->couponModel = $couponModel;
        $this->ruleRepository = $ruleRepository;
    }

    /**
     * @param Quote                    $quote
     * @param CreateTransactionRequest $createTransactionRequest
     *
     * @return void
     */
    public function build(Quote $quote, CreateTransactionRequest $createTransactionRequest)
    {
        /** @var $quoteItem QuoteItem */
        foreach ($quote->getAllItems() as $quoteItem) {
            if (intval(ceil(floatval($quoteItem->getRowTotal())))) {
                $basketItem = $this->createBasketItem(
                    $quoteItem->getName(),
                    $quoteItem->getQty(),
                    $quoteItem->getRowTotal()
                );

                $createTransactionRequest->addBasketItem($basketItem);
            }
        }

        $basketShippingItem = $this->buildShippingItem($quote);
        if ($basketShippingItem->getBasketItemAmount()) {
            $createTransactionRequest->addBasketItem($basketShippingItem);
        }

        $basketCouponItem = $this->buildCouponItem($quote);
        if ($basketCouponItem->getBasketItemAmount()) {
            $createTransactionRequest->addBasketItem($basketCouponItem);
        }
    }

    /**
     * @param Quote $quote
     *
     * @return BasketItem
     */
    private function buildShippingItem(Quote $quote)
    {
        $shippingAmount = 0;

        /** @var \Magento\Quote\Model\Quote\Address $shippingAddress */
        foreach ($quote->getAllShippingAddresses() as $shippingAddress) {
            /** @var \Magento\Quote\Model\Quote\Address\Rate $rate */
            foreach ($shippingAddress->getAllShippingRates() as $rate) {
                $shippingAmount += $rate->getPrice();
            }
        }

        $basketItem = $this->createBasketItem('(shipping)', 1, $shippingAmount);

        return $basketItem;
    }

    /**
     * @param Quote $quote
     *
     * @return BasketItem
     */
    private function buildCouponItem(Quote $quote)
    {
        $couponAmount = $quote->getSubtotal() - $quote->getSubtotalWithDiscount();
        return $this->createBasketItem('(discount)', 1, $couponAmount);
    }

    /**
     * @param string    $name
     * @param int|float $qty
     * @param float     $rowTotal
     *
     * @return BasketItem
     */
    private function createBasketItem($name, $qty, $rowTotal)
    {
        $basketItem = new BasketItem();
        $basketItemAmount = new Amount();
        $basketItemAmount->setAmount(round(floatval($rowTotal), 2) * 100);

        $basketItem->setBasketItemText($name);
        if (is_int($qty)) {
            $basketItem->setBasketItemCount($qty);
        } else {
            $basketItem->setBasketItemCount(1);
        }
        $basketItem->setBasketItemAmount($basketItemAmount);

        return $basketItem;
    }
}
