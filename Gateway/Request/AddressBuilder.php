<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Gateway\Request;

use Magento\Quote\Model\Quote\Address as MagentoAddress;
use Upg\Library\Request\Objects\Address;

class AddressBuilder
{
    /**
     * @param MagentoAddress $address
     * @return Address
     */
    public function build(MagentoAddress $address): Address
    {
        $crefoPayAddress = new Address();
        $crefoPayAddress->setStreet(join(', ', $address->getStreet()));
        $crefoPayAddress->setZip($address->getPostcode());
        $crefoPayAddress->setCity($address->getCity());
        $crefoPayAddress->setState($address->getRegion());
        $crefoPayAddress->setCountry($address->getCountry());

        return $crefoPayAddress;
    }
}
