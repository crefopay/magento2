<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Gateway\Request;

use Upg\Library\Request\Objects\Company;

class CompanyBuilder
{
    /**
     * @param string $companyName
     * @return Company
     */
    public function build(string $companyName): Company
    {
        $company = new Company();
        $company->setCompanyName($companyName);

        return $company;
    }
}
