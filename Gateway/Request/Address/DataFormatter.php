<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Gateway\Request\Address;

use Magento\Quote\Model\Quote\Address;

class DataFormatter
{
    /**
     * @param Address $address
     * @return string
     */
    public function formatPhoneNumber(Address $address): string
    {
        $result = "";
        $phoneNumber = $address->getTelephone();
        if ($phoneNumber) {
            $number = preg_replace('/\D/', '', $phoneNumber);
            $result = substr($number, 0, 1) === '0' ? $number : '0' . $number;
        }
        return $result;
    }

    /**
     * @param Address $address
     * @return string
     */
    public function formatFaxNumber(Address $address): string
    {
        $result = "";
        $fax = $address->getFax();
        if ($fax) {
            $number = preg_replace('/\D/', '', $address->getFax());
            $result = substr($number, 0, 1) === '0' ? $number : '0' . $number;
        }
        return $result;
    }
}
