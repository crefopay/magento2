<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Gateway\Helper;

class SubjectReader extends \Magento\Payment\Gateway\Helper\SubjectReader
{
    /**
     * @param array $subject
     *
     * @return int
     */
    public static function readAmount(array $subject): int
    {
        return (int) (round(floatval(parent::readAmount($subject)), 2) * 100);
    }
}
