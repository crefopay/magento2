<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Gateway\Config;

use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface as ConfigWriter;
use Magento\Framework\UrlInterface;
use Magento\Payment\Model\CcConfig;
use Magento\Payment\Gateway\Config\Config as MagentoConfig;
use Trilix\CrefoPay\Model\Adminhtml\Source\Environment;
use Trilix\CrefoPay\Model\AmazonPay\AmazonPayConfig;

class Config extends MagentoConfig
{
    public const KEY_ACTIVE                         = 'active';
    public const KEY_MERCHANT_ID                    = 'merchant_id';
    public const KEY_MERCHANT_PASSWORD              = 'private_key';
    public const KEY_STORE_ID                       = 'store_id';
    public const KEY_ENVIRONMENT                    = 'environment';
    public const KEY_PUBLIC_TOKEN                   = 'public_token';
    public const KEY_IS_B2B_ENABLED                 = 'is_b2b_enabled';
    public const KEY_IS_SMART_SIGNUP_ENABLED        = 'is_smart_signup_enabled';
    public const KEY_RISK_CLASS                     = 'risk_class';
    public const KEY_AUTO_CAPTURE_CATEGORIES        = 'auto_capture_categories';
    public const KEY_CUSTOMER_GROUP_RISK_CLASS      = 'user_group_risk_class/ranges';

    /**
     * @var string|null
     */
    private $methodCode;

    /**
     * @var string|null
     */
    private $pathPattern;

    /**
     * @var CcConfig
     */
    private $ccConfig;

    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @var string
     */
    private $secureFieldsUrl;

    /**
     * @var string
     */
    private $smartSignupUrl;

    /**
     * @var string
     */
    private $secureFieldsJsLink;

    /**
     * @var string
     */
    private $amazonClientJsUrl;

    /**
     * @var CategoryCollectionFactory
     */
    private $categoryCollectionFactory;

    /**
     * @var ConfigWriter
     */
    private $configWriter;

    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /** @var AmazonPayConfig */
    private $amazonPayConfig;

    /**
     * Config constructor.
     * @param AmazonPayConfig $amazonPayConfig
     * @param ScopeConfigInterface $scopeConfig
     * @param CcConfig $ccConfig
     * @param CategoryCollectionFactory $categoryCollectionFactory
     * @param ConfigWriter $configWriter
     * @param UrlInterface $urlBuilder
     * @param null $methodCode
     * @param string $pathPattern
     */
    public function __construct(
        AmazonPayConfig $amazonPayConfig,
        ScopeConfigInterface $scopeConfig,
        CcConfig $ccConfig,
        CategoryCollectionFactory $categoryCollectionFactory,
        ConfigWriter $configWriter,
        UrlInterface $urlBuilder,
        $methodCode = null,
        $pathPattern = self::DEFAULT_PATH_PATTERN
    ) {
        parent::__construct($scopeConfig, $methodCode, $pathPattern);
        $this->amazonPayConfig = $amazonPayConfig;
        $this->ccConfig = $ccConfig;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->configWriter = $configWriter;
        $this->urlBuilder = $urlBuilder;
        $this->methodCode = $methodCode;
        $this->pathPattern = $pathPattern;
    }

    /**
     * Get Payment configuration status
     *
     * @return bool
     */
    public function isActive(): bool
    {
        return (bool) $this->getValue(self::KEY_ACTIVE);
    }

    /**
     * @param int|null $storeId
     * @return string
     */
    public function getMerchantId(?int $storeId = null): string
    {
        return (string) $this->getValue(self::KEY_MERCHANT_ID, $storeId);
    }

    /**
     * @param int|null $storeId
     * @return string
     */
    public function getMerchantPassword(?int $storeId = null): string
    {
        return (string) $this->getValue(self::KEY_MERCHANT_PASSWORD, $storeId);
    }

    /**
     * @param int|null $storeId
     * @return string
     */
    public function getStoreId(?int $storeId = null): string
    {
        return (string) $this->getValue(self::KEY_STORE_ID, $storeId);
    }

    /**
     * @return bool
     */
    public function isB2BEnabled(): bool
    {
        return (bool) $this->getValue(self::KEY_IS_B2B_ENABLED);
    }

    /**
     * @return bool
     */
    public function isSmartSignupEnabled(): bool
    {
        return (bool) $this->getValue(self::KEY_IS_SMART_SIGNUP_ENABLED);
    }

    /**
     * @return int
     */
    public function getRiskClass(): int
    {
        return (int) $this->getValue(self::KEY_RISK_CLASS);
    }

    /**
     * @return array
     */
    public function getCustomerGroupsRiskClasses(): array
    {
        $result = [];
        $customerGroupRiskClasses = $this->getValue(self::KEY_CUSTOMER_GROUP_RISK_CLASS);

        if ($customerGroupRiskClasses) {
            $customerGroupRiskClassArray = json_decode($customerGroupRiskClasses);

            foreach ($customerGroupRiskClassArray as $value) {
                if (empty($value)) {
                    continue;
                }
                list($customerGroupId, $riskClass) = explode(":", $value);
                $result[$customerGroupId] = $riskClass;
            }
        }

        return $result;
    }

    /**
     * @return AmazonPayConfig
     */
    public function getAmazonConfig(): AmazonPayConfig
    {
        return $this->amazonPayConfig;
    }

    /**
     * @return string
     */
    public function getPublicToken(): string
    {
        return (string) $this->getValue(self::KEY_PUBLIC_TOKEN);
    }

    /**
     * @return string
     */
    public function getSecureFieldsUrl(): string
    {
        if ($this->secureFieldsUrl) {
            return $this->secureFieldsUrl;
        }
        if (!$this->isSandboxMode()) {
            $this->secureFieldsUrl = 'https://api.crefopay.de/secureFields/';
        } else {
            $this->secureFieldsUrl = 'https://sandbox.crefopay.de/secureFields/';
        }

        return $this->secureFieldsUrl;
    }

    /**
     * @return bool
     */
    public function isSandboxMode(): bool
    {
        $environment = self::KEY_ENVIRONMENT;

        switch ($this->getValue($environment)) {
            case Environment::ENVIRONMENT_PRODUCTION:
                $result = false;
                break;
            default:
                $result = true;
        }
        return $result;
    }

    /**
     * @return string
     */
    public function getSmartSignupUrl(): string
    {
        if ($this->smartSignupUrl) {
            return $this->smartSignupUrl;
        }
        if (!$this->isSandboxMode()) {
            $this->smartSignupUrl = 'https://api.crefopay.de/autocomplete/';
        } else {
            $this->smartSignupUrl = 'https://sandbox.crefopay.de/autocomplete/';
        }
        return $this->smartSignupUrl;
    }

    public function getSecureFieldsJsUrl(): string
    {
        if ($this->secureFieldsJsLink) {
            return $this->secureFieldsJsLink;
        }
        if (!$this->isSandboxMode()) {
            $this->secureFieldsJsLink = 'https://api.crefopay.de/libs/3.0/secure-fields.js';
        } else {
            $this->secureFieldsJsLink = 'https://sandbox.crefopay.de/libs/3.0/secure-fields.js';
        }

        return $this->secureFieldsJsLink;
    }

    /**
     * @return string
     */
    public function getAmazonClientJsUrl(): string
    {
        if ($this->amazonClientJsUrl) {
            return $this->amazonClientJsUrl;
        }
        if (!$this->isSandboxMode()) {
            $this->amazonClientJsUrl = 'https://api.crefopay.de/libs/3.0/amazon-client.js';
        } else {
            $this->amazonClientJsUrl = 'https://sandbox.crefopay.de/libs/3.0/amazon-client.js';
        }

        return $this->amazonClientJsUrl;
    }

    /**
     * @return string
     */
    public function getBaseUrl(): string
    {
        if ($this->baseUrl) {
            return $this->baseUrl;
        }

        $environment = self::KEY_ENVIRONMENT;

        switch ($this->getValue($environment)) {
            case Environment::ENVIRONMENT_PRODUCTION:
                $subDomain = 'api';
                break;
            case Environment::ENVIRONMENT_SANDBOX:
                $subDomain = 'sandbox';
                break;
            default:
                throw new \InvalidArgumentException(sprintf('Invalid value for CrefoPay environment: %s', $environment));
        }

        $this->baseUrl = sprintf('https://%s.crefopay.de/2.0/', $subDomain);

        return $this->baseUrl;
    }

    /**
     * Save auto capture category IDs.
     *
     * @param array  $categoryIds
     * @param string $scope
     * @param int    $scopeId
     *
     * @return void
     */
    public function setAutoCaptureCategoryIds(
        array $categoryIds,
        $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
        $scopeId = 0
    ) {
        $this->setValue(self::KEY_AUTO_CAPTURE_CATEGORIES, join(',', $categoryIds), $scope, $scopeId);
    }

    /**
     * Return auto capture category IDs as plain array.
     *
     * @return array
     */
    public function getAutoCaptureCategoryIds(): array
    {
        $autoCaptureCategories = $this->getValue(self::KEY_AUTO_CAPTURE_CATEGORIES);
        $result = [];
        if ($autoCaptureCategories) {
            $result = array_filter(explode(',', $autoCaptureCategories));
        }
        return $result;
    }

    /**
     * @param string $method
     * @return string
     */
    public function getLogoUrl(string $method): string
    {
        $logo = $this->getValue($method . '_logo');
        $result = '';
        if ($logo) {
            $uploadDir = 'crefopay' . DIRECTORY_SEPARATOR . $method . DIRECTORY_SEPARATOR . 'logo';
            $url = $uploadDir . DIRECTORY_SEPARATOR . $logo;

            $result = $this->urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . $url;
        }

        return $result;
    }

    /**
     * Save config value.
     *
     * @param string $field
     * @param        $value
     * @param string $scope
     * @param int    $scopeId
     *
     * @return void
     */
    protected function setValue(
        string $field,
        $value,
        string $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
        int $scopeId = 0
    ) {
        $this->configWriter->save(
            sprintf($this->pathPattern, $this->methodCode, $field),
            $value,
            $scope,
            $scopeId
        );
    }
}
