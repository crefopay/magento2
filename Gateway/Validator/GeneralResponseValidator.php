<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Gateway\Validator;

use Magento\Payment\Gateway\Validator\AbstractValidator;
use Magento\Payment\Gateway\Validator\ResultInterface;
use Upg\Library\Response\AbstractResponse;
use Upg\Library\Error\Codes;

class GeneralResponseValidator extends AbstractValidator
{
    /**
     * @inheritdoc
     */
    public function validate(array $validationSubject): ResultInterface
    {
        /** @var AbstractResponse $response */
        $response = $validationSubject['response'];

        $resultCode = $response->getData('resultCode');
        if (Codes::ERROR_PAYMENT_METHOD_REJECTED === $resultCode
            || Codes::ERROR_PAYMENT_DECLINED_FRAUD === $resultCode) {
            return $this->createResult(false, [], [$resultCode]);
        }

        return $this->createResult(true, [], [$resultCode]);
    }
}
