<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Gateway\Command;

use Magento\Payment\Gateway\CommandInterface;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Sales\Model\Order\Payment;
use Trilix\CrefoPay\Client\Request\RefundRequestFactory;
use Trilix\CrefoPay\Client\Transport;
use Trilix\CrefoPay\Model\TransactionService;
use Trilix\CrefoPay\Gateway\Helper\SubjectReader;

class Refund implements CommandInterface
{
    /**
     * @var Transport
     */
    private $transport;

    /**
     * @var RefundRequestFactory
     */
    private $refundRequestFactory;

    /**
     * @var TransactionService
     */
    private $transactionService;

    /**
     * @var Payment\Transaction\Repository
     */
    private $transactionRepository;

    /**
     * Refund constructor.
     *
     * @param Transport                      $transport
     * @param RefundRequestFactory           $refundRequestFactory
     * @param TransactionService             $transactionService
     * @param Payment\Transaction\Repository $transactionRepository
     */
    public function __construct(
        Transport $transport,
        RefundRequestFactory $refundRequestFactory,
        TransactionService $transactionService,
        Payment\Transaction\Repository $transactionRepository
    ) {
        $this->transport = $transport;
        $this->refundRequestFactory = $refundRequestFactory;
        $this->transactionService = $transactionService;
        $this->transactionRepository = $transactionRepository;
    }

    /**
     * @param array $commandSubject
     *
     * @return void
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Upg\Library\Api\Exception\AbstractException
     */
    public function execute(array $commandSubject)
    {
        $paymentDO = SubjectReader::readPayment($commandSubject);
        $amount = SubjectReader::readAmount($commandSubject);

        $storeId = (int) $paymentDO->getOrder()->getStoreId();

        $request = $this->refundRequestFactory->create(
            $paymentDO->getOrder()->getOrderIncrementId(),
            $this->getCaptureId($this->getPayment($paymentDO)),
            $amount,
            'Refund ' . $amount,
            $storeId
        );

        $response = $this->transport->sendRequest($request);
        $this->transactionService->addTransaction($paymentDO, $request, $response);
    }

    /**
     * Ensure that what we've got from data transfer object has correct type.
     *
     * @param PaymentDataObjectInterface $paymentDO
     *
     * @return Payment
     */
    private function getPayment(PaymentDataObjectInterface $paymentDO): Payment
    {
        $payment = $paymentDO->getPayment();

        if (!($payment instanceof Payment)) {
            throw new \InvalidArgumentException(sprintf('Expected instance of "%s", got "%s" instead', Payment::class, get_class($payment)));
        }

        return $payment;
    }

    /**
     * Retrieve 'capture_id' stored in capture (parent) transaction. It is added there by
     * \Trilix\CrefoPay\Model\TransactionService::addTransaction() during the capture execution.
     *
     * @param Payment $payment
     *
     * @return string
     * @throws \Magento\Framework\Exception\InputException
     */
    private function getCaptureId(Payment $payment): string
    {
        if (!$payment->getParentTransactionId()) {
            throw new \LogicException(sprintf('Parent (capture) transaction ID missing'));
        }

        /** @var Payment\Transaction $captureTx */
        $captureTx = $this->transactionRepository->getByTransactionId(
            $payment->getParentTransactionId(),
            $payment->getId(),
            $payment->getOrder()->getId()
        );

        if (!$captureTx) {
            throw new \LogicException(sprintf('Capture transaction "%s" not found', $payment->getParentTransactionId()));
        }

        $captureId = $captureTx->getAdditionalInformation()['capture_id'] ?? null;

        if (!$captureId) {
            throw new \LogicException(sprintf('"capture_id" not found in capture transaction "%s"', $captureTx->getTransactionId()));
        }

        return $captureId;
    }
}
