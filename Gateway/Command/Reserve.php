<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Gateway\Command;

use Magento\Customer\Model\Session;
use Magento\Payment\Gateway\Command\CommandException;
use Magento\Payment\Gateway\CommandInterface;
use Magento\Payment\Gateway\Response\HandlerInterface;
use Magento\Payment\Gateway\Validator\ValidatorInterface;
use Trilix\CrefoPay\Client\Request\ReserveRequestFactory;
use Trilix\CrefoPay\Client\Transport;
use Trilix\CrefoPay\Model\AmazonPay\AmazonPayCustomerSession;
use Trilix\CrefoPay\Model\AmazonPay\AmazonPayTransactionRepository;
use Trilix\CrefoPay\Model\TransactionService;
use Trilix\CrefoPay\Gateway\SubjectReader;
use Trilix\CrefoPay\Model\AmazonPay\AmazonPayTransactionFactory;
use Upg\Library\Risk\RiskClass;
use Upg\Library\Api\Exception\ApiError;

class Reserve implements CommandInterface
{
    public const CREFOPAY_RESERVE_ERROR = 'crefopay_reserve_error';

    /** @var Session */
    private $customerSession;

    /** @var AmazonPayCustomerSession */
    private $amazonPayCustomerSession;

    /** @var Transport */
    private $transport;

    /** @var ReserveRequestFactory */
    private $reserveRequestFactory;

    /** @var TransactionService */
    private $transactionService;

    /** @var AmazonPayTransactionRepository */
    private $amazonPayTransactionRepository;

    /** @var AmazonPayTransactionFactory */
    private $amazonPayTransactionFactory;

    /** @var SubjectReader */
    private $subjectReader;

    /** @var HandlerInterface|null */
    private $handler;

    /** @var ValidatorInterface|null */
    private $validator;

    /** @var string */
    private $methodCode;

    /**
     * Reserve constructor.
     * @param Session $customerSession
     * @param AmazonPayCustomerSession $amazonPayCustomerSession
     * @param Transport $transport
     * @param ReserveRequestFactory $reserveRequestFactory
     * @param TransactionService $transactionService
     * @param AmazonPayTransactionRepository $amazonPayTransactionRepository
     * @param AmazonPayTransactionFactory $amazonPayTransactionFactory
     * @param SubjectReader $subjectReader
     * @param string $methodCode
     * @param HandlerInterface|null $handler
     * @param ValidatorInterface|null $validator
     */
    public function __construct(
        Session $customerSession,
        AmazonPayCustomerSession $amazonPayCustomerSession,
        Transport $transport,
        ReserveRequestFactory $reserveRequestFactory,
        TransactionService $transactionService,
        AmazonPayTransactionRepository $amazonPayTransactionRepository,
        AmazonPayTransactionFactory $amazonPayTransactionFactory,
        SubjectReader $subjectReader,
        string $methodCode,
        HandlerInterface $handler = null,
        ValidatorInterface $validator = null
    ) {
        $this->customerSession                = $customerSession;
        $this->amazonPayCustomerSession       = $amazonPayCustomerSession;
        $this->transport                      = $transport;
        $this->reserveRequestFactory          = $reserveRequestFactory;
        $this->transactionService             = $transactionService;
        $this->amazonPayTransactionRepository = $amazonPayTransactionRepository;
        $this->amazonPayTransactionFactory    = $amazonPayTransactionFactory;
        $this->subjectReader                  = $subjectReader;
        $this->methodCode                     = $methodCode;
        $this->handler                        = $handler;
        $this->validator                      = $validator;
    }

    /**
     * @param array $commandSubject
     *
     * @return void
     */
    public function execute(array $commandSubject)
    {        
        try {
            $paymentDO = $this->subjectReader->readPayment($commandSubject);
            $request = $this->reserveRequestFactory->create(
                $this->methodCode,
                $paymentDO
            );

            $orderId = $request->getOrderID();

            if ($this->amazonPayCustomerSession->isAmazonPayCheckoutFlow()) {
                $amazonCustomerData = $this->amazonPayCustomerSession->getAmazonSessionDataFromCustomerSession();
                $amazonPayTransaction = $this->amazonPayTransactionFactory->create([]);
                $amazonPayTransaction->setOrderId($orderId);
                $amazonPayTransaction->setCrefoPayOrderId($amazonCustomerData->getOrderId());
                $this->amazonPayTransactionRepository->save($amazonPayTransaction);
            }

            $response = $this->transport->sendRequest($request);

            // move to handler
            $this->transactionService->addTransaction($paymentDO, $request, $response);

            if ($this->handler) {
                $this->handler->handle(
                    $commandSubject,
                    []
                );
            }
        } catch (ApiError $e) {
            $this->handleApiException($commandSubject, $e);
        }
    }

    /**
     * @param array    $commandSubject
     * @param ApiError $e
     *
     * @throws CommandException
     */
    private function handleApiException(array $commandSubject, ApiError $e)
    {
        $exception = new CommandException(
            !empty($e->getMessage())
                ? __($e->getMessage())
                : __('Transaction has been declined. Please try again later.')
        );

        if ($this->validator !== null) {
            $result = $this->validator->validate(
                array_merge($commandSubject, ['response' => $e->getParsedResponse()])
            );

            if (!$result->isValid()) {
                $this->customerSession->setUserRiskClass(['value' => RiskClass::RISK_CLASS_HIGH]);
                $exception = new CommandException(__(self::CREFOPAY_RESERVE_ERROR));
            }
        }

        throw $exception;
    }
}
