<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Gateway\Command;

use Magento\Customer\Model\Session;
use Magento\Payment\Gateway\Command\CommandException;
use Magento\Payment\Gateway\CommandInterface;
use Magento\Payment\Gateway\Response\HandlerInterface;
use Magento\Payment\Gateway\Validator\ValidatorInterface;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Trilix\CrefoPay\Client\Request\ReserveRequestFactory;
use Trilix\CrefoPay\Client\Transport;
use Trilix\CrefoPay\Client\Request\CreateTransactionRequestFactory;
use Trilix\CrefoPay\Model\TransactionService;
use Trilix\CrefoPay\Gateway\SubjectReader;
use Upg\Library\Risk\RiskClass;
use Upg\Library\Api\Exception\ApiError;

class CreateBackendOrder implements CommandInterface
{
    public const CREFOPAY_CREATE_BACKEN_ORDER_ERROR = 'crefopay_create_backend_order_error';

    /** @var Session */
    private $customerSession;

    /** @var Transport */
    private $transport;

    /** @var TransactionService */
    private $transactionService;

    /** @var SubjectReader */
    private $subjectReader;

    /** @var string */
    private $methodCode;

    /** @var HandlerInterface|null */
    private $handler;

    /** @var ValidatorInterface|null */
    private $validator;

    /** @var CreateTransactionRequestFactory */
    private $createTransactionRequestFactory;

    /** @var ReserveRequestFactory */
    private $reserveRequestFactory;

    /**
     * Reserve constructor.
     * @param Session $customerSession
     * @param Transport $transport
     * @param TransactionService $transactionService
     * @param CreateTransactionRequestFactory $createTransactionRequestFactory
     * @param ReserveRequestFactory $reserveRequestFactory
     * @param SubjectReader $subjectReader
     * @param string $methodCode
     * @param HandlerInterface|null $handler
     * @param ValidatorInterface|null $validator
     */
    public function __construct(
        Session $customerSession,
        Transport $transport,
        TransactionService $transactionService,
        CreateTransactionRequestFactory $createTransactionRequestFactory,
        ReserveRequestFactory $reserveRequestFactory,
        SubjectReader $subjectReader,
        string $methodCode,
        HandlerInterface $handler = null,
        ValidatorInterface $validator = null
    ) {
        $this->customerSession                 = $customerSession;
        $this->transport                       = $transport;
        $this->transactionService              = $transactionService;
        $this->createTransactionRequestFactory = $createTransactionRequestFactory;
        $this->reserveRequestFactory           = $reserveRequestFactory;
        $this->subjectReader                   = $subjectReader;
        $this->methodCode                      = $methodCode;
        $this->handler                         = $handler;
        $this->validator                       = $validator;
    }

    /**
     * @param array $commandSubject
     *
     * @return void
     */
    public function execute(array $commandSubject)
    {
        try {
            $paymentDO = $this->subjectReader->readPayment($commandSubject);
            $this->createTransaction($paymentDO, $commandSubject);
            $this->reserve($paymentDO, $commandSubject);

            if ($this->handler) {
                $this->handler->handle(
                    $commandSubject,
                    []
                );
            }

        } catch (ApiError $e) {
            $this->handleApiException($commandSubject, $e);
        }
    }

    /**
     * @param PaymentDataObjectInterface $paymentDO
     * @param array $commandSubject
     *
     * @return void
     */
    private function createTransaction(PaymentDataObjectInterface $paymentDO, array $commandSubject)
    {
        $paymentDO = $this->subjectReader->readPayment($commandSubject);
        $cartId = $paymentDO->getOrder()->getQuoteId();
        $orderId = $paymentDO->getOrder()->getOrderIncrementId();

        $createTransactionRequest = $this->createTransactionRequestFactory->createForAdminFlow($cartId, $orderId);
        $this->transport->sendRequest($createTransactionRequest);
    }

    /**
     * @param PaymentDataObjectInterface $paymentDO
     * @param array $commandSubject
     *
     * @return void
     */
    private function reserve(PaymentDataObjectInterface $paymentDO, array $commandSubject)
    {
        $reserveRequest = $this->reserveRequestFactory->create(
            $this->methodCode,
            $paymentDO
        );

        $response = $this->transport->sendRequest($reserveRequest);

        // move to handler
        $this->transactionService->addTransaction($paymentDO, $reserveRequest, $response);
    }

    /**
     * @param array    $commandSubject
     * @param ApiError $e
     *
     * @throws CommandException
     */
    private function handleApiException(array $commandSubject, ApiError $e)
    {
        if ($e->getCode() == 1002) {
            throw new CommandException(__('Payment declined by fraud check. Please use another payment method.'));
        }

        $exception = new CommandException(
            !empty($e->getMessage())
                ? __($e->getMessage())
                : __('Transaction has been declined. Please try again later.')
        );

        if ($this->validator !== null) {
            $result = $this->validator->validate(
                array_merge($commandSubject, ['response' => $e->getParsedResponse()])
            );

            if (!$result->isValid()) {
                $this->customerSession->setUserRiskClass(['value' => RiskClass::RISK_CLASS_HIGH]);
                $exception = new CommandException(__(self::CREFOPAY_CREATE_BACKEN_ORDER_ERROR));
            }
        }

        throw $exception;
    }
}
