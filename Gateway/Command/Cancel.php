<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Gateway\Command;

use Magento\Payment\Gateway\Command\Result\BoolResult;
use Magento\Payment\Gateway\CommandInterface;
use Magento\Payment\Gateway\Command\CommandException;
use Trilix\CrefoPay\Client\Request\CancelRequestFactory;
use Trilix\CrefoPay\Client\Transport;
use Trilix\CrefoPay\Gateway\Helper\SubjectReader;
use Trilix\CrefoPay\Model\TransactionService;

class Cancel implements CommandInterface
{
    /**
     * @var Transport
     */
    private $transport;

    /**
     * @var CancelRequestFactory
     */
    private $cancelRequestFactory;

    /**
     * @var TransactionService
     */
    private $transactionService;

    /**
     * Capture constructor.
     *
     * @param Transport            $transport
     * @param CancelRequestFactory $cancelRequestFactory
     * @param TransactionService   $transactionService
     */
    public function __construct(
        Transport $transport,
        CancelRequestFactory $cancelRequestFactory,
        TransactionService $transactionService
    ) {
        $this->transport = $transport;
        $this->cancelRequestFactory = $cancelRequestFactory;
        $this->transactionService = $transactionService;
    }

    /**
     * @param array $commandSubject
     * @return BoolResult|\Magento\Payment\Gateway\Command\ResultInterface|null
     * @throws CommandException
     */
    public function execute(array $commandSubject)
    {
        try {
            $paymentDO = SubjectReader::readPayment($commandSubject);
            $storeId = (int) $paymentDO->getOrder()->getStoreId();
            $orderId = $paymentDO->getOrder()->getOrderIncrementId();

            $request = $this->cancelRequestFactory->create($orderId, $storeId);
            $response = $this->transport->sendRequest($request);
            $this->transactionService->addTransaction($paymentDO, $request, $response);

            return new BoolResult(true);
        } catch (\Exception $e) {
            throw new CommandException(
                !empty($e->getMessage())
                    ? __($e->getMessage())
                    : __('Transaction has been declined. Please try again later.')
            );
        }
    }
}
