<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Gateway\Command;

use Magento\Payment\Gateway\Command\Result\BoolResult;
use Magento\Payment\Gateway\CommandInterface;
use Magento\Payment\Gateway\Command\CommandException;
use Trilix\CrefoPay\Client\Request\CaptureRequestFactory;
use Trilix\CrefoPay\Client\Transport;
use Trilix\CrefoPay\Gateway\Helper\SubjectReader;
use Trilix\CrefoPay\Model\TransactionService;

class Capture implements CommandInterface
{
    /**
     * @var Transport
     */
    private $transport;

    /**
     * @var CaptureRequestFactory
     */
    private $captureRequestFactory;

    /**
     * @var TransactionService
     */
    private $transactionService;

    /**
     * Capture constructor.
     *
     * @param Transport $transport
     * @param CaptureRequestFactory $captureRequestFactory
     * @param TransactionService $transactionService
     */
    public function __construct(
        Transport $transport,
        CaptureRequestFactory $captureRequestFactory,
        TransactionService $transactionService
    ) {
        $this->transport = $transport;
        $this->captureRequestFactory = $captureRequestFactory;
        $this->transactionService = $transactionService;
    }

    /**
     * @param array $commandSubject
     * @return BoolResult|\Magento\Payment\Gateway\Command\ResultInterface|null
     * @throws CommandException
     */
    public function execute(array $commandSubject)
    {
        try {
            $paymentDO = SubjectReader::readPayment($commandSubject);
            $amount = SubjectReader::readAmount($commandSubject);

            $storeId = (int) $paymentDO->getOrder()->getStoreId();
            $orderId = $paymentDO->getOrder()->getOrderIncrementId();

            $request = $this->captureRequestFactory->create($orderId, $amount, $storeId);
            $response = $this->transport->sendRequest($request);
            $this->transactionService->addTransaction($paymentDO, $request, $response);

            return new BoolResult(true);
        } catch (\Exception $e) {
            throw new CommandException(
                !empty($e->getMessage())
                    ? __($e->getMessage())
                    : __('Transaction has been declined. Please try again later.')
            );
        }
    }
}
