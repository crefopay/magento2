<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Gateway\Response;

use Magento\Payment\Gateway\Response\HandlerInterface;
use Magento\Payment\Model\InfoInterface;
use Magento\Sales\Api\Data\OrderPaymentExtensionInterface;
use Magento\Sales\Api\Data\OrderPaymentExtensionInterfaceFactory;
use Magento\Vault\Api\Data\PaymentTokenInterface;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Vault\Model\Ui\VaultConfigProvider;
use Upg\Library\Request\Objects\PaymentInstrument;
use Trilix\CrefoPay\Client\Transport;
use Trilix\CrefoPay\Model\CrefoPayTransactionRepository;
use Trilix\CrefoPay\Client\Request\GetUserPaymentInstrumentRequestFactory;

class VaultDetailsHandler implements HandlerInterface
{
    /**
     * @var CrefoPayPaymentTokenFactory
     */
    private $paymentTokenFactory;

    /**
     * @var OrderPaymentExtensionInterfaceFactory
     */
    private $paymentExtensionFactory;

    /**
     * @var CrefoPayTransactionRepository
     */
    private $crefoPayTransactionRepository;

    /**
     * @var Transport
     */
    private $transport;

    /**
     * @var GetUserPaymentInstrumentRequestFactory
     */
    private $getUserPaymentInstrumentRequestFactory;

    /**
     * VaultDetailsHandler constructor.
     *
     * @param CrefoPayPaymentTokenFactory $paymentTokenFactory
     * @param OrderPaymentExtensionInterfaceFactory $paymentExtensionFactory
     * @param CrefoPayTransactionRepository $crefoPayTransactionRepository
     * @param Transport $transport
     * @param GetUserPaymentInstrumentRequestFactory $getUserPaymentInstrumentRequestFactory
     */
    public function __construct(
        CrefoPayPaymentTokenFactory $paymentTokenFactory,
        OrderPaymentExtensionInterfaceFactory $paymentExtensionFactory,
        CrefoPayTransactionRepository $crefoPayTransactionRepository,
        Transport $transport,
        GetUserPaymentInstrumentRequestFactory $getUserPaymentInstrumentRequestFactory
    ) {
        $this->paymentTokenFactory           = $paymentTokenFactory;
        $this->paymentExtensionFactory       = $paymentExtensionFactory;
        $this->crefoPayTransactionRepository = $crefoPayTransactionRepository;
        $this->transport = $transport;
        $this->getUserPaymentInstrumentRequestFactory = $getUserPaymentInstrumentRequestFactory;
    }

    /**
     * @inheritdoc
     */
    public function handle(array $handlingSubject, array $response)
    {
        $paymentDO = SubjectReader::readPayment($handlingSubject);
        $payment = $paymentDO->getPayment();
        $quoteId = $payment->getOrder()->getQuoteId();
        $additionalInfo = $payment->getAdditionalInformation();

        if (!empty($additionalInfo[VaultConfigProvider::IS_ACTIVE_CODE])) {
            // add vault payment token entity to extension attributes
            $paymentToken = $this->getVaultPaymentToken($quoteId, $additionalInfo['paymentInstrumentId']);
            if ($paymentToken !== null) {
                $extensionAttributes = $this->getExtensionAttributes($payment);
                $extensionAttributes->setVaultPaymentToken($paymentToken);
            }
        }
    }

    /**
     * @param string $quoteId
     * @param string $paymentInstrumentId
     * @return PaymentTokenInterface
     * @throws \Exception
     */
    private function getVaultPaymentToken(string $quoteId, string $paymentInstrumentId)
    {
        $crefoPayTransaction = $this->crefoPayTransactionRepository->getByQuoteId($quoteId);

        $getUserPaymentInstrumentRequest = $this->getUserPaymentInstrumentRequestFactory
            ->create($crefoPayTransaction->getCrefoPayUserId());

        $response = $this->transport->sendRequest($getUserPaymentInstrumentRequest);

        /** @var PaymentInstrument[] $allowedPaymentInstrumentsFromResponse */
        $allowedPaymentInstrumentsFromResponse = $response->getData('paymentInstruments');

        $paymentToken = null;
        foreach ($allowedPaymentInstrumentsFromResponse as $item) {
            if ($item->getPaymentInstrumentID() === $paymentInstrumentId) {
                $paymentToken = $this->paymentTokenFactory->create($item);
            }
        }

        return $paymentToken;
    }

    /**
     * Get payment extension attributes
     * @param InfoInterface $payment
     * @return OrderPaymentExtensionInterface
     */
    private function getExtensionAttributes(InfoInterface $payment): OrderPaymentExtensionInterface
    {
        $extensionAttributes = $payment->getExtensionAttributes();
        if (null === $extensionAttributes) {
            $extensionAttributes = $this->paymentExtensionFactory->create();
            $payment->setExtensionAttributes($extensionAttributes);
        }
        return $extensionAttributes;
    }
}
