<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Gateway\Response;

use Magento\Vault\Api\Data\PaymentTokenFactoryInterface;
use Magento\Vault\Api\Data\PaymentTokenInterface;
use Magento\Framework\Intl\DateTimeFactory;
use Upg\Library\Request\Objects\PaymentInstrument;

class CrefoPayPaymentTokenFactory
{
    public const TOKEN_TYPE_DIRECT_DEBIT = 'crefopay_direct_debit';

    /**
     * @var PaymentTokenFactoryInterface
     */
    private $paymentTokenFactory;

    /**
     * @var DateTimeFactory
     */
    private $dateTimeFactory;

    /**
     * CrefoPayPaymentTokenFactory constructor.
     * @param PaymentTokenFactoryInterface $paymentTokenFactory
     * @param DateTimeFactory $dateTimeFactory
     */
    public function __construct(PaymentTokenFactoryInterface $paymentTokenFactory, DateTimeFactory $dateTimeFactory)
    {
        $this->paymentTokenFactory = $paymentTokenFactory;
        $this->dateTimeFactory = $dateTimeFactory;
    }

    /**
     * @param PaymentInstrument $paymentInstrument
     * @return PaymentTokenInterface|null
     * @throws \Exception
     */
    public function create(PaymentInstrument $paymentInstrument)
    {
        $paymentToken = null;

        switch ($paymentInstrument->getPaymentInstrumentType()) {
            case PaymentInstrument::PAYMENT_INSTRUMENT_TYPE_BANK:
                $paymentToken = $this->createBankAccountToken($paymentInstrument);
                break;
            case PaymentInstrument::PAYMENT_INSTRUMENT_TYPE_CARD:
                $paymentToken = $this->createCreditCardToken($paymentInstrument);
                break;
        }

        return $paymentToken;
    }

    /**
     * @param PaymentInstrument $paymentInstrument
     * @return PaymentTokenInterface
     */
    private function createCreditCardToken(PaymentInstrument $paymentInstrument): PaymentTokenInterface
    {
        $paymentToken = $this->paymentTokenFactory->create(PaymentTokenFactoryInterface::TOKEN_TYPE_CREDIT_CARD);
        $paymentToken->setGatewayToken($paymentInstrument->getPaymentInstrumentID());
        $paymentToken->setExpiresAt($paymentInstrument->getValidity());

        $paymentToken->setTokenDetails(json_encode([
            'accountHolder'  => $paymentInstrument->getAccountHolder(),
            'type'           => $paymentInstrument->getPaymentInstrumentType(),
            'maskedCC'       => substr($paymentInstrument->getNumber(), -4),
            'expirationDate' => $paymentInstrument->getValidity()->format('m/Y'),
            'issuer'         => $paymentInstrument->getIssuer()
        ]));

        return $paymentToken;
    }

    /**
     * @param PaymentInstrument $paymentInstrument
     * @return PaymentTokenInterface
     * @throws \Exception
     */
    private function createBankAccountToken(PaymentInstrument $paymentInstrument): PaymentTokenInterface
    {
        $paymentToken = $this->paymentTokenFactory->create(self::TOKEN_TYPE_DIRECT_DEBIT);

        // For Direct Debit there is no expiration date on CrefoPay side, we just set it to 10 years.
        $expDate = $this->dateTimeFactory->create('now', new \DateTimeZone('UTC'));
        $expDate->add(new \DateInterval('P10Y'));
        $paymentToken->setExpiresAt($expDate->format('Y-m-d 00:00:00'));

        $paymentToken->setGatewayToken($paymentInstrument->getPaymentInstrumentID());

        $paymentToken->setTokenDetails(json_encode([
            'accountHolder'  => $paymentInstrument->getAccountHolder(),
            'iban'           => $paymentInstrument->getIban(),
        ]));

        return $paymentToken;
    }
}
