<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\CustomerData;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\CustomerData\SectionSourceInterface;
use Magento\Customer\Model\Session;

class CrefoPay implements SectionSourceInterface
{
    /** @var Session */
    private $session;

    /** @var CustomerInterface */
    private $customer;

    /**
     * CrefoPay constructor.
     *
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public function getSectionData()
    {
        return ['gender' => $this->getGender(), 'dob' => $this->getCustomer()->getDob()];
    }

    private function getCustomer(): CustomerInterface
    {
        if (!$this->customer) {
            $this->customer = $this->session->getCustomer()->getDataModel();
        }

        return $this->customer;
    }

    private function getGender(): string
    {
        if (!$this->getCustomer()->getGender()) {
            return '';
        }

        return $this->getCustomer()->getGender() === 1 ? 'M' : 'F';
    }
}
