<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Cron;

use Trilix\CrefoPay\Model\Mns\MnsService;

class MnsProcess
{
    /**
     * @var MnsService
     */
    private $mnsService;

    /**
     * MnsProcess constructor.
     *
     * @param MnsService $mnsService
     */
    public function __construct(MnsService $mnsService)
    {
        $this->mnsService = $mnsService;
    }

    public function execute()
    {
        $this->mnsService->process();
    }
}
