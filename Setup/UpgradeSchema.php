<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Upgrade the CrefoPay module DB scheme
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.9.3', '<')) {
            $this->addAllowedPaymentInstrumentsColumn($setup);
        }

        if (version_compare($context->getVersion(), '1.2.5') < 0) {
            $this->addNumberOfAttemptsToProcessNotificationColumn($setup);
        }

        if (version_compare($context->getVersion(), '1.2.10') < 0) {
            $this->createCrefoPayAmazonPayTransactionTable($setup);
        }

        $setup->endSetup();
    }

    /**
     * @param SchemaSetupInterface $setup
     */
    private function addAllowedPaymentInstrumentsColumn(SchemaSetupInterface $setup): void
    {
        $connection = $setup->getConnection();
        $tableName = $setup->getTable('crefopay_transaction');

        if (!$connection->tableColumnExists($tableName, 'allowed_payment_instruments')) {
            $connection->addColumn(
                $tableName,
                'crefopay_user_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    '64',
                    [
                        'unsigned' => true,
                        'nullable' => true,
                    ],
                    'comment' => 'allowed payment instruments column'
                ]
            );
        }
    }

    /**
     * @param SchemaSetupInterface $setup
     */
    private function addNumberOfAttemptsToProcessNotificationColumn(SchemaSetupInterface $setup): void
    {
        $connection = $setup->getConnection();
        $tableName = $setup->getTable('crefopay_mns');

        if (!$connection->tableColumnExists($tableName, 'attempts_to_process_notification')) {
            $connection->addColumn(
                $tableName,
                'attempts_to_process_notification',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    'nullable' => false,
                    'default' => 0,
                    'comment' => 'Number of attempts to process mns notification'
                ]
            );
        }
    }

    /**
     * @param SchemaSetupInterface $setup
     *
     * @throws \Zend_Db_Exception
     */
    private function createCrefoPayAmazonPayTransactionTable(SchemaSetupInterface $setup)
    {
        $table = $setup->getConnection()
            ->newTable($setup->getTable('crefopay_amazon_pay_transaction'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'order_id',
                Table::TYPE_TEXT,
                64,
                [
                    'unsigned' => true,
                    'nullable' => false,
                ],
                'Quote Id'
            )
            ->addColumn(
                'crefopay_order_id',
                Table::TYPE_TEXT,
                64,
                [
                    'unsigned' => true,
                    'nullable' => false,
                ],
                'Unique CrefoPay Amazon Pay Order ID'
            )
            ->addColumn(
                'created_at',
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                'Created At'
            )
            ->addIndex(
                $setup->getIdxName(
                    'crefopay_amazon_pay_transaction',
                    ['order_id']
                ),
                ['order_id']
            );

        $setup->getConnection()->createTable($table);
    }
}
