<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->getConnection()->query(
            'update core_config_data set path="payment/crefopay_credit_card_tds/active" where path="payment/crefopay_credit_card_3d/active"'
        );
    }
}
