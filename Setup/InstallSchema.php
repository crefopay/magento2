<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Trilix\CrefoPay\Api\Data\CrefoPayMnsInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->createTransactionTable($setup);
        $this->createMnsTable($setup);
    }

    /**
     * @param SchemaSetupInterface $setup
     *
     * @throws \Zend_Db_Exception
     */
    private function createTransactionTable(SchemaSetupInterface $setup)
    {
        $table = $setup->getConnection()
            ->newTable($setup->getTable('crefopay_transaction'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'quote_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'unsigned' => true,
                    'nullable' => false,
                ],
                'Quote Id'
            )
            ->addColumn(
                'crefopay_order_id',
                Table::TYPE_TEXT,
                64,
                [
                    'unsigned' => true,
                    'nullable' => false,
                ],
                'CrefoPay Order ID equal to Magento Reserved Order ID'
            )
            ->addColumn(
                'payment_methods',
                Table::TYPE_TEXT,
                255,
                ['nullable' => true, 'default' => ''],
                'Payment Methods'
            )
            ->addForeignKey(
                $setup->getFkName(
                    'crefopay_payment_methods',
                    'quote_id',
                    'quote',
                    'entity_id'
                ),
                'quote_id',
                $setup->getTable('quote'),
                'entity_id',
                Table::ACTION_CASCADE
            )->setComment("CrefoPay Allowed Payment Methods per Quote");

        $setup->getConnection()->createTable($table);
    }

    /**
     * @param SchemaSetupInterface $setup
     *
     * @throws \Zend_Db_Exception
     */
    private function createMnsTable(SchemaSetupInterface $setup)
    {
        $table = $setup->getConnection()
            ->newTable($setup->getTable('crefopay_mns'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'merchant_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'unsigned' => true,
                    'nullable' => false,
                ],
                'CrefoPay Merchant ID'
            )
            ->addColumn(
                'store_id',
                Table::TYPE_TEXT,
                64,
                ['nullable' => false],
                'CrefoPay Store ID'
            )
            ->addColumn(
                'order_increment_id',
                Table::TYPE_TEXT,
                32,
                ['nullable' => false],
                'Order Increment ID'
            )
            ->addColumn(
                'capture_id',
                Table::TYPE_TEXT,
                64,
                ['nullable' => false, 'default' => ''],
                'CrefoPay Capture ID'
            )
            ->addColumn(
                'merchant_reference',
                Table::TYPE_TEXT,
                64,
                ['nullable' => false, 'default' => ''],
                'CrefoPay Merchant Reference'
            )
            ->addColumn(
                'payment_reference',
                Table::TYPE_TEXT,
                64,
                ['nullable' => false, 'default' => ''],
                'CrefoPay Payment Reference'
            )
            ->addColumn(
                'user_id',
                Table::TYPE_TEXT,
                64,
                ['nullable' => false],
                'CrefoPay User ID'
            )
            ->addColumn(
                'amount',
                Table::TYPE_TEXT,
                12,
                ['nullable' => false, 'default' => '0'],
                'Amount'
            )
            ->addColumn(
                'currency',
                Table::TYPE_TEXT,
                3,
                ['nullable' => false, 'default' => ''],
                'Currency'
            )
            ->addColumn(
                'transaction_status',
                Table::TYPE_TEXT,
                32,
                ['nullable' => false, 'default' => ''],
                'Transaction Status'
            )
            ->addColumn(
                'capture_status',
                Table::TYPE_TEXT,
                32,
                ['nullable' => false, 'default' => ''],
                'Order Status'
            )
            ->addColumn(
                'created_at',
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                'Created At'
            )
            ->addColumn(
                'mns_status',
                Table::TYPE_TEXT,
                '10',
                ['nullable' => false, 'default' => CrefoPayMnsInterface::STATUS_ACK],
                'Notification processing status'
            )
            ->addColumn(
                'processed_at',
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => true],
                'Timestamp of processing'
            )
            ->addColumn(
                'error_details',
                Table::TYPE_TEXT,
                16384,
                ['nullable' => true],
                'Error details (if the notification failed to be processed)'
            )
            ->addIndex(
                $setup->getIdxName('crefopay_mns', ['mns_status']),
                ['mns_status']
            )->setComment("CrefoPay MNS Notifications");

        $setup->getConnection()->createTable($table);
    }
}
