<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Trilix\CrefoPay\Gateway\Config\Config;
use Trilix\CrefoPay\Model\AmazonPay\AmazonPayCustomerSession;
use Trilix\CrefoPay\Model\CrefoPayTransactionRepository;

class ReservedOrderId implements ObserverInterface
{
    /** @var CrefoPayTransactionRepository */
    private $crefoPayTransactionRepository;

    /** @var Config */
    private $config;

    /** @var AmazonPayCustomerSession */
    private $amazonPayCustomerSession;

    /**
     * ReservedOrderId constructor.
     * @param CrefoPayTransactionRepository $crefoPayTransactionRepository
     * @param Config $config
     * @param AmazonPayCustomerSession $amazonPayCustomerSession
     */
    public function __construct(
        CrefoPayTransactionRepository $crefoPayTransactionRepository,
        Config $config,
        AmazonPayCustomerSession $amazonPayCustomerSession
    ) {
        $this->crefoPayTransactionRepository = $crefoPayTransactionRepository;
        $this->config                        = $config;
        $this->amazonPayCustomerSession      = $amazonPayCustomerSession;
    }

    public function execute(Observer $observer)
    {
        $amazonPayCheckout = $this->amazonPayCustomerSession->isAmazonPayCheckoutFlow();

        // check if module is enabled
        if (!$this->config->isActive() || $amazonPayCheckout) {
            return;
        }

        $quote = $observer->getEvent()->getQuote();

        $crefoPayTransaction = $this->crefoPayTransactionRepository->getByQuoteId($quote->getId());

        if (!$quote->getReservedOrderId()) {
            $quote->setReservedOrderId($crefoPayTransaction->getCrefoPayOrderId());
        }
    }
}
