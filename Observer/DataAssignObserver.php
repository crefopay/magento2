<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Observer;

use Magento\Framework\Event\Observer;
use Magento\Payment\Observer\AbstractDataAssignObserver;
use Magento\Quote\Api\Data\PaymentInterface;
use Trilix\CrefoPay\Client\Request\Structure\AdditionalInfo;

class DataAssignObserver extends AbstractDataAssignObserver
{
    public const PAYMENT_INSTRUMENT_ID = 'paymentInstrumentId';
    public const PAYMENT_TOKEN         = 'paymentToken';

    /**
     * @var array
     */
    protected $additionalInformationList = [
        self::PAYMENT_INSTRUMENT_ID,
        self::PAYMENT_TOKEN,
        AdditionalInfo::DATE_OF_BIRTH,
        AdditionalInfo::SALUTATION
    ];

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $data = $this->readDataArgument($observer);

        $additionalData = $data->getData(PaymentInterface::KEY_ADDITIONAL_DATA);
        if (!is_array($additionalData)) {
            return;
        }

        $paymentInfo = $this->readPaymentModelArgument($observer);

        foreach ($this->additionalInformationList as $additionalInformationKey) {
            if (isset($additionalData[$additionalInformationKey])) {
                $paymentInfo->setAdditionalInformation(
                    $additionalInformationKey,
                    $additionalData[$additionalInformationKey]
                );
            } elseif ($paymentInfo->hasAdditionalInformation($additionalInformationKey)) {
                $paymentInfo->unsAdditionalInformation(
                    $additionalInformationKey
                );
            }
        }
    }
}
