<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Observer;

use Trilix\CrefoPay\Model\Ui\CashInAdvance\ConfigProvider as CIA;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\OrderRepository;
use Magento\Sales\Model\Order;

class SalesOrderSaveObserver implements ObserverInterface
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /** @var ScopeConfigInterface */
    private $scopeConfig;

    /**
     * SalesOrderPlaceObserver constructor.
     *
     * @param OrderRepository $orderRepository
     */
    public function __construct(OrderRepository $orderRepository, ScopeConfigInterface $scopeConfig)
    {
        $this->orderRepository = $orderRepository;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Save additional transaction information for braintree methods
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getData('order');

        if (!$order->getId()) {
            return;
        }

        $payment = $order->getPayment();

        $newStatus = $this->scopeConfig->getValue(
            'payment/crefopay_cash_in_advance/order_status',
            ScopeInterface::SCOPE_WEBSITE,
            null
        );

        // Check if the payment method is Cash in Advance (CIA) and if the new status is set to pending payment
        if ($payment->getMethod() === CIA::CODE && $newStatus === Order::STATE_PENDING_PAYMENT) {
            $order->setState(Order::STATE_PENDING_PAYMENT);
            $order->setStatus(Order::STATE_PENDING_PAYMENT);
            $this->orderRepository->save($order);
        }
    }
}
