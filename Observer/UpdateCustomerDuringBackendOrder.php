<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Observer;

use Magento\Framework\Event\Observer;
use Magento\Payment\Observer\AbstractDataAssignObserver;
use Magento\Quote\Api\Data\PaymentInterface;
use Magento\Quote\Model\QuoteRepository;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Trilix\CrefoPay\Client\Request\Structure\AdditionalInfo;

class UpdateCustomerDuringBackendOrder extends AbstractDataAssignObserver
{
    /** @var CustomerRepositoryInterface */
    private $customerRepository;

    /**
     * Quote Repository
     *
     * @var QuoteRepository
     */
    private $quoteRepository;

    /**
     * UpdateCustomer constructor.
     *
     * @param QuoteRepository             $quoteRepository
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(QuoteRepository $quoteRepository, CustomerRepositoryInterface $customerRepository)
    {
        $this->quoteRepository = $quoteRepository;
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param Observer $observer
     *
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\State\InputMismatchException
     */
    public function execute(Observer $observer)
    {
        $data = $this->readDataArgument($observer);

        $additionalData = $data->getData(PaymentInterface::KEY_ADDITIONAL_DATA);
        if (!is_array($additionalData)) {
            return;
        }

        $payment = $this->readPaymentModelArgument($observer);

        $quote = $this->quoteRepository->get($payment->getQuoteId());
        $customer = $quote->getCustomer();

        if (!empty($additionalData[AdditionalInfo::SALUTATION])) {
            switch ($additionalData[AdditionalInfo::SALUTATION]) {
                case AdditionalInfo::SALUTATION_M:
                    $gender = 1;
                    break;
                case AdditionalInfo::SALUTATION_F:
                    $gender = 2;
                    break;
                case AdditionalInfo::SALUTATION_V:
                    $gender = 3;
                    break;
            }
            $customer->setGender($gender);
        }

        if (!empty($additionalData[AdditionalInfo::DATE_OF_BIRTH])) {
            $dob = $this->convertToYmdFormat($additionalData[AdditionalInfo::DATE_OF_BIRTH]);

            // we need to update date format for CrefoPay
            $payment->setAdditionalInformation(AdditionalInfo::DATE_OF_BIRTH, $dob);
            $customer->setDob($dob);
        }

        $this->customerRepository->save($customer);
    }

    /**
     * Converts a date from the format 'm/d/Y' to 'Y-m-d'.
     * This function is useful when you need to transform a date string
     * into a format that is more suitable for database storage or processing.
     *
     * @param string $date Date string in the format 'm/d/Y'.
     * @return string Formatted date in the format 'Y-m-d'.
     *                Returns an empty string if the input date is empty or invalid.
     */
    private function convertToYmdFormat($date)
    {
        $formattedDate = '';
        if (!empty($date)) {
            $dateObject = \DateTime::createFromFormat('m/d/Y', $date);
            if ($dateObject) {
                $formattedDate = $dateObject->format('Y-m-d');
            }
        }
        return $formattedDate;
    }
}
