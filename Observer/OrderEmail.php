<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;
use Magento\Sales\Model\OrderRepository;
use Trilix\CrefoPay\Helper\Order as OrderHelper;

class OrderEmail implements ObserverInterface
{
    /** @var OrderHelper */
    private $helper;

    /** @var OrderRepository */
    private $orderRepository;

    /** @var OrderSender */
    private $orderSender;

    /**
     * OrderEmail constructor.
     *
     * @param OrderHelper     $helper
     * @param OrderRepository $orderRepository
     * @param OrderSender     $orderSender
     */
    public function __construct(OrderHelper $helper, OrderRepository $orderRepository, OrderSender $orderSender)
    {
        $this->helper = $helper;
        $this->orderRepository = $orderRepository;
        $this->orderSender = $orderSender;
    }

    /**
     * @param Observer $observer
     *
     * @throws \Magento\Framework\Exception\InputException
     */
    public function execute(Observer $observer)
    {
        switch ($observer->getEvent()->getName()) {
            case 'sales_model_service_quote_submit_before': $this->postponeOrderEmail($observer);
                break;
            case 'checkout_onepage_controller_success_action': $this->sendOrderEmail($observer);
                break;
        }
    }

    /**
     * Don't send email just yet, its payment may still fail.
     *
     * @param Observer $observer
     */
    private function postponeOrderEmail(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getEvent()->getData('order');

        if (!$this->helper->hasCrefoPayment($order)) {
            return;
        }

        $order->setCanSendNewEmailFlag(false);
    }

    /**
     * Now we confident that payment gone through and may send the order email confirmation.
     *
     * @param Observer $observer
     *
     * @throws \Magento\Framework\Exception\InputException
     */
    private function sendOrderEmail(Observer $observer)
    {
        $orderIds = $observer->getEvent()->getData('order_ids');

        if (empty($orderIds)) {
            return;
        }

        $orderId = current($orderIds);

        try {
            $order = $this->orderRepository->get($orderId);
        } catch (NoSuchEntityException $e) {
            return;
        }

        if (!$this->helper->hasCrefoPayment($order) || $order->getEmailSent()) {
            return;
        }

        $this->orderSender->send($order);
    }
}
