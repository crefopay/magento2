<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Observer;

use Magento\Catalog\Block\ShortcutButtons;
use Magento\Framework\Event\Observer;
use Magento\Checkout\Block\QuoteShortcutButtons;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\View\Element\Template;
use Trilix\CrefoPay\Block\AmazonPay\Minicart\Button;
use Trilix\CrefoPay\Gateway\Config\Config;

class InitializeAmazonPayButton implements ObserverInterface
{
    /** @var Config */
    private $config;

    /**
     * InitializeAmazonPayButton constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function execute(Observer $observer)
    {
        if ($this->config->isActive() && $this->config->getAmazonConfig()->isEnabled()) {
            /** @var ShortcutButtons $shortcutButtons */
            $shortcutButtons = $observer->getEvent()->getContainer();
            /** @var Template $shortcut */
            $shortcut = $shortcutButtons->getLayout()->createBlock(Button::class);

            $shortcut->setIsInCatalogProduct(
                $observer->getEvent()->getIsCatalogProduct()
            )->setShowOrPosition(
                $observer->getEvent()->getOrPosition()
            );

            $shortcut->setIsCart(get_class($shortcutButtons) == QuoteShortcutButtons::class);
            $shortcutButtons->addShortcut($shortcut);
        }
    }
}
