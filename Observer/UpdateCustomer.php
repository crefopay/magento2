<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Observer;

use Magento\Framework\Event\Observer;
use Magento\Payment\Observer\AbstractDataAssignObserver;
use Magento\Customer\Model\Session;
use Magento\Quote\Api\Data\PaymentInterface;
use Trilix\CrefoPay\Client\Request\Structure\AdditionalInfo;
use Magento\Customer\Api\CustomerRepositoryInterface;

class UpdateCustomer extends AbstractDataAssignObserver
{
    /** @var Session */
    private $session;

    /** @var CustomerRepositoryInterface */
    private $repository;

    /**
     * UpdateCustomer constructor.
     *
     * @param Session                     $session
     * @param CustomerRepositoryInterface $repository
     */
    public function __construct(Session $session, CustomerRepositoryInterface $repository)
    {
        $this->session = $session;
        $this->repository = $repository;
    }

    /**
     * @param Observer $observer
     *
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\State\InputMismatchException
     */
    public function execute(Observer $observer)
    {
        $data = $this->readDataArgument($observer);

        $additionalData = $data->getData(PaymentInterface::KEY_ADDITIONAL_DATA);
        if (!is_array($additionalData)) {
            return;
        }

        if (!$this->session->isLoggedIn()) {
            return;
        }

        $customer = $this->session->getCustomer()->getDataModel();

        if (!empty($additionalData[AdditionalInfo::SALUTATION])) {
            switch ($additionalData[AdditionalInfo::SALUTATION]) {
                case AdditionalInfo::SALUTATION_M:
                    $gender = 1;
                    break;
                case AdditionalInfo::SALUTATION_F:
                    $gender = 2;
                    break;
                case AdditionalInfo::SALUTATION_V:
                    $gender = 3;
                    break;
            }
            $customer->setGender($gender);
        }

        if (!empty($additionalData[AdditionalInfo::DATE_OF_BIRTH])) {
            $customer->setDob($additionalData[AdditionalInfo::DATE_OF_BIRTH]);
        }

        $this->repository->save($customer);
    }
}
