<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Block\AmazonPay;

use Magento\Framework\Locale\ResolverInterface as Locale;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Trilix\CrefoPay\Gateway\Config\Config as CrefoPayConfig;

class Config extends Template
{
    /** @var CrefoPayConfig */
    private $config;

    /** @var Locale */
    private $locale;

    /**
     * Config constructor.
     * @param Context $context
     * @param CrefoPayConfig $config
     * @param Locale $locale
     */
    public function __construct(
        Context $context,
        CrefoPayConfig $config,
        Locale $locale
    ) {
        parent::__construct($context);
        $this->config = $config;
        $this->locale = $locale;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return [
            'shopPublicKey'          => $this->config->getPublicToken(),
            'amazonClientJsUrl'      => $this->config->getAmazonClientJsUrl(),
            'url'                    => $this->config->getSecureFieldsUrl(),
            'buttonColor'            => $this->config->getAmazonConfig()->getButtonColor(),
            'checkoutLanguage'       => $this->getCheckoutLanguageCode(),
            'sandbox'                => $this->config->isSandboxMode(),
            'autoCapture'            => $this->config->getAmazonConfig()->isAutoCaptureEnabled(),
            'deliverySpecifications' => $this->config->getAmazonConfig()->getDeliverySpecifications(),
        ];
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->config->getAmazonConfig()->isEnabled();
    }

    /**
     * @return string
     */
    private function getCheckoutLanguageCode(): string
    {
        $languageCode = $this->locale->getLocale();

        $supportedLanguages = [
            'en_GB', 'de_DE', 'fr_FR', 'it_IT', 'es_ES'
        ];

        if (!in_array($languageCode, $supportedLanguages)) {
            $languageCode = 'en_GB';
        }

        return $languageCode;
    }
}
