<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Block\AmazonPay;

use Magento\Catalog\Model\Product;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Trilix\CrefoPay\Gateway\Config\Config;

class ProductPageButton extends Template
{
    /** @var Config */
    private $config;

    /** @var Registry */
    private $registry;

    /**
     * ProductPageButton constructor.
     * @param Context $context
     * @param Config $config
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        Context $context,
        Config $config,
        Registry $registry,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->registry = $registry;
        $this->config   = $config;
    }

    /**
     * {@inheritdoc}
     */
    protected function _toHtml()
    {
        $isCrefoPayAmazonPayActive = $this->config->isActive() && $this->config->getAmazonConfig()->isEnabled();
        if (!$isCrefoPayAmazonPayActive) {
            return '';
        }

        // check for product stock and/or saleability
        $product = $this->_getProduct();
        // configurable products
        if ($product->getTypeId() == Configurable::TYPE_CODE) {
            if (!$product->isSaleable()) {
                return '';
            }
        } else {
            // other product types
            if ($product->isInStock() == 0 || !$product->isSaleable()) {
                return '';
            }
        }

        return parent::_toHtml();
    }

    /**
     * @return Product
     */
    protected function _getProduct()
    {
        return $this->registry->registry('product');
    }
}
