<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Block\AmazonPay\Minicart;

use Magento\Framework\View\Element\Template;
use Magento\Catalog\Block\ShortcutInterface;
use Magento\Framework\View\Element\Template\Context;
use Trilix\CrefoPay\Gateway\Config\Config;

class Button extends Template implements ShortcutInterface
{
    public const ALIAS_ELEMENT_INDEX = 'alias';

    /** @var bool */
    private $isMiniCart = false;

    /** @var Config */
    private $config;

    /**
     * Button constructor.
     * @param Context $context
     * @param Config $config
     * @param array $data
     */
    public function __construct(
        Context $context,
        Config $config,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->config = $config;
    }

    /**
     * @return bool
     */
    protected function shouldRender()
    {
        $isCrefoPayAmazonPayActive = $this->config->isActive() && $this->config->getAmazonConfig()->isEnabled();

        if ($this->getIsCart() && $isCrefoPayAmazonPayActive) {
            return true;
        }

        return $isCrefoPayAmazonPayActive
            && $this->isMiniCart;
    }

    /**
     * @inheritdoc
     */
    protected function _toHtml()
    {
        if (!$this->shouldRender()) {
            return '';
        }

        return parent::_toHtml();
    }

    /**
     * Get shortcut alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->getData(self::ALIAS_ELEMENT_INDEX);
    }

    /**
     * @param bool $isCatalog
     * @return $this
     */
    public function setIsInCatalogProduct($isCatalog)
    {
        $this->isMiniCart = !$isCatalog;

        return $this;
    }

    /**
     * @return string
     */
    public function getExtraClassname(): string
    {
        return $this->getIsCart() ? 'cart' : 'minicart';
    }
}
