<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Block\Adminhtml\System\Config\Form\Field;

use Magento\Backend\Block\Template;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Data\Form\Element\Renderer\RendererInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Ui\Model\UiComponentGenerator;
use Magento\Backend\Block\Template\Context;

class Categories extends Template implements RendererInterface
{
    /** @var UiComponentGenerator */
    private $uiComponentGenerator;

    /**
     * {@inheritdoc}
     */
    public function __construct(Context $context, UiComponentGenerator $uiComponentGenerator, array $data = [])
    {
        parent::__construct($context, $data);
        $this->uiComponentGenerator = $uiComponentGenerator;
    }

    /**
     * Get the UI component HTML as a form field.
     *
     * @param AbstractElement $element
     *
     * @return string
     * @throws LocalizedException
     */
    public function render(AbstractElement $element)
    {
        $uiComponent = $this->uiComponentGenerator->generateUiComponent('category_multiselect', $this->_layout);
        return $uiComponent->render();
    }
}
