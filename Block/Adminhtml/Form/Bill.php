<?php

namespace Trilix\CrefoPay\Block\Adminhtml\Form;

use Upg\Library\Request\Objects\Person;

class Bill extends \Magento\Payment\Block\Form
{
    /**
     * @var string
     */
    protected $_template = 'Trilix_CrefoPay::form/bill.phtml';

    /**
     * Payment configuration model instance.
     *
     * @var \Magento\Payment\Model\Config
     */
    protected $_paymentConfig;

    /**
     * Quote Repository
     *
     * @var \Magento\Quote\Model\QuoteRepository
     */
    protected $_quoteRepository;

    /**
     * @var \Trilix\CrefoPay\Gateway\Config\Config
     */
    private $config;

    /**
      * Constructor for the Bill block.
      * Initializes necessary dependencies.
      *
      * @param \Magento\Framework\View\Element\Template\Context $context Context object with additional data.
      * @param \Magento\Payment\Model\Config $paymentConfig Payment configuration model.
      * @param \Magento\Quote\Model\QuoteRepository $quoteRepository Repository for quote data.
      * @param array $data Additional data array.
      */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Payment\Model\Config $paymentConfig,
        \Magento\Quote\Model\QuoteRepository $quoteRepository,
        \Trilix\CrefoPay\Gateway\Config\Config $config,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_paymentConfig = $paymentConfig;
        $this->_quoteRepository = $quoteRepository;
        $this->config = $config;
    }

    /**
     * Determines whether to use Date of Birth and Gender fields based on business type.
     *
     * This function checks if the B2B (Business to Business) mode is enabled in the configuration.
     * If B2B is enabled and the billing address of the quote has a company name, it implies that
     * the transaction is B2B. In such cases, Date of Birth and Gender fields are usually not required,
     * hence the function returns false.
     *
     * If B2B is not enabled or the billing address does not have a company name, it implies a B2C
     * (Business to Customer) transaction where such personal fields are likely required. Therefore,
     * the function returns true in these cases.
     *
     * @return bool Returns true if Date of Birth and Gender fields should be used, false otherwise.
     */
    public function useDateOfBirthAndGenderFields()
    {
        $isB2BEnabled = $this->config->isB2BEnabled();
        $quoteID = $this->getInfoData('quote_id');
        $quote = $this->_quoteRepository->get($quoteID);
        $company = $quote->getBillingAddress()->getCompany();

        $result = true;
        if ($isB2BEnabled && $company) {
            $result = false;
        }

        return $result;
    }

    /**
     * Retrieves the formatted date of birth from the quote.
     * Formats the date from 'Y-m-d' to 'm/d/Y'.
     *
     * @return string Formatted date of birth.
     */
    public function getDob()
    {
        $quoteID = $this->getInfoData('quote_id');
        $quote = $this->_quoteRepository->get($quoteID);

        $dob = $quote->getCustomer()->getDob();

        $formattedDob = '';
        if (!empty($dob)) {
            $date = \DateTime::createFromFormat('Y-m-d', $dob);
            if ($date) {
                $formattedDob = $date->format('m/d/Y');
            }
        }

        return $formattedDob;
    }

    /**
     * Retrieves the gender from the quote and converts it to a standard format.
     * Maps gender ID to predefined salutation constants.
     *
     * @return string|null Gender string or null if gender is not set.
     */
    public function getGender()
    {
        $quoteID = $this->getInfoData('quote_id');
        $quote = $this->_quoteRepository->get($quoteID);

        $gender = null;
        $genderId = $quote->getCustomer()->getGender();

        switch ((int) $genderId) {
            case 1:
                $gender = Person::SALUTATIONMALE;
                break;
            case 2:
                $gender = Person::SALUTATIONFEMALE;
                break;
            default:
                $gender = Person::SALUTATIONVARIOUS;
                break;
        }

        return $gender;
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        $this->_eventManager->dispatch('crefopay_bill_payment_form_block_to_html_before', ['block' => $this]);
        return parent::_toHtml();
    }
}
