<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Block\Adminhtml\Form\Field;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Customer\Model\ResourceModel\Group\Collection as CustomerGroup;
use Magento\Framework\View\Helper\SecureHtmlRenderer;
use Trilix\CrefoPay\Gateway\Config\Config;

/**
 * Class RiskClassByCustomerGroup
 */
class RiskClassByCustomerGroup extends AbstractFieldArray
{
    /** @var CustomerGroup */
    private $customerGroup;

    /** @var bool */
    protected $_addAfter = false;

    /** @var Config */
    private $config;

    /** @var string */
    protected $_template = 'Trilix_CrefoPay::system/config/form/field/array.phtml';

    /**
     * @param CustomerGroup $customerGroup
     * @param Config $config
     * @param Context $context
     * @param array $data
     * @param SecureHtmlRenderer|null $secureRenderer
     */
    public function __construct(
        CustomerGroup $customerGroup,
        Config $config,
        Context $context,
        array $data = [],
        ?SecureHtmlRenderer $secureRenderer = null
    ) {
        parent::__construct($context, $data, $secureRenderer);
        $this->customerGroup = $customerGroup;
        $this->config = $config;
    }

    /**
     * Prepare rendering the new field by adding all the needed columns
     */
    protected function _prepareToRender()
    {
        $this->addColumn('customer_group', ['label' => __('Customer Group'), 'class' => '']);
        $this->addColumn('user_risk_class', ['label' => __('User Risk Class'), 'class' => '']);
    }

    /**
     * @return string[]
     */
    public function getRiskClasses(): array
    {
        return [
            '0.0',
            '0.1',
            '0.2',
            '0.3',
            '0.4',
            '0.5',
            '0.6',
            '0.7',
            '0.8',
            '0.9',
            '1.0',
            '1.1',
            '1.2',
            '1.3',
            '1.4',
            '1.5',
            '1.6',
            '1.7',
            '1.8',
            '1.9',
            '2.0',
        ];
    }

    /**
     * @return array
     */
    public function getArrayRows()
    {
        $result = [];
        $groups = $this->customerGroup->toArray();
        foreach ($groups['items'] as $row) {
            $customerGroupId = (int) $row['customer_group_id'];
            $result[] = [
                'customer_group_id' => $customerGroupId,
                'customer_group_label' => $row['customer_group_code'],
                'name' => "groups[crefopay][groups][user_group_risk_class][fields][ranges][value][" . $row['customer_group_id'] . "]",
                'saved_value' => $this->getSavedRiskClassValueByCustomerGroupId($customerGroupId)
            ];
        }

        $this->config->getCustomerGroupsRiskClasses();
        return $result;
    }

    /**
     * @param int $customerGroupId
     * @return string
     */
    private function getSavedRiskClassValueByCustomerGroupId(int $customerGroupId): string
    {
        $defaultRiskClass = (string) $this->config->getRiskClass();
        $element = $this->getElement();
        if ($element->getValue() && is_array($element->getValue())) {
            foreach ($element->getValue() as $key => $value) {
                list($savedCustomerGroupId, $riskClass) = explode(":", $value);
                if ($savedCustomerGroupId == $customerGroupId) {
                    return $riskClass;
                }
            }
        }
        return $defaultRiskClass;
    }
}
