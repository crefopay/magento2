<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Block\Customer;

use Magento\Vault\Block\AbstractTokenRenderer;
use Trilix\CrefoPay\Model\Ui\DirectDebit\ConfigProvider;
use Magento\Vault\Api\Data\PaymentTokenInterface;
use Magento\Framework\View\Element\Template;

class DirectDebitRenderer extends AbstractTokenRenderer
{
    /**
     * Initialize dependencies.
     *
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     * Can render specified token
     *
     * @param PaymentTokenInterface $token
     * @return boolean
     */
    public function canRender(PaymentTokenInterface $token)
    {
        return $token->getPaymentMethodCode() === ConfigProvider::CODE;
    }

    /**
     * @return string
     */
    public function getAccountHolder(): string
    {
        return $this->getTokenDetails()['accountHolder'];
    }

    /**
     * @return string
     */
    public function getIban(): string
    {
        return $this->getTokenDetails()['iban'];
    }

    /**
     * @return string
     */
    public function getIconUrl()
    {
        return '';
    }

    /**
     * @return int
     */
    public function getIconHeight()
    {
        return 0;
    }

    /**
     * @return int
     */
    public function getIconWidth()
    {
        return 0;
    }
}
