<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Block\Customer;

use Magento\Vault\Block\Customer\PaymentTokens;
use Trilix\CrefoPay\Gateway\Response\CrefoPayPaymentTokenFactory;

class DirectDebitTokens extends PaymentTokens
{
    /**
     * @inheritdoc
     */
    public function getType()
    {
        return CrefoPayPaymentTokenFactory::TOKEN_TYPE_DIRECT_DEBIT;
    }
}
