<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Block\Callback;

class Failure extends \Magento\Checkout\Block\Onepage\Failure
{
    /**
     * @return string
     */
    public function getReorderUrl(): string
    {
        $orderId = $this->_checkoutSession->getLastRealOrder()->getId();

        return $this->getUrl('sales/order/reorder', ['order_id' => $orderId]);
    }
}
