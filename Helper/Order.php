<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Helper;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order as SalesOrder;
use Magento\Sales\Model\OrderRepository;
use Trilix\CrefoPay\Gateway\Config\Config as GatewayConfig;

class Order
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var GatewayConfig
     */
    private $gatewayConfig;

    /**
     * Order constructor.
     *
     * @param OrderRepository $orderRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param GatewayConfig $gatewayConfig
     */
    public function __construct(
        OrderRepository $orderRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        GatewayConfig $gatewayConfig
    ) {
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->gatewayConfig = $gatewayConfig;
    }

    /**
     * @param string $incrementOrderId
     *
     * @return SalesOrder
     * @throws LocalizedException
     */
    public function getOrderByIncrementId(string $incrementOrderId): SalesOrder
    {
        $searchCriteria = $this->searchCriteriaBuilder->addFilter('increment_id', $incrementOrderId)->create();
        $orders = $this->orderRepository->getList($searchCriteria)->getItems();

        if (empty($orders)) {
            throw new LocalizedException(__('Order %1 not found', $incrementOrderId));
        }

        return current($orders);
    }

    /**
     * @param SalesOrder $order
     *
     * @return bool
     */
    public function hasCrefoPayment(SalesOrder $order): bool
    {
        if (!$order->getPayment()) {
            return false;
        }

        if (strpos($order->getPayment()->getMethod(), 'crefopay') === false) {
            return false;
        }

        return true;
    }

    /**
     * @return OrderRepository
     */
    public function getOrderRepository(): OrderRepository
    {
        return $this->orderRepository;
    }

    /**
     * Checks whether at least one product from the order is assigned to the allowed categories for auto capture.
     * And whether payment method allowed to perform auto capture.
     *
     * @param SalesOrder $order
     *
     * @return bool
     * @throws LocalizedException
     */
    public function isEligibleForAutoCapture(SalesOrder $order): bool
    {
        /** @var \Magento\Sales\Model\Order\Payment $payment */
        $payment = $order->getPayment();

        if (!$payment->canCapture()) {
            return false;
        }

        /** @var \Trilix\CrefoPay\Model\Method\Adapter $methodInstance */
        $methodInstance = $payment->getMethodInstance();

        if ($methodInstance->isAutoCaptureEnabled()) {
            return true;
        }

        $allowedCategoryIds = array_map('intval', $this->gatewayConfig->getAutoCaptureCategoryIds());

        if (empty($allowedCategoryIds)) {
            return false;
        }

        /** @var \Magento\Sales\Model\Order\Item $orderItem */
        foreach ($order->getItemsCollection() as $orderItem) {
            $productCategoryIds = array_map('intval', $orderItem->getProduct()->getCategoryIds());

            foreach ($productCategoryIds as $categoryId) {
                if (in_array($categoryId, $allowedCategoryIds)) {
                    return true;
                }
            }
        }

        return false;
    }
}
