# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Known issues]  

- Default Risk Class is set to Trusted instead of Default after installing
- Auto-Fill function from some browser put salutation data into company name
- Checkout button will not be disabled while redirect is loading

## [2.0.5] - 2025-03-07

- adjusted the failure page

## [2.0.4] - 2025-01-22

- implemented Lynck-specific translation for "Cardholder Name"

## [2.0.3] - 2024-12-11

- added an agreements block to GooglePay

## [2.0.2] - 2024-11-19

- added GooglePay intetgration
- corrected discount calculation and total amount validation during createTransaction
- credit card inupts braught back to magento default instead of using a popup window

## [2.0.1] - 2024-07-22

- hotfixed direct debit dob/gender issues

## [2.0.0] - 2024-07-18

- added magento 2.4.7 compatibility

## [1.6.0] - 2024-02-29

### Changed in [1.6.0]

- hotfixed usage of Json class from Zend package, because it's not longer supportet by framework

## [1.5.4] - 2024-01-15

### Fixed in [1.5.4]

- hotfixed basketItemBuilder to accept shops with squaremeter with decimals as quantity values

## [1.5.3] - 2023-12-05

### Added in [1.5.3]

- Initial payment state for prepaid payment method can be directly set to `pending payment`

## [1.5.2] - 2023-11-07

### Added in [1.5.2]

- Backend order creation is now supported for CrefoPay bill payments
- Backend order creation is now supported for CrefoPay prepaid payments

### Fixed in [1.5.2]

- A min-heigth has been added for bill select fields to prevent visualisation issues

## [1.5.1] - 2023-10-26

### Added in [1.5.1]

- New payment method Zinia splitpay
- New payment method Zinie BNPL

## [1.5.0] - 2023-07-21

### Changed in [1.5.0]

- Improved handling while userRiskClasses config values will be loaded from database

## [1.4.9] - 2023-04-11

### Changed in [1.4.9]

- External library from cloudflare has been removed

### Fixed in [1.4.9]

- error handling for frontend error "cancelled by user" has been added
- payment method can be changed again after the PayPal popup window has been closed

## [1.4.8] - 2023-02-03

### Changed in [1.4.8]

- Date of birth labels have become translatable

## [1.4.7] - 2023-01-19

### Fixed in [1.4.7]

- Additional Type-Cast to improve PHP 8.x compatibility

## [1.4.6] - 2022-12-12

### Fixed in [1.4.6]

- Session risk class is considered correctly again
- Sirius-Validation dependency in the clientlibrary has been solved

## [1.4.5] - 2022-11-23

### Changed in [1.4.5]

- Improved handling of the credit card vaults

## [1.4.4] - 2022-11-07

### Changed in [1.4.4]

- Hotfix for handling of credit card 3D secure payments
- Hotfix for missing translations

## [1.4.3] - 2022-10-12

### Changed in [1.4.3]

- Updade PHP clientLibrary version to use PHP8.1 compatible sirius validation
- Update French translations

## [1.4.2] - 2022-08-24

### Added in [1.4.2]

- Added missing SecureFields translations (ES/FR/IT/NL)
- Added PHP 8.0 and 8.1 compatibility

## [1.4.1] - 2022-08-22

### Added in [1.4.1]

- Frontend Error messages are now translated in DE/EN

### Fixed in [1.4.1]

- Fixed some annotations that breaks Magento2 SOAP API
- Fixed an issue with AmazonPay getButtonColor function

## [1.4.0] - 2022-01-14

### Added in [1.4.0]

- CrefoPay UserRiskClass can be defined for Magento2 customer groups

## [1.3.2] - 2022-03-04

### Added in [1.3.2]

- Added third gender support

## [1.3.1] - 2022-01-14

### Changed in [1.3.1]

- Improved handling of multi website configurations

## [1.3.0] - 2021-12-16

### Added in [1.3.0]

- Added AmazonPay as CrefoPay payment gateway
- Added new PayPal gateway flow to prevent redirect after placing order

## [1.2.9] - 2021-05-03

### Fixed in [1.2.9]

- Bugfix: We've removed a typacast in amountBuilder to prevent rounding issues between reserve and order amount
- Improvement: The credit card input field will now be set to 100% width instead of 400px

## [1.2.8] - 2021-01-08

### Fixed in [1.2.8]

- Bugfix: MNS cronjob references to wrong orders

### Added in [1.2.8]

- Improved date of birth input handling

## [1.2.7] - 2020-12-07

### Fixed in [1.2.7]

- Capture Amount
- Translation Issue for Bankaccount Holder

## [1.2.6] - 2020-11-13

### Fixed in [1.2.6]

- BasketBuilder.php now also takes basket items with amounts > 1€ and put it into CrefoPay basket

## [1.2.5] - 2020-11-11

### Added in [1.2.5]

- Plugin now checks if MNS information is already stored in database, before it creates new entries
- When the processing of a notification fails it will be marked for retry. The consumed flag will be now set after 3 processing failures.

### Changed in [1.2.5]

- We are now using round() function instead of ceil() for any amount calculation in AmountBuilder.php and BasketBuilder.php

## [1.2.4] - 2020-11-05

### Fixed in [1.2.4]

- Refund.php calls now $payment->getId() instead of $payment->getParentId()

## [1.2.3] - 2020-10-16

### Added in [1.2.3]

- Full language support for ES, FR, IT, NL

## [1.2.2] - 2020-10-08

### Fixed in [1.2.2]

- In some Magento2/PHP versions the autoCapture didn't work caused by a type missmatch in SubjectReader.php

## [1.2.1] - 2020-10-07

### Fixed in [1.2.1]  

- BIC field has been removed for direct debit payments
- The gender and date of birth fields have been removed for business transactions
- Date of birth input field can now be set to a German format (depending on Magento locale setting)

## [1.2.0] - 2020-09-15

### Added in [1.2.0]

- Compatibility to Magento2.4

## [1.1.3] - 2020-09-04

### Fixed in [1.1.3]

- Error during order placement when module is disabled

## [1.1.2] - 2020-07-08

### Added in [1.1.2]

- Support for CrefoPay config for multiple Websites in Magento admin

## [1.1.1] - 2020-06-18

### Added in [1.1.1]

- Possibility to install module via composer

## [1.1.0] - 2020-05-15

### Changed in [1.1.0]

- Address data management has been improved to accept more formats
- Company names like "Herr", "Mrs", ... will not longer lead into a business check

### Fixed in [1.1.0]

- Datepicker is now working fine also for the latest Safari and IE

### Added in [1.1.0]

- **Feature**: Firmenwissen Smart Sign Up [see also](https://www.firmenwissen.de/)
- A new internal API improves notification handling in high transaction count systems

## [1.0.4] - 2020-05-15

### Changed in [1.0.4]

- Adds compatibility matrix in README.md
- Adds supported browsers in README.md
- Test compatibility for Magento Version 2.3.4

## [1.0.3] - 2020-01-30

### Changed in [1.0.3]

- Structural adaption to prepare composer installation/update support for unreleased version 1.1.x
- Structural adaption to make changelog following md formatting guide line

### Fixed in [1.0.3]

- Invalid Token bug has been completely fixed

## [1.0.2] - 2020-01-13

### Added in [1.0.2]

- Debugging info for communication between Magento and CrefoPay

### Fixed in [1.0.2]

- Checkout errors: Invalid Token
- An error when uncheck/check "My billing and shipping address are the same" on the Payments Step during Checkout

## [1.0.1] - 2019-12-12

### Added in [1.0.1]

- Partial captures and refunds are now supported

### Changed in [1.0.1]

- If some inputs in our secure fields are invalid, we now display the errors below them
- The secure fields library was updated based on the official version
  
## Fixed in [1.0.1]

- Internet Explorer 11 now finds the selected payment method again
- Some cases of a one cent difference were fixed
- The error handling for reservation errors was improved to stop sideeffects with other modules
- Phone numbers and fax numbers should be less often denied by the API
- An error after the install of the module was fixed regarding the environment setting
- The module is now compatible with Magento 2.3.3
- Any redirect payment method now sets the payment transaction to pending until the user authorized the payment and returned

## [1.0.0] - 2019-10-04

### Fixed in [1.0.0]

- EXPIRED notifications now increase stock of products when they are cancelling an order
- Reorder link now works on Magento 2.3.2 again
- Suspected Fraud status is now circumvented when the module captures automatically after a MERCHANTPENDING notification is received
- Notifications now work without form key in Magento 2.3.2
- The shipping costs are now transferred to CrefoPay correctly as an individual basket item
- The gender options on the payment selection are now translatable
- If a transaction could not be created we no longer display our payment methods
- We fixed cases, in which the module was even though it was disabled
- A 0 is added to all phone numbers that don't start with it to avoid rejection by the CrefoPay API
- IBANs with non-numeric characters after the first 4 are now able to be input

## [0.9.4] - 2019-06-04

### Added in [0.9.4]

- Added a setting to do an automatic capture based on categories

### Fixed in [0.9.4]

- Error messages in the frontend no longer default to an empty string in some cases
- Translations for all the translatable module strings were added
- Help texts for the module configuration were added
- Changed the initialization of the Javascript Library to always happen
- Changing the shipping address no longer causes a frontend error (#8)
- AutoCapture per payment method now works with a 0 amount in the notification (#9)
- AutoCapture category setting is now saved under the right config path

## [0.9.3] - 2019-04-04 / 2019-05-02

### Fixed in [0.9.3]

- The MNS handling no longer throws an error when an order was paid with CrefoPay
- Reloading the payment selection does not display the credit card inputs again (#5)
- Cancelling an order does not change the products quantity in the stock (#7)
- Customers can now save their credit cards and bank accounts in Magentos Vault
- Order confirmation emails will be held back until a customer returns from the third-party provider
- Guest checkouts don't use some strange address data anymore
- The date of birth will no longer be filled with the current date
- Translations for the most common checkout errors are now provided in German and English
- The notification handler no longer throws fatal errors in some cases

## [0.9.2] - 2019-03-22 / 2019-03-26

### Fixed in [0.9.2]

- Customers can now pay with another payment method after getting rejected by solvency checks (#6)
- The module now works with PHP 7.0 as well
- The module now works with Magento 2.2.0 as well
- Reserve now displays an error message if it fails and no longer creates an order
- Terms and Conditions can now be confirmed (#1)
- Notifications are now processing correctly and add order status history comments
- Notifications are only processed if the orders are paid via CrefoPay
- Guest checkout uses the input data by the customer and no longer some hard-coded data
- The basket content is now correctly sent to CrefoPay
- Notifications can now process EXPIRED status when another payment method was used(#3)
- JavaScript functionality now works when using the production environment setting (#4)

### Added in [0.9.2]

- Cancellation on a third party site will cancel the Magento 2 order and display a link to re-order immediately  

## [0.9.1] - 2019-02-11

### Added in [0.9.1]

- Asynchronous Processing for CrefoPay notifications via Magento Cron
- Gender and date of birth will be asked for when needed
- Added command to specifically process the CrefoPay notifications

## [0.9] - 2018-12-21

- Initial release
