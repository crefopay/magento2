<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Client;

use Magento\Framework\App\Filesystem\DirectoryList;
use Trilix\CrefoPay\Gateway\Config\Config as GatewayConfig;
use Upg\Library\Config as CrefoPayConfig;

class ConfigFactory
{
    /** @var GatewayConfig */
    private $gatewayConfig;

    /** @var DirectoryList */
    private $directoryList;

    /**
     * ConfigFactory constructor.
     * @param GatewayConfig $gatewayConfig
     * @param DirectoryList $directoryList
     */
    public function __construct(GatewayConfig $gatewayConfig, DirectoryList $directoryList)
    {
        $this->gatewayConfig = $gatewayConfig;
        $this->directoryList = $directoryList;
    }

    /**
     * @param int|null $storeId
     * @return CrefoPayConfig
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function create(?int $storeId = null): CrefoPayConfig
    {
        return new CrefoPayConfig([
            'merchantID'         => $this->gatewayConfig->getMerchantId($storeId),
            'merchantPassword'   => $this->gatewayConfig->getMerchantPassword($storeId),
            'storeID'            => $this->gatewayConfig->getStoreId($storeId),
            'baseUrl'            => $this->gatewayConfig->getBaseUrl(),
            'logEnabled'         => true,
            'logLocationRequest' => $this->directoryList->getPath('var') .
                DIRECTORY_SEPARATOR . 'log' . DIRECTORY_SEPARATOR . 'crefopay.log',
            'logLevel' => 100,
        ]);
    }
}
