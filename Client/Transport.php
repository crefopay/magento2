<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Client;

use Trilix\CrefoPay\Model\AmazonPay\AmazonPayTransactionRepository;
use Upg\Library\Request as UpgRequest;
use Upg\Library\Api as UpgCall;
use Upg\Library\Response\SuccessResponse;
use Upg\Library\Serializer\Exception\VisitorCouldNotBeFound;

class Transport
{
    /** @var ConfigFactory */
    private $configFactory;

    /** @var AmazonPayTransactionRepository */
    private $amazonPayTransactionRepository;

    /**
     * Transport constructor.
     * @param ConfigFactory $configFactory
     * @param AmazonPayTransactionRepository $amazonPayTransactionRepository
     */
    public function __construct(
        ConfigFactory $configFactory,
        AmazonPayTransactionRepository $amazonPayTransactionRepository
    ) {
        $this->configFactory                  = $configFactory;
        $this->amazonPayTransactionRepository = $amazonPayTransactionRepository;
    }

    /**
     * @param UpgRequest\AbstractRequest $request
     * @return SuccessResponse
     * @throws UpgCall\Exception\ApiError
     * @throws UpgCall\Exception\CurlError
     * @throws UpgCall\Exception\InvalidHttpResponseCode
     * @throws UpgCall\Exception\InvalidUrl
     * @throws UpgCall\Exception\JsonDecode
     * @throws UpgCall\Exception\MacValidation
     * @throws UpgCall\Exception\RequestNotSet
     * @throws UpgCall\Exception\Validation
     * @throws \Upg\Library\Mac\Exception\MacInvalid
     */
    public function sendRequest(UpgRequest\AbstractRequest $request): SuccessResponse
    {
        $config = $this->configFactory->create();

        switch (true) {
            case $request instanceof UpgRequest\Finish:
                $call = new UpgCall\Finish($config, $request);
                $this->setCrefoPayOrderId($request);
                break;
            case $request instanceof UpgRequest\Cancel:
                $call = new UpgCall\Cancel($config, $request);
                $this->setCrefoPayOrderId($request);
                break;
            case $request instanceof UpgRequest\Refund:
                $call = new UpgCall\Refund($config, $request);
                $this->setCrefoPayOrderId($request);
                break;
            case $request instanceof UpgRequest\Reserve:
                $call = new UpgCall\Reserve($config, $request);
                $this->setCrefoPayOrderId($request);
                break;
            case $request instanceof UpgRequest\Capture:
                $call = new UpgCall\Capture($config, $request);
                $this->setCrefoPayOrderId($request);
                break;
            case $request instanceof UpgRequest\CreateTransaction:
                $call = new UpgCall\CreateTransaction($config, $request);
                $this->setCrefoPayOrderId($request);
                break;
            case $request instanceof UpgRequest\GetUserPaymentInstrument:
                $call = new UpgCall\GetUserPaymentInstrument($config, $request);
                break;
            case $request instanceof UpgRequest\DeleteUserPaymentInstrument:
                $call = new UpgCall\DeleteUserPaymentInstrument($config, $request);
                break;
            case $request instanceof UpgRequest\GetTransactionStatus:
                $call = new UpgCall\GetTransactionStatus($config, $request);
                $this->setCrefoPayOrderId($request);
                break;
            case $request instanceof UpgRequest\UpdateTransactionData:
                $call = new UpgCall\UpdateTransactionData($config, $request);
                $this->setCrefoPayOrderId($request);
                break;
            default:
                throw new \InvalidArgumentException(
                    sprintf('Can not instantiate CrefoPay call for "%s"', get_class($request))
                );
        }

        try {
            $response = $call->sendRequest();
        } catch (VisitorCouldNotBeFound $e) {
            throw new \InvalidArgumentException($e->getMessage(), $e->getCode(), $e);
        }

        return $response;
    }

    /**
     * @param $request
     */
    private function setCrefoPayOrderId($request)
    {
        $amazonPayTransaction = $this->amazonPayTransactionRepository->getByOrderId($request->getOrderId());
        if ($amazonPayTransaction) {
            $orderId = $amazonPayTransaction->getCrefoPayOrderId();
            $request->setOrderID($orderId);
        }
    }
}
