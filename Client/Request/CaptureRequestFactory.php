<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Client\Request;

use Magento\Framework\Exception\LocalizedException;
use Trilix\CrefoPay\Client\ConfigFactory;
use Trilix\CrefoPay\Helper\Order as OrderHelper;
use Upg\Library\Request\Capture;
use Upg\Library\Request\Objects\Amount;

class CaptureRequestFactory extends AbstractRequestFactory
{
    /** @var OrderHelper */
    private $orderHelper;

    /**
     * CaptureRequestFactory constructor.
     *
     * @param ConfigFactory         $configFactory
     * @param OrderHelper           $orderHelper
     */
    public function __construct(ConfigFactory $configFactory, OrderHelper $orderHelper)
    {
        $this->orderHelper = $orderHelper;
        parent::__construct($configFactory);
    }

    /**
     * @param string $orderId Increment order ID
     * @param int $amount
     * @param int|null $storeId
     * @return Capture
     * @throws LocalizedException
     */
    public function create(string $orderId, int $amount, ?int $storeId = null): Capture
    {
        $captureRequest = new Capture($this->getConfig($storeId));
        $amountObj = new Amount($amount);

        $captureRequest->setOrderID($orderId);
        $captureRequest->setCaptureID($this->getCaptureId($orderId));
        $captureRequest->setAmount($amountObj);

        return $captureRequest;
    }

    /**
     * @param string $orderId Increment order ID
     *
     * @return string
     * @throws LocalizedException
     */
    private function getCaptureId(string $orderId): string
    {
        $order = $this->orderHelper->getOrderByIncrementId($orderId);
        $captureIdx = $order->getInvoiceCollection()->count() + 1;

        return sprintf('%s:%s', $orderId, $captureIdx);
    }
}
