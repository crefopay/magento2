<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Client\Request;

use Magento\Customer\Model\Session;
use Trilix\CrefoPay\Gateway\Config\Config;

class UserRiskManager
{
    /** @var Session */
    private $customerSession;

    /** @var Config */
    private $config;

    /**
     * UserRiskManager constructor.
     * @param Session $customerSession
     * @param Config $config
     */
    public function __construct(Session $customerSession, Config $config)
    {
        $this->customerSession = $customerSession;
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function getUserRiskClass(): string
    {
        $userRiskClass = $this->customerSession->getUserRiskClass();
        if (!empty($userRiskClass)) {
            return strval($userRiskClass['value']);
        }

        $customerGroupId = $this->customerSession->getCustomerGroupId();
        $customerGroupsRiskClass = $this->config->getCustomerGroupsRiskClasses();

        if (array_key_exists($customerGroupId, $customerGroupsRiskClass)) {
            return strval($customerGroupsRiskClass[$customerGroupId]);
        }

        return strval($this->config->getRiskClass());
    }
}
