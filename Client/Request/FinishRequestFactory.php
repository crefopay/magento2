<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Client\Request;

use Upg\Library\Request\Finish;

class FinishRequestFactory extends AbstractRequestFactory
{
    /**
     * @param string $orderId Increment order ID
     *
     * @return Finish
     */
    public function create(string $orderId): Finish
    {
        $refundRequest = new Finish($this->getConfig());

        $refundRequest->setOrderID($orderId);

        return $refundRequest;
    }
}
