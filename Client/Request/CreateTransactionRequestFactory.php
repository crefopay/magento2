<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Client\Request;

use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\Quote\Address;
use Magento\Quote\Model\Quote;
use Magento\Framework\Locale\ResolverInterface as Locale;
use Magento\Customer\Model\Session;
use Upg\Library\Request\CreateTransaction as CreateTransactionRequest;
use Upg\Library\User\Type as UserType;
use Upg\Library\Integration\Type as IntegrationType;
use Trilix\CrefoPay\Gateway\Request\CompanyBuilder;
use Trilix\CrefoPay\Gateway\Request\AddressBuilder;
use Trilix\CrefoPay\Gateway\Request\AmountBuilder;
use Trilix\CrefoPay\Gateway\Request\BasketBuilder;
use Trilix\CrefoPay\Gateway\Request\PersonBuilder;
use Trilix\CrefoPay\Client\ConfigFactory;
use Trilix\CrefoPay\Client\Constants;
use Trilix\CrefoPay\Gateway\Request\User\CrefoPayUserFactory;

class CreateTransactionRequestFactory extends AbstractRequestFactory
{
    /** @var CartRepositoryInterface  */
    private $quoteRepository;

    /** @var CrefoPayUserFactory */
    private $crefoPayUserFactory;

    /** @var Locale */
    private $locale;

    /** @var PersonBuilder */
    private $personBuilder;

    /** @var AddressBuilder */
    private $addressBuilder;

    /** @var BasketBuilder */
    private $basketBuilder;

    /** @var AmountBuilder */
    private $amountBuilder;

    /** @var CompanyBuilder */
    private $companyBuilder;

    /** @var UserRiskManager */
    private $userRiskManager;

    /** @var Session */
    private $customerSession;

    /**
     * CreateTransactionRequestFactory constructor.
     * @param CartRepositoryInterface $quoteRepository
     * @param CrefoPayUserFactory $crefoPayUserFactory
     * @param ConfigFactory $configFactory
     * @param Locale $locale
     * @param PersonBuilder $personBuilder
     * @param AddressBuilder $addressBuilder
     * @param BasketBuilder $basketBuilder
     * @param AmountBuilder $amountBuilder
     * @param CompanyBuilder $companyBuilder
     * @param UserRiskManager $userRiskManager
     * @param Session $customerSession
     */
    public function __construct(
        CartRepositoryInterface $quoteRepository,
        CrefoPayUserFactory $crefoPayUserFactory,
        ConfigFactory $configFactory,
        Locale $locale,
        PersonBuilder $personBuilder,
        AddressBuilder $addressBuilder,
        BasketBuilder $basketBuilder,
        AmountBuilder $amountBuilder,
        CompanyBuilder $companyBuilder,
        UserRiskManager $userRiskManager,
        Session $customerSession
    ) {
        parent::__construct($configFactory);

        $this->quoteRepository = $quoteRepository;
        $this->crefoPayUserFactory = $crefoPayUserFactory;
        $this->locale = $locale;
        $this->personBuilder = $personBuilder;
        $this->addressBuilder = $addressBuilder;
        $this->basketBuilder = $basketBuilder;
        $this->amountBuilder = $amountBuilder;
        $this->companyBuilder = $companyBuilder;
        $this->userRiskManager = $userRiskManager;
        $this->customerSession = $customerSession;
    }

    /**
     * @param int $quoteId
     * @return CreateTransactionRequest
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Upg\Library\Serializer\Exception\VisitorCouldNotBeFound
     */
    public function createForFrontedFlow(int $quoteId): CreateTransactionRequest
    {
        /** @var Quote $quote */
        $quote = $this->quoteRepository->getActive($quoteId);
        $quote->reserveOrderId();

        $integrationType = IntegrationType::INTEGRATION_TYPE_SECURE_FIELDS;
        return $this->create($quote, $integrationType, $quote->getReservedOrderId());
    }

    /**
     * Create a reinitialization request for a CrefoPay transaction.
     *
     * This method generates a new transaction request using the given quote ID and order ID.
     * It retrieves the active quote, prepares the integration type, and creates the transaction request.
     *
     * @param int $quoteId The ID of the quote to be used for the transaction.
     * @param string $orderId The CrefoPay order ID associated with the transaction.
     * 
     * @return CreateTransactionRequest The prepared transaction request object.
     *
     * @throws \Magento\Framework\Exception\LocalizedException If the quote cannot be retrieved or is invalid.
     * @throws \Magento\Framework\Exception\NoSuchEntityException If no active quote is found for the given quote ID.
     * @throws \Upg\Library\Serializer\Exception\VisitorCouldNotBeFound If an error occurs during serialization.
     */
    public function createRecreateRequest(int $quoteId, $orderId): CreateTransactionRequest
    {
        /** @var Quote $quote */
        $quote = $this->quoteRepository->getActive($quoteId);

        $integrationType = IntegrationType::INTEGRATION_TYPE_SECURE_FIELDS;
        return $this->create($quote, $integrationType, $orderId);
    }

    /**
     * @param int $quoteId
     * @param string $orderId
     * @return CreateTransactionRequest
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Upg\Library\Serializer\Exception\VisitorCouldNotBeFound
     */
    public function createForAdminFlow(int $quoteId, string $orderId): CreateTransactionRequest
    {
        /** @var Quote $quote */
        $quote = $this->quoteRepository->get($quoteId);

        $integrationType = IntegrationType::INTEGRATION_TYPE_API;
        return $this->create($quote, $integrationType, $orderId);
    }

    /**
     * @param Quote $quote
     * @return CreateTransactionRequest
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Upg\Library\Serializer\Exception\VisitorCouldNotBeFound
     */
    public function create(Quote $quote, string $integrationType, string $orderId): CreateTransactionRequest
    {
        $createTransactionRequest = new CreateTransactionRequest($this->getConfig());

        $isQuoteVirtual = $quote->isVirtual();

        $billingAddress = $this->getAddress($quote->getBillingAddress(), $isQuoteVirtual);
        $shippingAddress = $this->getAddress($quote->getShippingAddress(), $isQuoteVirtual);

        $email = $shippingAddress->getEmail();

        $crefoPayUser = $this->crefoPayUserFactory->create($billingAddress, $email);

        if (UserType::USER_TYPE_BUSINESS === $crefoPayUser->getType()) {
            $company = $this->companyBuilder->build($billingAddress->getCompany());
            $createTransactionRequest->setCompanyData($company);
        }

        $createTransactionRequest->setUserType($crefoPayUser->getType());
        $createTransactionRequest->setUserID($crefoPayUser->getId());

        $createTransactionRequest->setContext(CreateTransactionRequest::CONTEXT_ONLINE);
        $createTransactionRequest->setIntegrationType($integrationType);

        $createTransactionRequest->setUserData($this->personBuilder->build($billingAddress, $email));

        $createTransactionRequest->setBillingAddress($this->addressBuilder->build($billingAddress));
        $createTransactionRequest->setShippingAddress($this->addressBuilder->build($shippingAddress));

        $createTransactionRequest->setAmount($this->amountBuilder->buildFromQuote($quote));

        $this->basketBuilder->build($quote, $createTransactionRequest);

        $createTransactionRequest->setLocale($this->getLanguageCode());
        $createTransactionRequest->setOrderID($orderId);
        $createTransactionRequest->setUserRiskClass(floatval($this->userRiskManager->getUserRiskClass()));

        $this->setMac($createTransactionRequest);

        return $createTransactionRequest;
    }

    /**
     * Try to map current store's locale to one of the supported CrefoPay locales. Use English if no match.
     *
     * @return string e.g. 'EN'
     */
    private function getLanguageCode(): string
    {
        $locale = $this->locale->getLocale();
        $languageCode = strtoupper(substr($locale, 0, 2));

        if (!in_array($languageCode, Constants::getSupportedLocales())) {
            $languageCode = 'EN';
        }

        return $languageCode;
    }

    /**
     * Mock required address attributes in case user is not logged in.
     *
     * @param Address $address
     * @param bool $isQuoteVirtual
     * @return Address
     */
    private function getAddress(Address $address, bool $isQuoteVirtual): Address
    {
        // On reorder there is email, but no other data, as customer hasn't picked up an address yet
        if ($isQuoteVirtual && (!$address->getEmail() || !$address->getFirstname())) {
            $address->setCountryId('DE');
            $address->setCity('Unknown');
            $address->setPostcode('00000');
            $address->setStreet('Unknown');
            $address->setEmail(sprintf('%s@example.com', $this->customerSession->getSessionId()));
            $address->setFirstname('Guest');
            $address->setLastname('Guest');
        }

        return $address;
    }
}
