<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Client\Request;

use Upg\Library\Request\DeleteUserPaymentInstrument as CrefoPayDeleteUserPaymentInstrument;

class DeleteUserPaymentInstrumentRequestFactory extends AbstractRequestFactory
{
    /**
     * @param string $paymentInstrumentId
     * @return CrefoPayDeleteUserPaymentInstrument
     */
    public function create(string $paymentInstrumentId): CrefoPayDeleteUserPaymentInstrument
    {
        $deleteUserPaymentInstrumentRequest = new CrefoPayDeleteUserPaymentInstrument($this->getConfig());
        $deleteUserPaymentInstrumentRequest->setPaymentInstrumentID($paymentInstrumentId);

        return $deleteUserPaymentInstrumentRequest;
    }
}
