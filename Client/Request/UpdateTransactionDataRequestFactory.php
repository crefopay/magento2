<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Client\Request;

use Magento\Framework\Exception\LocalizedException;
use Trilix\CrefoPay\Api\Data\AmazonPay\AmazonPayTransactionInterface;
use Trilix\CrefoPay\Client\ConfigFactory;
use Upg\Library\Request\UpdateTransactionData;
use Trilix\CrefoPay\Helper\Order;
use Upg\Library\Serializer\Exception\VisitorCouldNotBeFound;

class UpdateTransactionDataRequestFactory extends AbstractRequestFactory
{
    /** @var Order */
    private $orderHelper;

    /**
     * UpdateTransactionDataRequestFactory constructor.
     * @param ConfigFactory $configFactory
     * @param Order $orderHelper
     */
    public function __construct(
        ConfigFactory $configFactory,
        Order $orderHelper
    ) {
        parent::__construct($configFactory);
        $this->orderHelper = $orderHelper;
    }

    /**
     * @param AmazonPayTransactionInterface $amazonPayTransaction
     * @return UpdateTransactionData
     * @throws LocalizedException
     * @throws VisitorCouldNotBeFound
     */
    public function create(AmazonPayTransactionInterface $amazonPayTransaction): UpdateTransactionData
    {
        $crefoPayOrderId = $amazonPayTransaction->getCrefoPayOrderId();
        $magentoIncrementOrderId = $amazonPayTransaction->getOrderId();
        $order = $this->orderHelper->getOrderByIncrementId($magentoIncrementOrderId);

        $updateTransactionDataRequest = new UpdateTransactionData($this->getConfig());
        $updateTransactionDataRequest->setOrderID($crefoPayOrderId);

        $updateTransactionDataRequest->setMerchantReference($order->getRealOrderId());

        $this->setMac($updateTransactionDataRequest);
        return $updateTransactionDataRequest;
    }
}
