<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Client\Request;

use Upg\Library\Request\Objects\Amount;
use Upg\Library\Request\Refund;

class RefundRequestFactory extends AbstractRequestFactory
{
    /**
     * @param string $orderId Increment order ID
     * @param string $captureId
     * @param int $amount
     * @param string $refundDescription
     * @param int|null $storeId
     * @return Refund
     */
    public function create(string $orderId, string $captureId, int $amount, string $refundDescription, ?int $storeId = null): Refund
    {
        $refundRequest = new Refund($this->getConfig($storeId));
        $amountObj = new Amount($amount);

        $refundRequest->setOrderID($orderId);
        $refundRequest->setCaptureID($captureId);
        $refundRequest->setAmount($amountObj);
        $refundRequest->setRefundDescription($refundDescription);

        return $refundRequest;
    }
}
