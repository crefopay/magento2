<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Client\Request;

use Upg\Library\Request\GetUserPaymentInstrument;
use Upg\Library\Serializer\Exception\VisitorCouldNotBeFound;

class GetUserPaymentInstrumentRequestFactory extends AbstractRequestFactory
{
    /**
     * @param string $userId
     * @return GetUserPaymentInstrument
     * @throws VisitorCouldNotBeFound
     */
    public function create(string $userId): GetUserPaymentInstrument
    {
        $getUserPaymentInstrumentRequest = new GetUserPaymentInstrument($this->getConfig());
        $getUserPaymentInstrumentRequest->setUserID($userId);

        $this->setMac($getUserPaymentInstrumentRequest);

        return $getUserPaymentInstrumentRequest;
    }
}
