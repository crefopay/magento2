<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Client\Request;

use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Upg\Library\Request\Reserve as ReserveRequest;
use Trilix\CrefoPay\Client\ConfigFactory;
use Trilix\CrefoPay\Gateway\Request\AmountBuilder;
use Trilix\CrefoPay\Client\Request\Structure\AdditionalInfo as AdditionalInfoStructure;
use Upg\Library\Serializer\Exception\VisitorCouldNotBeFound;

class ReserveRequestFactory extends AbstractRequestFactory
{
    /** @var AmountBuilder */
    private $amountBuilder;

    /**
     * ReserveRequestFactory constructor.
     * @param ConfigFactory $configFactory
     * @param AmountBuilder $amountBuilder
     */
    public function __construct(
        ConfigFactory $configFactory,
        AmountBuilder $amountBuilder
    ) {
        parent::__construct($configFactory);

        $this->amountBuilder = $amountBuilder;
    }

    /**
     * @param string $paymentMethod
     * @param PaymentDataObjectInterface $paymentDO
     * @return ReserveRequest
     * @throws VisitorCouldNotBeFound
     */
    public function create(string $paymentMethod, PaymentDataObjectInterface $paymentDO): ReserveRequest
    {
        $reserveRequest = new ReserveRequest($this->getConfig());
        $reserveRequest->setPaymentMethod($paymentMethod);
        $reserveRequest->setOrderID($paymentDO->getOrder()->getOrderIncrementId());

        $payment = $paymentDO->getPayment();
        $additionalInfo = $payment->getAdditionalInformation();

        if (!empty($additionalInfo['paymentToken']) && $paymentMethod == "GOOGLE_PAY") {
            $reserveRequest->setPaymentToken($additionalInfo['paymentToken']);
            unset($additionalInfo['paymentToken']);
        }

        $additionalInfoStruc = new AdditionalInfoStructure($additionalInfo);


        if (!empty($additionalInfo['paymentInstrumentId'])) {
            $reserveRequest->setPaymentInstrumentID($additionalInfo['paymentInstrumentId']);
        }

        if ($additionalInfoStruc->hasData()) {
            $reserveRequest->setAdditionalInformation((string)$additionalInfoStruc);
        }

        $reserveRequest->setAmount($this->amountBuilder->buildFromOrder($paymentDO->getOrder()));

        $this->setMac($reserveRequest);

        return $reserveRequest;
    }
}
