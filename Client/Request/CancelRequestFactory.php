<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Client\Request;

use Upg\Library\Request\Cancel;

class CancelRequestFactory extends AbstractRequestFactory
{
    /**
     * @param string $orderId
     * @param int|null $storeId
     * @return Cancel
     */
    public function create(string $orderId, ?int $storeId = null): Cancel
    {
        $cancelRequest = new Cancel($this->getConfig($storeId));
        $cancelRequest->setOrderID($orderId);

        return $cancelRequest;
    }
}
