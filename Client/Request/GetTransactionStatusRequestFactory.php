<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Client\Request;

use Trilix\CrefoPay\Client\ConfigFactory;
use Upg\Library\Request\GetTransactionStatus;
use Upg\Library\Serializer\Exception\VisitorCouldNotBeFound;

class GetTransactionStatusRequestFactory extends AbstractRequestFactory
{
    /**
     * GetTransactionStatusRequestFactory constructor.
     * @param ConfigFactory $configFactory
     */
    public function __construct(ConfigFactory $configFactory)
    {
        parent::__construct($configFactory);
    }

    /**
     * @param string $orderId
     * @return GetTransactionStatus
     * @throws VisitorCouldNotBeFound
     */
    public function create(string $orderId): GetTransactionStatus
    {
        $getTransactionStatus = new GetTransactionStatus($this->getConfig());
        $getTransactionStatus->setOrderID($orderId);

        $this->setMac($getTransactionStatus);

        return $getTransactionStatus;
    }
}
