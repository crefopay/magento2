<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Client\Request;

use Trilix\CrefoPay\Client\ConfigFactory;
use Upg\Library\Config as CrefoPayConfig;
use Upg\Library\Request\AbstractRequest;
use Upg\Library\Request\MacCalculator;

abstract class AbstractRequestFactory
{
    /** @var ConfigFactory */
    private $configFactory;

    /** @var CrefoPayConfig */
    private $config;

    /**
     * AbstractRequestFactory constructor.
     * @param ConfigFactory $configFactory
     */
    public function __construct(ConfigFactory $configFactory)
    {
        $this->configFactory = $configFactory;
    }

    /**
     * @param AbstractRequest $request
     * @throws \Upg\Library\Serializer\Exception\VisitorCouldNotBeFound
     */
    protected function setMac(AbstractRequest $request)
    {
        $macCalculator = new MacCalculator();
        $macCalculator->setConfig($this->getConfig());
        $macCalculator->setRequest($request);
        $request->setMac($macCalculator->calculateMac());
    }

    /**
     * @param int|null $storeId
     * @return CrefoPayConfig
     */
    protected function getConfig(?int $storeId = null): CrefoPayConfig
    {
        if (!$this->config) {
            $this->config = $this->configFactory->create($storeId);
        }

        return $this->config;
    }
}
