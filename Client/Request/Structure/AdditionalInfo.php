<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Client\Request\Structure;

/**
 * Represents https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/additional-reserve-information
 */
class AdditionalInfo
{
    public const IDEAL_BANK_DATA = 'idealBankData';
    public const PAYMENT_TOKEN = 'paymentToken';
    public const SALUTATION = 'salutation';
    public const DATE_OF_BIRTH = 'dateOfBirth';

    public const SALUTATION_M = 'M'; // Male
    public const SALUTATION_F = 'F'; // Female
    public const SALUTATION_V = 'V'; // Various

    /** @var array */
    private $data = [];

    public function __construct(array $data)
    {
        foreach (self::getAllowedFields() as $fieldName) {
            if (!empty($data[$fieldName])) {
                $this->data[$fieldName] = $data[$fieldName];
            }
        }
    }

    /**
     * @return array
     */
    public static function getAllowedFields(): array
    {
        return [
            self::IDEAL_BANK_DATA,
            self::SALUTATION,
            self::DATE_OF_BIRTH,
            self::PAYMENT_TOKEN,
        ];
    }

    /**
     * @return bool
     */
    public function hasData(): bool
    {
        foreach ($this->data as $item) {
            if (!empty($item)) {
                return true;
            }
        }

        return false;
    }

    public function __toString()
    {
        return json_encode($this->data);
    }
}
