<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Client;

use Magento\Framework\Exception\FileSystemException;
use Upg\Library\Callback\MacCalculator as CallbackMacCalculator;

class UpgFactory
{
    /** @var ConfigFactory */
    private $configFactory;

    /**
     * UpgFactory constructor.
     *
     * @param ConfigFactory $configFactory
     */
    public function __construct(ConfigFactory $configFactory)
    {
        $this->configFactory = $configFactory;
    }

    /**
     * @param array $request
     *
     * @return CallbackMacCalculator
     * @throws FileSystemException
     */
    public function createCallbackMacCalculator(array $request): CallbackMacCalculator
    {
        $config = $this->configFactory->create();

        return new CallbackMacCalculator($config, $request);
    }
}
