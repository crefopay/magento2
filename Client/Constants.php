<?php

declare(strict_types=1);

namespace Trilix\CrefoPay\Client;

class Constants
{
    /**
     * Transaction statuses
     */
    public const TX_NEW          = 'NEW';
    public const TX_ACK          = 'ACKNOWLEDGEPENDING';
    public const TX_FRAUD        = 'FRAUDPENDING';
    public const TX_FRAUD_CANCEL = 'FRAUDCANCELLED';
    public const TX_CIA          = 'CIAPENDING';
    public const TX_MERCHANT     = 'MERCHANTPENDING';
    public const TX_CANCELLED    = 'CANCELLED';
    public const TX_EXPIRED      = 'EXPIRED';
    public const TX_IN_PROGRESS  = 'INPROGRESS';
    public const TX_DONE         = 'DONE';

    /**
     * Order statuses
     */
    public const O_PENDING    = 'PAYPENDING';
    public const O_PAID       = 'PAID';
    public const O_CLEARED    = 'CLEARED';
    public const O_FAILED     = 'PAYMENTFAILED';
    public const O_CHARGEBACK = 'CHARGEBACK';
    public const O_INDUNNING  = 'INDUNNING';
    public const O_COLLECTION = 'IN_COLLECTION';

    /**
     * @return array
     */
    public static function getAllTransactionStatuses(): array
    {
        return [
            self::TX_NEW,
            self::TX_ACK,
            self::TX_FRAUD,
            self::TX_FRAUD_CANCEL,
            self::TX_CIA,
            self::TX_MERCHANT,
            self::TX_CANCELLED,
            self::TX_EXPIRED,
            self::TX_IN_PROGRESS,
            self::TX_DONE,
        ];
    }

    /**
     * @return array
     */
    public static function getAllOrderStatuses(): array
    {
        return [
            self::O_PENDING,
            self::O_PAID,
            self::O_CLEARED,
            self::O_FAILED,
            self::O_CHARGEBACK,
            self::O_INDUNNING,
            self::O_COLLECTION,
        ];
    }

    /**
     * Supported locales by CrefoPay
     * @link https://docs.crefopay.de/api/?php#languages
     *
     * @return string[]
     */
    public static function getSupportedLocales(): array
    {
        return ['EN', 'DE', 'ES', 'FR', 'IT', 'NL'];
    }

    /**
     * @param string $transactionStatus See TX_* constants
     *
     * @return bool
     */
    public static function isValidTransactionStatus(string $transactionStatus): bool
    {
        return in_array($transactionStatus, self::getAllTransactionStatuses());
    }

    /**
     * @param string $orderStatus See O_* constants
     *
     * @return bool
     */
    public static function isValidOrderStatus(string $orderStatus): bool
    {
        return in_array($orderStatus, self::getAllOrderStatuses());
    }
}
