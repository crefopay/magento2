define([
    'Magento_Ui/js/form/element/ui-select',
    'underscore'
], function (Select, _) {
    'use strict';

    return Select.extend({
        initialize: function () {
            this._super();

            _.each(this.source.data, function (id) {
                this.toggleOptionSelected({value: String(id)});
            }, this);
        }
    });
});
