define([
    'Magento_Ui/js/form/provider',
    'jquery'
], function (Provider, $) {
    'use strict';

    return Provider.extend({
        save: function (options) {
            this.data = {
                categories: this.categories,
                scope: {
                    // website: $('#website_switcher').val() // per-website loading not implemented
                }
            };

            this._super(options);
        }
    });
});
