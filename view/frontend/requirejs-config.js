var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/view/payment/list': {
                'Trilix_CrefoPay/js/view/payment/list-mixin': true
            },
            'Magento_Checkout/js/action/set-shipping-information': {
                'Trilix_CrefoPay/js/action/set-shipping-information': true
            },
            'Magento_Checkout/js/model/payment-service': {
                'Trilix_CrefoPay/js/model/payment-service': true
            },
            'Magento_Checkout/js/view/billing-address': {
                'Trilix_CrefoPay/js/view/billing-address': true
            },
            'Magento_Checkout/js/model/error-processor': {
                'Trilix_CrefoPay/js/model/error-processor': true
            },
            'Magento_Checkout/js/view/shipping-address/list': {
                'Trilix_CrefoPay/js/view/shipping-address/list': true
            },
            'Magento_Checkout/js/view/shipping-address/address-renderer/default': {
                'Trilix_CrefoPay/js/view/shipping-address/address-renderer/default': true
            },
            'Magento_Checkout/js/view/form/element/email': {
                'Trilix_CrefoPay/js/view/form/element/email': true
            },
            'Magento_Checkout/js/view/shipping': {
                'Trilix_CrefoPay/js/view/shipping': true
            }
        }
    },
    map: {
        '*': {
            sha: 'Trilix_CrefoPay/js/lib/sha',
            cookies: 'Trilix_CrefoPay/js/lib/jscookie',
            smartsignup: 'Trilix_CrefoPay/js/lib/smartsignup',
            smartui: 'Trilix_CrefoPay/js/lib/smartui',
            amazonPayButton: 'Trilix_CrefoPay/js/amazon-button',
            amazonPayProductAdd: 'Trilix_CrefoPay/js/amazon-product-add'          
        }
    },
    paths: {
        amazonPayCheckout: 'https://static-eu.payments-amazon.com/checkout.js',
        amazonPayClient: 'https://sandbox.crefopay.de/libs/3.0/amazon-client.js'        
    },
};
