define([
    'Magento_Checkout/js/model/quote',
    'mage/storage',
    'Magento_Checkout/js/model/url-builder',
    'Magento_Checkout/js/model/full-screen-loader',
    'Magento_Checkout/js/model/error-processor',
    'Trilix_CrefoPay/js/model/amazon-storage'
], function (quote, storage, urlBuilder, fullScreenLoader, errorProcessor, amazonStorage) {
    'use strict';

    return function (addressType, callback) {
        var serviceUrl = urlBuilder.createUrl('/crefopay/amazon-pay-checkout/:orderId/' + addressType + '-address', {
            orderId: amazonStorage.getOrderId()
        });

        fullScreenLoader.startLoader();

        var handleResponse = function(data) {
            fullScreenLoader.stopLoader(true);
            callback(data.length ? data.shift() : {});
        }

        var validateAndHandle = function(data) {
            if (!data) {
                amazonStorage.clearAmazonCheckout();
                window.location.replace(window.checkoutConfig.checkoutUrl);
            }
            handleResponse(data);
        }

        var responseCallback = addressType == 'shipping' ? validateAndHandle : handleResponse;

        return storage.get(serviceUrl).done(responseCallback).fail(function (response) {
            errorProcessor.process(response);
            fullScreenLoader.stopLoader(true);
        });
    };
});
