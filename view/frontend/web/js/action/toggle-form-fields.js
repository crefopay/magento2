define([
    'jquery',
    'Trilix_CrefoPay/js/model/amazon-storage'
], function ($, amazonStorage) {
    'use strict';

    return function (formSelector, hideValid) {
        var result = true;
        if (amazonStorage.isAmazonCheckout()) {
            var $form = $(formSelector);
            var $errorFields = $form.find('.field._error');
            if (hideValid) {
                $form.find('.field').hide();
                $errorFields.show();
            }
            result = $errorFields.length === 0;
        }
        return result;
    };
});
