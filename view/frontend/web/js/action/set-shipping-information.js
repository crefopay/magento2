/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote',
    'Magento_Customer/js/model/customer',
], function (wrapper, quote, customer) {
    'use strict';

    return function (target) {
        return wrapper.wrap(target, function (originalAction) {

            // Checkout for guest
            if (!customer.isLoggedIn()) {
                quote.shippingAddress()['email'] = quote.guestEmail;
            }

            return originalAction();
        });
    };
});

