/*browser:true*/
/*global define*/
define([
    'sha',
    'cookies',
    'jquery',
    'smartui'
], function (jsSHA, Cookies, $) {

    function ApiCommunicator(apiUrlConfiguration) {

        var _apiConfiguration = setIfArgumentIsValid(apiUrlConfiguration);

        var _csrfCheck = false;

        var _macCalculator = new MacCalculator();

        function setIfArgumentIsValid(argument, errorMessage) {
            if (typeof(argument) == 'undefined' || argument === null) {
                throw errorMessage === null ? "invalidArgument" : errorMessage;
            }

            return argument;
        }

        function getRequestData(request, contentType) {
            if (contentType === 'application/json; charset=UTF-8') {
                return JSON.stringify(request);
            }
            return request;
        }

        function isMacPartOfRequestObject(contentType) {
            return contentType === 'application/json; charset=UTF-8' ? false : true;
        }

        function randomCSRFToken() {
            return Math.random().toString(36).substring(7);
        }

        this.turnOnCsrfCheck = function () {
            _csrfCheck = true;
        };

        this.sendRequest = function (request, token, handler) {
            var requestUrl = _apiConfiguration[request.requestType].url;
            var hMacAlgorithm = _apiConfiguration['HMAC_ALGORITHM'];

            var contentType = request.contentType;
            var async = request.async ? request.async : true;
            delete request.requestType;
            delete request.contentType;
            delete request.async;

            var requestData = getRequestData(request, contentType);

            if (isMacPartOfRequestObject(contentType)) {
                requestData.mac = _macCalculator.calculateRequestMac(requestData, token, hMacAlgorithm);
            }

            var csrfTokenName = "CSRF-TOKEN-" + randomCSRFToken();

            $.ajax({
                type: 'POST',
                url: requestUrl,
                data: requestData,
                contentType: contentType,
                beforeSend: function (request) {
                    if (_csrfCheck) {
                        var csrfToken = randomCSRFToken();
                        request.setRequestHeader(csrfTokenName, csrfToken);
                        request.setRequestHeader("TOKEN-NAME", csrfTokenName);
                    }
                    if (!isMacPartOfRequestObject(contentType)) {
                        request.setRequestHeader(_apiConfiguration['MAC_HEADER'], _macCalculator.calculateMac(requestData, token, hMacAlgorithm));
                        request.setRequestHeader(_apiConfiguration['TOKEN_HEADER'], token);
                    }

                    Cookies.set(csrfTokenName, csrfToken);
                }
            })  // Remove cookie
                .always(function () {
                    Cookies.remove(csrfTokenName);
                })
                // Will be called in case of successfully processed request
                .done(function (data, textStatus, jqXHR) {
                    // Check if response is manipulated
                    handleResponse(jqXHR, token, handler, data);
                })
                // Will call handler with JSON object if possible in other case it will send a text message
                .fail(function (jqXHR, textStatus, errorThrown) {
                    handleResponse(jqXHR, token, handler);
                });
        };


        function isResponseMacCorrect(data, token, jqXHR) {
            var hMacAlgorithm = _apiConfiguration['HMAC_ALGORITHM'];
            var calculatedResponseMac = _macCalculator.calculateResponseMac(data, token, hMacAlgorithm);

            var macHeader = _apiConfiguration['MAC_HEADER'];

            if (macHeader) {
                var responseMac = jqXHR.getResponseHeader(macHeader);
                return responseMac === calculatedResponseMac;
            }

            return true;
        }

        function getDataFromJqXHR(jqXHR) {
            var data = null;
            if (typeof jqXHR.responseJSON === 'undefined') {
                data = jqXHR.responseText;
            } else {
                data = jqXHR.responseJSON;
            }
            return data;
        }

        function handleResponse(jqXHR, token, handler, data) {
            var response = data ? data : getDataFromJqXHR(jqXHR);

            var respondWith = response;
            var isMacCorrect = false;
            if (jqXHR.status == 200 || jqXHR.status == 400) {
                isMacCorrect = isResponseMacCorrect(response, token, jqXHR);
                if (!isMacCorrect) {
                    respondWith = createErrorResponse(5000, "invalidMac", response);
                }
            } else {
                respondWith = createErrorResponse(5000, "communicationError", response);
            }

            handler(respondWith, jqXHR, isMacCorrect);
        }

        function createErrorResponse(resultCode, message, serverResponse) {
            var response = {resultCode: resultCode, message: message};
            if(serverResponse){
                response.details = serverResponse;
            }
            return response;
        }
    }
    function MacCalculator() {

        this.calculateMac = function (text, key, algorithm) {
            var cleanText = text.replace(/\s+/g, "");

            var shaObj = new jsSHA(algorithm ? algorithm : "SHA-1", "TEXT");
            shaObj.setHMACKey(key, "TEXT");
            shaObj.update(cleanText);

            return shaObj.getHMAC("HEX");
        };

        this.calculateRequestMac = function (requestObject, key, algorithm) {
            var text = "";
            Object.keys(requestObject).sort().forEach(function (element) {
                if (element != "requestType" && requestObject[element] !== undefined && requestObject[element] !== null) {
                    text += requestObject[element];
                }
            });

            return this.calculateMac(text, key, algorithm);
        };

        this.calculateResponseMac = function (responseObject, key, algorithm) {
            var dataAsJson = typeof responseObject == 'string' ? getObject(responseObject) : responseObject;

            var dataAsString = JSON.stringify(dataAsJson);

            return this.calculateMac(dataAsString, key, algorithm);
        };

        function getObject(data){
            if (typeof data == 'string'){
                var tmpData = data.trim();
                if(tmpData.length <= 0){
                    return {};
                }
                try{
                    return JSON.parse(data);
                } catch(e){
                    return {};
                }
            }
        }
    }
    /**
     * Class used for collecting information from UI
     *
     * @constructor
     */
    function UIHelper() {

        var _self = this;

        var _placeHolderPrefix = 'data-crefopay';

        /**
         * Helper function for adding key value paris to object
         *
         * @param requestObject
         * @param key String
         * @param value String
         * @param trimSpaces boolean
         */
        function addValueToRequest(requestObject, key, value, trimSpaces) {
            if (value) {
                requestObject[key] = trimSpaces ? value.replace(/\s/g, '') : value.trim();
            }
        }

        /**
         * Get selected payment method
         * @returns {string} selected payment method
         */
        this.getPaymentMethod = function () {
            return getElementValue("*[" + _placeHolderPrefix + "='paymentMethod']", "checked", "CC");
        };

        function getPaymentInstrumentCvv() {
            return getElementValue("*[" + _placeHolderPrefix + "='paymentInstrument.cvv']", "visible");
        }

        /**
         * Get element by element locator, if element locator give more elements they will be filtered by provided filter
         * @param elementLocator
         * @param filter
         * @param defaultValue
         * @returns element valu or default value
         */
        function getElementValue(elementLocator, filter, defaultValue) {
            var element = $(elementLocator);
            if (element.length > 1) {
                element = element.filter(":" + filter);
            }
            return element.val() ? element.val() : defaultValue;
        }

        /**
         * Get Payment Instrument ID from UI
         * @returns payment Instrument ID
         */
        this.getPaymentInstrumentId = function () {
            return getElementValue("*[" + _placeHolderPrefix + "='paymentInstrument.id']", "checked");
        };

        /**
         * Get payment instrument for payment method
         *
         * @param paymentMethod
         * @returns payment instrument (BankAccount or CreditCard) or undefined if there is no fields for payment instrument presented
         */
        this.getPaymentInstrument = function (paymentMethod) {
            var paymentInstrument = {};

            switch (paymentMethod) {
                case  "CC":
                case  "CC3D":
                    addValueToRequest(paymentInstrument, 'paymentInstrumentID', _self.getPaymentInstrumentId());
                    addValueToRequest(paymentInstrument, 'paymentInstrumentType', 'CREDITCARD');
                    addValueToRequest(paymentInstrument, 'number', $("*[" + _placeHolderPrefix + "='paymentInstrument.number']").val(), true);
                    addValueToRequest(paymentInstrument, 'accountHolder', $("*[" + _placeHolderPrefix + "='paymentInstrument.accountHolder']").val());
                    addValueToRequest(paymentInstrument, 'validity', $("*[" + _placeHolderPrefix + "='paymentInstrument.validity']").val());
                    addValueToRequest(paymentInstrument, 'cvv', getPaymentInstrumentCvv(), true);
                    return jQuery.isEmptyObject(paymentInstrument) ? undefined : paymentInstrument;
                case "DD":
                    addValueToRequest(paymentInstrument, 'paymentInstrumentType', 'BANKACCOUNT');
                    addValueToRequest(paymentInstrument, 'iban', $("*[" + _placeHolderPrefix + "='paymentInstrument.iban']").val(), true);
                    addValueToRequest(paymentInstrument, 'bic', $("*[" + _placeHolderPrefix + "='paymentInstrument.bic']").val(), true);
                    addValueToRequest(paymentInstrument, 'accountHolder', $("*[" + _placeHolderPrefix + "='paymentInstrument.bankAccountHolder']").val());
                    return jQuery.isEmptyObject(paymentInstrument) ? undefined : paymentInstrument;
                case "AMAZON_PAY":
                    addValueToRequest(paymentInstrument, 'amazonPayReference', document.cookie.replace(/(?:(?:^|.*;\s*)crefopay_amazonToken\s*\=\s*([^;]*).*$)|^.*$/, "$1"));
                    addValueToRequest(paymentInstrument, 'amazonAccessToken', document.cookie.replace(/(?:(?:^|.*;\s*)crefopay_amazonAccessToken\s*\=\s*([^;]*).*$)|^.*$/, "$1"));
                    return jQuery.isEmptyObject(paymentInstrument) ? undefined : paymentInstrument;
                default:
                    return undefined;
            }
        };

        /**
         * Collect billing information from UI
         * @returns billing information of undefined in case if no billing information presented on UI
         */
        this.getBillingAddress = function () {
            var billingAddress = {};
            addValueToRequest(billingAddress, 'street', $("*[" + _placeHolderPrefix + "='billingAddress.street']:visible").val());
            addValueToRequest(billingAddress, 'no', $("*[" + _placeHolderPrefix + "='billingAddress.no']:visible").val());
            addValueToRequest(billingAddress, 'zip', $("*[" + _placeHolderPrefix + "='billingAddress.zip']:visible").val());
            addValueToRequest(billingAddress, 'city', $("*[" + _placeHolderPrefix + "='billingAddress.city']:visible").val());
            addValueToRequest(billingAddress, 'state', $("*[" + _placeHolderPrefix + "='billingAddress.state']:visible").val());
            addValueToRequest(billingAddress, 'country', $("*[" + _placeHolderPrefix + "='billingAddress.country']:visible").val());
            return jQuery.isEmptyObject(billingAddress) ? undefined : billingAddress;
        };

        /**
         * Collect user information from UI
         *
         * @returns {*} user data or undefined if object is empty
         */
        this.getUserData = function () {
            var userData = {};
            addValueToRequest(userData, 'salutation', $("*[" + _placeHolderPrefix + "='userData.salutation']:visible").val(), true);
            addValueToRequest(userData, 'name', $("*[" + _placeHolderPrefix + "='userData.name']:visible").val());
            addValueToRequest(userData, 'surname', $("*[" + _placeHolderPrefix + "='userData.surname']:visible").val());
            addValueToRequest(userData, 'dateOfBirth', $("*[" + _placeHolderPrefix + "='userData.dateOfBirth']:visible").val(), true);
            addValueToRequest(userData, 'email', $("*[" + _placeHolderPrefix + "='userData.email']:visible").val(), true);
            addValueToRequest(userData, 'phoneNumber', $("*[" + _placeHolderPrefix + "='userData.phoneNumber']:visible").val(), true);
            addValueToRequest(userData, 'faxNumber', $("*[" + _placeHolderPrefix + "='userData.faxNumber']:visible").val(), true);
            return jQuery.isEmptyObject(userData) ? undefined : userData;
        };

        /**
         * Get userId used in case of recurring customer.
         *
         * @returns userId or undefined
         */
        this.getUserId = function () {
            return $("*[" + _placeHolderPrefix + "='userData.userID']").val();
        };

        /**
         * Get billingRecipient information from UI
         * @returns billingRecipient or undefined
         */
        this.getBillingRecipient = function () {
            return $("*[" + _placeHolderPrefix + "='billingRecipient']:visible").val();
        };

        /**
         * Get billingRecipientAddition information from UI
         * @returns billingRecipientAddition or undefined
         */
        this.getBillingRecipientAddition = function () {
            return $("*[" + _placeHolderPrefix + "='billingRecipientAddition']:visible").val();
        };
    }

    var CrefoPayConnectUiHelpr = new UIHelper();
    var CrefopayUiHelpr = new UIHelper();
    function AbstractRequestBuilder() {
        /**
         * Helper function for adding key value paris to object
         *
         * @param requestObject
         * @param key
         * @param value
         */
        this.addValueToRequest = function (requestObject, key, value) {
            if (value) {
                requestObject[key] = value;
            }
        };

    }

    function UIRegisterPaymentToSessionRequestBuilder(token) {

        var _token = token;

        var _self = this;

        AbstractRequestBuilder.apply(this);

        this.createRegisterPaymentRequest = function (systemUrl, sessionId) {
            var paymentMethod = CrefopayUiHelpr.getPaymentMethod();
            var request = {
                call: 'REGISTER_PAYMENT',
                paymentMethod: paymentMethod,
                shopPublicKey: _token,
                systemUrl: systemUrl,
                sessionId: sessionId
            };

            _self.addValueToRequest(request, 'paymentInstrumentID', CrefopayUiHelpr.getPaymentInstrumentId());

            if ((paymentMethod === 'DD' && !request.paymentInstrumentID) || paymentMethod === 'AMAZON_PAY') {
                _self.addValueToRequest(request, 'paymentInstrument', CrefopayUiHelpr.getPaymentInstrument(paymentMethod));
            }

            return request;
        };

    }
    return {
        SmartSignUpClient: function(shopPublicToken, onInit, configuration, elements) {
            var _token = setToken(shopPublicToken);
            var _systemUrl = getUrl(configuration);
            var _itemCount = getItemCount(configuration);
            var _suggestionConfiguration = getSuggestionConfiguration(configuration);
            var _apiCommunicator = new ApiCommunicator(SmartSignUpApi);

            function SmartSignUpApi() {
            }

            SmartSignUpApi["MAC_HEADER"] = 'X-UPG-HMAC';
            SmartSignUpApi ["TOKEN_HEADER"] = 'X-UPG-TOKEN';
            SmartSignUpApi ["HMAC_ALGORITHM"] = 'SHA-512';
            registerEndPoint('GET_SUGGESTION', 'suggestCompany');
            registerEndPoint('INIT_AUTOCOMPLETE', 'init');

            initSmartSignUp();

            function initSmartSignUp() {
                sendApiRequest({token: _token, async: false, requestType: 'INIT_AUTOCOMPLETE'}, function (data) {
                    if (data.resultCode === 0) {
                        initAutoComplete();
                    }
                    try {
                        onInit(data);
                    } catch (err) {
                        console.log(err);
                    }
                });
            }

            function initAutoComplete() {
                if (elements['company'].value()) {
                    $('#' + elements['companyDomElement']).autocomplete({
                        minLength: 3,
                        delay: 300,
                        source: suggestionApiSource,
                        select: function (event, ui) {
                            elements['company'].value(ui.item.name);
                            elements['country'].value(ui.item.land);
                            elements['postcode'].value(ui.item.plz);
                            elements['city'].value(ui.item.ort);

                            var strasseHausnummer = ui.item.strasseHausnummer;
                            var streetNo = strasseHausnummer.match(/\d+\D*$/g);
                            var street = strasseHausnummer;
                            if (streetNo) {
                                street = strasseHausnummer.substr(0, strasseHausnummer.length - streetNo[0].length);
                                elements['streetNo'].value(streetNo[0]);
                            }
                            $("input[data-crefopay-placeholder='address.streetWithNumber']").val(strasseHausnummer).change();
                            elements['street'].value(street.trim());
                            $("input[data-crefopay-placeholder='crefoNo']").val(ui.item.crefonummer);
                            return false;
                        },

                    }).data("uiAutocomplete")._renderItem = function (ul, item) {
                        var suggestionText = String(item.name).replace(new RegExp(this.term, "gi"), "<span class='ui-state-highlight'>$&</span>");
                        if (_suggestionConfiguration["zip"]) {
                            suggestionText += ", <span class='plz'>" + item.plz + "</span>";
                        }
                        if (_suggestionConfiguration["city"]) {
                            suggestionText += ", <span class='ort'>" + item.ort + "</span>";
                        }
                        if (_suggestionConfiguration["county"]) {
                            suggestionText += ", <span class='county'>" + item.bundesland + "</span>";
                        }
                        if (_suggestionConfiguration["country"]) {
                            suggestionText += ", <span class='country'>" + item.land + "</span>";
                        }
                        return $("<li>").append(suggestionText).appendTo(ul);
                    };
                }

                function suggestionApiSource(request, response) {
                    sendApiRequest({
                        token: _token,
                        searchTerm: request.term,
                        async: false,
                        requestType: 'GET_SUGGESTION'
                    }, function (data) {
                        if (data.resultCode === 0) {
                            response(data.result.companyNameSuggestions.slice(0, _itemCount));
                        }
                    });
                }
            }

            function sendApiRequest(request, handler) {
                var requestObject = request;
                requestObject.token = _token;
                requestObject.contentType = 'application/json; charset=UTF-8';
                _apiCommunicator.sendRequest(requestObject, _token, handler);
            }

            function checkIfArgumentIsValid(argument, errorMessage) {
                if (typeof(argument) == 'undefined' || argument === null || (argument instanceof String && argument.trim() === '')) {
                    throw errorMessage === null ? "invalidArgument" : errorMessage;
                }
            }

            function setToken(shopPublicToken) {
                checkIfArgumentIsValid(shopPublicToken, "invalidToken");
                return shopPublicToken;
            }

            function registerEndPoint(endPoint, url) {
                SmartSignUpApi[endPoint] = {url: _systemUrl + url};
            }

            function getUrl(configuration) {
                var configUrl = configuration && typeof configuration === 'object' ? configuration['url'] : configuration;
                return configUrl ? configUrl : 'https://api.crefopay.de/autocomplete/';
            }

            function getSuggestionConfiguration(configuration) {
                var config = {"city": false, "county": false, "country": false, "zip": false};
                if (configuration && typeof configuration === 'object' && configuration['suggestion'] && typeof configuration['suggestion'] === 'object') {
                    return configuration['suggestion'];
                }
                return config;
            }

            function getItemCount(configuration) {
                var itemCount = configuration && typeof configuration === 'object' ? configuration['itemCount'] : 15;
                return itemCount ? itemCount : 15;
            }
        }
    };
});
