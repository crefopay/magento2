define(
    ['uiRegistry'],
    function (registry) {
        'use strict';

        var config = registry.get('crefopay-amazon-pay') || {};

        return {
            /**
             * @returns {string}
             */
            getCode: function () {
                return this.getValue('code');
            },

            /**
             * Get config value
             */
            getValue: function (key, defaultValue) {
                if (config.hasOwnProperty(key)) {
                    return config[key];
                } else if (defaultValue !== undefined) {
                    return defaultValue;
                }
            },

            /**
             * Is CrefoPay AmazonPay defined?
             */
            isDefined: function () {
                return registry.get('crefopay-amazon-pay') !== undefined;
            }

        };
    }
);
