/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery',
    'mage/utils/wrapper',
    'mage/translate'
], function ($, wrapper) {
    'use strict';

    return function (targetModule) {
        var func = targetModule.process;
        var processWrapper = wrapper.wrap(func, function (original, response, messageContainer) {
            var isCrefopayError = response.responseJSON.message === 'crefopay_reserve_error';

            if (isCrefopayError) {
                response.responseText = JSON.stringify(
                    {
                        "message": $.mage.__('Transaction has been declined. Please try again later.'),
                        "trace": response.responseJSON.trace
                    }
                );
            }

            original(response, messageContainer);

            if (isCrefopayError) {
                window.location.reload();
            }
        });

        targetModule.process = processWrapper;
        return targetModule;
    };
});
