/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/step-navigator',
    'Trilix_CrefoPay/js/view/payment/adapter',
    'Trilix_CrefoPay/js/model/amazon-storage'
], function ($, wrapper, stepNavigator, adapter, amazonStorage) {
    'use strict';

    return function(targetModule) {
        if (amazonStorage.getCheckoutSessionId()) {
            return targetModule;
        }
        var func = targetModule.setPaymentMethods;
        var setPaymentMethodsWrapper = wrapper.wrap(func, function(original) {
            // check if module is enabled
            if (
                window.checkoutConfig.payment['crefopay_bill'].isActive
                && (stepNavigator.isProcessed('shipping') === false ||
                stepNavigator.isProcessed('payment') === false)
            ) {
                adapter.initialize();
            }
            original();
        });

        targetModule.setPaymentMethods = setPaymentMethodsWrapper;
        return targetModule;
    };
});
