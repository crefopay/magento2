define([
    'Trilix_CrefoPay/js/model/crefopay-amazonpay-config',
    'jquery',
    'mage/storage',
    'mage/url',
    'jquery/jquery-storageapi',
], function (crefoPayAmazonPayConfig, $, mageStorage, urlBuilder) {
    'use strict';

    var isEnabled = crefoPayAmazonPayConfig.isDefined(),
        storage = null,
        getStorage = function () {
            if (storage === null) {
                storage = $.initNamespaceStorage('crefopay-amazon-checkout-session').localStorage;
            }
            return storage;
        };

    return {
        isEnabled: isEnabled,

        /**
         * Return CrefoPay Amazon Checkout Session ID
         */
        getCheckoutSessionId: function () {
            var sessionId = getStorage().get('id');
            var param = 'crefoPayAmazonCheckoutSessionId';

            var myParams = new URLSearchParams(window.location.search);
            if (myParams.has(param)) {
                var paramSessionId = myParams.get(param);
                if (typeof sessionId === 'undefined' || paramSessionId !== sessionId) {
                    sessionId = paramSessionId;
                    getStorage().set('id', sessionId);
                }
            }
            return sessionId;
        },

        /**
         * Return Amazon Order ID
         */
        getOrderId: function () {
            var orderId = getStorage().get('orderId');
            var param = 'orderId';

            var myParams = new URLSearchParams(window.location.search);
            if (myParams.has(param)) {
                var paramOrderId = myParams.get(param);
                if (typeof orderId === 'undefined' || paramOrderId !== orderId) {
                    orderId = paramOrderId;
                    getStorage().set('orderId', orderId);
                }
            }
            return orderId;
        },

        /**
         * Is checkout using Amazon Pay?
         *
         * @returns {boolean}
         */
        isAmazonCheckout: function () {
            return typeof this.getCheckoutSessionId() === 'string';
        },

        /**
         * Clear Amazon Checkout Session ID and revert checkout
         */
        clearAmazonCheckout: function() {
            getStorage().removeAll();
            let url = urlBuilder.build('/crefopay/amazonpay/clearamazonsessiondata');
            mageStorage.post(url).done().fail(function (response) {
                console.log("Error during CrefoPay Amazon Pay session data deletion");
            });
        },
    };
});
