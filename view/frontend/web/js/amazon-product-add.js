define([
    'jquery',
    'Magento_Customer/js/customer-data'
], function ($, customerData) {
    'use strict';

    var _this,
        addedViaAmazon = false;

    $.widget('amazon.AmazonProductAdd', {
        options: {
            addToCartForm: '#product_addtocart_form',
            parentSelector: '.crefopay-amazon-button-product-page'
        },

        /**
         * Create triggers
         */
        _create: function () {
            _this = this;
            // Hide for Edge, since click event does not trigger
            if (/Edge\/\d./i.test(navigator.userAgent)) {
                $(this.options.parentSelector).hide();
            }
            this.setupTriggers();
        },

        /**
         * Setup triggers when item added to cart if amazon pay button pressed
         */
        setupTriggers: function () {
            this.cart = customerData.get('cart');

            //subscribe to add to cart event
            this.cart.subscribe(function () {
                //only trigger the amazon button click if the user has chosen to add to cart via this method
                if (addedViaAmazon) {
                   addedViaAmazon = false;
                    $('#PayWithAmazon-Product').click();
                }
            }, this);

            //setup binds for click
            $('.crefopay-amazon-addtoCart').on('click', function (e) {
                var target = e.target;
                if (target.classList.contains('crefopay-amazon-addtoCart') && $(_this.options.addToCartForm).valid()) {
                    addedViaAmazon = true;
                    $(_this.options.addToCartForm).submit();
                }
            });
        }

    });

    return $.amazon.AmazonProductAdd;
});
