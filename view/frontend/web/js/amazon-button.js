define([
    'uiComponent',
    'Trilix_CrefoPay/js/amazon-checkout',
], function (
    Component,
    amazonCheckout
) {
    'use strict';
    return Component.extend({
        initialize: function (config, node) {
            switch (config.place) {
                case 'minicart':
                    amazonCheckout.renderMinicartButton();
                    break;
                case 'cart':
                    amazonCheckout.renderCartButton();
                    break;
                case 'checkout':
                    amazonCheckout.renderCheckoutButton();
                    break;
                case 'product':
                    amazonCheckout.renderProductPageButton();
                    break;
            }
        }
    });
});
