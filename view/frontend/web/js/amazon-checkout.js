define([
    'jquery',
    'require',
    'Trilix_CrefoPay/js/model/crefopay-amazonpay-config',
], function ($, require, crefoPayAmazonPayConfig) {
    'use strict';

    return {
        client: null,

        renderProductPageButton: function() {
            this.getAmazonClient('Product', function(client) {
                client.createButton("amazonPayButton-product");
            })
        },

        renderMinicartButton: function() {
            this.getAmazonClient('Cart', function(client) {
                client.createButton("amazonPayButton-minicart");
            })
        },

        renderCartButton: function() {
            this.getAmazonClient('Cart', function(client) {
                client.createButton("amazonPayButton-cart");
            })
        },

        renderCheckoutButton: function() {
            this.getAmazonClient('Checkout', function(client) {
                client.createButton("amazonPayButton-checkout");
            })
        },

        getAmazonClient: function (placement, callback) {
            require(['https://static-eu.payments-amazon.com/checkout.js', crefoPayAmazonPayConfig.getValue('amazonClientJsUrl')], function() {
                function initializationCompleteCallback(response) {
                    console.log(placement + ': crefopay_amazon_pay_button_initialization');
                }
                function onButtonClickCallback() {
                }

                if (!this.client) {
                    this.client = new AmazonClient(
                        crefoPayAmazonPayConfig.getValue('shopPublicKey'),
                        {
                            'url': crefoPayAmazonPayConfig.getValue('url'),
                            'buttonColor': crefoPayAmazonPayConfig.getValue('buttonColor'),
                            'placement': placement,
                            'checkoutLanguage': crefoPayAmazonPayConfig.getValue('checkoutLanguage'),
                            'sandbox': crefoPayAmazonPayConfig.getValue('sandbox'),
                            'autoCapture': crefoPayAmazonPayConfig.getValue('autoCapture'),
                            'deliverySpecifications': crefoPayAmazonPayConfig.getValue('deliverySpecifications'),
                        },
                        initializationCompleteCallback,
                        onButtonClickCallback
                    );
                }
                callback(this.client)

            }.bind(this));
        },
    };
});
