/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'Magento_Checkout/js/action/set-billing-address',
    'Magento_Ui/js/model/messageList'
], function(setBillingAddressAction, globalMessageList) {
    'use strict';

    var mixin = {

        /**
         * Trigger action to update shipping and billing addresses
         */
        updateAddresses: function () {
            setBillingAddressAction(globalMessageList);
        }
    };

    return function (target) { // target == Result that Magento_Ui/.../columns returns.
        return target.extend(mixin); // new result that all other modules receive
    };
});
