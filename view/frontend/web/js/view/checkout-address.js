/*global define*/
define(
    [
        'jquery',
        'uiComponent',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/model/quote',
        'Trilix_CrefoPay/js/model/amazon-storage',
        'Magento_Checkout/js/model/shipping-service',
        'Magento_Checkout/js/model/address-converter',
        'Magento_Checkout/js/action/create-shipping-address',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/model/checkout-data-resolver',
        'Magento_Checkout/js/model/step-navigator',
        'uiRegistry',
        'Trilix_CrefoPay/js/action/checkout-session-address-load',
    ],
    function (
        $,
        Component,
        customer,
        quote,
        amazonStorage,
        shippingService,
        addressConverter,
        createShippingAddress,
        checkoutData,
        checkoutDataResolver,
        stepNavigator,
        registry,
        checkoutSessionAddressLoad,
    ) {
        'use strict';

        var self;

        return Component.extend({
            isCustomerLoggedIn: customer.isLoggedIn,
            isAmazonCheckout: amazonStorage.isAmazonCheckout(),
            isPayOnly: false,
            rates: shippingService.getShippingRates(),

            /**
             * Init
             */
            initialize: function () {
                self = this;
                this._super();
                if (this.isAmazonCheckout) {
                    this.getShippingAddressFromAmazon();
                }
            },

            /**
             * Retrieve shipping address from Amazon API
             */
            getShippingAddressFromAmazon: function() {
                checkoutSessionAddressLoad('shipping', function (amazonAddress) {
                    setTimeout((function() {
                        return function() {
                            var addressData = createShippingAddress(amazonAddress),
                                checkoutProvider = registry.get('checkoutProvider');
                            var addressConvert;

                            self.setEmail(amazonAddress.email);

                            // Fill in blank street fields
                            if ($.isArray(addressData.street)) {
                                for (var i = addressData.street.length; i <= 2; i++) {
                                    addressData.street[i] = '';
                                }
                            }

                            // Save shipping address
                            addressConvert = addressConverter.quoteAddressToFormAddressData(addressData);
                            checkoutData.setShippingAddressFromData(addressConvert);
                            checkoutProvider.set('shippingAddress', addressConvert);

                            checkoutDataResolver.resolveEstimationAddress();
                        };
                    })(self), 500);
                });
            },

            /**
             * Set email address
             * @param email
             */
            setEmail: function(email) {
                $('#customer-email').val(email).trigger('change');
                $('#customer-email').prop( "disabled", true );
                checkoutData.setInputFieldEmailValue(email);
                checkoutData.setValidatedEmailValue(email);
                quote.guestEmail = email;
            }

        });
    }
);
