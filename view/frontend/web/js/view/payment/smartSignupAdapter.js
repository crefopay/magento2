/*browser:true*/
/*global define*/
define([
    'jquery',
    'smartsignup'
], function ($, smartsignup) {
    'use strict';

    return {
        /**
         * @returns {Object}
         */
        initialize: function (elements) {
            let publicToken = window.checkoutConfig.payment['crefopay_bill'].publicToken;
            let smartSignupUrl = window.checkoutConfig.payment['crefopay_bill'].smartSignupUrl;

            let configuration = {
                "url": smartSignupUrl,
                "itemCount": 20,
                "suggestion": {"city": true, "country": true, "zip": true, "county": true}
            };

            new smartsignup.SmartSignUpClient(
                publicToken, function (data) {
                if (data.resultCode !== 0) {
                    console.log(JSON.stringify(data, null, 4));
                }

            }, configuration, elements);
        },
    };
});
