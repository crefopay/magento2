define([
    'Trilix_CrefoPay/js/model/amazon-storage'
], function (storage) {
    'use strict';

    var mixin = {
        /**
         * Check if a payment method is applicable with Amazon Pay
         * @param {String} method
         * @returns {Boolean}
         * @private
         */
        hidePaymentMethod: function (method) {
            return (storage.isAmazonCheckout() && method !== 'crefopay_amazon_pay') ||
                (!storage.isAmazonCheckout() && method === 'crefopay_amazon_pay')
        },

        /**
         * Create renderer.
         *
         * @param {Object} paymentMethodData
         */
        createRenderer: function (paymentMethodData) {
            if (!this.hidePaymentMethod(paymentMethodData.method)) {
                this._super();
            }
        }
    };

    return function (target) {
        return target.extend(mixin);
    };
});
