/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function(
        Component,
        rendererList
    ) {
        'use strict';

        rendererList.push({
            type: 'crefopay_bill',
            component: 'Trilix_CrefoPay/js/view/payment/method-renderer/crefopay_bill'
        }, {
            type: 'crefopay_cash_in_advance',
            component: 'Trilix_CrefoPay/js/view/payment/method-renderer/crefopay_cash_in_advance'
        }, {
            type: 'crefopay_cash_on_delivery',
            component: 'Trilix_CrefoPay/js/view/payment/method-renderer/crefopay_cash_on_delivery'
        }, {
            type: 'crefopay_credit_card',
            component: 'Trilix_CrefoPay/js/view/payment/method-renderer/crefopay_credit_card'
        }, {
            type: 'crefopay_credit_card_tds',
            component: 'Trilix_CrefoPay/js/view/payment/method-renderer/crefopay_credit_card_3d'
        }, {
            type: 'crefopay_direct_debit',
            component: 'Trilix_CrefoPay/js/view/payment/method-renderer/crefopay_direct_debit'
        }, {
            type: 'crefopay_paypal',
            component: 'Trilix_CrefoPay/js/view/payment/method-renderer/crefopay_paypal'
        }, {
            type: 'crefopay_sofort',
            component: 'Trilix_CrefoPay/js/view/payment/method-renderer/crefopay_sofort'
        }, {
            type: 'crefopay_zinia_invoice',
            component: 'Trilix_CrefoPay/js/view/payment/method-renderer/crefopay_zinia_invoice'
        }, {
            type: 'crefopay_zinia_ratepay',
            component: 'Trilix_CrefoPay/js/view/payment/method-renderer/crefopay_zinia_ratepay'
        }, {
            type: 'crefopay_ideal',
            component: 'Trilix_CrefoPay/js/view/payment/method-renderer/crefopay_ideal'
        }, {
            type: 'crefopay_amazon_pay',
            component: 'Trilix_CrefoPay/js/view/payment/method-renderer/amazon_pay'
        },
        {
            type: 'crefopay_google_pay',
            component: 'Trilix_CrefoPay/js/view/payment/method-renderer/crefopay_google_pay'
        });

        /** Add view logic here if needed */
        return Component.extend({

        });
    }
);