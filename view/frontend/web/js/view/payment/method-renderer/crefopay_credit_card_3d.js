/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'Trilix_CrefoPay/js/view/payment/adapter',
        'mage/url',
        'Magento_Vault/js/view/payment/vault-enabler',
    ],
    function ($, Component, adapter, urlBuilder, VaultEnabler) {
        'use strict';

        return Component.extend({
            redirectAfterPlaceOrder: false, // Don't redirect to the success page

            defaults: {
                template: 'Trilix_CrefoPay/payment/credit_card',
                paymentInstrumentId: null,
                orderIncrementId: 0
            },

            /**
             * @returns {exports.initialize}
             */
            initialize: function () {
                this._super();

                this.vaultEnabler = new VaultEnabler();
                this.vaultEnabler.setPaymentCode(this.getVaultCode());

                return this;
            },

            /**
             * Get payment name
             *
             * @returns {String}
             */
            getCode: function() {
                return 'crefopay_credit_card_tds';
            },

            /**
             * Place order
             */
            beforePlaceOrder: function () {
                var self = this;
                var deferred = $.Deferred();

                adapter.registerPayment(deferred);
                $.when(deferred).done(function (response) {
                    console.log(response);
                    self.orderIncrementId = response.hasOwnProperty('orderNo') ? response.orderNo : 0;
                    self.paymentInstrumentId = response.paymentInstrumentId;
                    self.placeOrder();
                })
            },

            afterPlaceOrder: function () {
                if (!this.orderIncrementId) {
                    return;
                }

                var redirectUrl = urlBuilder.build('crefopay/payment/redirect?oid=' + this.orderIncrementId);
                window.location.replace(redirectUrl);
            },

            /**
             * @returns {Object}
             */
            getData: function () {
                var data = {
                    'method': this.getCode(),
                    'additional_data': {
                        'paymentInstrumentId': this.paymentInstrumentId
                    }
                };

                this.vaultEnabler.visitAdditionalData(data);

                return data;
            },

            /**
             * @returns {Bool}
             */
            isVaultEnabled: function () {
                return this.vaultEnabler.isVaultEnabled();
            },

            /**
             * @returns {String}
             */
            getVaultCode: function () {
                return window.checkoutConfig.payment[this.getCode()].ccVaultCode;
            },

            /**
             * @returns {String}
             */
            getCrefoPayMethodCode: function () {
                return window.checkoutConfig.payment[this.getCode()].crefoPayMethodCode;
            },

            initializeAdapter: function () {
                adapter.initialize();
            },

            getLogoSrc: function () {
                return window.checkoutConfig.payment[this.getCode()].logo;
            },
        });
    }
);
