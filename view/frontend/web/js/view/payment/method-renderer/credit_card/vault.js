define([
    'jquery',
    'Magento_Vault/js/view/payment/method-renderer/vault',
    'Trilix_CrefoPay/js/view/payment/adapter',
    'mage/url',
], function ($, VaultComponent, adapter, urlBuilder) {
    'use strict';

    return VaultComponent.extend({
        redirectAfterPlaceOrder: false, // Don't redirect to the success page

        defaults: {
            template: 'Trilix_CrefoPay/payment/credit_card/vault',
            additionalData: {},
            orderIncrementId: 0
        },

        /**
         * Get last 4 digits of card
         * @returns {String}
         */
        getMaskedCard: function () {
            return this.details.maskedCC;
        },

        /**
         * Get expiration date
         * @returns {String}
         */
        getExpirationDate: function () {
            return this.details.expirationDate;
        },

        /**
         * Get card type
         * @returns {String}
         */
        getCardType: function () {
            return this.details.type;
        },

        /**
         * Place order
         */
        beforePlaceOrder: function () {
            var self = this;
            var deferred = $.Deferred();

            adapter.registerPayment(deferred);
            $.when(deferred).done(function (response) {
                console.log(response);
                self.orderIncrementId = response.hasOwnProperty('orderNo') ? response.orderNo : 0;
                self.paymentInstrumentId = response.paymentInstrumentId;
                self.placeOrder();
            });
        },

        afterPlaceOrder: function () {
            if (!this.orderIncrementId) {
                return;
            }

            var redirectUrl = urlBuilder.build('crefopay/payment/redirect?oid=' + this.orderIncrementId);
            window.location.replace(redirectUrl);
        },

        /**
         * Get payment method data
         * @returns {Object}
         */
        getData: function () {
            var data = {
                'method': this.code,
                'additional_data': {
                    'public_hash': this.publicHash,
                    'paymentInstrumentId': this.paymentInstrumentId
                }
            };

            data['additional_data'] = _.extend(data['additional_data'], this.additionalData);

            return data;
        },

        /**
         *
         * @returns {String}
         */
        getPaymentInstrumentId: function () {
            return this.paymentInstrumentId;
        },

        initializeAdapter: function () {
            adapter.initialize();
        },

        /**
         * Get CrefoPay LibraryCode for Payment Method
         * @returns {String}
         */
        getCrefoPayMethodCode: function () {
            return this.methodCode;
        }
    });
});
