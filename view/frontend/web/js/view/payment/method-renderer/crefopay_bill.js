/*browser:true*/
/*global define*/
define(
    [
        'ko',
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'Trilix_CrefoPay/js/view/payment/adapter',
        'Magento_Customer/js/customer-data',
        'Magento_Checkout/js/model/quote',
        'Trilix_CrefoPay/js/lib/dobpicker'
    ],
    function (ko, $, Component, adapter, customerData, quote, dobpicker) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Trilix_CrefoPay/payment/bill'
            },

            getCode: function() {
                return 'crefopay_bill';
            },

            getData: function() {
                let data = {
                    'method': this.getCode(),
                };
                if (this.useDateOfBirthAndGenderFields()) {
                    data['additional_data'] = {
                        'dateOfBirth': $('#crefopay-bill-dob-year').val() + '-' + $('#crefopay-bill-dob-month').val() + '-' + $('#crefopay-bill-dob-day').val(),
                        'salutation': $('#crefopay-bill-gender').val()
                    }
                }
                return data;
            },

            beforePlaceOrder: function () {
                var self = this;
                var deferred = $.Deferred();

                adapter.registerPayment(deferred);
                $.when(deferred).done(function (response) {
                    console.log(response);
                    self.placeOrder();
                });
            },

            getCrefoPayMethodCode: function () {
                return window.checkoutConfig.payment[this.getCode()].crefoPayMethodCode;
            },

            getLogoSrc: function () {
                return window.checkoutConfig.payment[this.getCode()].logo;
            },

            initializeDobField: function () {
                var day = '#crefopay-bill-dob-day';
                var month = '#crefopay-bill-dob-month';
                var year = '#crefopay-bill-dob-year';

                dobpicker.initialize({
                    daySelector: day,                   /* Required */
                    monthSelector: month,               /* Required */
                    yearSelector: year,                 /* Required */
                    dayDefault: $.mage.__('Day'),       /* Optional */
                    monthDefault: $.mage.__('Month'),   /* Optional */
                    yearDefault: $.mage.__('Year'),     /* Optional */
                    minimumAge: 12,                     /* Optional */
                    maximumAge: 88                      /* Optional */
                });
                if (this.getCustomerDob()) {
                    //console.log(this.getCustomerDob().split('-'));
                    dobpicker.setDate(
                        {
                            daySelector: day,
                            monthSelector: month,
                            yearSelector: year,
                        },
                        this.getCustomerDob().split('-')
                    );
                }
            },

            getGender: function () {
                return window.checkoutConfig.payment[this.getCode()].gender
            },

            getCustomerDob: function () {
                let dob = null;
                let registeredCustomerDob = window.checkoutConfig.payment[this.getCode()].dob;
                if (registeredCustomerDob) {
                    dob = registeredCustomerDob;
                }
                return dob;
            },

            useDateOfBirthAndGenderFields: function () {
                let registeredCustomerDob = window.checkoutConfig.payment[this.getCode()].isB2BEnabled;
                let billingAddressData = quote.billingAddress();
                return !(registeredCustomerDob && billingAddressData && billingAddressData.company);
            }
        });
    }
);
