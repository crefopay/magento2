/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'Trilix_CrefoPay/js/view/payment/adapter',
        'mage/url'
    ],
    function ($, Component, adapter, urlBuilder) {
        'use strict';

        return Component.extend({
            redirectAfterPlaceOrder: false, // Don't redirect to the success page

            defaults: {
                template: 'Trilix_CrefoPay/payment/form',
                orderIncrementId: 0
            },

            getCode: function() {
                return 'crefopay_zinia_invoice';
            },

            beforePlaceOrder: function () {
                var self = this;
                var deferred = $.Deferred();

                adapter.registerPayment(deferred);
                $.when(deferred).done(function (response) {
                    self.orderIncrementId = response.hasOwnProperty('orderNo') ? response.orderNo : 0;
                    console.log(response);
                    self.placeOrder();
                });
            },

            afterPlaceOrder: function () {
                if (!this.orderIncrementId) {
                    return;
                }

                var redirectUrl = urlBuilder.build('crefopay/payment/redirect?oid=' + this.orderIncrementId);
                window.location.replace(redirectUrl);
            },

            getData: function () {
                return {
                    'method': this.getCode(),
                    'additional_data': {}
                };
            },

            getCrefoPayMethodCode: function () {
                return window.checkoutConfig.payment[this.getCode()].crefoPayMethodCode;
            },

            getLogoSrc: function () {
                return window.checkoutConfig.payment[this.getCode()].logo;
            },
        });
    }
);
