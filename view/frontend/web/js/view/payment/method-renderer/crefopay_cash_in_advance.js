/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'Trilix_CrefoPay/js/view/payment/adapter'
    ],
    function ($, Component, adapter) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Trilix_CrefoPay/payment/form'
            },

            getCode: function() {
                return 'crefopay_cash_in_advance';
            },

            beforePlaceOrder: function () {
                var self = this;
                var deferred = $.Deferred();

                adapter.registerPayment(deferred);
                $.when(deferred).done(function (response) {
                    console.log(response);
                    self.placeOrder();
                });
            },

            getData: function () {
                return {
                    'method': this.getCode(),
                    'additional_data': {}
                };
            },

            getCrefoPayMethodCode: function () {
                return window.checkoutConfig.payment[this.getCode()].crefoPayMethodCode;
            },

            getLogoSrc: function () {
                return window.checkoutConfig.payment[this.getCode()].logo;
            },
        });
    }
);
