/*browser:true*/
/*global define*/
define(
    [
        'ko',
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'Trilix_CrefoPay/js/view/payment/adapter',
        'Magento_Customer/js/customer-data',
        'Magento_Vault/js/view/payment/vault-enabler',
        'Magento_Checkout/js/model/quote',
        'Trilix_CrefoPay/js/lib/dobpicker'
    ],
    function (ko, $, Component, adapter, customerData, VaultEnabler, quote, dobpicker) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Trilix_CrefoPay/payment/direct_debit',
                paymentInstrumentId: null,
            },

            /**
             * @returns {exports.initialize}
             */
            initialize: function () {
                this._super();

                this.vaultEnabler = new VaultEnabler();
                this.vaultEnabler.setPaymentCode(this.getVaultCode());

                return this;
            },

            getCode: function() {
                return 'crefopay_direct_debit';
            },

            beforePlaceOrder: function () {
                var self = this;
                var deferred = $.Deferred();

                adapter.registerPayment(deferred);
                $.when(deferred).done(function (response) {
                    console.log(response);
                    self.paymentInstrumentId = response.paymentInstrumentId;
                    self.placeOrder();
                });
            },

            getData: function () {
                let data = {
                    'method': this.getCode(),
                };
                if (this.useDateOfBirthAndGenderFields()) {
                    data['additional_data'] = {
                        'paymentInstrumentId': this.paymentInstrumentId,
                        'dateOfBirth': $('#crefopay-dd-dob-year').val() + '-' + $('#crefopay-dd-dob-month').val() + '-' + $('#crefopay-dd-dob-day').val(),
                        'salutation': $('#crefopay-dd-gender').val()
                    }
                } else {
                    data['additional_data'] = {
                        'paymentInstrumentId': this.paymentInstrumentId
                    }
                }

                this.vaultEnabler.visitAdditionalData(data);

                return data;
            },

            /**
             * @returns {Bool}
             */
            isVaultEnabled: function () {
                return this.vaultEnabler.isVaultEnabled();
            },

            /**
             * @returns {String}
             */
            getVaultCode: function () {
                return window.checkoutConfig.payment[this.getCode()].ccVaultCode;
            },

            getCrefoPayMethodCode: function () {
                return window.checkoutConfig.payment[this.getCode()].crefoPayMethodCode;
            },

            initializeAdapter: function () {
                adapter.initialize();
                this.initializeDobField();
            },

            getLogoSrc: function () {
                return window.checkoutConfig.payment[this.getCode()].logo;
            },

            initializeDobField: function () {
                var day = '#crefopay-dd-dob-day';
                var month = '#crefopay-dd-dob-month';
                var year = '#crefopay-dd-dob-year';
                dobpicker.initialize({
                    daySelector: day, /* Required */
                    monthSelector: month, /* Required */
                    yearSelector: year, /* Required */
                    dayDefault: '   Day', /* Optional */
                    monthDefault: 'Month', /* Optional */
                    yearDefault: 'Year', /* Optional */
                    minimumAge: 12, /* Optional */
                    maximumAge: 88 /* Optional */
                });
                if (this.getCustomerDob()) {
                    dobpicker.setDate(
                        {
                            daySelector: day,
                            monthSelector: month,
                            yearSelector: year,
                        },
                        this.getCustomerDob().split('-')
                    );
                }
            },

            getGender: function () {
                return window.checkoutConfig.payment[this.getCode()].gender
            },

            getCustomerDob: function () {
                let dob = null;
                let registeredCustomerDob = window.checkoutConfig.payment[this.getCode()].dob;
                if (registeredCustomerDob) {
                    dob = registeredCustomerDob;
                }
                return dob;
            },

            useDateOfBirthAndGenderFields: function () {
                let registeredCustomerDob = window.checkoutConfig.payment[this.getCode()].isB2BEnabled;
                let billingAddressData = quote.billingAddress();
                return !(registeredCustomerDob && billingAddressData && billingAddressData.company);
            }
        });
    }
);
