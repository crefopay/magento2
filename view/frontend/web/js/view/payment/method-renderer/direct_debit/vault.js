define([
    'jquery',
    'Magento_Vault/js/view/payment/method-renderer/vault',
    'Trilix_CrefoPay/js/view/payment/adapter',
], function ($, VaultComponent, adapter) {
    'use strict';

    return VaultComponent.extend({
        defaults: {
            template: 'Trilix_CrefoPay/payment/direct_debit/vault',
            additionalData: {}
        },

        /**
         * @returns {String}
         */
        getAccountHolder: function () {
            return this.details.accountHolder;
        },

        /**
         * @returns {String}
         */
        getIban: function () {
            return this.details.iban;
        },

        /**
         * Place order
         */
        beforePlaceOrder: function () {
            var self = this;
            var deferred = $.Deferred();

            adapter.registerPayment(deferred);
            $.when(deferred).done(function (response) {
                console.log(response);
                self.paymentInstrumentId = response.paymentInstrumentId;
                self.placeOrder();
            });
        },

        /**
         * Get payment method data
         * @returns {Object}
         */
        getData: function () {
            var data = {
                'method': this.code,
                'additional_data': {
                    'public_hash': this.publicHash,
                    'paymentInstrumentId': this.paymentInstrumentId
                }
            };

            data['additional_data'] = _.extend(data['additional_data'], this.additionalData);

            return data;
        },

        /**
         *
         * @returns {String}
         */
        getPaymentInstrumentId: function () {
            return this.paymentInstrumentId;
        },

        initializeAdapter: function () {
            adapter.initialize();
        },

        getCrefoPayMethodCode: function () {
            return window.checkoutConfig.payment['crefopay_direct_debit'].crefoPayMethodCode;
        }
    });
});
