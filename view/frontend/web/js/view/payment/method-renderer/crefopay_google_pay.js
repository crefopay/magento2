/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'Trilix_CrefoPay/js/view/payment/adapter',
        'Trilix_CrefoPay/js/googlepay-checkout',        
        'mage/url',
        'Magento_Checkout/js/model/totals',
        'Magento_Checkout/js/model/quote'
    ],
    function($, Component, adapter, gpcheckout, urlBuilder, totals, quote) {
        'use strict';

        return Component.extend({
            redirectAfterPlaceOrder: false, // Don't redirect to the success page

            defaults: {
                template: 'Trilix_CrefoPay/payment/google_pay',   
                paymentToken: null,             
            },

            getCode: function() {
                return 'crefopay_google_pay';
            },

            initGP: function() {                
                gpcheckout.onGooglePayLoaded();
            },
            
            beforePlaceOrder: function() {
                var self = this;
                var deferredAdapter = $.Deferred();
                var paymentTokenPromise;

                adapter.registerPayment(deferredAdapter);
                paymentTokenPromise = gpcheckout.onGooglePaymentButtonClicked(this.getTransactionData());                

                $.when(deferredAdapter, paymentTokenPromise).done(function(response, paymentToken) {
                    self.orderIncrementId = response.hasOwnProperty('orderNo') ? response.orderNo : 0;
                    console.log(response);
                    self.paymentToken = paymentToken;
                    self.placeOrder();
                }).fail(function(err) {
                    console.error(err);
                    // Handle error, e.g., show an error message
                });
            },

            afterPlaceOrder: function() {
                if (!this.orderIncrementId) {
                    return;
                }

                var redirectUrl = urlBuilder.build('crefopay/payment/redirect?oid=' + this.orderIncrementId);
                window.location.replace(redirectUrl);
            },

            getData: function() {
                return {
                    'method': this.getCode(),
                    'additional_data': {
                        'paymentToken': this.paymentToken
                    }
                };
            },

            getCrefoPayMethodCode: function() {
                return window.checkoutConfig.payment[this.getCode()].crefoPayMethodCode;
            },

            getLogoSrc: function() {
                return window.checkoutConfig.payment[this.getCode()].logo;
            },

            /**
             * Retrieve dynamic transaction data
             *
             * @returns {Object} transactionData
             */
            getTransactionData: function () {
                // Fetch the currency code from the quote totals
                var currencyCode = quote.totals() && quote.totals()['base_currency_code'] ? quote.totals()['base_currency_code'] : 'EUR'; // Default to 'EUR' if not found

                // Fetch the shipping address from the quote
                var shippingAddress = quote.shippingAddress();
                var countryCode = shippingAddress && shippingAddress.countryId ? shippingAddress.countryId : 'DE'; // Default to 'DE' if not found

                // Fetch the grand total from the totals model
                var grandTotal = '0.00'; // Default value
                if (totals.totals()) {
                    grandTotal = parseFloat(totals.getSegment('grand_total').value).toFixed(2);
                }

                // Construct the transactionData object
                var transactionData = {
                    countryCode: countryCode,
                    currencyCode: currencyCode,
                    totalPrice: grandTotal
                };

                return transactionData;
            },
        });
    }
);
