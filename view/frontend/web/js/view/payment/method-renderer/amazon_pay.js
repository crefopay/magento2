/*browser:true*/
/*global define*/
define(
    [
        'ko',
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'Trilix_CrefoPay/js/model/amazon-storage',
        'uiComponent',
        'uiRegistry',
        'Trilix_CrefoPay/js/action/checkout-session-address-load',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/model/address-converter',
        'Magento_Checkout/js/model/checkout-data-resolver',
        'Magento_Checkout/js/action/create-billing-address',
        'Magento_Checkout/js/model/quote'
    ],
    function (
              ko,
              $,
              Component,
              storage,
              parentComponent,
              registry,
              checkoutSessionAddressLoad,
              checkoutData,
              addressConverter,
              checkoutDataResolver,
              createBillingAddress,
              quote,
    ) {
        'use strict';

        var self;

        return Component.extend({
            redirectAfterPlaceOrder: false,

            defaults: {
                template: 'Trilix_CrefoPay/payment/amazon_pay',
                amazonOrderId: 0,
                isBillingAddressVisible: ko.observable(false),
            },

            initialize: function () {
                self = this;
                parentComponent.prototype.initialize.apply(this, arguments);
                this.initChildren();
                if (storage.isAmazonCheckout()) {
                    this.initBillingAddress();
                    this.selectPaymentMethod();
                }
            },

            getCode: function() {
                return 'crefopay_amazon_pay';
            },

            beforePlaceOrder: function () {
                var self = this;
                this.amazonOrderId = storage.getCheckoutSessionId()
                self.placeOrder();
            },

            afterPlaceOrder: function () {
                storage.clearAmazonCheckout();
                window.location.replace("https://payments.amazon.de/checkout/processing?amazonCheckoutSessionId=" + this.amazonOrderId);
            },

            getData: function () {
                return {
                    'method': this.getCode(),
                    'additional_data': {}
                };
            },

            getCrefoPayMethodCode: function () {
                return window.checkoutConfig.payment[this.getCode()].crefoPayMethodCode;
            },

            getLogoSrc: function () {
                return window.checkoutConfig.payment[this.getCode()].logo;
            },

            bindEditPaymentAction: function (elem) {
                var $elem = $(elem);
                amazon.Pay.bindChangeAction('#' + $elem.uniqueId().attr('id'), {
                    amazonCheckoutSessionId: storage.getCheckoutSessionId(),
                    changeAction: 'changePayment'
                });
            },

            initBillingAddress: function () {
                var checkoutProvider = registry.get('checkoutProvider');
                checkoutSessionAddressLoad('billing', function (billingAddress) {
                    if ($.isEmptyObject(billingAddress)) {
                        self.isPlaceOrderActionAllowed(true);
                        return;
                    }

                    self.setEmail(billingAddress.email);

                    var quoteAddress = createBillingAddress(billingAddress);

                    // Fill in blank street fields
                    if ($.isArray(quoteAddress.street)) {
                        for (var i = quoteAddress.street.length; i <= 2; i++) {
                            quoteAddress.street[i] = '';
                        }
                    }

                    // Amazon does not return telephone, so use previous provider values
                    var checkoutShipping = $.extend(true, {}, checkoutProvider.shippingAddress);
                    var checkoutBilling = $.extend(true, {}, checkoutProvider.billingAddress);
                    if (!quoteAddress.telephone) {
                        quoteAddress.telephone = checkoutBilling.telephone || checkoutShipping.telephone;
                    }
                    if (!quoteAddress.lastname) {
                        quoteAddress.lastname = checkoutBilling.lastname || checkoutShipping.lastname;
                    }

                    // Save billing address
                    checkoutData.setSelectedBillingAddress(quoteAddress.getKey());
                    var formAddress = addressConverter.quoteAddressToFormAddressData(quoteAddress);
                    checkoutData.setBillingAddressFromData(formAddress);
                    checkoutData.setNewCustomerBillingAddress(formAddress);
                    checkoutProvider.set('billingAddress' + (window.checkoutConfig.displayBillingOnPaymentMethod ? self.getCode() : 'shared'), formAddress);
                    checkoutDataResolver.resolveBillingAddress();

                    self.isBillingAddressVisible(true);
                });
            },

            /**
             * Set email address
             * @param email
             */
            setEmail: function(email) {
                $('#customer-email').val(email);
                checkoutData.setInputFieldEmailValue(email);
                checkoutData.setValidatedEmailValue(email);
                quote.guestEmail = email;
            }
        });
    }
);
