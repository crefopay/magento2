/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'Trilix_CrefoPay/js/view/payment/adapter',
        'Magento_Checkout/js/model/quote',
        'mage/url'
    ],
    function ($, Component, adapter, quote, urlBuilder) {
        'use strict';

        return Component.extend({
            redirectAfterPlaceOrder: false, // Don't redirect to the success page

            defaults: {
                template: 'Trilix_CrefoPay/payment/form',
                orderIncrementId: 0,
            },

            getCode: function() {
                return 'crefopay_paypal';
            },

            beforePlaceOrder: function () {
                var self = this;
                var deferred = $.Deferred();
            
                // Call `reinit` and chain it with `adapter.registerPayment`
                this.reinit()
                    .done(function () {
                        console.log('Recreation of transaction completed successfully.');
                        adapter.registerPayment(deferred);
                    })
                    .fail(function (xhr, status, error) {
                        console.error('Recreation of transaction failed:', error);
                        deferred.reject();
                    });
            
                // Proceed only after `adapter.registerPayment` is resolved
                $.when(deferred).done(function (response) {
                    self.orderIncrementId = response.hasOwnProperty('orderNo') ? response.orderNo : 0;
                    console.log(response);
                    if (!self.isPayPalOldGateway()) {
                        self.redirectAfterPlaceOrder = true;
                    }
                    self.placeOrder();
                }).fail(function () {
                    console.error('Failed to register payment.');
                });
            },

            afterPlaceOrder: function () {
                if (this.orderIncrementId && this.isPayPalOldGateway()) {
                    var redirectUrl = urlBuilder.build('crefopay/payment/redirect?oid=' + this.orderIncrementId);
                    window.location.replace(redirectUrl);
                }
            },

            getData: function () {
                return {
                    'method': this.getCode(),
                    'additional_data': {}
                };
            },

            getCrefoPayMethodCode: function () {
                return window.checkoutConfig.payment[this.getCode()].crefoPayMethodCode;
            },

            getLogoSrc: function () {
                return window.checkoutConfig.payment[this.getCode()].logo;
            },

            isPayPalOldGateway: function () {
                return window.checkoutConfig.payment[this.getCode()].isPayPalOldGateway;
            },

            reinit: function () {
                let self = this;
                let serviceUrl = urlBuilder.build('crefopay/payment/recreateTransaction'); 
            
                let payload = {
                    cartId: quote.getQuoteId()
                };
            
                return $.ajax({
                    url: serviceUrl,
                    type: 'POST',
                    data: payload,
                    dataType: 'json',
                }).done(function (response) {
                    if (response.success) {
                        console.log('Recreation of transaction completed successful:', response);
                    } else {
                        console.error('Recreation of transaction failed:', response.message);                        
                    }
                }).fail(function (xhr, status, error) {
                    console.error('Error during recreation of transaction:', error);                    
                });
            },
        });
    }
);
