/*browser:true*/
/*global define*/
define([
    'jquery',
    'require',
    'Magento_Ui/js/model/messageList',
    'mage/url',
    'Magento_Checkout/js/model/quote',
    'Magento_Customer/js/model/customer',
    'mage/translate'
], function ($, require, globalMessageList, urlBuilder, quote, customer) {
    'use strict';

    let deferred;

    /**
     * @param response
     */
    let paymentRegisteredCallback = function (response) {

        // clear previous errors messages
        $('.crefopay_hosted_fields_errors').text('');

        if (response.resultCode === 0) {
            // Successful registration, continue to next page using Javascript
            console.log('registered');
            deferred.resolve(response);

        } else if (response.resultCode == 6005) {
            let message = $.mage.__('Cancelled by user.');
            globalMessageList.addErrorMessage({message: message});

            $(window).scrollTop(0); // Scroll error message into view

        } else if (response.errorDetails) {
            console.log(response.errorDetails);

            response.errorDetails.forEach(function (errorElement) {
                processErrorsMessages(errorElement);
            });

            let message = $.mage.__('Please check entered data.');
            globalMessageList.addErrorMessage({message: message});

            $(window).scrollTop(0); // Scroll error message into view
            
        } else {
            let message = $.mage.__('Unexpected error.');
            globalMessageList.addErrorMessage({message: message});

            $(window).scrollTop(0); // Scroll error message into view
        }
    };

    /**
     * @param errorElement
     * @returns {*|string}
     */
    function processErrorsMessages(errorElement) {
        let message = '';

        switch ('error.' + errorElement.field + '.' + errorElement.message) {
            case 'error.paymentInstrument.iban.field.invalid':
                message = $.mage.__('error.paymentInstrument.iban.field.invalid');
                $('#directDebit_iban').text(message);
                break;
            case 'error.paymentInstrument.accountHolder.field.invalid':
                message = $.mage.__('error.paymentInstrument.accountHolder.field.invalid');
                $('#creditCard_accountHolder').text(message);
                break;
            case 'error.paymentInstrument.validity.field.invalid':
                message = $.mage.__('error.paymentInstrument.validity.field.invalid');
                $('#creditCard_validity').text(message);
                break;
            case 'error.paymentInstrument.issuer.field.invalid':
                message = $.mage.__('error.paymentInstrument.issuer.field.invalid');
                break;
            case 'error.paymentInstrument.number.field.invalid':
                message = $.mage.__('error.paymentInstrument.number.field.invalid');
                $('#creditCard_number').text(message);
                break;
            case 'error.cvv.field.invalid':
                message = $.mage.__('error.cvv.field.invalid');
                break;
            case 'error.paymentInstrument.cvv.field.invalid':
                message = $.mage.__('error.cvv.field.invalid');
                $('#creditCard_cvv').text(message);
                break;
            default:
                message = $.mage.__(errorElement.message);
        }
        return message;
    }

    /**
     * @param response
     */
    let initializationCompleteCallback = function (response) {
        if (response.resultCode === 0) {
            console.log('initialized');
            // Successful registration, continue to next page using Javascript
        } else {
            // Error during registration, check the response for more details and dynamically show a message for the customer
            console.log(response.message);
            globalMessageList.addErrorMessage({
                message: response.message
            });
        }
    };

    return {
        apiClient: null,
        configuration: {
            url: window.checkoutConfig.payment['crefopay_bill'].secureFieldsUrl,
            ccOverlay: false,
            ddOverlay: false,
            placeholders: {}
        },

        /**
         * @returns {Object}
         */
        initialize: function () {
            let self = this;
            let serviceUrl = urlBuilder.build('crefopay/payment/guestGetCreateTransaction');
            let payload = {
                quoteId: quote.getQuoteId()
            };

            // Checkout for logged in customer
            if (customer.isLoggedIn()) {
                serviceUrl = urlBuilder.build('crefopay/payment/getCreateTransaction');
            }

            $.ajax({
                url: serviceUrl,
                type: 'GET',
                data: payload,
                dataType: 'json',
            }).fail(function (response) {
                // @todo implement error handling
                console.log(response);
            }).done(function (data) {
                if (data.areAnyPaymentMethodsAllowed) {
                    self.createClient(data.crefoPayOrderId);
                }
            })
        },

        /**
         * @param crefoPayOrderId
         */
        createClient: function (crefoPayOrderId) {
            // @todo remove dependency from concrete payment method
            let publicToken = window.checkoutConfig.payment['crefopay_bill'].publicToken;
            let secureFieldsJsUrl = window.checkoutConfig.payment['crefopay_bill'].secureFieldsJsUrl;
            console.log(crefoPayOrderId);

            this.crefoPayOrderId = crefoPayOrderId;
            require(['https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js', secureFieldsJsUrl], function () {
                window.$ = $;
                this.apiClient = new SecureFieldsClient(
                    publicToken,
                    crefoPayOrderId,
                    paymentRegisteredCallback,
                    initializationCompleteCallback,
                    this.configuration
                );
            }.bind(this));
        },

        /**
         * @returns {null}
         */
        getClient: function () {
            return this.apiClient;
        },

        /**
         * @param deferredParam
         */
        registerPayment: function (deferredParam) {
            deferred = deferredParam || $.Deferred();

            this.getClient().registerPayment();
        }
    };
});
