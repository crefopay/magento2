/*global define*/
define(
    [
        'uiComponent',
        'Trilix_CrefoPay/js/model/amazon-storage',
    ],
    function (
        Component, amazonStorage
    ) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Trilix_CrefoPay/amazon-pay-checkout-shipping'
            },
            isAmazonCheckout: amazonStorage.isAmazonCheckout(),

            /**
             * Init
             */
            initialize: function () {
                this._super();
            },

            /**
             * Revert checkout
             */
            revertCheckout: function () {
                amazonStorage.clearAmazonCheckout();
                window.location.replace(window.checkoutConfig.checkoutUrl);
            }
        });
    }
);
