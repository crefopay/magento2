define([
    'underscore',
    'Magento_Ui/js/form/element/abstract',
    'Trilix_CrefoPay/js/view/payment/smartSignupAdapter'
], function (_, Abstract, smartSignupAdapter) {
    'use strict';

    return Abstract.extend({
        defaults: {
            modules: {
                country: '${ $.parentName }.country_id',
                city: '${ $.parentName }.city',
                street: '${ $.parentName }.street',
                postcode: '${ $.parentName }.postcode',
            },
            isSmartSignUpInitialized: false
        },

        /**
         * Callback that fires when 'value' property is updated.
         */
        onUpdate: function () {
            this._super();
            if (this.isSmartSignUpInitialized === false) {
                let elements = {
                    'companyDomElement': this.uid,
                    'company': this,
                    'country': this.country(),
                    'city': this.city(),
                    'street': this.street().elems()[0],
                    'streetNo': this.street().elems()[1],
                    'postcode': this.postcode(),
                };

                smartSignupAdapter.initialize(elements);
                this.isSmartSignUpInitialized = true;
            }
        }
    });
});
