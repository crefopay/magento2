define([
    'jquery',
    'uiRegistry',
    'Trilix_CrefoPay/js/action/toggle-form-fields',
    'Trilix_CrefoPay/js/model/amazon-storage',
], function ($, registry, toggleFormFields, amazonStorage) {
    'use strict';

    return function(Component) {
        if (!amazonStorage.isEnabled || !amazonStorage.isAmazonCheckout()) {
            return Component;
        }

        var self;
        var editSelector = '.edit-address-link';

        return Component.extend({
            defaults: {
                template: {
                    name: Component.defaults.template,
                    afterRender: function () {
                        if ($(editSelector).length) {
                            amazon.Pay.bindChangeAction(editSelector, {
                                amazonCheckoutSessionId: amazonStorage.getCheckoutSessionId(),
                                changeAction: 'changeAddress'
                            });
                            $(editSelector).removeClass('edit-address-link')
                        }
                        self.toggleShippingAddressFormFields();
                    }
                }
            },

            /** @inheritdoc */
            initObservable: function () {
                self = this;
                this._super();
                this.address.subscribe(function () {
                    self.toggleShippingAddressFormFields();
                });
                return this;
            },

            toggleShippingAddressFormFields: function () {
                var checkoutProvider = registry.get('checkoutProvider');
                checkoutProvider.trigger('shippingAddress.data.validate');
                if (checkoutProvider.get('shippingAddress.custom_attributes')) {
                    checkoutProvider.trigger('shippingAddress.custom_attributes.data.validate');
                }

                toggleFormFields('#co-shipping-form', true);
            },

            /**
             * Edit address (using amazon.Pay.bindChangeAction).
             */
            editAddress: function () {
            }
        });
    }
});
