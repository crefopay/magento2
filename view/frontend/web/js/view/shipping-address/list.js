
define([
    'Trilix_CrefoPay/js/model/amazon-storage'
], function (amazonStorage) {
    'use strict';

    return function(Component) {
        if (!amazonStorage.isEnabled || !amazonStorage.isAmazonCheckout()) {
            return Component;
        }

        return Component.extend({
            /**
             * Init address list
             */
            initObservable: function () {
                this._super();
                this.visible = true;
                return this;
            },

            createRendererComponent: function (address, index) {
                if (address.getType() === 'new-customer-address') {
                    // Only display one address from Amazon
                    return this._super();
                }
            }
        });
    }
});
