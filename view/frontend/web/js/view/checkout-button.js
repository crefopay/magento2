/*global define*/
define(
    [
        'uiComponent',
        'Trilix_CrefoPay/js/model/amazon-storage'
    ],
    function (
        Component,
        amazonStorage
    ) {
        'use strict';

        return Component.extend(
            {
                defaults: {
                    template: 'Trilix_CrefoPay/checkout-button'
                },
                isVisible: window.checkoutConfig.payment.crefopay_amazon_pay.isActive
                    && !amazonStorage.isAmazonCheckout(),

                /**
                 * Init
                 */
                initialize: function () {
                    this._super();
                }
            }
        );
    }
);

