/*global define*/
define(
    [
        'jquery',
        'Trilix_CrefoPay/js/model/amazon-storage'
    ],
    function (
        $,
        amazonStorage
    ) {
        'use strict';

        return function(Component) {
            if (!amazonStorage.isAmazonCheckout()) {
                return Component;
            }

            return Component.extend({

                /**
                 * Initialize shipping
                 */
                initialize: function () {
                    this._super();
                    this.isNewAddressAdded(true);
                    return this;
                },
            });
        }
    }
);
