define([
    'jquery',
    'require'
], function($, require) {
    'use strict';

    const baseRequest = {
        apiVersion: 2,
        apiVersionMinor: 0
    };
    const allowedCardNetworks = ["AMEX", "MASTERCARD", "VISA"];
    const allowedCardAuthMethods = ["PAN_ONLY"];
    const tokenizationSpecification = {
        type: 'PAYMENT_GATEWAY',
        parameters: {
            'gateway': 'lynck',
            'gatewayMerchantId': window.checkoutConfig.payment['crefopay_google_pay'].merchantID
        }
    };
    const baseCardPaymentMethod = {
        type: 'CARD',
        parameters: {
            allowedAuthMethods: allowedCardAuthMethods,
            allowedCardNetworks: allowedCardNetworks
        }
    };
    const cardPaymentMethod = Object.assign({}, baseCardPaymentMethod, {
        tokenizationSpecification: tokenizationSpecification
    });

    let paymentsClient = null;

    return {
        loadGooglePayScript: function(callback) {
            // Load the Google Pay script dynamically
            require(['https://pay.google.com/gp/p/js/pay.js'], function() {
                if (typeof callback === 'function') {
                    callback();
                }
            });
        },

        getGoogleIsReadyToPayRequest: function() {
            return Object.assign({}, baseRequest, {
                allowedPaymentMethods: [baseCardPaymentMethod]
            });
        },

        getGooglePaymentDataRequest: function(transactionData) {
            const paymentDataRequest = Object.assign({}, baseRequest);
            paymentDataRequest.allowedPaymentMethods = [cardPaymentMethod];
            paymentDataRequest.transactionInfo = {
                countryCode: transactionData.countryCode,
                currencyCode: transactionData.currencyCode,
                totalPriceStatus: 'FINAL', 
                totalPrice: transactionData.totalPrice
            }
            paymentDataRequest.merchantInfo = {
                merchantName: 'Example Merchant'
            };
            return paymentDataRequest;
        },

        getGooglePaymentsClient: function() {
            // Retrieve the isSandbox flag from checkoutConfig
            var isSandbox = window.checkoutConfig.payment['crefopay_google_pay'].isSandbox;
            
            // Determine the env based on isSandbox
            var env = isSandbox ? 'TEST' : 'PRODUCTION';
            if (paymentsClient === null) {
                paymentsClient = new google.payments.api.PaymentsClient({                    
                    environment: env
                });
            }
            return paymentsClient;
        },

        onGooglePayLoaded: function() {
            this.loadGooglePayScript(function() {
                // Google Pay script loaded, now initialize
                const paymentsClient = this.getGooglePaymentsClient(); // Ensure this refers to the correct context
                paymentsClient.isReadyToPay(this.getGoogleIsReadyToPayRequest()) // Ensure proper context here
                    .then(function(response) {
                        if (response.result) {
                            //this.addGooglePayButton(); // Ensure proper context here
                        }
                    }.bind(this)) // Binding 'this' to ensure context is correct
                    .catch(function(err) {
                        console.error(err);
                    });
            }.bind(this)); // Binding 'this' to the callback function
        },

        // addGooglePayButton: function() {
        //     const paymentsClient = this.getGooglePaymentsClient(); // Ensure this refers to the correct context
        //     const button = paymentsClient.createButton({
        //         onClick: this.onGooglePaymentButtonClicked.bind(this), // Bind 'this' to the click handler
        //         allowedPaymentMethods: [baseCardPaymentMethod]
        //     });
        //     document.getElementById('google-container').appendChild(button);
        // },

        onGooglePaymentButtonClicked: function(transactionData) {
            var deferred = $.Deferred();
            const paymentDataRequest = this.getGooglePaymentDataRequest(transactionData);

            const paymentsClient = this.getGooglePaymentsClient();
            paymentsClient.loadPaymentData(paymentDataRequest)
                .then(function(paymentData) {
                    var paymentToken = paymentData.paymentMethodData.tokenizationData.token;
                    deferred.resolve(paymentToken);
                })
                .catch(function(err) {
                    console.error(err);
                    deferred.reject(err);
                });
            return deferred.promise();
        },        
    };
});